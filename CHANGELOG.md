[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Core
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Updater migration FafcmsComponent.php#318 @cmoeke
- Plugin namespaces to updater FafcmsComponent.php#324 @cmoeke
- Updater module main.php#148 @cmoeke
- Function to export single models @cmoeke
- InjectorComponent to make it easier to inject / overwrite classes and overwrite settings @cmoeke
- Implemented BeautifulModelTrait and OptionTrait in language model @cmoeke
- String helper to indent code and pass expression to var export @cmoeke
- Migrations and updates to move old settings to project and project language model @cmoeke
- EmailInput, UrlInput and PhoneInput to show input fields formatted with MaskedInput @StefanBrandenburger
- Return type in scenario functions of search models @StefanBrandenburger
- Prepared usage of fomantic @cmoeke
- Added createObject to InjectorComponent to make it easier to overwrite classes @cmoeke
- Added instance method to InjectorComponent to make it possible to overwrite instance method in ModelTrait @cmoeke
- BaseUser.php @StefanBrandenburger
- search scenario to model class and create model with InjectorComponent in IndexView.php#99 @StefanBrandenburger
- Update and migration for changing user table columns (notifications_email and notificaitons_browser) to integer @StefanBrandenburger
- Scenario for requesting a new passwort in User.php @StefanBrandenburger
- Implemented search function in User.php @StefanBrandenburger
- Possibility to have child child relations in edit views of models @cmoeke
- Possibility to inject relations and attributes to models in InjectorComponent.php @cmoeke
- Output of open graph data @cmoeke
- Possibility to pass items as closure to chips input @cmoeke
- MatthiasMullie JS and CSS minifier @cmoeke
- Possibility to general rewrite special columns to query classes like hashid or encrypted columns @cmoeke
- Device detector to core module to make it easier to add polyfills @cmoeke
- core-js-bundle polyfill library @cmoeke
- installer steps "Settings" and "Demo Data" @StefanBrandenburger
- Added and updated psr yii2 bridges @cmoeke
- Own ace editor widget to make it possible to register it as webcomponent module @cmoeke
- Global js fafcmsConfig to pass backend url etc. @cmoeke
- LinkAction setting to DataColumn.php @cmoeke
- possibility to use more index-views in IndexViewInterface implementation @StefanBrandenburger
- css for language selection @cmoeke
- option to data cache map method to ignore the language filter @cmoeke
- check for updater update namspaces to fix pipelines @cmoeke
- translation action @cmoeke
- change current project action @cmoeke
- change current language action @cmoeke
- project and language filter to default query @cmoeke
- Translations for user sex selection @cmoeke
- Actions to translate items @cmoeke
- Export all and translate all buttons to index view @cmoeke

### Changed
- Moved default translations to make them separate installable @cmoeke
- Re enabled db translations @cmoeke
- Moved class map and model query map to InjectorComponent @cmoeke
- Started to implement project and project language model views @cmoeke
- Moved languages to make them downloadable @cmoeke
- Model in EditView.php#114 initializes now with possible dependency injection @StefanBrandenburger
- Code cleanup + return types in Language.php @StefanBrandenburger
- Code cleanup in Country.php @StefanBrandenburger
- UserController now extends DefaultController @StefanBrandenburger #21
- Return types in User.php @StefanBrandenburger #21
- Implemented FieldConfigInterface, IndexViewInterface, EditViewInterface, AttributeOptionTrait, BeautifulModelTrait and OptionTrait in User.php @StefanBrandenburger #21
- Code cleanup + return types in User.php @StefanBrandenburger #21
- Fixed contentmeta query @cmoeke
- Removed $this as parameter if items is a callable in DropdownList.php#101 and Select2.php#15 @StefanBrandenburger
- Replaced filed config for some attributes in User.php with new through gii generated config @StefanBrandenburger
- Export button to now only show up if given model is an instance of ActiveRecord class @StefanBrandenburger
- Regenerated project model @cmoeke
- Regenerated projectlanguage model @cmoeke
- Replaced Yii::createObject with InjectorComponent::createObject @cmoeke
- Improved edit view @cmoeke
- Cleanup and merge with BaseUser.php in User.php @StefanBrandenburger
- overwritten functions of User.php now use array_merge instead of ArrayHelper::merge @StefanBrandenburger
- notifications_email and notificaitons_browser to unsigned int instead int @StefanBrandenburger
- Improved field config, edit config and rules in User.php @StefanBrandenburger
- Cleanup in PasswordRequestForm.php @StefanBrandenburger
- Added return types to attributeLabels, rules and prefixableTableName @cmoeke
- Unified save method @cmoeke
- Unified loadDefaultValues method @cmoeke
- Namespace of ContentmetaTrait @cmoeke
- Temporary implemented search methods for project model @cmoeke
- Filemanager as core module @cmoeke
- Improved InstallerController.php @StefanBrandenburger
- Improved InstallerForm.php @StefanBrandenburger
- Changed parser data for the new template engine @cmoeke
- Updated css structure to make it easier to recompile it @cmoeke
- Moved "renderView" to new render component @cmoeke
- Temporary truncated breadcrumb @cmoeke
- Temporary re-added translations @cmoeke
- Default sizeFormatBase to 1000 @cmoeke
- Translation url in action column @cmoeke

### Fixed
- Bug when guzzlehttp isn't required by an other package @cmoeke
- Db translation cache @cmoeke
- Bug when a file which should be minified has multiple dots in name @cmoeke
- Output format of fallback minifier @cmoeke
- Error page @cmoeke
- Function renderEditView now throws an InvalidCallException when modelClass is not defined @StefanBrandenburger
- Missing attributes in User.php and UserSearch.php @StefanBrandenburger #21
- AttributeOptionTrait implementation in Project.php#475 @StefanBrandenburger
- Wrong translation in BeautifulModelTrait implementation of Country.php @StefanBrandenburger
- FieldConfigInterface implementation in Project.php @StefanBrandenburger
- Rewritten old project setting usage @cmoeke
- Fixed disabled or readonly DateTimePicker buttons @cmoeke
- Functions renderIndexView and renderEditView of FafcmsComponent.php to use InjectorComponent @StefanBrandenburger
- Use InjectorComponent in IndexView.php#109 @StefanBrandenburger
- Compatibility of search function in User.php#1145 @StefanBrandenburger
- Password labels in User.php @StefanBrandenburger
- NotSupported exception when search function is not implemented in model class in IndexView.php#95 @StefanBrandenburger
- Update files to make it possible to make a clean installation @cmoeke
- GetStartPageId when no start page is defined @cmoeke
- Run migrations while installing @StefanBrandenburger
- Dont read translations from db if cms is not installed @StefanBrandenburger
- Dont show sidebar content if cms is not installed @StefanBrandenburger
- Static functions @cmoeke
- Fixes for PHP 8 @cmoeke
- Language and project change methods @cmoeke
- Added option to disable cookie manager for specific routes to fix @cmoeke fafcms-module-filemanager#21
- Refreshed user date after creating a password change request to fix encrypted user tables @cmoeke
- check if Yii::$app->fafcms is loaded @cmoeke
- display_start and display_end check to use the correct timezone @cmoeke
- DefaultQuery.php when formatter isn't loaded @cmoeke

### Deprecated
- getAttributesOptions function in User.php#1491 @StefanBrandenburger #21
- UserSearch.php @StefanBrandenburger

### Removed
- Old query of user model @cmoeke
- index, update and delete actions in UserController.php @StefanBrandenburger #21
- Views for user model (index.php & _form.php) @StefanBrandenburger #21
- Create scenario constant in User.php because it is now already defined in parent class @StefanBrandenburger
- Create action in UserController.php @StefanBrandenburger
- Possibility to pass status in default query methods @cmoeke
- Google closure compiler @cmoeke
- ungap--url-search-params polyfill library @cmoeke
- lav45/yii2-aceeditor @cmoeke
- Removed ungap--url-search-params polyfill library @cmoeke
- searchModelClass from IndexViewInterface implementation in User.php#235 @StefanBrandenburger

## [0.1.0] - 2020-09-29
### Added
- CHANGELOG.md @cmoeke fafcms-core#39
- Basic doc folder structure @cmoeke fafcms-core#37
- .gitlab-ci.yml for doc an build creation @cmoeke fafcms-core#38
- padaliyajay/php-autoprefixer @cmoeke
- Configuration less fafcms bin script, mostly use full for build scripts @cmoeke
- Option to run fafcms without configuration and not write generated config files, mostly use full for build scripts @cmoeke
- Console controller to prefix css with padaliyajay/php-autoprefixer @cmoeke
- Option to ignore type of contentmeta elements at getContentmeta method in FrontendController.php @cmoeke
- Basic plugin detail view @cmoeke
- General render view method @cmoeke
- $ignoreType to getIdByUrl, to get non content contentmeta elements @cmoeke
- $authModelClass to make it possible to check access rules of a real model instead of a used form model @cmoeke
- Added option to disable loader animation on submit buttons @cmoeke
- Added translations for some countries @cmoeke
- Added currentDataType and currentDataId to parser data @cmoeke
- Added $loginAjaxResponse to FafcmsComponent.php to make it possible to modify the ajax response when a login is required @cmoeke
- Added cron expression package @cmoeke
- Added option to disable log for model changes @cmoeke
- Added css, js and html minifier @cmoeke
- Added open graph data @cmoeke

### Changed
- composer.json to use correct dependencies @cmoeke
- LICENSE to LICENSE.md @cmoeke
- Usage of setting manager module (used simplified static getLoadedModule method) @cmoeke
- Position of getInstalledPackages method. Moved from InstallerController.php to FafcmsComponent.php @cmoeke
- Dashboard view to make it easier to create custom dashboards (Rewritten on base of fafcms\helpers\abstractions\Content) @cmoeke
- Renamed ajax close button to cancel @cmoeke 
- Moved GetFieldDefinition to separate method to make it possible to get options like inputId outside of FormField.php @cmoeke
- Changed filter of login contentmeta to make it possible to use dummy pages as login site @cmoeke 
- Added default data for field config in getFieldDefinition method @cmoeke
- Passed viewData to html item renderer @cmoeke
- PHP dependency to 7.4 @cmoeke
- Improved hashId generation @cmoeke
- Changed fafcms module dependency versions @cmoeke
- Changed ci config to handle new build branch @cmoeke
- Replaced moment js with luxon @cmoeke
- Improved performance @cmoeke

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46
- Uninstalled execution of fafcms (console) @cmoeke
- Captcha when gd is configured as image manipulation library @cmoeke
- Plugin settings by adding missing namespace @cmoeke 
- Wrong passed parameter to getIdByUrl when getContentmeta with $ignoreType = true has been used @cmoeke 
- CSS fixes for ajax modals @cmoeke
- Added filter for visible attribute in getActionButtons method to hide ajax cancel button @cmoeke fafcms-core#48
- Fixed JS Bug of missing $submitButtons @cmoeke
- Fixed ColorInput to use Spectrum @sm-es
- Fixed Upper create button in index views isn't working because of pjax @cmoeke fafcms-core#30
- TinyMce content doesn't get saved after updating via AJAX @cmoeke fafcms-core#27

### Removed
- matthiasmullie/minify css minifier, please use tubalmartin/cssmin instead @cmoeke
- Pre compressed scss output in yii asset converter configuration @cmoeke
- Removed user relation validation in Domain.php @cmoeke

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-core/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-core/-/tree/v0.1.0-alpha
