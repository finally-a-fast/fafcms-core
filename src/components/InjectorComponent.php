<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\components;

use fafcms\fafcms\queries\ContentmetaQuery;
use fafcms\sitemanager\models\Contentmeta;
use Yii;
use yii\base\Component;
use Closure;

/**
 * Class InjectorComponent
 *
 * @package fafcms\fafcms\components
 */
class InjectorComponent extends Component
{
    /**
     * @var string[]
     */
    public array $classMap = [];

    /**
     * @param string $className
     *
     * @return string
     */
    public static function getClass(string $className): string
    {
        return Yii::$app->injector->classMap[$className] ?? $className;
    }

    /**
     * @var string[]
     */
    public array $modelActiveQueryMap = [];

    /**
     * @param string $className
     *
     * @return string|null
     */
    public static function getActiveQueryForModel(string $className): ?string
    {
        return Yii::$app->injector->modelActiveQueryMap[$className] ?? null;
    }

    /**
     * @var object[] instances in format: `[className => object]`
     */
    public array $instances = [];

    /**
     * Returns static class instance, which can be used to obtain meta information.
     *
     * @param string $className
     * @param bool   $refresh whether to re-create static instance even, if it is already cached.
     *
     * @return object class instance.
     * @throws \yii\base\InvalidConfigException
     */
    public static function instance(string $className, bool $refresh = false): object
    {
        if ($refresh || !isset(Yii::$app->injector->instances[$className])) {
            Yii::$app->injector->instances[$className] = self::createObject($className);
        }

        return Yii::$app->injector->instances[$className];
    }

    /**
     * @param string|array|callable $type
     * @param array  $params
     *
     * @return object
     * @throws \yii\base\InvalidConfigException
     */
    public static function createObject($type, array $params = []): object
    {
        if (is_string($type)) {
            $type = self::getClass($type);
        }

        if (is_array($type)) {
            if (isset($type['__class'])) {
                $type['__class'] = self::getClass($type['__class']);
            }

            if (isset($type['class'])) {
                $type['class'] = self::getClass($type['class']);
            }
        }

        return Yii::createObject($type, $params);
    }

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();

        $this->modelActiveQueryMap[Contentmeta::class] ??= ContentmetaQuery::class;
    }

    /**
     * @var array[]
     */
    protected array $modelExtensionAttributes = [];

    /**
     * @var array[]
     */
    protected array $modelExtensionMethods = [];

    /**
     * @var array[]
     */
    protected array $modelExtensionRelations = [];

    /**
     * @param string $className
     * @param string $name
     * @param Closure|array $value
     */
    public function setModelExtensionAttribute(string $className, string $name, $value): void
    {
        $this->modelExtensionAttributes[$className][$name] = $value;
    }

    /**
     * @param string $className
     * @param string $name
     */
    public function unsetModelExtensionAttribute(string $className, string $name): void
    {
        unset($this->modelExtensionAttributes[$className][$name]);
    }

    /**
     * @param string $className
     *
     * @return array
     */
    public function getModelExtensionAttributes(string $className): array
    {
        if (!isset($this->modelExtensionAttributes[$className])) {
            $this->modelExtensionAttributes[$className] = [];
        }

        return $this->modelExtensionAttributes[$className];
    }

    /**
     * @param string  $className
     * @param string  $name
     * @param Closure $function
     */
    public function setModelExtensionMethod(string $className, string $name, Closure $function): void
    {
        $this->modelExtensionMethods[$className][mb_strtolower($name)] = $function;
    }

    /**
     * @param string $className
     * @param string $name
     */
    public function unsetModelExtensionMethod(string $className, string $name): void
    {
        unset($this->modelExtensionMethods[$className][mb_strtolower($name)]);
    }

    /**
     * @param string $className
     *
     * @return array
     */
    public function &getModelExtensionMethods(string $className): array
    {
        if (!isset($this->modelExtensionMethods[$className])) {
            $this->modelExtensionMethods[$className] = [];
        }

        return $this->modelExtensionMethods[$className];
    }

    /**
     * @param string  $className
     * @param string  $name
     * @param Closure $function
     */
    public function setModelExtensionRelation(string $className, string $name, Closure $function): void
    {
        $name = 'get' . mb_strtolower($name);
        $this->modelExtensionRelations[$className][$name] = $function;
    }

    /**
     * @param string $className
     * @param string $name
     */
    public function unsetModelExtensionRelation(string $className, string $name): void
    {
        $name = 'get' . mb_strtolower($name);
        unset($this->modelExtensionRelations[$className][$name]);
    }

    /**
     * @param string $className
     *
     * @return array
     */
    public function &getModelExtensionRelations(string $className): array
    {
        if (!isset($this->modelExtensionRelations[$className])) {
            $this->modelExtensionRelations[$className] = [];
        }

        return $this->modelExtensionRelations[$className];
    }
}
