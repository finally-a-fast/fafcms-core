<?php

namespace fafcms\fafcms\components;

use DateTime;

use fafcms\fafcms\{
    controllers\InstallerController,
    helpers\RestRule,
    items\FormField,
    models\Domain,
    models\Language,
    models\Project,
    models\Projectlanguage,
    models\User,
    Module,
    widgets\ActiveForm};
use Exception;
use fafcms\filemanager\controllers\FilemanagerController;
use fafcms\helpers\{
    abstractions\FormInput,
    ActiveRecord,
    ClassFinder,
    interfaces\ContentmetaInterface,
};
use fafcms\settingmanager\Bootstrap as SettingmanagerBootstrap;
use fafcms\sitemanager\{
    models\Contentmeta,
    models\Site,
    traits\ContentmetaTrait
};

use Hashids\Hashids;

use RuntimeException;
use Yii;
use yii\base\{
    BootstrapInterface,
    Component,
    ErrorException,
    InvalidArgumentException,
    InvalidCallException,
    InvalidConfigException,
    Model};
use yii\captcha\{
    Captcha,
    CaptchaValidator,
};
use yii\helpers\{
    ArrayHelper,
    FormatConverter,
    Html,
    Inflector};
use yii\i18n\PhpMessageSource;
use yii\log\Logger;
use yii\web\{
    Cookie,
    ForbiddenHttpException,
    Response};

use fafcms\fafcms\components\ViewComponent;

/**
 * Class FafcmsComponent
 *
 * @package fafcms\fafcms\components
 */
class FafcmsComponent extends Component implements BootstrapInterface
{
    public const TYPE_FRONTEND = 'frontend';
    public const TYPE_BACKEND = 'backend';
    public const TYPE_API = 'api';

    public const VALUE_TYPE_CUSTOM = 0;
    public const VALUE_TYPE_INT = 'int';
    public const VALUE_TYPE_VARCHAR = 'varchar';
    public const VALUE_TYPE_ARRAY = 'array';
    public const VALUE_TYPE_TRANSLATION = 'array';
    public const VALUE_TYPE_TEXT = 'text';
    public const VALUE_TYPE_BOOL = 'bool';
    public const VALUE_TYPE_MODEL = 'model';
    public const VALUE_TYPE_DATE = 'date';
    public const VALUE_TYPE_TIME = 'time';
    public const VALUE_TYPE_DATETIME = 'datetime';

    public const NAVIGATION_HEADER_LEFT = 1;
    public const NAVIGATION_HEADER_RIGHT = 2;
    public const NAVIGATION_SIDEBAR_LEFT = 3;
    public const NAVIGATION_SIDEBAR_RIGHT = 4;
    public const NAVIGATION_ACTION_BAR = 5;
    public const NAVIGATION_FOOTER_LEFT = 6;
    public const NAVIGATION_FOOTER_RIGHT = 7;

    public const CLASS_NAME_NUMBER_MAP = [
        1 => 'one',
        2 => 'two',
        3 => 'three',
        4 => 'four',
        5 => 'five',
        6 => 'six',
        7 => 'seven',
        8 => 'eight',
        9 => 'nine',
        10 => 'ten',
        11 => 'eleven',
        12 => 'twelve',
        13 => 'thirteen',
        14 => 'fourteen',
        15 => 'fifteen',
        16 => 'sixteen',
    ];

    //region js config
    /**
     * @var array <string, mixed>
     */
    protected array $jsConfig = [];

    /**
     * @return array<string, mixed>
     */
    public function getJsConfig(): array
    {
        return $this->jsConfig;
    }

    /**
     * @param array<string, mixed> $jsConfig
     *
     * @return $this
     */
    public function setJsConfig(array $jsConfig): self
    {
        $this->jsConfig = $jsConfig;
        return $this;
    }

    /**
     * @param string $key
     *
     * @return mixed|null
     */
    public function getJsConfigValue(string $key)
    {
        return $this->jsConfig[$key] ?? null;
    }

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    public function setJsConfigValue(string $key, $value): self
    {
        $this->jsConfig[$key] = $value;
        return $this;
    }

    /**
     * @param array<string, mixed> $values
     *
     * @return $this
     */
    public function setJsConfigValues(array $values): self
    {
        $this->jsConfig = array_merge($this->jsConfig, $values);
        return $this;
    }
    //endregion js config

    /**
     * @var string[]
     */
    public array $disabledCookieManagerRoutes = [];

    /**
     * @return string[]
     */
    public function getDisabledCookieManagerRoutes(): array
    {
        return $this->disabledCookieManagerRoutes;
    }

    /**
     * @param string[] $disabledCookieManagerRoutes
     *
     * @return $this
     */
    public function setDisabledCookieManagerRoutes(array $disabledCookieManagerRoutes): self
    {
        $this->disabledCookieManagerRoutes = $disabledCookieManagerRoutes;
        return $this;
    }

    /**
     * @param string $id
     * @param string[] $disabledCookieManagerRoutes
     *
     * @return $this
     */
    public function addDisabledCookieManagerRoutes(string $id, array $disabledCookieManagerRoutes): self
    {
        if ($id !== '') {
            $id .= '/';
        }

        foreach ($disabledCookieManagerRoutes as $disabledCookieManagerRoute) {
            $this->disabledCookieManagerRoutes[$id . $disabledCookieManagerRoute] = true;
        }

        return $this;
    }

    /**
     * @var array
     */
    public array $modelTablePrefixes = [];

    /**
     * @var array
     */
    public array $sidebarItemCache = [];

    /**
     * @var array|null
     */
    private ?array $_generatedConfig = null;

    /**
     * @return array
     * @throws \Exception
     */
    public function getGeneratedConfig(): array
    {
        if ($this->_generatedConfig === null) {
            $generatedConfig = @include(FAFCMS_CONFIG_PATH . '/generated.php');
            $generatedConfig = $generatedConfig ?: [];
            $installed = $generatedConfig['installed'] ?? false;

            InstallerController::checkConfig($installed, $generatedConfig);
            $this->_generatedConfig = $generatedConfig;
        }

        return $this->_generatedConfig;
    }

    /**
     * @var array|null
     */
    private ?array $_loadedPlugins = null;

    /**
     * @return array
     */
    public function getLoadedPlugins(): array
    {
        if ($this->_loadedPlugins === null) {
            $plugins = @include(FAFCMS_CONFIG_PATH . '/plugins.php');
            $plugins = $plugins === false ? [] : $plugins;

            $plugins['finally-a-fast/fafcms-core'] = [
                'module'    => 'fafcms\\fafcms\\Module',
                'bootstrap' => 'fafcms\\fafcms\\Bootstrap',
                'order'     => 1,
                'core'      => true
            ];

            $plugins['finally-a-fast/fafcms-module-filemanager'] = [
                'module'    => 'fafcms\\filemanager\\Module',
                'bootstrap' => 'fafcms\\filemanager\\Bootstrap',
                'core'      => true
            ];

            $plugins['finally-a-fast/fafcms-module-settingmanager'] = [
                'module'    => 'fafcms\\settingmanager\\Module',
                'bootstrap' => 'fafcms\\settingmanager\\Bootstrap',
                'core'      => true
            ];

            $plugins['finally-a-fast/fafcms-module-parser'] = [
                'module'    => 'fafcms\\parser\\Module',
                'bootstrap' => 'fafcms\\parser\\Bootstrap',
                'core'      => true
            ];

            $plugins['finally-a-fast/fafcms-module-sitemanager'] = [
                'module'    => 'fafcms\\sitemanager\\Module',
                'bootstrap' => 'fafcms\\sitemanager\\Bootstrap',
                'core'      => true
            ];

            $plugins['finally-a-fast/fafcms-module-mailmanager'] = [
                'module'    => 'fafcms\\mailmanager\\Module',
                'bootstrap' => 'fafcms\\mailmanager\\Bootstrap',
                'core'      => true
            ];

            $loadedPlugins = [];
            $modelTablePrefixes = [];

            ArrayHelper::multisort($plugins, function($item) {
                return [$item['order'] ?? PHP_INT_MAX, ($item['core'] ?? false) ? 1 : 2];
            });

            foreach ($plugins as $pluginFqn => $plugin) {
                if (class_exists($plugin['bootstrap'])) {
                    $loadedPlugins[$pluginFqn] = $plugin;
                    $loadedPlugins[$pluginFqn]['id'] = $plugin['bootstrap']::$id;
                    $loadedPlugins[$pluginFqn]['namespace'] = substr($plugin['module'], 0, -6);

                    $tablePrefix = $plugin['bootstrap']::getTablePrefix();

                    if ($plugin['bootstrap']::$tablePrefixTargets === null) {
                        $this->modelTablePrefixes[substr($plugin['module'], 0, -6)] = $tablePrefix;
                    } else {
                        $modelTablePrefixes[] = array_fill_keys($plugin['bootstrap']::$tablePrefixTargets, $tablePrefix);
                    }

                    continue;
                }

                if ($plugin['core'] ?? false) {
                    throw new RuntimeException('Missing core module: ' . $pluginFqn);
                }

                $this->unloadPlugin($pluginFqn, true, 'Class "' . $plugin['bootstrap'] . '" not found.');
            }

            $this->modelTablePrefixes = array_merge($this->modelTablePrefixes, ...$modelTablePrefixes);

            $this->_loadedPlugins = $loadedPlugins;
        }

        return $this->_loadedPlugins;
    }

    /**
     * @param string $pluginFqn
     * @param bool   $auto
     * @param string $errorMessage
     *
     * @return bool
     */
    public function unloadPlugin(string $pluginFqn, bool $auto = false, string $errorMessage = ''): bool
    {
        $loadedPlugins = @include(FAFCMS_CONFIG_PATH . '/plugins.php');
        $loadedPlugins = $loadedPlugins === false ? [] : $loadedPlugins;

        if (isset($loadedPlugins[$pluginFqn])) {
            unset($loadedPlugins[$pluginFqn]);

            if (InstallerController::writeConfig($loadedPlugins, FAFCMS_CONFIG_PATH . '/plugins.php')) {
                $type = 'success';
                $message = '{module} has been disabled.';

                if ($auto) {
                    $type = 'warning';
                    $message = '{module} has been automatically disabled because an error occurred.';
                }

                Yii::$app->session->setFlash($type, Yii::t('fafcms-core', $message, [
                    'module' => $pluginFqn,
                ]) . ($errorMessage === '' ? '' : '<br><br>' . $errorMessage));

                return true;
            }
        }

        return false;
    }

    /**
     * @var string
     */
    public $type;

    /**
     * @var bool
     */
    public bool $addAppTranslation = true;

    /**
     * @var array
     */
    public array $accessRules = [
        'tag' => [
            [
                'allow' => true,
                'roles' => ['@'],
            ]
        ],
        'custom-field' => [
            [
                'allow' => true,
                'roles' => ['@'],
            ]
        ],
        'project' => [
            [
                'allow' => true,
                'roles' => ['@'],
            ]
        ],
        'default' => [
            [
                'allow' => true,
                'roles' => ['@'],
            ]
        ],
    ];

    /**
     * @var array
     */
    public array $loginAjaxResponse = [
        'modalFixed'              => true,
        'headerRemoveFromContent' => true,
        'headerSelector'          => '.form-header',
        'footerRemoveFromContent' => true,
        'footerSelector'          => '.form-footer',
        'modalSelector'           => '.form-content',
    ];

    /**
     * @return bool
     */
    public function getIsInstalled(): bool
    {
        return $this->generatedConfig['installed'];
    }

    /**
     * @return string
     */
    public function getBackendUrl(): string
    {
        return $this->generatedConfig['backendUrl'];
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return $this->generatedConfig['apiUrl'];
    }

    /**
     * @param string|null $size
     *
     * @return string
     * @throws \Throwable
     */
    public function getCurrentProjectLogo(?string $size = null): string
    {
        if ($size === null) {
            $size = '100,100,bestfit:true;extend:true;';
        }

        if ($this->getIsInstalled() && ($currentProject = $this->getCurrentProject()) !== null && $currentProject->media !== '' && $currentProject->media !== null) {
            return FilemanagerController::getMedia($currentProject->media, 'Preview', $size, 'true');
        }

        return Html::img(['/img/logo.jpg']);
    }

    public function init()
    {
        parent::init();

        if (YII_CONSOLE) {
            return;
        }

        if ($this->getIsInstalled()) {
            Yii::$app->user->identityClass = InjectorComponent::getClass(User::class);
            /*
            $config['components']['user'] = [
                'identityClass' => User::class,
                'enableAutoLogin' => true,
                'loginUrl' => ['main/login']
            ];*/
        }
    }

    private array $_loadedPluginPaths = [];

    /**
     * @return array
     */
    public function getLoadedPluginPaths(): array
    {
        return $this->_loadedPluginPaths;
    }

    /**
     * @param \yii\base\Application $app
     *
     * @throws ForbiddenHttpException
     * @throws InvalidConfigException
     * @throws \Throwable
     */
    public function bootstrap($app)
    {
        $loadedPlugins = $this->getLoadedPlugins();

        $this->setLanguage();
        $this->addTranslations();

        foreach ($loadedPlugins as $pluginFqn => $loadedPlugin) {
            try {
                $app->setModule($loadedPlugin['id'], $loadedPlugin['module']);
                $module = $app->getModule($loadedPlugin['id']);
                $modulePath = $module->getBasePath();

                $this->_loadedPluginPaths[$loadedPlugin['id']] = $modulePath;

                Yii::setAlias(str_replace('\\', '/', trim($loadedPlugin['namespace'], '\\')), $modulePath);

                if (isset($loadedPlugin['bootstrap'])) {
                    $component = InjectorComponent::createObject($loadedPlugin['bootstrap']);

                    if ($component instanceof BootstrapInterface) {
                        Yii::debug('Bootstrap with ' . $loadedPlugin['bootstrap'] . '::bootstrap()', 'bootstrap');
                        $component->bootstrap($app);
                    } else {
                        Yii::debug('Bootstrap with ' . $loadedPlugin['bootstrap'], 'bootstrap');
                    }
                }
            } catch (Exception $e) {
                if (YII_DEBUG) {
                    throw $e;
                }

                $this->unloadPlugin($pluginFqn, true, $e->getMessage());
            }
        }

        if (YII_CONSOLE) {
            if (!isset($app->controllerMap['migrate'])) {
                $app->controllerMap['migrate'] = [
                    'class' => 'yii\console\controllers\MigrateController',
                    'migrationNamespaces' => [
                        'yii\queue\db\migrations',
                    ],
                    'migrationPath' => [
                        $this->generatedConfig['defaultMigrationPath'] ?? '@app/migrations',
                        '@yii/rbac/migrations',
                        '@fafcms/updater/migrations'
                    ]
                ];
            }

            $updater = $app->getModule('updater');

            $updaterSetNamespaces = property_exists($updater, 'updateNamespaces');

            foreach ($this->loadedPlugins as $loadedPlugin) {
                $app->controllerMap['migrate']['migrationNamespaces'][] = $loadedPlugin['namespace'] . 'migrations';

                if ($updaterSetNamespaces) {
                    $updater->updateNamespaces[] = $loadedPlugin['namespace'] . 'updates';
                }
            }

            return;
        }

        if (!$this->getIsInstalled() || $this->getIsBackend()) {
            $app->layout = 'backend';
            $app->homeUrl = '/' . $this->getBackendUrl();
        } else {
            $app->layout = 'frontend';
            $app->errorHandler->errorAction = 'frontend/error';
            $app->homeUrl = $this->getStartPageContentmeta()->relativeUrl ?? null;

            $loginContentmetaId = Yii::$app->fafcms->getCurrentProjectLanguage()->login_page_contentmeta_id ?? Yii::$app->fafcms->getCurrentProject()->login_page_contentmeta_id;

            if ($loginContentmetaId !== null) {
                $loginContentmeta = Contentmeta::find()->andWhere(['id' => $loginContentmetaId])->one();

                if ($loginContentmeta !== null) {
                    $app->user->loginUrl = $loginContentmeta->relativeUrl;
                }
            }

            if (!isset($app->view->params['breadcrumbs'])) {
                $app->view->params['breadcrumbs'] = [
                    $this->getStartPageContentmeta()
                ];
            }
        }

        if ($this->getIsInstalled()) {
            if (($currentProject = $this->getCurrentProject()) !== null) {
                if ($this->getIsFrontend()) {
                    $app->name = Yii::$app->fafcms->getCurrentProjectLanguage()->site_name ?? Yii::$app->fafcms->getCurrentProject()->site_name ?? $currentProject->name;
                } else {
                    $app->name = $currentProject->name;
                }
            }

            //TODO remove me
            Yii::setAlias('@frontendUrl', $app->getModule(SettingmanagerBootstrap::$id)->getSetting('frontend_url_'.YII_ENV, null));

            $app->getUrlManager()->addRules([
                'cron' => 'main/cron',
                'assets/translations/<language:[a-zA-Z\-]+>-<cache:[0-9]+>.js' => 'main/js-translation',
            ], true);

            if (Module::getLoadedModule()->getPluginSettingValue('use_web_cronjob') && !Yii::$app->getRequest()->getIsAjax()) {
                Yii::$app->getView()->registerJs('$.get(\''. Yii::$app->getUrlManager()->getBaseUrl() .'/cron\');');
            }

            $lastRealCronjob = Module::getLoadedModule()->getPluginSettingValue('last_real_cronjob');

            if ($lastRealCronjob !== null) {
                $now = InjectorComponent::createObject(DateTime::class, ['now', null]);
                $diff = $this->getTotalInterval($now->diff($lastRealCronjob), 'hours');

                if ($diff > 2) {
                 //   var_dump($diff);
                }
            }

            if ($this->getIsBackend()) {
                $app->getUrlManager()->addRules([
                    $this->getBackendUrl() => 'main/index',
                    $this->getBackendUrl() . '/change-project/<id:[a-zA-Z0-9\-\_]+>' => 'main/change-project',
                    $this->getBackendUrl() . '/change-project-language/<id:[a-zA-Z0-9\-\_]+>' => 'main/change-project-language',
                    $this->getBackendUrl() . '/flush-cache/<cache:[a-zA-Z0-9\-]+>' => 'main/flush-cache',
                    $this->getBackendUrl() . '/login' => 'user/login',
                    $this->getBackendUrl() . '/logout' => 'user/logout',
                    $this->getBackendUrl() . '/request-password-reset' => 'user/request-password-reset',
                    $this->getBackendUrl() . '/<action:[a-zA-Z0-9\-]+>' => 'main/<action>',
                    $this->getBackendUrl() . '/<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>' => '<controller>/<action>',
                    $this->getBackendUrl() . '/<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>' => '<controller>/<action>',
                ], true);

                $app->getAssetManager()->bundles = array_merge($app->getAssetManager()->bundles ?? [], require(FAFCMS_CORE_PATH . '/config/bundles.php'));

                if (!Yii::$app->getUser()->getIsGuest() && !Yii::$app->user->can('accessBackend')) {
                    throw new ForbiddenHttpException(Yii::t('fafcms-core', 'Your not allowed to access this page.'));
                }
            } else {
                Yii::$container->set(Captcha::class, [
                    'captchaAction' => 'frontend/captcha',
                ]);

                Yii::$container->set(CaptchaValidator::class, [
                    'captchaAction' => 'frontend/captcha',
                ]);

                $app->getUrlManager()->addRules([
                    'robots.txt' => 'frontend/robots',
                    'sitemap.xml' => 'frontend/sitemap',
                    'sitemap-<page:[0-9]+>.xml' => 'frontend/sitemap-page',
                    'captcha' => 'frontend/captcha',
                    '<site:(.*)>' => 'frontend/view',
                ], true);

                if (isset($this->generatedConfig['assets']['compressed']) && in_array(YII_ENV, ($this->generatedConfig['assets']['compressedEnv'] ?? ['prod']))) {
                    $configFile = Yii::getAlias($this->generatedConfig['assets']['compressed']);

                    if (is_file($configFile)) {
                        $app->getAssetManager()->bundles = ArrayHelper::merge(array_merge($app->getAssetManager()->bundles ?? [], include($configFile), $this->generatedConfig['assets'][$this->getAppType()]['compressedBundleReplacements'] ?? []), $this->generatedConfig['assets'][$this->getAppType()]['compressedBundleExtras'] ?? []);
                    }
                } else {
                    $app->getAssetManager()->bundles = ArrayHelper::merge(array_merge($app->getAssetManager()->bundles ?? [], $this->generatedConfig['assets'][$this->getAppType()]['bundleReplacements'] ?? []), $this->generatedConfig['assets'][$this->getAppType()]['bundleExtras'] ?? []);
                }
            }
        } else {
            $app->getUrlManager()->addRules([
                '' => 'installer/index',
                '<action:[a-zA-Z0-9\-]+>' => 'installer/<action>',
            ], false);
        }

        /*
                Yii::$container->set(\yii\validators\DateValidator::class, [
                    'format' => 'MM dd, y, h:mm:ss a'
                ]);

                Yii::$app->formatter->datetimeFormat = 'd';*/
    }

    /**
     * @var Project
     */
    private $_currentProject;

    /**
     * @return Project|null
     * @throws \Throwable
     */
    public function getCurrentProject(): ?Project
    {
        if ($this->_currentProject === null) {
            $this->_currentProject = Project::find()->where(['id' => $this->getCurrentProjectId()])->one();
        }

        return $this->_currentProject;
    }

    public function prepareRender(): void
    {
        Yii::$app->fafcmsParser->data['language'] = &Yii::$app->language;
        Yii::$app->fafcmsParser->data['charset'] = &Yii::$app->charset;
        Yii::$app->fafcmsParser->data['homeUrl'] = Yii::$app->getHomeUrl();
        Yii::$app->fafcmsParser->data['name'] = &Yii::$app->name;
        Yii::$app->fafcmsParser->data['params'] = &Yii::$app->getView()->params;
        Yii::$app->fafcmsParser->data['request'] = Yii::$app->getRequest();
        Yii::$app->fafcmsParser->data['queryParams'] = Yii::$app->getRequest()->getQueryParams();
        Yii::$app->fafcmsParser->data['currentProject'] = Yii::$app->fafcms->getCurrentProject();
        Yii::$app->fafcmsParser->data['currentProjectId'] = Yii::$app->fafcms->getCurrentProjectId();
        Yii::$app->fafcmsParser->data['currentProjectLanguage'] = Yii::$app->fafcms->getCurrentProjectLanguage();
        Yii::$app->fafcmsParser->data['currentProjectLanguageId'] = Yii::$app->fafcms->getCurrentProjectLanguageId();
        Yii::$app->fafcmsParser->data['isGuest'] = true;

        if (isset(Yii::$app->components['user'])) {
            Yii::$app->fafcmsParser->data['isGuest'] = Yii::$app->getUser()->isGuest;
            Yii::$app->fafcmsParser->data['userIdentity'] = Yii::$app->getUser()->identity;
            Yii::$app->fafcmsParser->data['user'] = Yii::$app->getUser();
        }

        Yii::$app->fafcmsParser->data['fafcms'] = $this;
    }

    /**
     * @return int|null
     * @throws \Throwable
     */
    public function getStartPageContentmetaId(): ?int
    {
        return Yii::$app->fafcms->getCurrentProjectLanguage()->start_page_contentmeta_id ?? Yii::$app->fafcms->getCurrentProject()->start_page_contentmeta_id;
    }

    /**
     * @var Contentmeta|null
     */
    private ?Contentmeta $startPageContentmeta = null;

    /**
     * @return Contentmeta|null
     * @throws InvalidConfigException
     * @throws \Throwable
     */
    public function getStartPageContentmeta(): ?Contentmeta
    {
        if ($this->startPageContentmeta === null) {
            $this->startPageContentmeta = Contentmeta::find()->where(['id' => $this->getStartPageContentmetaId()])->one();
        }

        return $this->startPageContentmeta;
    }

    /**
     * TODO remove me...
     */
    public function getStartPageId()
    {
        return $this->getStartPageContentmeta()['model_id'] ?? null;
    }

    /**
     * @var int|null
     */
    private ?int $_currentPageId = null;

    /**
     * @param int $id
     */
    public function setCurrentPageId(int $id)
    {
        $this->_currentPageId = $id;
    }

    /**
     * @return int
     */
    public function getCurrentPageId(): ?int
    {
        return $this->_currentPageId;
    }

    /**
     * @var int|null
     */
    private ?int $_currentProjectId = null;

    /**
     * @param int $id
     *
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     */
    public function setCurrentProjectId(int $id): void
    {
        try {
            if (
                !YII_CONSOLE &&
                $this->getIsBackend() &&
                $id !== Module::getLoadedModule()->getUserSettingValue('currentProject')
            ) {
                Module::getLoadedModule()->setUserSettingValue('currentProject', $id);
            }
        } catch (ErrorException $e) {}

        $this->_currentProjectId = $id;
        $this->_currentProject = null;
        $this->_currentProjectLanguageId = null;
        $this->_currentProjectLanguage = null;
    }

    /**
     * @var Domain|null
     */
    private ?Domain $_requestDomain = null;

    /**
     * @return Domain|null
     * @throws InvalidConfigException
     */
    public function getRequestDomain(): ?Domain
    {
        $domain = $_SERVER['HTTP_HOST'] ?? $_SERVER['SERVER_NAME'] ?? '';

        if ($this->_requestDomain === null) {
            $this->_requestDomain = Domain::find()
                ->where(['domain' => $domain])
                ->byProject('all')
                ->byProjectLanguage('all')
                ->one();
        }

        return $this->_requestDomain;
    }

    /**
     * @return int|null
     * @throws InvalidConfigException
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     */
    public function getCurrentProjectId(): ?int
    {
        if ($this->_currentProjectId === null) {
            $projectId = null;

            if (!YII_CONSOLE) {
                $projectId = $this->getRequestDomain()->project_id ?? null;
            }

            if (YII_CONSOLE || $this->getIsBackend()) {
                try {
                    $projectId = Module::getLoadedModule()->getUserSettingValue('currentProject');
                } catch (ErrorException $e) {}
            }

            if ($projectId === null) {
                $project = Project::find()->one();

                if ($project !== null) {
                    $projectId = $project->id;
                }
            }

            $this->setCurrentProjectId($projectId);
        }

        return $this->_currentProjectId;
    }

    /**
     * @var int|null
     */
    private ?int $_currentLanguageId = null;

    /**
     * @param int $id
     */
    public function setCurrentLanguageId(int $id): void
    {
        if (isset(Yii::$app->response->cookies)) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'language',
                'value' => $id,
                'expire' => time() + (24 * 60 * 60 * 30)
            ]));
        }

        if (isset(Yii::$app->user, Yii::$app->user->identity->language_id) && !Yii::$app->user->isGuest) {
            Yii::$app->user->identity->language_id = $id;
            Yii::$app->user->identity->save(false);
        }

        $this->_currentLanguageId = $id;
        $this->_currentLanguage = null;
    }

    /**
     * @return int|null
     */
    public function getCurrentLanguageId(): ?int
    {
        return $this->_currentLanguageId;
    }

    /**
     * @var array|null
     */
    private ?array $_currentLanguage = null;

    /**
     * @return array
     */
    public function getCurrentLanguage(): array
    {
        if ($this->_currentLanguage === null) {
            $availableLanguagesById = $this->getAvailableLanguagesById();
            $this->_currentLanguage = $availableLanguagesById[$this->getCurrentLanguageId()] ?? null;
        }

        return $this->_currentLanguage;
    }

    /**
     * @var int|null
     */
    private ?int $_currentProjectLanguageId = null;

    /**
     * @param int $id
     *
     * @throws \yii\base\ErrorException
     */
    public function setCurrentProjectLanguageId(int $id): void
    {
        try {
            if (
                !YII_CONSOLE &&
                $this->getIsBackend() &&
                $id !== Module::getLoadedModule()->getUserSettingValue('currentProjectLanguage')
            ) {
                Module::getLoadedModule()->setUserSettingValue('currentProjectLanguage', $id);
            }
        } catch (ErrorException $e) {}

        $this->_currentProjectLanguageId = $id;
        $this->_currentProjectLanguage = null;
    }

    /**
     * @return int
     * @throws InvalidConfigException
     * @throws \yii\base\ErrorException
     */
    public function getCurrentProjectLanguageId(): int
    {
        if ($this->_currentProjectLanguageId === null) {
            $projectLanguageId = null;

            if (YII_CONSOLE) {
                return 1; //TODO
            }

            $domain = $this->getRequestDomain();

            if ($domain !== null) {
                $projectLanguageId = $domain->projectlanguage_id;

                if ($projectLanguageId === null) {
                    $projectLanguageId = Projectlanguage::find()->select('id')->where(['domain_id' => $domain->id])->scalar();

                    if ($projectLanguageId === false) {
                        $projectLanguageId = null;
                    }
                }
            }

            if ($this->getIsBackend()) {
                try {
                    $projectLanguageId = Module::getLoadedModule()->getUserSettingValue('currentProjectLanguage');

                    if ($projectLanguageId === null) {
                        $projectLanguageId = $this->getCurrentProject()->primaryProjectlanguage->id;
                    }
                } catch (ErrorException $e) {}
            }

            if ($projectLanguageId === null) {
                $projectLanguage = $this->getCurrentProject()->primaryProjectlanguage;

                if ($projectLanguage !== null) {
                    $projectLanguageId = $projectLanguage->id;
                }
            }

            $this->setCurrentProjectLanguageId($projectLanguageId);
        }

        return $this->_currentProjectLanguageId;
    }

    /**
     * @var Projectlanguage|null
     */
    private ?Projectlanguage $_currentProjectLanguage = null;

    /**
     * @return Projectlanguage
     * @throws InvalidConfigException
     * @throws \yii\base\ErrorException
     */
    public function getCurrentProjectLanguage(): Projectlanguage
    {
        if ($this->_currentProjectLanguage === null) {
            $this->_currentProjectLanguage = Projectlanguage::find()->where(['id' => $this->getCurrentProjectLanguageId()])->one();
        }

        return $this->_currentProjectLanguage;
    }

    /**
     * @param ViewComponent $view
     *
     * @throws InvalidConfigException
     */
    public function registerAssets(ViewComponent $view): void
    {
        $bundles = $this->generatedConfig['assets'][$this->getAppType()]['bundles'] ?? [];
        $cssFiles = $this->generatedConfig['assets'][$this->getAppType()]['cssFiles'] ?? [];
        $jsFiles = $this->generatedConfig['assets'][$this->getAppType()]['jsFiles'] ?? [];

        foreach ($bundles as $bundle) {
            $bundle::register($view);
        }

        foreach ($cssFiles as $cssFile) {
            $view->registerCssFile($cssFile['url'], $cssFile['options'] = [], $cssFile['key']= null);
        }

        foreach ($jsFiles as $jsFile) {
            $view->registerJsFile($jsFile['url'], $jsFile['options'] = [], $jsFile['key'] = null);
        }
    }

    /**
     * @param string $id
     * @param array $rules
     * @param bool $append
     */
    public function addBackendUrlRules(string $id, array $rules, bool $append = true): void
    {
        $backendRules = [];

        if ($id !== '') {
            $id .= '/';
        }

        foreach ($rules as $key => $rule) {
            $backendRules[$this->getBackendUrl() . '/' . $id . $key] = $id . $rule;
        }

        Yii::$app->getUrlManager()->addRules($backendRules, $append);
    }

    /**
     * @param string $id
     */
    public function addApiUrlRules(string $id = ''): void
    {
        if ($id === 'fafcms-core') {
            $id = '';
        }

        if ($id !== '') {
            $id .= '/';
        }

        $prefix = $this->getApiUrl() . '/' . $id;

        Yii::$app->getUrlManager()->addRules([
            [
                'verb'    => [
                    'PUT',
                    'PATCH',
                ],
                'pattern' => $prefix . '<controller:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>',
                'route'   => $id . '<controller>/api-update',
            ],
            [
                'verb'    => [
                    'DELETE',
                ],
                'pattern' => $prefix . '<controller:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>',
                'route'   => $id . '<controller>/api-delete',
            ],
            [
                'verb'    => [
                    'GET',
                    'HEAD',
                ],
                'pattern' => $prefix . '<controller:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>',
                'route'   => $id . '<controller>/api-view',
            ],
            [
                'verb'    => [
                    'POST',
                ],
                'pattern' => $prefix . '<controller:[a-zA-Z0-9\-]+>',
                'route'   => $id . '<controller>/api-create',
            ],
            [
                'verb'    => [
                    'GET',
                    'HEAD',
                ],
                'pattern' => $prefix . '<controller:[a-zA-Z0-9\-]+>',
                'route'   => $id . '<controller>/api-index',
            ],
            [
                'verb'    => [],
                'pattern' => $prefix . '<controller:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>',
                'route'   => $id . '<controller>/api-options',
            ],
            [
                'verb'    => [],
                'pattern' => $prefix . '<controller:[a-zA-Z0-9\-]+>',
                'route'   => $id . '<controller>/api-options',
            ]
        ], true);
    }

    /**
     * @param string $id
     * @param array $rules
     * @param bool $append
     */
    public function addFrontendUrlRules(string $id, array $rules, bool $append = true): void
    {
        $frontendRules = [];

        if ($id !== '') {
            $id .= '/';
        }

        foreach ($rules as $key => $rule) {
            $frontendRules[$key] = $id.$rule;
        }

        Yii::$app->getUrlManager()->addRules($frontendRules, $append);
    }

    private function addTranslations(): void
    {
        Yii::$app->i18n->translations = array_merge(Yii::$app->i18n->translations, ($this->addAppTranslation ? [
            'app' => [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en_US',
                'basePath' => '@project/messages'
            ],
        ] : []), [
            'passwordvalidation' => [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en_US',
                'basePath' => '@fafcmsCore/messages'
            ],
            'fafcms-core*' => [
                'class' => PhpMessageSource::class,
                'basePath' => '@fafcmsCore/messages',
                'forceTranslation' => true,
            ]
        ], $this->generatedConfig['i18n']['translations'] ?? []);

        $appTranslationBasePath = Yii::getAlias(Yii::$app->i18n->translations['app']['basePath']??null);

        if ($appTranslationBasePath !== null && !is_dir($appTranslationBasePath) && !mkdir($appTranslationBasePath) && !is_dir($appTranslationBasePath)) {
            Yii::$app->getLog()->getLogger()->log('Cannot create the folder \''.$appTranslationBasePath.'\'. Please check the file permissions', Logger::LEVEL_ERROR);
        }
    }

    /**
     * @throws InvalidConfigException
     * @throws \yii\base\ErrorException
     */
    private function setLanguage(): void
    {
        if (YII_CONSOLE || $this->getIsBackend()) {
            $language = null;
            $languageId = null;
            $languageCode = null;
            $availableLanguages = ['en', 'de', 'jp'];
            $availableLanguagesById = null;

            if ($this->getIsInstalled()) {
                $availableLanguagesById = $this->getAvailableLanguagesById();
                $availableLanguages = $this->getAvailableLanguages();
            }

            if (isset(Yii::$app->user, Yii::$app->user->identity->language_id) && !Yii::$app->user->isGuest && (Yii::$app->user->identity->language_id ?? null) !== null) {
                $languageId = Yii::$app->user->identity->language_id;
            }

            if ($languageId === null && isset(Yii::$app->request->cookies)) {
                $languageId = Yii::$app->request->cookies->getValue('language', null);
            }

            if ($languageId === null) {
                if (YII_CONSOLE) {
                    $languageCode = Yii::$app->language;
                } else {
                    $languageCode = Yii::$app->getRequest()->getPreferredLanguage(array_keys($availableLanguages));
                }
            }

            if ($languageId !== null && $this->getIsInstalled()) {
                $language = $availableLanguagesById[$languageId] ?? null;
            }

            if ($language === null && $this->getIsInstalled()) {
                $language = $availableLanguages[$languageCode] ?? null;

                if ($language !== null) {
                    $this->setCurrentLanguageId((int)$language['id']);
                }
            }

            $this->_currentLanguageId = $language['id'];
        } else {
            $language = $this->getAvailableLanguagesById()[$this->getCurrentProjectLanguage()->language_id];
            $this->setCurrentLanguageId((int)$language['id']);
        }

        Yii::$app->language = $language['ietfcode'] ?? ($this->generatedConfig['defaultLanguage'] ?? 'en');
    }

    private ?array $availableLanguages = null;

    /**
     * @return array
     */
    public function getAvailableLanguages(): array
    {
        if ($this->availableLanguages === null) {
            $this->availableLanguages = ArrayHelper::index($this->getAvailableLanguagesById(), 'ietfcode');
        }

        return $this->availableLanguages;
    }

    private ?array $availableLanguagesById = null;

    /**
     * @return array
     */
    public function getAvailableLanguagesById(): array
    {
        if ($this->availableLanguagesById === null) {
            $this->availableLanguagesById = Yii::$app->dataCache->index(Language::class, 'id', true);
        }

        return $this->availableLanguagesById;
    }

    /**
     * @param array $removeWhenEndsWith
     * @return array
     */
    public function getModelClasses(array $removeWhenEndsWith = ['Search', 'Form']): array
    {
        $contentClasses = [];

        $finder = new ClassFinder();

        $modelClasses = array_merge($finder->findClasses(Yii::getAlias('@project/models')), $finder->findClasses(Yii::getAlias('@project/**/models')));

        foreach ($modelClasses as $modelClass) {
            if (preg_match('/('.implode('|', $removeWhenEndsWith).')$/', $modelClass['fullyQualifiedClassName']) !== 1) {
                $contentClasses[$modelClass['fullyQualifiedClassName']] = $modelClass;
            }
        }

        return $contentClasses;
    }

    /**
     * @return array
     */
    public function getContentClasses(): array
    {
        $contentClasses = [];
        $finder = new ClassFinder();

        $filter = [
            'implements' => ContentmetaInterface::class,
            'traits' => ContentmetaTrait::class
        ];

        $modelClasses = [
            $finder->findClasses(Yii::getAlias('@project/models'), $filter)
        ];

        foreach (Yii::$app->fafcms->getLoadedPluginPaths() as $loadedPluginPath) {
            $modelClasses[] = $finder->findClasses($loadedPluginPath . '/models', $filter);
        }

        $modelClasses = array_merge([], ...$modelClasses);

        foreach ($modelClasses as $modelClass) {
            $contentClasses[$modelClass['fullyQualifiedClassName']] = [
                'name' => $modelClass['fullyQualifiedClassName']::contentmetaName(),
                'id' => $modelClass['fullyQualifiedClassName']::contentmetaId(),
                'site_id' => $modelClass['fullyQualifiedClassName']::contentmetaSiteId(),
            ];
        }

        return $contentClasses;
    }

    /**
     * @param string $contentClassName
     * @return array|null
     */
    public function getContentClass(string $contentClassName): ?array
    {
        return $this->getContentClasses()[$contentClassName]??null;
    }

    /**
     * @param string|null $contentClassName
     * @return bool
     */
    public function isValidContentClass(?string $contentClassName): bool
    {
        if ($contentClassName === null) {
            return false;
        }

        $contentClass = $this->getContentClass($contentClassName);
        return isset($contentClass);
    }

    /**
     * @param DateTime|null $dateTime
     * @return string
     * @throws \Exception
     */
    public function getNiceDateTime(DateTime $dateTime = null): string
    {
        if ($dateTime === null)
        {
            $dateTime = new DateTime('now');
        }

        $dateTime->setTime($dateTime->format('H'), round(($dateTime->format('i') + 10) / 5) * 5, 0);
        return $dateTime->format(FormatConverter::convertDateIcuToPhp(Yii::$app->formatter->datetimeFormat, 'datetime'));
    }

    /**
     * @return string
     */
    public function getAppType(): string
    {
        if ($this->type !== null) {
            return $this->type;
        }

        if (YII_CONSOLE || strpos(Yii::$app->request->pathInfo, $this->getBackendUrl()) === 0) {
            return self::TYPE_BACKEND;
        }

        if (strpos(Yii::$app->request->pathInfo, $this->getApiUrl()) === 0) {
            return self::TYPE_API;
        }

        return self::TYPE_FRONTEND;
    }

    /**
     * @return bool
     */
    public function getIsFrontend(): bool
    {
        return $this->getAppType() === self::TYPE_FRONTEND;
    }

    /**
     * @return bool
     */
    public function getIsBackend(): bool
    {
        return $this->getAppType() === self::TYPE_BACKEND;
    }

    /**
     * @return bool
     */
    public function getIsApi(): bool
    {
        return $this->getAppType() === self::TYPE_API;
    }

    public function checkRule($rule, $user, $request)
    {
        if ($allow = $rule->allows(Yii::$app->requestedAction, $user, $request)) {
            return true;
        }

        if ($allow === false) {
            if (isset($rule->denyCallback)) {
                call_user_func($rule->denyCallback, $rule);
            } elseif ($this->denyCallback !== null) {
                call_user_func($this->denyCallback, $rule);
            }
        }

        return false;
    }

    public function isSidebarVisible():bool
    {
        return Module::getLoadedModule()->getUserSettingValue('sidebarVisible', null, null, 'main') !== 0;
    }

    /**
     * @param Model      $model
     * @param string     $fieldName
     * @param array|null $fieldConfigs
     * @param array|null $fieldConfig
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getFieldDefinition(Model $model,  string $fieldName, ?array $fieldConfigs = null, ?array $fieldConfig = null): array
    {
        if ($fieldConfigs === null) {
            $fieldConfigs = $this->getModelFieldConfig($model);
        }

        if ($fieldConfig === null) {
            $fieldConfig = [];
        }

        if (($fieldConfig['type'] ?? null) === null) {
            $fieldConfig = ArrayHelper::merge($fieldConfigs[$fieldName] ?? [], $fieldConfig);
        }

        $fieldParts = explode('.', $fieldName);
        $relation = false;

        if (count($fieldParts) > 1) {
            $relation = true;

            $relationFieldName = array_pop($fieldParts);
            $relationName = implode('.', $fieldParts);

            $parentRelationModel = $model;
            $fieldPartCount = count($fieldParts);

            foreach ($fieldParts as $index => $fieldPart) {
                $relationModel = ArrayHelper::getValue($parentRelationModel, $fieldPart);

                if ($relationModel === null) {
                    $relationQuery = $parentRelationModel->getRelation($fieldPart);
                    $relationModel = InjectorComponent::createObject($relationQuery->modelClass);
                    $relationModel->loadDefaultValues();
                }

                if ($index < $fieldPartCount - 1) {
                    $parentRelationModel = $relationModel;
                }
            }

            if (($fieldConfig['type'] ?? null) === null) {
                $fieldConfigs = $this->getModelFieldConfig($relationModel);
                $fieldConfig = ArrayHelper::merge($fieldConfigs[$relationFieldName] ?? null, $fieldConfig);
            }
        }

        if (($fieldConfig['type'] ?? null) === null) {
            throw new InvalidArgumentException('Unknown form field "' . $fieldName . '" in model "' . get_class($model) . '".');
        }

        $labelOptions = $fieldConfig['labelOptions'] ?? [];
        $fieldType = ArrayHelper::remove($fieldConfig, 'type');

        if ($relation) {
            $fieldName = $relationFieldName;
            $inputName = Html::getInputName($model, 'relations[' . $relationName . '][' . ($relationModel->id ?? FormField::NEW_RELATION). '][' . $relationFieldName . ']');

            $model = $relationModel;

            if (!isset($fieldConfig['options']['id'])) {
                $fieldConfig['options']['id'] = Inflector::slug(str_replace('[', '-', $inputName));
            }

            $labelOptions['for'] = $fieldConfig['options']['id'];

            if (!isset($fieldConfig['options']['name'])) {
                $fieldConfig['options']['name'] = $inputName;
            }

            $inputId = $fieldConfig['options']['id'];
        } else {
            $inputName = $fieldConfig['options']['name'] ?? Html::getInputName($model, $fieldName);
            $inputId = $fieldConfig['options']['id'] ?? Html::getInputId($model, $fieldName);
        }

        return [
            'model' => $model,
            'fieldName' => $fieldName,
            'fieldType' => $fieldType,
            'labelOptions' => $labelOptions,
            'fieldConfig' => $fieldConfig,
            'inputName' => $inputName,
            'inputId' => $inputId
        ];
    }

    /**
     * @param Model $model
     *
     * @return array
     */
    public function getModelFieldConfig(Model $model): array
    {
        $method_names = preg_grep('/^getFieldConfig/', get_class_methods($model));
        $fieldConfig = [];

        array_walk($method_names, static function ($method) use ($model, &$fieldConfig) {
            $fieldConfig[] = $model->$method();
        });

        return array_merge(...$fieldConfig);
    }

    /**
     * @param ActiveForm $form
     * @param Model      $model
     * @param string     $name
     * @param string     $inputType
     * @param array      $labelOptions
     * @param array      $fieldConfig
     * @param string     $inputName
     * @param string     $inputId
     *
     * @return string
     */
    public function getInput(ActiveForm $form, Model $model, string $name, string $inputType, array $labelOptions, array $fieldConfig, string $inputName, string $inputId): string
    {
        if (class_exists($inputType)) {
            $formInput = new $inputType(array_merge([
                'form' => $form,
                'model' => $model,
                'name' => $name,
                'labelOptions' => $labelOptions,
                'inputName' => $inputName,
                'inputId' => $inputId,
            ], $fieldConfig));

            if ($formInput instanceof FormInput) {
                return $formInput->run();
            }
        }

        return 'Unknow input type: ' . $inputType;
    }

    /**
     * @param array|string $translation
     * @return string
     */
    public function getTextOrTranslation($translation): string
    {
        if ($translation === null) {
            return 'null';
        }

        if (is_string($translation)) {
            return $translation;
        }

        return Yii::t($translation['category'] ?? $translation[0], $translation['message'] ?? $translation[1], $translation['params'] ?? $translation[2] ?? [], $translation['language'] ?? $translation[3] ?? null);
    }

    /**
     * Calculate total years|months|days|hours|minutes|seconds|milliseconds of an \DateInterval
     * Original by pankajs7590 at gmail dot com https://www.php.net/manual/de/dateinterval.format.php#121237
     * The PHP manual text and comments are covered by the Creative Commons Attribution 3.0 License,
     * copyright (c) the PHP Documentation Group (see https://www.php.net/manual/add-note.php and https://www.php.net/license/index.php#doc-lic).
     *
     * @licence Creative Commons Attribution 3.0 License, copyright (c) the PHP Documentation Group
     * @param \DateInterval $interval
     * @param string $type
     * @return float|int|string|null
     */
    public function getTotalInterval(\DateInterval $interval, string $type)
    {
        $seconds = 0;
        $minutes = 0;
        $hours = 0;
        $months = 0;

        switch($type) {
            case 'years':
                return $interval->format('%Y');
                break;
            case 'months':
                $years = $interval->format('%Y');

                if ($years) {
                    $months += $years * 12;
                }

                $months += (int)$interval->format('%m');

                return $months;
                break;
            case 'days':
                return $interval->format('%a');
                break;
            case 'hours':
                $days = $interval->format('%a');

                if ($days) {
                    $hours += 24 * $days;
                }

                $hours += (int)$interval->format('%H');

                return $hours;
                break;
            case 'minutes':
                $days = $interval->format('%a');

                if ($days) {
                    $minutes += 24 * 60 * $days;
                }

                $hours = $interval->format('%H');

                if ($hours) {
                    $minutes += 60 * $hours;
                }

                $minutes += (int)$interval->format('%i');

                return $minutes;
                break;
            case 'seconds':
                $days = (int)$interval->format('%a');

                if ($days) {
                    $seconds += 24 * 60 * 60 * $days;
                }

                $hours = $interval->format('%H');

                if ($hours) {
                    $seconds += 60 * 60 * $hours;
                }

                $minutes = $interval->format('%i');

                if ($minutes) {
                    $seconds += 60 * $minutes;
                }

                $seconds += (int)$interval->format('%s');

                return $seconds;
                break;
            case 'milliseconds':
                $days = $interval->format('%a');

                if ($days) {
                    $seconds += 24 * 60 * 60 * $days;
                }

                $hours = $interval->format('%H');

                if ($hours) {
                    $seconds += 60 * 60 * $hours;
                }

                $minutes = $interval->format('%i');

                if ($minutes) {
                    $seconds += 60 * $minutes;
                }

                $seconds += (int)$interval->format('%s');

                return $seconds * 1000;
                break;
        }

        return null;
    }

    /**
     * @param Model         $model
     * @param array         $data
     * @param string        $action
     * @param \Closure|null $afterLoad
     * @param bool          $validateBeforeAction
     *
     * @return int
     * @throws InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function saveModelData(Model $model, array $data, string $action = 'save', ?\Closure $afterLoad = null, bool $validateBeforeAction = true): int
    {
        $relationHasError = false;
        $forceRedirect = false;

        if ($model->load($data)) {
            $fieldConfigs = Yii::$app->fafcms->getModelFieldConfig($model);

            if ($afterLoad instanceof \Closure) {
                $model = $afterLoad($model);
            }

            if ($model instanceof ActiveRecord) {
                $formName    = $model->formName();
                $transaction = Yii::$app->db->beginTransaction();
                $isNewRecord = $model->getIsNewRecord();

                if ((!$validateBeforeAction || $model->validate()) && $model->$action()) {
                    foreach ($fieldConfigs as $name => $config) {
                        if (($config['linkRelation'] ?? false) === true) {
                            $records = $model->{'new' . ucfirst($name)};

                            $savedRecords = $model->$name;
                            $deletedRecords = array_udiff($savedRecords, $records, static function ($obj_a, $obj_b) {
                                return $obj_a->id - $obj_b->id;
                            });

                            $newRecords = array_udiff($records, $savedRecords, static function ($obj_a, $obj_b) {
                                return $obj_a->id - $obj_b->id;
                            });

                            foreach ($newRecords as $newRecord) {
                                try {
                                    $model->link($name, $newRecord);
                                } catch (InvalidCallException $e) {
                                    $relationHasError = true;
                                    $model->addError($name, $e->getMessage());
                                }
                            }

                            foreach ($deletedRecords as $deletedRecord) {
                                try {
                                    $model->unlink($name, $deletedRecord, true);
                                } catch (InvalidCallException $e) {
                                    $relationHasError = true;
                                    $model->addError($name, $e->getMessage());
                                }
                            }
                        }
                    }

                    $relations = $data[$formName]['relations'] ?? [];
                    $relationNames = array_keys($relations);

                    usort($relationNames, static function($a, $b) {
                        $aCount = mb_substr_count($a, '.');
                        $bCount = mb_substr_count($b, '.');

                        if ($aCount === $bCount) {
                            if ($a === $b) {
                                return 0;
                            }

                            return ($a < $b) ? -1 : 1;
                        }

                        return ($aCount < $bCount) ? -1 : 1;
                    });

                    foreach ($relationNames as $relationName) {
                        $relationRecords = $relations[$relationName];

                        try {
                            $fullRelationName = $relationName;
                            $fieldParts = explode('.', $fullRelationName);
                            $parentRelationModel = $model;
                            $fieldPartCount = count($fieldParts);


                            foreach ($fieldParts as $index => $fieldPart) {
                                $relationModel = ArrayHelper::getValue($parentRelationModel, $fieldPart);
                                $relation = $parentRelationModel->getRelation($fieldPart);

                                if ($relation === null) {
                                    continue 2;
                                }

                                if ($relationModel === null) {
                                    $relationModel = InjectorComponent::createObject($relation->modelClass);
                                    $relationModel->loadDefaultValues();
                                }

                                if ($index < $fieldPartCount - 1) {
                                    $parentRelationModel = $relationModel;
                                }
                            }

                            if ($parentRelationModel === null || ($relation ?? null) === null) {
                                continue;
                            }

                            $relationName = $fieldPart;

                            $relationMethodName          = ucfirst($relationName);
                            $checkCreateCondition        = 'get' . $relationMethodName . 'CreateCondition';
                            $checkUpdateCondition        = 'get' . $relationMethodName . 'UpdateCondition';
                            $getModelName                = 'get' . $relationMethodName . 'InverseOf';
                            $beforeSetRelationAttributes = 'beforeSet' . $relationMethodName . 'Attributes';
                            $afterSetRelationAttributes  = 'afterSet' . $relationMethodName . 'Attributes';

                            foreach ($relationRecords as $relationRecordId => $relationRecordData) {
                                if ($relationRecordId === FormField::NEW_RELATION) {
                                    if ($parentRelationModel->hasMethod($checkCreateCondition) && !$parentRelationModel->$checkCreateCondition()) {
                                        continue;
                                    }

                                    $relationModel = InjectorComponent::createObject($relation->modelClass);
                                    $relationModel->loadDefaultValues();

                                    $forceRedirect = true;
                                } else {
                                    if ($parentRelationModel->hasMethod($checkUpdateCondition) && !$parentRelationModel->$checkUpdateCondition()) {
                                        continue;
                                    }

                                    $primaryKey    = $relation->modelClass::primaryKey();
                                    $relationModel = $relation->modelClass::find()->where([$primaryKey[0] ?? 'id' => $relationRecordId])->one();
                                }

                                if ($parentRelationModel->hasMethod($getModelName)) {
                                    $inverseRelationName = $parentRelationModel->$getModelName();
                                } elseif (!empty($relation->inverseOf)) {
                                    $inverseRelationName = $relation->inverseOf;
                                } else {
                                    $class = explode('\\', get_class($parentRelationModel));
                                    $inverseRelationName = strtolower($class[count($class) - 1]);
                                }

                                $inverseRelationMethodName = ucfirst($inverseRelationName);

                                if (method_exists($relationModel, 'set' . $inverseRelationMethodName) || method_exists($relationModel, 'get' . $inverseRelationMethodName)) {
                                    $relationModel->$inverseRelationName = $parentRelationModel;
                                }

                                $parentRelationModel->executeExtendedMethods($beforeSetRelationAttributes, [&$relationModel, &$relationRecordData]);
                                $relationModel->setAttributes($relationRecordData);
                                $parentRelationModel->executeExtendedMethods($afterSetRelationAttributes, [&$relationModel, &$relationRecordData]);

                                if ($relationModel->save()) {
                                    try {
                                        $parentRelationModel->link($relationName, $relationModel);
                                    } catch (InvalidCallException $e) {
                                        $relationHasError = true;
                                        $model->addError($relationName, $e->getMessage());
                                    }
                                } else {
                                    $relationHasError = true;

                                    foreach ($relationModel->getErrors() as $attributeName => $errorMessages) {
                                        foreach ($errorMessages as $errorMessage) {
                                            $model->addError($relationName . '.' . $attributeName, $errorMessage);
                                        }
                                    }

                                    break;
                                }
                            }
                        } catch (InvalidArgumentException $e) {
                            //relation doesnt exists
                            //Yii::error($e->getMessage());
                        }
                    }

                    if (!$relationHasError && method_exists($model, 'afterSaveRelations') && !$model->afterSaveRelations($isNewRecord)) {
                        $relationHasError = true;
                    }

                    if ($relationHasError) {
                        $transaction->rollBack();
                    } else {
                        $transaction->commit();
                        return ($forceRedirect ? 3 : 2);
                    }
                }
            } elseif ((!$validateBeforeAction || $model->validate()) && $model->$action()) {
                return ($forceRedirect ? 3 : 2);
            }

            return 1;
        }

        return 0;
    }

    /**
     * @param $model
     * @param array $error
     * @param array $success
     * @param string|null $view
     * @param array $viewParams
     * @param string $action
     * @param \Closure|null $afterLoad
     * @param bool $allowGet
     * @return mixed|string
     */
    public function handleAjaxForm($model, array $error, array $success, ?string $view = null, array $viewParams = [], string $action = 'save', ?\Closure $afterLoad = null, bool $allowGet = false, bool $validateBeforeAction = true)
    {
        $data = Yii::$app->request->post();
        $result = $error;

        if ($allowGet && Yii::$app->request->getIsGet())  {
            $data = Yii::$app->request->get();
        }

        $modelResult = $this->saveModelData($model, $data, $action, $afterLoad, $validateBeforeAction);

        if ($modelResult === 0) {
            $result = $this->handleResult($model, $result);
        } else {
            if ($modelResult === 2 || $modelResult === 3) {
                $result = $success;
            }

            $result = $this->handleResult($model, $result, $isNewRecord ?? false);

            if (isset($result['redirect'])) {
                if ($modelResult !== 3 && !$result['redirect']) {
                    unset($result['url']);
                }

                unset($result['redirect']);
            }

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                if ($view !== null) {
                    Yii::$app->view->renderedSiteMode = 'raw';
                    Yii::$app->view->renderedSiteContent = $result;
                    return '';
                }

                return $result;
            }

            if (isset($result['message'])) {
                Yii::$app->session->setFlash($result['type'], $result['message']);
            }

            if (isset($result['url'])) {
                return Yii::$app->getResponse()->redirect($result['url']);
            }
        }

        if ($view !== null) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->view->renderedSiteMode = 'ajax';
                return Yii::$app->controller->renderPartial($view, array_merge($viewParams, ['model' => $model]));
            }

            return Yii::$app->controller->renderPartial($view, array_merge($viewParams, ['model' => $model]));
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    /**
     * @param Model $model
     * @param array $result
     * @param bool  $isNewRecord
     *
     * @return array
     */
    private function handleResult(Model $model, array $result, bool $isNewRecord = false): array
    {
        if (array_key_exists('url', $result) && $result['url'] === null) {
            unset($result['url']);
        } elseif (isset($result['url']) && $result['url'] instanceof \Closure) {
            $result['url'] = call_user_func($result['url'], $model, [&$result], $isNewRecord);

            if ($result['url'] === null) {
                unset($result['url']);
            }
        }

        if (array_key_exists('redirect', $result) && $result['redirect'] === null) {
            unset($result['redirect']);
        } elseif (isset($result['redirect']) && $result['redirect'] instanceof \Closure) {
            $result['redirect'] = call_user_func($result['redirect'], $model, [&$result], $isNewRecord);

            if ($result['redirect'] === null) {
                unset($result['redirect']);
            }
        }

        if (array_key_exists('reload', $result) && $result['reload'] === null) {
            unset($result['reload']);
        } elseif (isset($result['reload']) && $result['reload'] instanceof \Closure) {
            $result['reload'] = call_user_func($result['reload'], $model, [&$result], $isNewRecord);

            if ($result['reload'] === null) {
                unset($result['reload']);
            }
        }

        if (array_key_exists('call', $result) && $result['call'] === null) {
            unset($result['call']);
        } elseif (isset($result['call']) && $result['call'] instanceof \Closure) {
            $result['call'] = call_user_func($result['call'], $model, [&$result], $isNewRecord);

            if ($result['call'] === null) {
                unset($result['call']);
            }
        }

        if (isset($result['message']) && $result['message'] instanceof \Closure) {
            $result['message'] = call_user_func($result['message'], $model, [&$result], $isNewRecord);
        }

        if (array_key_exists('message', $result) && $result['message'] === null) {
            unset($result['message']);
        }

        if (isset($result['action'])) {
            $model = call_user_func($result['action'], $model, [&$result], $isNewRecord);
        }

        return $result;
    }

    /**
     * @param int      $id
     * @param string   $salt
     * @param int|null $minLength
     *
     * @return string
     * @throws \yii\base\ErrorException
     */
    public function idToHash(int $id, string $salt = '', ?int $minLength = null): string
    {
        $hashids = $this->getHashidsObject($salt, $minLength);
        return $hashids->encode($id);
    }

    /**
     * @param string      $salt
     * @param int|null    $minLength
     * @param string|null $idHash
     *
     * @return Hashids
     * @throws \yii\base\ErrorException
     */
    private function getHashidsObject(string $salt, ?int $minLength = null, ?string $idHash = null): Hashids
    {
        $fullSalt = Module::getLoadedModule()->getPluginSettingValue('hash_id_security_key') . $salt;

        if ($minLength === null && $idHash !== null && mb_strlen($idHash) === 10) {
            $minLength = 10;
        } else {
            if ($minLength === null) {
                $minLength = 25;
            }

            $fullSalt = hash('sha256', $fullSalt);
        }

        return new Hashids($fullSalt, $minLength);
    }

    /**
     * @param string $idHash
     * @param string $salt
     * @param int    $minLength
     *
     * @return int|null
     * @throws \yii\base\ErrorException
     */
    public function hashToId(string $idHash, string $salt = '', ?int $minLength = null): ?int
    {
        $hashids = $this->getHashidsObject($salt, $minLength, $idHash);
        $ids = $hashids->decode($idHash);

        return $ids[0] ?? null;
    }

    /**
     * @var array
     */
    private $_loadedPluginsByIndex = [];

    /**
     * @param string $index
     *
     * @return array
     */
    public function getLoadedPluginsByIndex(string $index): array
    {
        if (!isset($this->_loadedPluginsByIndex[$index])) {
            $this->_loadedPluginsByIndex[$index] = ArrayHelper::index(Yii::$app->fafcms->loadedPlugins, $index);
        }

        return $this->_loadedPluginsByIndex[$index];
    }

    /**
     * @param string $cacheName
     *
     * @return string
     * @throws InvalidConfigException
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     */
    public function getCacheName(string $cacheName): string
    {
        $cacheName .= '_P' . $this->getCurrentProjectId();
        $cacheName .= '_L' . $this->getCurrentProjectLanguageId();
        return $cacheName;
    }
}
