<?php

namespace fafcms\fafcms\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\caching\DbDependency;
use Yii;

class DataCacheComponent extends Component
{
    public array $data = [];

    public function index($modelName, $index = 'id', bool $asArray = false)
    {
        $data = Yii::$app->cache->get($modelName . '_' . $index . '_' . ($asArray ? 'array' : 'object'));

        if ($data === false) {
            if ($index === 'id') {
                $data = ArrayHelper::index($modelName::find()->asArray($asArray)->all(), 'id');
            } else {
                $data = ArrayHelper::index($this->index($modelName, 'id', $asArray), $index);
            }

            $dependencyQuery = 'SELECT COUNT(*)' . ($modelName::instance()->hasAttribute('created_at') ? ', MAX(created_at)' : '') . ($modelName::instance()->hasAttribute('updated_at') ? ', MAX(updated_at)' : '') . ($modelName::instance()->hasAttribute('deleted_at') ? ', MAX(deleted_at)' : '') . ' FROM ' . $modelName::tableName();

            $cacheName = $modelName . '_' . $index;

            if (method_exists($modelName, 'getProject_id')) {
                $cacheName .= '_P' . Yii::$app->fafcms->getCurrentProjectId();
            }

            if (method_exists($modelName, 'getProjectlanguage_id')) {
                $cacheName .= '_L' . Yii::$app->fafcms->getCurrentProjectLanguageId();
            }

            Yii::$app->cache->set($cacheName, $data, null,
                new DbDependency(['sql' => $dependencyQuery])
            );
        }

        return $data;
    }

    /**
     * @param string $modelName
     * @param array|null $select
     * @param string|\Closure|null $from
     * @param string|\Closure|null $to
     * @param string|\Closure|null $group
     * @param array|null $orderBy
     * @param array|null $where
     * @param array|null $joinWith
     * @return array
     */
    public function map(string $modelName, array $select = null, $from = null, $to = null, $group = null, array $orderBy = null, array $where = null, array $joinWith = null, $filterLanguage = true): array
    {
        if ($from === null) {
            $from = 'id';
        }

        if ($to === null) {
            $to = 'name';
        }

        if ($select === null) {
            $select = [$from, $to];
        }

        if ($orderBy === null) {
            $orderBy = array_fill_keys($select, SORT_ASC);
        }

        $query = $modelName::find()->select($select)->orderBy($orderBy)->where($where)->joinWith($joinWith);
        $cacheName = $modelName . '_' . hash('sha256', $query->createCommand()->getRawSql());

        if (method_exists($modelName, 'getProject_id')) {
            $cacheName .= '_P' . Yii::$app->fafcms->getCurrentProjectId();
        }

        if (method_exists($modelName, 'getProjectlanguage_id')) {
            $cacheName .= '_L' . Yii::$app->fafcms->getCurrentProjectLanguageId();
        }

        $dataCacheName = $cacheName;
        $data = false;
        $hasClosure = false;

        if ($from instanceof \Closure) {
            $hasClosure = true;
        } else {
            $dataCacheName .= '_' . $from;
        }

        if ($to instanceof \Closure) {
            $hasClosure = true;
        } else {
            $dataCacheName .= '_' . $to;
        }

        if ($group instanceof \Closure) {
            $hasClosure = true;
        } else {
            $dataCacheName .= '_' . $group;
        }

        if (!$hasClosure) {
            $data = Yii::$app->cache->get($dataCacheName);
        }

        if ($data === false) {
            $dependencyQuery = 'SELECT COUNT(*)' . ($modelName::instance()->hasAttribute('created_at') ? ', MAX(created_at)':'') . ($modelName::instance()->hasAttribute('updated_at') ? ', MAX(updated_at)' : '') . ($modelName::instance()->hasAttribute('deleted_at') ? ', MAX(deleted_at)' : '').' FROM ' . $modelName::tableName();
            $queryData = Yii::$app->cache->get($cacheName);

            if ($queryData === false) {
                $queryData = $query->asArray();

                if (!$filterLanguage) {
                    $queryData = $queryData->byProjectLanguage('all');
                }

                $queryData = $queryData->all();

                Yii::$app->cache->set(
                    $cacheName,
                    $queryData,
                    null,
                    new DbDependency(['sql' => $dependencyQuery])
                );
            }

            if ($from === 'id' && $modelName::primaryKey()[0] === 'hashId') {
                $from = static function ($model) use ($modelName) {
                    return Yii::$app->fafcms->idToHash($model['id'], $modelName);
                };
            }

            $data = ArrayHelper::map($queryData, $from, $to, $group);

            if (!$hasClosure) {
                Yii::$app->cache->set(
                    $dataCacheName,
                    $data,
                    null,
                    new DbDependency(['sql' => $dependencyQuery])
                );
            }
        }

        return $data;
    }
}
