<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

namespace fafcms\fafcms\components;

use Closure;
use fafcms\fafcms\assets\fafcms\backend\FafcmsBackendAppAsset;
use fafcms\fafcms\assets\LoadCSSRelPreloadAsset;
use fafcms\fafcms\Bootstrap as FafcmsBootstrap;
use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\helpers\StringHelper;
use fafcms\fafcms\minifier\Minifier;
use fafcms\fafcms\minifier\MinifierSrcFile;
use fafcms\fafcms\widgets\Breadcrumbs;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\helpers\interfaces\EditViewInterface;
use fafcms\helpers\interfaces\FieldConfigInterface;
use fafcms\helpers\interfaces\IndexViewInterface;
use Yii;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use yii\filters\AccessRule;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\AssetBundle;
use yii\web\Controller;
use yii\web\Cookie;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ViewComponent extends \yii\web\View
{
    /**
     * @var array the page title
     */
    public $title;

    /**
     * This is internally used as the placeholder for receiving the content registered for the head section.
     */
    const PH_HEAD = '<fafcms>YII-BLOCK-HEAD</fafcms>';
    /**
     * This is internally used as the placeholder for receiving the content registered for the beginning of the body section.
     */
    const PH_BODY_BEGIN = '<fafcms>YII-BLOCK-BODY-BEGIN</fafcms>';
    /**
     * This is internally used as the placeholder for receiving the content registered for the end of the body section.
     */
    const PH_BODY_END = '<fafcms>YII-BLOCK-BODY-END</fafcms>';

    private $pushAssetsCache = [];

    /**
     * @param array $assets
     *
     * @return string
     */
    private function buildPushString(array $assets): string
    {
        $pushString = '';
        $i = 0;

        foreach($assets as $asset) {
            $i++;
            $pushString .= '<' . $asset['url'] . '>; rel=preload; as=' . $asset['type'];

            if ($i < count($assets)) {
                $pushString .= ',';
            }
        }

        return $pushString;
    }

    /**
     * This exactly the same as the parent method but its needed to use the overwritten registerFile method
     *
     * {@inheritdoc}
     */
    public function registerCssFile($url, $options = [], $key = null): string
    {
        return $this->registerFile('css', $url, $options, $key);
    }

    /**
     * This exactly the same as the parent method but its needed to use the overwritten registerFile method
     *
     * {@inheritdoc}
     */
    public function registerJsFile($url, $options = [], $key = null): string
    {
        return $this->registerFile('js', $url, $options, $key);
    }

    /**
     * @var array
     */
    public $jsWithOptions = [];

    /**
     * @param string      $js
     * @param int         $position
     * @param string|null $key
     * @param array|null  $scriptOptions
     */
    public function registerJs($js, $position = self::POS_READY, $key = null, ?array $scriptOptions = null): void
    {
        $key = $key ?: md5($js);

        if ($this->getRenderMode() === 'renderAjax') {
            $scriptOptions['class'] = 'check-ajax-dupe';
            $js = '(function() {' . $js . '})';
        }

        if ($scriptOptions === null) {
            parent::registerJs($js, $position, $key);
        } else {
            $this->jsWithOptions[$position][$key] = [
                'js' => $js,
                'options' => $scriptOptions
            ];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function clear(): void
    {
        $this->jsWithOptions = [];
        parent::clear();
    }

    /**
     * {@inheritdoc}
     */
    protected function renderHeadHtml(): string
    {
        $lines = [];

        if (!empty($this->jsWithOptions[self::POS_HEAD])) {
            $lines = $this->getScriptWithOptions($this->jsWithOptions[self::POS_HEAD]);
        }

        $default = parent::renderHeadHtml();
        return $default.(!empty($default)?"\n":'').(empty($lines) ? '' : implode("\n", $lines));
    }

    /**
     * {@inheritdoc}
     */
    protected function renderBodyBeginHtml()
    {
        $lines = [];

        if (!empty($this->jsWithOptions[self::POS_BEGIN])) {
            $lines = $this->getScriptWithOptions($this->jsWithOptions[self::POS_BEGIN]);
        }

        $default = parent::renderBodyBeginHtml();
        return $default.(!empty($default)?"\n":'').(empty($lines) ? '' : implode("\n", $lines));
    }

    /**
     * @param array  $scripts
     * @param string $prepend
     * @param string $append
     *
     * @return array
     */
    private function getScriptWithOptions(array $scripts, string $prepend = '', string $append = ''): array
    {
        $lines = [];

        foreach ($scripts as $jsWithOptions) {
            $lines[] = Html::script($prepend.$jsWithOptions['js'].$append, $jsWithOptions['options']);
        }

        return $lines;
    }

    /**
     * @param bool $ajaxMode
     *
     * @return string
     */
    protected function renderBodyEndHtml($ajaxMode): string
    {
        $lines = [];

        if ($ajaxMode) {
            if (!empty($this->jsWithOptions[self::POS_END])) {
                $lines = array_merge($lines, $this->getScriptWithOptions($this->jsWithOptions[self::POS_END]));
            }
            if (!empty($this->jsWithOptions[self::POS_READY])) {
                $lines = array_merge($lines, $this->getScriptWithOptions($this->jsWithOptions[self::POS_READY]));
            }
            if (!empty($this->jsWithOptions[self::POS_LOAD])) {
                $lines = array_merge($lines, $this->getScriptWithOptions($this->jsWithOptions[self::POS_LOAD]));
            }
        } else {
            if (!empty($this->jsWithOptions[self::POS_END])) {
                $lines = array_merge($lines, $this->getScriptWithOptions($this->jsWithOptions[self::POS_END]));
            }
            if (!empty($this->jsWithOptions[self::POS_READY])) {
                $lines = array_merge($lines, $this->getScriptWithOptions($this->jsWithOptions[self::POS_READY], 'jQuery(function ($) {'."\n", "\n".'});'));
            }
            if (!empty($this->jsWithOptions[self::POS_LOAD])) {
                $lines = array_merge($lines, $this->getScriptWithOptions($this->jsWithOptions[self::POS_LOAD], 'jQuery(window).on(\'load\', function () {'."\n", "\n".'});'));
            }
        }

        $default = parent::renderBodyEndHtml($ajaxMode);
        return $default.(!empty($default)?"\n":'').(empty($lines) ? '' : implode("\n", $lines));
    }

    /**
     * Marks the position of an HTML head section.
     */
    public function head()
    {
        echo self::PH_HEAD;
    }

    /**
     * Marks the beginning of an HTML body section.
     */
    public function beginBody()
    {
        echo self::PH_BODY_BEGIN;
        $this->trigger(self::EVENT_BEGIN_BODY);
    }

    /**
     * Marks the ending of an HTML body section.
     */
    public function endBody()
    {
        $this->trigger(self::EVENT_END_BODY);
        echo self::PH_BODY_END;

        foreach (array_keys($this->assetBundles) as $bundle) {
            $this->registerAssetFiles($bundle);
        }
    }

    /**
     * @param bool $ajaxMode
     *
     * @throws InvalidConfigException
     */
    public function endPage($ajaxMode = false): void
    {
        $this->trigger(self::EVENT_END_PAGE);

        $content = ob_get_clean();

        if ($this->getRenderMode() !== 'renderAjax') {
            $this->registerJs('window.faf = window.faf || {}; window.faf.config = Object.assign(window.faf.config || {}, ' . Json::encode(Yii::$app->fafcms->getJsConfig()) . ');', ViewComponent::POS_HEAD);
        }

        $this->generateMergedAndCompressedFiles('css');
        $this->generateMergedAndCompressedFiles('js');
        $this->pushAssets();

        echo strtr($content, [
            self::PH_HEAD => $this->renderHeadHtml(),
            self::PH_BODY_BEGIN => $this->renderBodyBeginHtml(),
            self::PH_BODY_END => $this->renderBodyEndHtml($ajaxMode),
        ]);

        $this->clear();
    }

    /**
     * @param string $url
     * @param string $type
     */
    public function pushAsset(string $url, string $type): void
    {
        $time = @filemtime(Yii::getAlias('@webroot/' . ltrim($url, '/'), false));
        $key = md5($url . $time . $type);

        $this->pushAssetsCache[$key] = [
            'url' => $url,
            'time' => $time,
            'type' => $type
        ];
    }

    private function pushAssets(): void
    {
        $pushAssets = $this->pushAssetsCache;
        $pushAssetsKeys = array_keys($pushAssets);
        $pushedAssets = Yii::$app->getRequest()->getCookies()->getValue('pushedAssets', []);
        $newAssets = array_diff($pushAssetsKeys, $pushedAssets);

        if (count($newAssets) > 0) {
            Yii::$app->getResponse()->getHeaders()->add('Link', $this->buildPushString($pushAssets));
            Yii::$app->getResponse()->getCookies()->add(new Cookie([
                'name' => 'pushedAssets',
                'value' => $pushAssetsKeys,
            ]));
        }
    }

    public array $availablePolyfills = [
        [
            'file' => '@npm/intersection-observer/intersection-observer.js',
            'keywords' => ['IntersectionObserver']
        ],
        [
            //TODO load only files depending on browser version
            //https://github.com/zloirock/core-js/tree/master/packages/core-js-compat and https://github.com/ungap
            'file' => '@npm/core-js-bundle/minified.js',
            'minify' => false,
            'keywords' => ['URLSearchParams', 'Symbol', 'Promise']
        ],
    ];

    /**
     * @param string $url
     *
     * @throws InvalidConfigException
     */
    protected function checkForPolyfills(string $url): void
    {
        $fileName = Yii::getAlias('@webroot/' . ltrim(substr($url, 0, strpos($url, '?') ?: strlen($url)), '/'));
        $timestamp = @filemtime($fileName);

        $neededPolyfills = Yii::$app->cache->getOrSet(['check-for-polyfills', 'file' => $fileName, 'timestamp' => $timestamp], function () use ($fileName, $timestamp) {
            try {
                $file = file_get_contents($fileName);
                $neededPolyfills = [];

                foreach ($this->availablePolyfills as $availablePolyfill) {
                    if (StringHelper::contains($file, $availablePolyfill['keywords'])) {
                        unset($availablePolyfill['keywords']);
                        $neededPolyfills[] = $availablePolyfill;
                    }
                }

                return $neededPolyfills;
            } catch (\Exception $e) {}

            return [];
        },0);

        foreach ($neededPolyfills as $neededPolyfill) {
            if (($neededPolyfill['type'] ?? 'file') === 'file') {
                $neededPolyfillAsset = $this->assetManager->publish($neededPolyfill['file']);

                if (isset($neededPolyfillAsset[1])) {
                    $neededPolyfillAssetFile = $neededPolyfillAsset[1];
                    $this->registerJsFile($neededPolyfillAssetFile, ['position' => self::POS_HEAD, 'checkForPolyfills' => false, 'minify' => $neededPolyfill['minify'] ?? true]);
                }
            } else {
                $this->registerWebComponent($neededPolyfill['file'], $neededPolyfill['minify'] ?? true);
            }
        }
    }

    /**
     * @param string     $url
     * @param array|null $options
     */
    public function registerWebComponent(string $url, ?array $options = []): void
    {
        $options['webComponent'] = true;
        $script = $options['script'] ?? '';
        $initScript = $options['initScript'] ?? '';
        unset($options['script']);
        unset($options['initScript']);

        $moduleAsset = $this->registerJsFile($url, $options);

        if ($script !== '') {
            $script = '.then((Module) => {' . $script . '})';
        }

        $this->registerJs($initScript . 'WebComponents.waitFor(function(){return import(\'' . $moduleAsset . '\')' . $script . '})', self::POS_HEAD, 'module-' . $moduleAsset, ['type' => 'module']);
    }

    /**
     * This method does nearly the same as the original registerFile method except that it just passes an array to jsFiles or cssFiles instead of the rendered html
     *
     * {@inheritdoc}
     * @throws InvalidConfigException
     */
    private function registerFile(string $type, string $url, ?array $options = [], string $key = null): string
    {
        $url = Yii::getAlias($url);
        $key = $key ?: $url;
        $depends = ArrayHelper::remove($options, 'depends', []);
        $position = ArrayHelper::remove($options, 'position', self::POS_END);

        try {
            $asssetManagerAppendTimestamp = $this->getAssetManager()->appendTimestamp;
        } catch (InvalidConfigException $e) {
            $depends = null; // the AssetManager is not available
            $asssetManagerAppendTimestamp = false;
        }

        $appendTimestamp = ArrayHelper::remove($options, 'appendTimestamp', $asssetManagerAppendTimestamp);

        if (empty($depends)) {
            // register directly without AssetManager
            $filePath = Yii::getAlias('@webroot/' . ltrim($url, '/'), false);
            $isRelative = Url::isRelative($url);

            if ($isRelative && $appendTimestamp && ($timestamp = @filemtime($filePath)) > 0) {
                $url = $timestamp ? "$url?v=$timestamp" : $url;
            }

            if ($type === 'js') {
                $checkForPolyfills = false;

                if ($isRelative) {
                    $checkForPolyfills = $options['checkForPolyfills'] ?? true;
                }

                unset($options['checkForPolyfills']);

                if ($checkForPolyfills) {
                    $this->checkForPolyfills($url);
                }

                if (($options['webComponent'] ?? false) === false) {
                    $this->jsFiles[$position][$key] = [
                        'url' => $url,
                        'options' => $options
                    ];
                }
            } else {
                $this->cssFiles[$key] = [
                    'url' => $url,
                    'options' => $options
                ];
            }
        } else {
            $this->getAssetManager()->bundles[$key] = InjectorComponent::createObject([
                'class' => AssetBundle::class,
                'baseUrl' => '',
                'basePath' => '@webroot',
                (string)$type => [!Url::isRelative($url) ? $url : ltrim($url, '/')],
                "{$type}Options" => $options,
                'depends' => (array)$depends,
            ]);

            $this->registerAssetBundle($key);
        }

        return $url;
    }

    public function getMinFile(string $type, string $url)
    {
        /*header("Content-Type: $type;charset=utf-8");
        header("Cache-Control: max-age=31536000");
        echo $content;*/

        //TODO add option to download external files and minify it
        if (Url::isRelative($url) && strrpos($url,  '.min.' . $type) === false) {
            $filePath = Yii::getAlias('@webroot');
            $minUrl = str_replace('.' . $type,  '.min.' . $type, $url);
            $fileName = $filePath . '/' . ltrim(substr($url, 0, strpos($url, '?') ?: strlen($url)), '/');
            $minFileName = $filePath . '/' . ltrim(substr($minUrl, 0, strpos($minUrl, '?') ?: strlen($minUrl)), '/');

            if (!file_exists($minFileName)) {
                try {
                    (new Minifier([
                        'src' => [
                            new MinifierSrcFile([
                                'file' => $fileName
                            ])
                        ],
                        'dist' => $minFileName
                    ]))->run();
                } catch (\Exception $e) {
                    $minUrl = $url;
                }
            }

            return $minUrl;
        }

        return $url;
    }

    /**
     * @param string     $type
     * @param string     $url
     * @param array|null $options
     *
     * @return string
     * @throws InvalidConfigException
     */
    private function handleFafcmsFile(string $type, string $url, ?array $options = []): string
    {
        if ($options['minify'] ?? true) {
            $url = $this->getMinFile($type, $url);
        }

        unset($options['minify']);

        if (isset($options['fafcmsPush'])) {
            unset($options['fafcmsPush']);
            $this->pushAsset($url, $type === 'css' ? 'stylesheet' : 'script');
        }

        $fileHtml = '';

        if (isset($options['fafcmsAsync']) && $options['fafcmsAsync']) {
            unset($options['fafcmsAsync']);

            if ($type === 'css') {
                $optionsAsync = $options;
                $optionsAsync['rel'] = 'stylesheet';
                $optionsAsync['media'] = 'print';
                $optionsAsync['onload'] = 'this.media=\'all\';this.onload=null;';

                $options['noscript'] = true;

                $preloadScript = $this->getAssetManager()->publish('@npm/fg-loadcss/dist/cssrelpreload.min.js');
                $this->registerJsFile($preloadScript[1], ['fafcmsPush' => true]);

                $fileHtml .= Html::{$type . 'File'}($url, $optionsAsync);
            } elseif ($type === 'js') {
                $options['async'] = true;
                $options['defer'] = true;
            }
        }

        if ($type === 'js' && $this->getRenderMode() === 'renderAjax') {
            $options['data-src'] = $url;
            $options['class'] = 'check-ajax-dupe';

            return $fileHtml . Html::{$type . 'File'}(null, $options);
        }

        return $fileHtml . Html::{$type . 'File'}($url, $options);
    }

    /**
     * @param string $type
     *
     * @throws InvalidConfigException
     */
    private function generateMergedAndCompressedFiles(string $type): void
    {
        $filePositionGroups = $this->{$type . 'Files'};

        if ($type === 'css') {
            $filePositionGroups = [
                $filePositionGroups
            ];
        }

        foreach ($filePositionGroups as $filePositionGroupIndex => $filePositionGroup) {
            foreach ($filePositionGroup as $fileIndex => $file) {
                $filePositionGroups[$filePositionGroupIndex][$fileIndex] = $this->handleFafcmsFile($type, $file['url'], $file['options']);
            }
        }

        if ($type === 'css') {
            $this->{$type . 'Files'} = $filePositionGroups[0];
        } else {
            $this->{$type . 'Files'} = $filePositionGroups;
        }
    }

    //region cms render helpers
    /**
     * @return string
     */
    public function getRenderMode(): string
    {
        $renderMode = $this->renderedSiteMode;

        if ($renderMode === null) {
            if (YII_CONSOLE) {
                $renderMode = 'default';
            } else {
                $renderMode = Yii::$app->request->get('render-mode', (Yii::$app->request->isAjax ? 'ajax' : 'default'));
            }
        }

        switch ($renderMode) {
            case 'ajax':
                return 'renderAjax';
            case 'partial':
                return 'renderPartial';
            case 'raw':
                return 'raw';
        }

        return 'render';
    }

    /**
     * @var null|string|Response
     */
    public $renderedSiteContent = null;

    /**
     * @var null|string
     */
    public ?string $renderedSiteMode = null;

    /**
     * @param Controller $controller
     * @param string     $renderMode
     * @param string     $content
     *
     * @return mixed|string|null
     * @throws InvalidConfigException
     */
    public function renderSiteContent(Controller $controller, string $renderMode, string $content)
    {
        if (Yii::$app->view->renderedSiteContent !== null) {
            $content = Yii::$app->view->renderedSiteContent;
        }

        if ($renderMode === 'raw') {
            return $content;
        }

        if ($renderMode === 'renderAjax') {
            Yii::$app->fafcms->registerAssets($controller->view);
        }

        return $controller->$renderMode('//common/content', ['content' => $content]);
    }
    //endregion cms render helpers

    //region backend render actions
    /**
     * @param string $content
     *
     * @return array|string|null
     * @throws \Exception
     */
    public function renderActionContent(string $content)
    {
        $renderMode = $this->getRenderMode();
        $renderTitle = Yii::$app->request->get('render-title', 'true') === 'true';
        $renderBreadcrumb = Yii::$app->request->get('render-breadcrumb', 'true') === 'true';

        if ($renderMode === 'renderAjax' && ($renderTitle || $renderBreadcrumb)) {
            $headerContent = '';

            if ($renderTitle) {
                $headerContent .= Html::tag('h4', end(Yii::$app->view->title));
            }

            if ($renderBreadcrumb) {
                $headerContent .= Html::tag(
                    'div',
                    Breadcrumbs::widget(['links' => Yii::$app->view->params['breadcrumbs']??[]]),
                    ['class' => 'ui vertical inverted segment breadcrumb-header']
                );
            }

            $content .= Html::tag(
                'header',
                $headerContent,
                ['class' => 'modal-header']
            );
        }

        return $this->renderSiteContent(Yii::$app->controller, $renderMode, $content);
    }

    /**
     * @var array|\array[][]
     */
    public array $contentViews = [
        'fafcms\\Plugin' => [
            'default' => [
                'label' => ['fafcms-core', 'Default plugin details'],
                'file' => '@fafcms/fafcms/views/content/default-plugin.php'
            ]
        ],
        'fafcms\\HomeDashboard' => [
            'default' => [
                'label' => ['fafcms-core', 'Default home dashboard'],
                'file' => '@fafcms/fafcms/views/content/default-home-dashboard'
            ]
        ],
        'fafcms\\ProjectDashboard' => [
            'default' => [
                'label' => ['fafcms-core', 'Default project dashboard'],
                'file' => '@fafcms/fafcms/views/content/default-project-dashboard'
            ]
        ],
    ];

    /**
     * @var array
     */
    private array $viewItems = [];

    /**
     * @param string      $fullyQualifiedTypeName
     * @param string|null $contentViewId
     *
     * @return array
     */
    public function getViewItems(string $fullyQualifiedTypeName, ?string $contentViewId = null): array
    {
        $nameParts = explode('\\', $fullyQualifiedTypeName);
        $viewItems = [];

        for ($i = count($nameParts); $i > 0; $i--) {
            $currentParts = $nameParts;
            $currentName = implode('\\', array_splice($currentParts, 0, $i));

            $viewItems[] = $this->viewItems[$currentName][$contentViewId] ?? [];
            $viewItems[] = $this->viewItems[$currentName . '\\'][$contentViewId] ?? [];
        }

        return array_merge(...$viewItems);
    }

    /**
     * @param array       $items
     * @param string      $fullyQualifiedTypeName
     * @param string|null $contentViewId
     * @param string|null $interface
     */
    public function addViewItems(array $items, string $fullyQualifiedTypeName, ?string $contentViewId = null, ?string $interface = null): void
    {
        if ($contentViewId === null) {
            $contentViewId = 'default';
        }

        $this->viewItems[$fullyQualifiedTypeName][$contentViewId][$interface ?? 'all'][] = $items;
    }

    /**
     * @param string      $fullyQualifiedTypeName
     * @param string|null $contentViewId
     * @param array|null  $settings
     *
     * @return string
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function renderView(string $fullyQualifiedTypeName, ?string $contentViewId = null, ?array $settings = []): string
    {
        $contentView = null;

        if ($contentViewId === null) {
            $contentViewId = 'default';
        }

        if (!is_numeric($contentViewId)) {
            $contentViews = [];

            if (isset($this->contentViews[$fullyQualifiedTypeName])) {
                $contentViews = $this->contentViews[$fullyQualifiedTypeName];
            } elseif (strpos($fullyQualifiedTypeName, 'model\\edit\\') === 0 || strpos($fullyQualifiedTypeName, 'model\\index\\') === 0) {
                $modelClass = $settings['modelClass'] ?? null;

                if ($modelClass === null) {
                    throw new InvalidCallException('Model class is missing');
                }

                $modelClass = InjectorComponent::getClass($modelClass);

                if (!isset(class_implements($modelClass)[FieldConfigInterface::class])) {
                    throw new InvalidConfigException('Class "' . $modelClass . '" must implement "' . FieldConfigInterface::class . '".');
                }

                if (strpos($fullyQualifiedTypeName, 'model\\edit\\') === 0) {
                    if (!isset(class_implements($modelClass)[EditViewInterface::class])) {
                        throw new InvalidConfigException('Class "' . $modelClass . '" must implement "' . EditViewInterface::class . '".');
                    }

                    $contentViews = $modelClass::editView();
                } elseif (strpos($fullyQualifiedTypeName, 'model\\index\\') === 0) {
                    if (!isset(class_implements($modelClass)[IndexViewInterface::class])) {
                        throw new InvalidConfigException('Class "' . $modelClass . '" must implement "' . IndexViewInterface::class . '".');
                    }

                    $contentViews = $modelClass::indexView();
                }
            }

            if (isset($contentViews)) {
                $defaultContentView = null;
                $contentView = $contentViews[$contentViewId] ?? null;
            }
        }

        if ($contentView === null) {
            throw new NotFoundHttpException('Requested view not found');
        }

        $class = \fafcms\fafcms\items\BaseView::class;

        if (strpos($fullyQualifiedTypeName, 'model\\index\\') === 0) {
            $class = \fafcms\fafcms\items\IndexView::class;
        } elseif (strpos($fullyQualifiedTypeName, 'model\\edit\\') === 0) {
            $class = \fafcms\fafcms\items\EditView::class;
        }

        $checkAccess = ArrayHelper::remove($settings, 'checkAccess', true);

        return $this->renderContentItems([[
            'class'                  => $class,
            'settings'               => $settings,
            'fullyQualifiedTypeName' => $fullyQualifiedTypeName,
            'contentViewId'          => $contentViewId,
            'checkAccess'            => $checkAccess
        ]]);
    }

    /**
     * @param array $items
     * @param bool $forceArray
     * @return array|string
     * @throws InvalidConfigException
     */
    public function renderContentItems(array $items, bool $forceArray = false)
    {
        $renderedContent = [];
        $hasArrayChild = false;

        foreach ($items as $item) {
            try {
                $renderedItem = $this->renderContentItem($item);
            } catch (\TypeError $e) {
                echo $e->getMessage();
                echo '<pre>' . VarDumper::export($item) . '</pre>';
                Yii::$app->end();
            }

            $renderedContent[] = $renderedItem;

            if (is_array($renderedItem)) {
                $hasArrayChild = true;
            }
        }

        if (!$forceArray && !$hasArrayChild) {
            return implode('', $renderedContent);
        }

        return $renderedContent;
    }

    /**
     * @param array $item
     * @param ContentItem|null $contentItem
     * @return array|string
     * @throws InvalidConfigException
     */
    public function renderContentItem(array $item, ?ContentItem &$contentItem = null)
    {
        /**
         * @var $contentItem ContentItem
         */
        $contentItem = InjectorComponent::createObject($item);

        if ($contentItem->preRenderContent && isset($contentItem->contents)) {
            if ($contentItem->isContainer() || $contentItem->isGrid()) {
                $contentItem->renderedContent = $this->renderContentItems($contentItem->contents);
            } else {
                $contentItem->renderedContent = $contentItem->contents;
            }
        }

        return $contentItem->run();
    }

    public const NAVIGATION_HEADER_LEFT = 1;
    public const NAVIGATION_HEADER_RIGHT = 2;
    public const NAVIGATION_SIDEBAR_LEFT = 3;
    public const NAVIGATION_SIDEBAR_RIGHT = 4;
    public const NAVIGATION_ACTION_BAR = 5;
    public const NAVIGATION_FOOTER_LEFT = 6;
    public const NAVIGATION_FOOTER_RIGHT = 7;

    /**
     * @var array<int, array<string, array>>
     */
    protected array $navigationItems = [];

    /**
     * @param int   $position
     * @param array<string, array> $items
     *
     * @return RenderComponent
     */
    public function addNavigationItems(int $position, array $items): self
    {
        $this->navigationItems[$position] = ArrayHelper::merge($this->navigationItems[$position] ?? [], $items);
        return $this;
    }

    /**
     * @param int $position
     *
     * @return array<string, array>
     */
    public function getNavigationItems(int $position): array
    {
        return $this->navigationItems[$position] ?? [];
    }

    /**
     * @param int   $position
     * @param array<string, array> $navigationItems
     *
     * @return RenderComponent
     */
    public function setNavigationItems(int $position, array $navigationItems): self
    {
        $this->navigationItems[$position] = $navigationItems;
        return $this;
    }

    /**
     * @param string                             $identifier
     * @param array<string,array<string, mixed>> $items
     * @param array<string,mixed>                $navigationOptions
     * @param array<string,mixed>                $itemOptions
     * @param string                             $defaultChildItemType
     * @param bool                               $addChildItemIcon
     *
     * @return string
     * @throws InvalidConfigException
     */
    public function renderNavigationItems(
        string $identifier,
        array $items,
        array $navigationOptions = [],
        array $itemOptions = [],
        string $defaultChildItemType = 'accordion',
        bool $addChildItemIcon = true
    ): string {
        if (!isset($navigationOptions['tag'])) {
            $navigationOptions['tag'] = 'div';
        }

        if (!isset($navigationOptions['class'])) {
            $navigationOptions['class'] = [];
        }

        if (!is_array($navigationOptions['class'])) {
            $navigationOptions['class'] = [$navigationOptions['class']];
        }

        $navigationOptions['class'] = array_merge(['menu' => 'menu'], $navigationOptions['class']);

        $navigationContent = '';
        $itemKeys = array_flip(array_keys($items));

        uksort($items, static function($posA, $posB) use ($items, $itemKeys) {
            $positionA = $items[$posA]['position'] ?? 'default';
            $positionB = $items[$posB]['position'] ?? 'default';

            if ($positionB === 'first' || $positionA === 'last') {
                return 1;
            }

            $posAItem = $itemKeys[$posA];

            if (isset($items[$posA]['after'], $itemKeys[$items[$posA]['after']])) {
                $posAItem = $itemKeys[$items[$posA]['after']];
            }

            $posBItem = $itemKeys[$posB];

            if (isset($items[$posB]['after'], $itemKeys[$items[$posB]['after']])) {
                $posBItem = $itemKeys[$items[$posB]['after']];
            }

            return $posAItem > $posBItem;
        });

        foreach ($items as $index => $item) {
            if (!$this->canAccessNavItem($item)) {
                continue;
            }

            if (!isset($item['type'])) {
                $item['type'] = 'item';

                if (isset($item['html'])) {
                    $item['type'] = 'html';
                }

                if (isset($item['items'])) {
                    $item['type'] = $defaultChildItemType;
                }
            }

            $identifier .= $index;
            $navigationContent .= $this->renderNavigationItem($identifier, $item, $itemOptions, $defaultChildItemType, $addChildItemIcon);
        }

        $navigationTag = $navigationOptions['tag'] ?? null;
        unset($navigationOptions['tag']);

        $renderEmpty = $navigationOptions['renderEmpty'] ?? true;
        unset($navigationOptions['renderEmpty']);

        if (!$renderEmpty && $navigationContent === '') {
            return '';
        }

        return Html::tag($navigationTag, $navigationContent, $navigationOptions);
    }

    /**
     * @param array<string, mixed> $item
     *
     * @return bool
     * @throws InvalidConfigException
     */
    protected function canAccessNavItem(array $item): bool
    {
        if (Yii::$app->fafcms->getIsInstalled()) {
            $rules = $item['rules'] ?? [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ];

            if ($rules !== false) {
                if (!Yii::$app->getUser()->getIsGuest() && !Yii::$app->user->can('accessBackend')) {
                    return false;
                }

                foreach ($rules as $rule) {
                    if (is_array($rule)) {
                        $rule['class'] = AccessRule::class;
                        $rule = InjectorComponent::createObject($rule);
                    }

                    $rule->actions = null;
                    $rule->controllers = null;

                    if (!Yii::$app->fafcms->checkRule($rule, Yii::$app->user, Yii::$app->request)) {
                        return false;
                    }
                }
            }
        }

        if (!isset($item['visible'])) {
            return true;
        }

        if ($item['visible'] instanceof Closure) {
            return $item['visible']($item);
        }

        return $item['visible'];
    }

    /**
     * @param string $identifier
     * @param array  $item
     * @param array  $itemOptions
     * @param string $defaultChildItemType
     * @param bool   $addChildItemIcon
     *
     * @return string
     * @throws InvalidConfigException
     */
    protected function renderNavigationItem(string $identifier, array $item, array $itemOptions = [], string $defaultChildItemType = 'accordion', bool $addChildItemIcon = true): string
    {
        $itemOptions = ArrayHelper::merge($itemOptions, $item['options'] ?? []);

        if (!isset($itemOptions['class']['item'])) {
            Html::addCssClass($itemOptions, ['item' => 'item']);
        }

        if (!isset($item['label'])) {
            Html::addCssClass($itemOptions, 'icon');
        }

        if ($item['type'] === 'group') {
            $titleOptions = $itemOptions;
            Html::addCssClass($titleOptions, 'header');

            $childMenu = $this->renderNavigationItems($identifier . '-child', $item['items'], ['tag' => false], [], $defaultChildItemType, $addChildItemIcon);

            return $this->getNavItem(
                $item,
                $this->getNavItemLabel($item),
                null,
                $titleOptions
            ) . $childMenu;
        }

        if ($item['type'] === 'html') {
            if ($item['html'] instanceof Closure) {
                return $item['html']();
            }

            return $item['html'];
        }

        if ($item['type'] === 'divider') {
            return '<div class="divider"></div>';
        }

        if ($item['type'] === 'dropdown') {
            $childMenu = $this->renderNavigationItems($identifier . '-child', $item['items'], [], [], $defaultChildItemType, $addChildItemIcon);

            Html::addCssClass($itemOptions, 'ui dropdown');

            return $this->getNavItem(
                $item,
                $this->getNavItemLabel($item) . ($addChildItemIcon ? '<i class="dropdown icon"></i>' : '') . $childMenu,
                null,
                $itemOptions
            );
        }

        if ($item['type'] === 'accordion') {
            $titleOptions = ['class' => 'ui item title'];
            $contentOptions = ['tag' => 'div', 'class' => ['ui', 'content', 'menu' => false]];

            $hashedIdentifier = 'nav-item-' . hash('sha512', $identifier);

            $collapsed = Yii::$app->getModule(FafcmsBootstrap::$id)->getUserSettingValue('collapsibleCollapsed', null, null, $hashedIdentifier) === 0;

            if ($collapsed || ($item['active'] ?? false)) {
                Html::addCssClass($titleOptions, 'active');
                Html::addCssClass($contentOptions, 'active');
            }

            $childMenu = $this->renderNavigationItems($identifier . '-child', $item['items'], $contentOptions, [], $defaultChildItemType, $addChildItemIcon);

            Html::removeCssClass($itemOptions, 'item');
            Html::addCssClass($itemOptions, 'ui accordion');

            return $this->getNavItem(
                $item,
                Html::tag('a', ($addChildItemIcon ? '<i class="dropdown icon"></i>' : '') . $this->getNavItemLabel($item), $titleOptions) . $childMenu,
                null,
                $itemOptions
            );
        }

        return $this->getNavItem($item, $this->getNavItemLabel($item), $item['url'] ?? null, $itemOptions);
    }

    /**
     * @param int                 $position
     * @param array<string,mixed> $navigationOptions
     * @param array<string,mixed> $itemOptions
     * @param string              $defaultChildItemType
     * @param bool                $addChildItemIcon
     *
     * @return string
     * @throws InvalidConfigException
     */
    public function renderNavigation(
        int $position,
        array $navigationOptions = [],
        array $itemOptions = [],
        string $defaultChildItemType = 'accordion',
        bool $addChildItemIcon = true
    ): string {
        $items = $this->navigationItems[$position] ?? [];
        return $this->renderNavigationItems((string)$position, $items, $navigationOptions, $itemOptions, $defaultChildItemType, $addChildItemIcon);
    }

    /**
     * @param array<string, mixed> $item
     * @return string
     */
    protected function getNavItemLabel(array &$item): string
    {
        $label = $item['label'] ?? '';

        if ($label instanceof Closure) {
            $label = $label();
        }

        $label = Yii::$app->fafcms->getTextOrTranslation($label);
        $iconPosition = ($item['iconPosition'] ?? 'left');
        $iconHtml = '';

        if (($item['icon'] ?? null) !== null) {
            $iconHtml = Html::tag('i', '', [
                'class' => 'icon mdi mdi-' . $item['icon']
            ]);
        }

        if (($item['icons'] ?? null) !== null) {
            $iconHtml .= '<i class="icons">';

            foreach ($item['icons'] as $icon) {
                $iconHtml .= Html::tag('i', '', [
                    'class' => 'icon ' . $icon
                ]);
            }

            $iconHtml .= '</i>';
        }

        if ($iconPosition === 'right') {
            return $label . $iconHtml;
        }

        return $iconHtml . $label;
    }

    /**
     * @param array             $item
     * @param string            $content
     * @param string|array|null $url
     * @param array             $itemOptions
     *
     * @return string
     */
    protected function getNavItem(array $item, string $content, $url = null, array $itemOptions = []): string
    {
        if ($item['type'] === 'submit') {
            return Html::submitButton($content, $itemOptions);
        }

        if ($url === null) {
            return Html::tag('div', $content, $itemOptions);
        }

        return Html::a($content, $url, $itemOptions);
    }

    public function registerBackendAssetComponents(): void
    {
        FafcmsBackendAppAsset::register(Yii::$app->view);
    }
    //endregion backend render actions
}
