<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\components;

use fafcms\fafcms\models\Translation;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\Query;
use yii\helpers\FileHelper;
use yii\i18n\DbMessageSource;
use yii\i18n\MessageSource;
use yii\log\Logger;

/**
 * Class I18NComponent
 *
 * @package fafcms\fafcms\components
 */
class I18NComponent extends \yii\i18n\I18N
{
    public function init()
    {
        Yii::$classMap['yii\i18n\MessageSource'] = FAFCMS_CORE_PATH . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'MessageSource.php';

        $date = Yii::$app->translationCache->getOrSet('js-translation', function () {
            return time();
        }, 0);

        Yii::$app->view->registerJsFile('/assets/translations/' . Yii::$app->language . '-' . $date . '.js');

        parent::init();
    }

    public function translate($category, $message, $params, $language): string
    {
        $translation = Yii::$app->fafcms->getIsInstalled() ? Yii::$app->translationCache->getOrSet(
            $category . '-' . $language . '-' . $message,
            static function () use ($category, $message, $language) {
                $translation = Translation::find()->select('translation')->where(
                    [
                        'language' => $language,
                        'category' => $category,
                        'message'  => $message
                    ]
                )->asArray()->one();

                return $translation['translation'] ?? null;
            },
            0
        ) : null;

        if ($translation === null) {
            try {
                $translation = parent::translate($category, $message, $params, $language);
            } catch (InvalidConfigException $e) {
                $translation = $message;
            }
        }

        return $this->format($translation, $params, $language);
    }

    /**
     * @return array
     */
    public function getAllMessageCategories(): array
    {
        $categories = [];

        foreach (Yii::$app->i18n->translations as $category => $messageSource) {
            if (strpos($category, '*') === false) {
                $categories[$category] = $messageSource;
                continue;
            }

            if ($messageSource instanceof DbMessageSource) {
                $categoryRows = (new Query())
                    ->select(['category'])
                    ->from($messageSource->messageTable)
                    ->groupBy('category')
                    ->where([
                        'like', 'category', str_replace('*', '%', $category), false
                    ])
                    ->column();

                foreach ($categoryRows as $categoryRow) {
                    $categories[$categoryRow] = $messageSource;
                }
                continue;
            }

            $categoryFiles = glob(Yii::getAlias($messageSource->basePath) . DIRECTORY_SEPARATOR . '*' . DIRECTORY_SEPARATOR . '*' . $category);

            foreach ($categoryFiles as $categoryFile) {
                $categories[pathinfo($categoryFile, PATHINFO_FILENAME)] = $messageSource;
            }
        }

        return $categories;
    }

    /**
     * @param string        $category
     * @param MessageSource $messageSource
     *
     * @return array
     */
    public function getAllMessageLanguages(string $category, MessageSource $messageSource): array
    {
        if ($messageSource instanceof DbMessageSource) {
            return (new Query())
                ->select('t1.language')
                ->from(['t1' => $messageSource->messageTable])
                ->innerJoin(['t2' => $messageSource->sourceMessageTable], 't1.id = t2.id')
                ->where([
                    't2.category' => $category,
                ])
                ->groupBy('t1.language')
                ->column();
        }

        $messageLanguages = glob(Yii::getAlias($messageSource->basePath) . DIRECTORY_SEPARATOR . '*', GLOB_ONLYDIR);
        $languages = [];

        foreach ($messageLanguages as $messageLanguage) {
            $languages[] = basename($messageLanguage);
        }

        return $languages;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function searchTranslationMessages(): array
    {
        return Yii::$app->translationCache->getOrSet('translation_messages', function () {
  /*          $files = FileHelper::findFiles(FAFCMS_BASE_PATH, [
                'only' => ['*.php'],
            ]);
*/
            $messageCategories = [];
/*
 * TODO IMPROVE
            foreach ($files as $file) {
                $messageCategories[] = $this->searchTranslationMessagesInFile($file);
            }

            $messageCategories = array_merge_recursive(...$messageCategories);*/

            $translationMessages = [];

            $categories = array_keys($this->getAllMessageCategories());

            foreach ($categories as $category) {
                $translationMessages[] = $this->getTranslationMessagesByCategory($messageCategories, $category);
            }

            return array_merge_recursive(...$translationMessages);
        }, 0);
    }

    /**
     * @param array  $messageCategories
     * @param string $category
     *
     * @return array
     * @throws \Exception
     */
    protected function getTranslationMessagesByCategory(array $messageCategories, string $category): array
    {
        $languages = Yii::$app->fafcms->getAvailableLanguages();
        $translationMessages = [];
        $messages = [];

        if (isset($messageCategories[$category])) {
            $messages = array_unique($messageCategories[$category]);
        }

        $translationMessages[$category]['id'] = $category;
        $translationMessages[$category]['category'] = $category;
        $translationMessages[$category]['available'] = array_unique($messages);
        $translationMessages[$category]['translations'] = [];
        $translationMessages[$category]['messageCount'] = count($translationMessages[$category]['available']);
        $translationMessages[$category]['totalMessageCount'] = 0;
        $translationMessages[$category]['translatedCount'] = 0;
        $translationMessages[$category]['translatedPercent'] = 0;
        $translationMessages[$category]['languages'] = [];

        try {
            $messageSource = $this->getMessageSource($category);

            foreach ($languages as $language) {
                $messageLanguages = $this->getAllMessageLanguages($category, $messageSource);

                if (!in_array($language['ietfcode'], $messageLanguages, true)) {
                    continue;
                }

                $logMessageCount = count(Yii::$app->getLog()->getLogger()->messages);
                $languageMessages = $messageSource->getMessages($category, $language['ietfcode']);
                $translatedCount = 0;

                if (count(Yii::$app->getLog()->getLogger()->messages) > $logMessageCount &&
                    ($lastError = end(Yii::$app->getLog()->getLogger()->messages)) !== false &&
                    mb_stripos($lastError[2], 'loadFallbackMessages') !== false &&
                    mb_stripos($lastError[0], 'category \''.$category.'\'') !== false) {
                    continue;
                }

                foreach ($translationMessages[$category]['available'] as $message) {
                    if (isset($languageMessages[$message])) {
                        $translatedCount++;
                    }
                }

                $translatedPercent = 0;

                if ($translationMessages[$category]['messageCount'] > 0) {
                    $translatedPercent = $translatedCount / $translationMessages[$category]['messageCount'];
                }

                $translationMessages[$category]['translations'][$language['ietfcode']] = [
                    'code'              => $language['ietfcode'],
                    'model'             => $language,
                    'messageCount'      => $translationMessages[$category]['messageCount'],
                    'translatedCount'   => $translatedCount,
                    'translatedPercent' => $translatedPercent,
                    'messages'          => $languageMessages,
                ];

                if ($translatedCount > 0) {
                    $languageAttributes = $language;
                    $languageAttributes['language_id'] = $languageAttributes['id'];

                    $translationMessages[$category]['languages'][$language['ietfcode']] = array_merge($languageAttributes, [
                        'id'                => $category . '_' . $language['ietfcode'],
                        'translatedCount'   => $translatedCount,
                        'translatedPercent' => $translatedPercent,
                    ]);
                    $translationMessages[$category]['translatedCount'] += $translatedCount;
                }
            }

            $translationMessages[$category]['totalMessageCount'] = count($translationMessages[$category]['available']) * count($translationMessages[$category]['translations']);
            $translationMessages[$category]['translatedPercent'] = 0;

            if ($translationMessages[$category]['totalMessageCount'] > 0) {
                $translationMessages[$category]['translatedPercent'] = $translationMessages[$category]['translatedCount'] / $translationMessages[$category]['totalMessageCount'];
            }
        }
        catch (InvalidConfigException $e) {
            Yii::$app->getLog()->getLogger()->log($e->getMessage(), Logger::LEVEL_WARNING);
        }

        return $translationMessages;
    }

    /**
     * @param string $file
     * @return array|null
     */
    public function searchTranslationMessagesInFile(string $file): array
    {
        $fileContent = file_get_contents($file);

        if (strpos($fileContent, '::t') !== false) {
            /*if (static::EXTENSION !== '*.php') {
                $fileContent = "<?php\n" . $fileContent;
            }*/
            $translatorTokens = token_get_all('<?php ' . '::t');
            $tokens = token_get_all($fileContent);

            array_shift($translatorTokens);

            try {
                return $this->extractMessagesFromTokens($tokens, $translatorTokens);
            } catch (\Exception $e) {
                var_dump($file);
                var_dump($e->getMessage());
                die();
            }
        }

        return [];
    }

    /**
     * Extracts messages from a parsed PHP tokens list.
     * Original by yii\console\controllers\MessageController
     *
     * @copyright Copyright (c) 2008 Yii Software LLC
     * @param array $tokens tokens to be processed.
     * @param array $translatorTokens translator tokens.
     * @return array messages.
     */
    protected function extractMessagesFromTokens(array $tokens, array $translatorTokens): array
    {
        $messages = [];
        $translatorTokensCount = count($translatorTokens);
        $matchedTokensCount = 0;
        $buffer = [];
        $pendingParenthesisCount = 0;

        foreach ($tokens as $tokenIndex => $token) {
            // finding out translator call
            if ($matchedTokensCount < $translatorTokensCount) {
                if ($this->tokensEqual($token, $translatorTokens[$matchedTokensCount])) {
                    $matchedTokensCount++;
                } else {
                    $matchedTokensCount = 0;
                }
            } elseif ($matchedTokensCount === $translatorTokensCount) {
                // translator found

                // end of function call
                if ($this->tokensEqual(')', $token)) {
                    $pendingParenthesisCount--;

                    if ($pendingParenthesisCount === 0) {
                        // end of translator call or end of something that we can't extract
                        if (isset($buffer[0][0], $buffer[1], $buffer[2][0]) && $buffer[0][0] === T_CONSTANT_ENCAPSED_STRING && $buffer[1] === ',' && $buffer[2][0] === T_CONSTANT_ENCAPSED_STRING) {
                            // is valid call we can extract
                            $category = stripcslashes($buffer[0][1]);
                            $category = mb_substr($category, 1, -1);

                            //if (!$this->isCategoryIgnored($category, $ignoreCategories)) {
                                $fullMessage = mb_substr($buffer[2][1], 1, -1);
                                $i = 3;
                                while ($i < count($buffer) - 1 && !is_array($buffer[$i]) && $buffer[$i] === '.') {
                                    $fullMessage .= mb_substr($buffer[$i + 1][1], 1, -1);
                                    $i += 2;
                                }

                                $message = stripcslashes($fullMessage);
                                $messages[$category][] = $message;
                            //}

                            $nestedTokens = array_slice($buffer, 3);
                            if (count($nestedTokens) > $translatorTokensCount) {
                                // search for possible nested translator calls
                                $messages = array_merge_recursive($messages, $this->extractMessagesFromTokens($nestedTokens, $translatorTokens));
                            }
                        } else {
                            // invalid call or dynamic call we can't extract
                            $line = $this->getLine($buffer);
                            $skipping = 'Skipping line';

                            Yii::warning("$skipping $line. Make sure both category and message are static strings.\n");
                        }

                        // prepare for the next match
                        $matchedTokensCount = 0;
                        $pendingParenthesisCount = 0;
                        $buffer = [];
                    } else {
                        $buffer[] = $token;
                    }
                } elseif ($this->tokensEqual('(', $token)) {
                    // count beginning of function call, skipping translator beginning

                    // If we are not yet inside the translator, make sure that it's beginning of the real translator.
                    // See https://github.com/yiisoft/yii2/issues/16828
                    if ($pendingParenthesisCount === 0) {
                        $previousTokenIndex = $tokenIndex - $matchedTokensCount - 1;
                        if (is_array($tokens[$previousTokenIndex])) {
                            $previousToken = $tokens[$previousTokenIndex][0];
                            if (in_array($previousToken, [T_OBJECT_OPERATOR, T_PAAMAYIM_NEKUDOTAYIM], true)) {
                                $matchedTokensCount = 0;
                                continue;
                            }
                        }
                    }

                    if ($pendingParenthesisCount > 0) {
                        $buffer[] = $token;
                    }
                    $pendingParenthesisCount++;
                } elseif (isset($token[0]) && !in_array($token[0], [T_WHITESPACE, T_COMMENT])) {
                    // ignore comments and whitespaces
                    $buffer[] = $token;
                }
            }
        }

        return $messages;
    }

    /**
     * Finds out a line of the first non-char PHP token found.
     *
     * @param array $tokens
     * @return int|string
     * @since 2.0.1
     */
    protected function getLine($tokens)
    {
        foreach ($tokens as $token) {
            if (isset($token[2])) {
                return $token[2];
            }
        }

        return 'unknown';
    }

    /**
     * Finds out if two PHP tokens are equal.
     * Original by yii\console\controllers\MessageController
     *
     * @copyright Copyright (c) 2008 Yii Software LLC
     * @param array|string $a
     * @param array|string $b
     * @return bool
     */
    protected function tokensEqual($a, $b): bool
    {
        if (is_string($a) && is_string($b)) {
            return $a === $b;
        }
        if (isset($a[0], $a[1], $b[0], $b[1])) {
            return $a[0] === $b[0] && $a[1] == $b[1];
        }

        return false;
    }
}
