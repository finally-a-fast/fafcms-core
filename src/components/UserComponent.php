<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\fafcms\components;

use fafcms\fafcms\models\User as UserModel;
use fafcms\filemanager\controllers\FilemanagerController;
use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filetype;
use fafcms\filemanager\Module as FileModule;
use GuzzleHttp\Client;
use Mexitek\PHPColors\Color;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\web\User;
use Yii;

class UserComponent extends User
{
    public $identityClass = UserModel::class;
    public $enableAutoLogin = true;
    public $loginUrl = ['user/login'];

    /**
     * @var UserModel[]
     */
    private array $_users;

    /**
     * @param int|null $id
     *
     * @return UserModel|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getUserById(?int $id): ?UserModel
    {
        if (!isset($this->_users[$id])) {
            $this->_users[$id] = UserModel::find()
                ->where(['id' => $id])
                ->one();

            if ($this->_users[$id] === null) {
                $this->_users[$id] = false;
            }
        }

        return ($this->_users[$id] !== false ? $this->_users[$id] : null);
    }

    /**
     * @return string|null
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function getChip(): ?string
    {
        return $this->isGuest ? null : $this->getChipByModel($this->identity);
    }

    /**
     * @param int|null $id
     *
     * @return string|null
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function getChipById(?int $id): ?string
    {
        $user = $this->getUserById($id);
        return $user === null ? null: $this->getChipByModel($user);
    }

    /**
     * @param UserModel $user
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function getChipByModel(UserModel $user): string
    {
        return '<div class="user-container chip">' .
            $this->getImageByModel($user) .
            $this->getNameByModel($user) .
        '</div>';
    }

    /**
     * @param bool $link
     *
     * @return string|null
     */
    public function getName(bool $link = false): ?string
    {
        return $this->isGuest ? null : $this->getNameByModel($this->identity, $link);
    }

    /**
     * @param int|null $id
     * @param bool     $link
     *
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getNameById(?int $id, bool $link = false): ?string
    {
        $user = $this->getUserById($id);
        return $user === null ? null: $this->getNameByModel($user, $link);
    }

    /**
     * @param UserModel $user
     * @param bool      $link
     *
     * @return string|null
     */
    public function getNameByModel(UserModel $user, bool $link = false): ?string
    {
        if ($link) {
            return Html::a($user->firstname . ' ' . $user->lastname, ['/user/' . $user->id]);
        }

        return $user->firstname . ' ' . $user->lastname;
    }

    /**
     * @param string|null $size
     * @param bool        $link
     *
     * @return string|null
     * @throws \Throwable
     */
    public function getImage(?string $size = null, bool $link = false): ?string
    {
        return $this->isGuest ? null : $this->getImageByModel($this->identity, $size, $link);
    }

    /**
     * @param int|null    $id
     * @param string|null $size
     * @param bool        $link
     *
     * @return string|null
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getImageById(?int $id, ?string $size = null, bool $link = false): ?string
    {
        $user = $this->getUserById($id);
        return $user === null ? null: $this->getImageByModel($user, $size, $link);
    }

    /**
     * @param UserModel   $user
     * @param string|null $size
     * @param bool        $link
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function getImageByModel(UserModel $user, ?string $size = null, bool $link = false): string
    {
        if ($size === null) {
            $size = '100,100,bestfit:true;extend:true;';
        }

        $image = FilemanagerController::getMedia($this->generateImage($user), 'User icon', $size, 'true');

        if ($link) {
            return Html::a($image, ['/user/' . $user->id]);
        }

        return $image;
    }

    /**
     * @return int|null
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function generateImage(): ?int
    {
        return $this->isGuest ? null : $this->generateImageByModel($this->identity);
    }

    /**
     * @param int|null $id
     *
     * @return int|null
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function generateImageById(?int $id): ?int
    {
        $user = $this->getUserById($id);
        return $user === null ? null: $this->generateImageByModel($user);
    }

    /**
     * @param UserModel $user
     *
     * @return int|null
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function generateImageByModel(UserModel $user): ?int
    {
        $emailHash = md5(strtolower(trim($user->email)));

        $filegroup = FileModule::getLoadedModule()->getGetFilegroupByPath('Profile Pictures'); // TODO setting

        $fileName = $emailHash . '.jpg';
        $mimeType = FileHelper::getMimeTypeByExtension($fileName);
        $fileType = Filetype::find()->where(['mime_type' => $mimeType])->one();

        if ($fileType === null) {
            Yii::error('Unknown mime type. Mime type: '. $mimeType);
            return null;
        }

        $fileOptions = [
            'filename' => substr($fileName, 0, strrpos($fileName, '.')),
            'filegroup_id' => $filegroup->id,
        ];

        $file = File::find()->where($fileOptions)->one();

        if ($file !== null) {
            $filePath = File::getFilePath($file);

            if (!file_exists($filePath)) {
                $file->delete();
                $file = null;
            }
        }

        if ($file === null) {
            $file = new File(array_merge($fileOptions, [
                'size' => 0,
                'is_public' => 0,
                'filetype_id' => $fileType->id,
            ]));

            FileHelper::createDirectory(File::getFileTarget($file));

            if (!$file->save()) {
                Yii::error('Cannot save file. ' . $file->getErrors());
                return null;
            }

            $file = File::find()->where(['id' => $file->id])->one();

            $filePath = File::getFilePath($file);

            if (true) { // TODO setting
                $client = new Client();

                $url = Yii::$app->urlManager->createAbsoluteUrl(['https://secure.gravatar.com/avatar/' . $emailHash . '.jpg',
                    'r' => 'g', // TODO setting http://en.gravatar.com/site/implement/images/#rating
                    's' => 200,
                    'd' => '404'
                ], 'https');

                $res = $client->get($url, [
                    'sink' => $filePath,
                ]);

                if ($res->getStatusCode() === 200) {
                    $file->size = filesize($filePath);

                    if (!$file->save()) {
                        Yii::error('Cannot update file size. '. $file->getErrors());
                    }

                    return $file->id;
                }
            }

            $fileName = $emailHash . '.svg';
            $mimeType = FileHelper::getMimeTypeByExtension($fileName);
            $fileType = Filetype::find()->where(['mime_type' => $mimeType])->one();

            if ($fileType === null) {
                Yii::error('Unknown mime type. Mime type: ' . $mimeType);
                return null;
            }

            $file->filetype_id = $fileType->id;
            $filePath = File::getFilePath($file);

            $initials = substr($user->firstname, 0, 1) . substr($user->lastname, 0, 1);

            $colors = '#DB2828,#F2711C,#FBBD08,#B5CC18,#21BA45,#00B5AD,#2185D0,#6435C9,#A333C8,#E03997,#A5673F,#767676,#1B1C1D'; // TODO setting
            $colors = explode(',', $colors);

            $backgroundColor = $colors[random_int(0, count($colors) - 1)];

            $browserColorObject = new Color($backgroundColor);
            $backgroundColorDark = '#' . $browserColorObject->darken(15);

            $browserColorObject = new Color($backgroundColor);
            $backgroundColorLight = '#' . $browserColorObject->lighten(15);

            $textColor = '#ffffff';
            $shadowColor = '#000000';

            $svg = <<<XML
<svg width="200" height="200" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
    <filter id="drop-shadow" x="-100%" y="-100%" width="300%" height="300%">
        <feGaussianBlur in="SourceAlpha" stdDeviation="3"/>
        <feOffset dx="0" dy="0" result="offsetblur"/>
        <feFlood flood-color="$shadowColor"/>
        <feComposite in2="offsetblur" operator="in" />
        <feMerge>
              <feMergeNode/>
              <feMergeNode in="SourceGraphic"/>
        </feMerge>
      </filter>
    <linearGradient id="gradient" x1="0%" y1="0%" x2="100%" y2="0%">
        <stop offset="0%" style="stop-color:$backgroundColorDark;stop-opacity:1" />
        <stop offset="50%" style="stop-color:$backgroundColor;stop-opacity:1" />
        <stop offset="100%" style="stop-color:$backgroundColorLight;stop-opacity:1" />
    </linearGradient>
</defs>
<rect x="0" y="0" width="200" height="200" fill="url(#gradient)"></rect>
<text x="50%" y="50%" fill="$textColor" font-size="80" dy=".1em" font-family="Verdana" text-anchor="middle" dominant-baseline="middle" filter="url(#drop-shadow)">$initials</text>

</svg>
XML;
            if (file_put_contents($filePath, $svg) !== false) {
                $file->size = filesize($filePath);

                if (!$file->save()) {
                    Yii::error('Cannot update file size. '. $file->getErrors());

                    $file->delete();
                    return null;
                }
            }
        }

        return $file->id;
    }
}
