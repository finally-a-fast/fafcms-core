<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\components;

use fafcms\helpers\abstractions\PluginModule;
use yii\base\Component;
use Yii;
use yii\base\ErrorException;
use yii\helpers\Json;

/**
 * Class InstallerComponent
 *
 * @package fafcms\fafcms\components
 *
 * @property-read array $installedPackages
 */
class InstallerComponent extends Component
{
    /**
     * @return array
     * @throws ErrorException
     */
    public function getInstalledPackages(): array
    {
        $this->checkComposer();

        $composerPath = FAFCMS_BASE_PATH . '/composer.json';

        if (!file_exists($composerPath)) {
            $composerPath = dirname(FAFCMS_BASE_PATH) . '/composer.json';
        }

        $rootPackages = Json::decode(file_get_contents($composerPath));
        $installedPackages = Json::decode(file_get_contents(FAFCMS_VENDOR_PATH . '/composer/installed.json'));
        $installedPlugins = [];

        $loadedPackages = Yii::$app->fafcms->loadedPlugins;

        foreach (($installedPackages['packages'] ?? $installedPackages) as $installedPackage) {
            $installedPackageName = $installedPackage['name'];
            $installedPackageKeywords = $installedPackage['keywords']??[];
            $type = null;
// composer info --direct --format=json --outdated --minor-only

            if ($installedPackageName === 'finally-a-fast/fafcms-core') {
                $type = 'core';
                $typeIcon = 'desktop-mac';
            } elseif (in_array('fafcms-module', $installedPackageKeywords, false)) {
                $type = 'module';
                $typeIcon = 'desktop-mac-dashboard';
            } elseif (in_array('fafcms-theme', $installedPackageKeywords, false)) {
                $type = 'theme';
                $typeIcon = 'view-compact';
            } elseif (isset($rootPackages->require->$installedPackageName)) {
                $type = 'misc';
                $typeIcon = 'puzzle';
            } else {
                $type = 'dependency';
                $typeIcon = 'puzzle-outline';
            }

            if ($type !== 'dependency' || Yii::$app->session->get('installerShowDependencies', false)) {
                $packageSettings = [];
                $packageSettingRules = [];

                if (isset($loadedPackages[$installedPackageName], $loadedPackages[$installedPackageName]['bootstrap']::$id) && ($module = Yii::$app->getModule($loadedPackages[$installedPackageName]['bootstrap']::$id)) instanceof PluginModule) {
                    /**
                     * @var $module PluginModule
                     */
                    $packageSettings = $module->getPluginSettings();
                    $packageSettingRules = $module->getPluginSettingRules();
                }

                $installedPlugins[$installedPackageName] = [
                    'name' => $installedPackageName,
                    'type' => $type,
                    'typeIcon' => $typeIcon,
                    'version' => $installedPackage['version_normalized'],
                    'license' => implode(', ', $installedPackage['license'] ?? []),
                    'homepage' => $installedPackage['homepage'] ?? null,
                    'description' => $installedPackage['description'] ?? null,
                    'keywords' => implode(', ', $installedPackageKeywords),
                    'extra' => $installedPackage['extra'] ?? [],
                    'loaded' => isset($loadedPackages[$installedPackageName]),
                    'settings' => $packageSettings,
                    'settingRules' => $packageSettingRules,
                    'installed' => true,
                ];
            }
        }

        return $installedPlugins;
    }

    /**
     * @throws ErrorException
     */
    public function checkComposer(): void
    {
        try {
            if (file_exists(FAFCMS_BASE_PATH.'/composer.phar')) {
                return;
            }

            $curlSignatureRequest = curl_init();
            curl_setopt($curlSignatureRequest, CURLOPT_URL, 'https://composer.github.io/installer.sig');
            curl_setopt($curlSignatureRequest, CURLOPT_RETURNTRANSFER, true);
            $composerSignature = curl_exec($curlSignatureRequest);

            if (is_string($composerSignature)) {
                $composerSignature = trim($composerSignature);
            }

            if (curl_errno($curlSignatureRequest) || curl_getinfo($curlSignatureRequest, CURLINFO_HTTP_CODE) !== 200 || strlen($composerSignature) !== 96) {
                curl_close($curlSignatureRequest);
                throw new ErrorException('Cannot download signature from composer.'."\n".'CURL ERROR: '.curl_error($curlSignatureRequest)."\n".'HTTP CODE: '.curl_getinfo($curlSignatureRequest, CURLINFO_HTTP_CODE)."\n".'Signature Length: '.strlen(trim($composerSignature)));
            }

            curl_close($curlSignatureRequest);

            $curlInstallerRequest = curl_init();
            curl_setopt($curlInstallerRequest, CURLOPT_URL, 'https://getcomposer.org/installer');
            curl_setopt($curlInstallerRequest, CURLOPT_RETURNTRANSFER, true);
            $composerInstaller = curl_exec($curlInstallerRequest);

            if (curl_errno($curlInstallerRequest) || curl_getinfo($curlInstallerRequest, CURLINFO_HTTP_CODE) !== 200) {
                curl_close($curlInstallerRequest);
                throw new ErrorException('Cannot download composer installer script.'."\n".'CURL ERROR: '.curl_error($curlInstallerRequest)."\n".'HTTP CODE: '.curl_getinfo($curlInstallerRequest, CURLINFO_HTTP_CODE));
            }

            curl_close($curlInstallerRequest);

            $installerSignature = hash('sha384', $composerInstaller);

            if ($installerSignature !== $composerSignature) {
                throw new ErrorException('Installer has an invalid sha384 signature.'."\n".'installer: '.$installerSignature."\n".'expected: '.$composerSignature);
            }

            if (!file_put_contents(FAFCMS_BASE_PATH.'/composer-setup.php', $composerInstaller)) {
                throw new ErrorException('Cannot write installer to '.FAFCMS_BASE_PATH.'/composer-setup.php');
            }

            $command = 'export COMPOSER_HOME={composer} && php composer-setup.php';
            $command = strtr($command, [
                '{composer}' => FAFCMS_BASE_PATH.'/.composer'
            ]);

            $descriptor = [
                1 => ['pipe', 'w'],
                2 => ['pipe', 'w'],
            ];
            $pipes = [];
            $proc = proc_open($command, $descriptor, $pipes, FAFCMS_BASE_PATH);
            $stdout = stream_get_contents($pipes[1]);
            $stderr = stream_get_contents($pipes[2]);

            foreach ($pipes as $pipe) {
                fclose($pipe);
            }

            $status = proc_close($proc);

            if ($status !== 0) {
                throw new ErrorException(htmlentities($stdout)."\n".htmlentities($stderr));
            }

            unlink(FAFCMS_BASE_PATH.'/composer-setup.php');
        } catch (\Exception $exception) {
            throw new ErrorException('Cannot install composer. Please try to install composer manually into the folder \''.FAFCMS_BASE_PATH.'\' See https://getcomposer.org/download/ for details.' . PHP_EOL . PHP_EOL . $exception->getMessage());
        }
    }
}
