<?php

namespace fafcms\fafcms\classes;

use fafcms\fafcms\components\FafcmsComponent;
use yii\base\InvalidConfigException;

/**
 * Class ConsoleApplication
 *
 * @package fafcms\fafcms\classes
 *
 * @property-read FafcmsComponent                              $fafcms           The fafcms component. This property is read-only.
 * @property-read \fafcms\fafcms\components\InjectorComponent  $injector         The installer component. This property is read-only. Extended component.
 * @property-read \fafcms\fafcms\components\InstallerComponent $installer        The installer component. This property is read-only. Extended component.
 * @property-read \fafcms\parser\component\Parser              $fafcmsParser     The fafcmsParser component. This property is read-only. Extended component.
 * @property-read \fafcms\fafcms\components\DataCacheComponent $dataCache        The dataCache component. This property is read-only. Extended component.
 * @property-read \fafcms\fafcms\components\ViewComponent      $view             The view component. This property is read-only. Extended component.
 * @property-read \fafcms\fafcms\components\I18NComponent      $i18n             The i18n component. This property is read-only. Extended component.
 * @property-read \fafcms\fafcms\components\UserComponent      $user             The user component. This property is read-only. Extended component.
 * @property-read \yii\caching\CacheInterface                  $pluginCache      The settingCache component. This property is read-only. Extended component.
 * @property-read \yii\caching\CacheInterface                  $settingCache     The settingCache component. This property is read-only. Extended component.
 * @property-read \yii\caching\CacheInterface                  $translationCache The translationCache component. This property is read-only. Extended component.
 * @property-read \yii\caching\CacheInterface                  $fileCache        The fileCache component. This property is read-only. Extended component.
 * @property-read \yii\queue\db\Queue                          $queue            The queue component. This property is read-only. Extended component.
 */
class ConsoleApplication extends \yii\console\Application
{
    /**
     * Returns the configuration of the built-in commands.
     * @return array the configuration of the built-in commands.
     */
    /*public function coreCommands()
    {
        return [
            'asset' => 'yii\console\controllers\AssetController',
            'cache' => 'yii\console\controllers\CacheController',
            'fixture' => 'yii\console\controllers\FixtureController',
            'help' => 'yii\console\controllers\HelpController',
            'message' => 'yii\console\controllers\MessageController',
            'migrate' => 'yii\console\controllers\MigrateController',
            'serve' => 'yii\console\controllers\ServeController',
        ];
    }*/

    /**
     * Returns the fafcms component.
     * @return FafcmsComponent the fafcms component.
     * @throws InvalidConfigException
     */
    public function getFafcms(): FafcmsComponent
    {
        return $this->get('fafcms');
    }

    /**
     * {@inheritdoc}
     */
    public function coreComponents(): array
    {
        return array_merge(parent::coreComponents(), [
            'fafcms' => ['class' => FafcmsComponent::class],
        ]);
    }
}
