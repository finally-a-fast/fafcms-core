<?php

namespace fafcms\fafcms\inputs;

use Closure;
use yii\helpers\Html;

/**
 * Class ExtendedDropDownList
 *
 * @package fafcms\fafcms\inputs
 */
class ExtendedDropDownList extends DropDownList
{
    /**
     * @var bool
     */
    public bool $clearable = true;

    /**
     * @var bool
     */
    public bool $forceSelection = false;

    /**
     * @var bool
     */
    public bool $search = true;

    /**
     * @var string
     */
    public string $match = 'text';

    public function run(): string
    {
        if ($this->items instanceof Closure) {
            $this->items = call_user_func($this->items);
        }

        $this->options['data-clearable'] = $this->clearable ? 'true' : 'false';
        $this->options['data-force-selection'] = $this->forceSelection ? 'true' : 'false';
        $this->options['data-match'] = $this->match;

        if ($this->search) {
            Html::addCssClass($this->options, 'search');
        }

        $this->addRelationPopup();

        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->extendedDropDownList($this->items, $this->getInputOptions(true));
    }
}
