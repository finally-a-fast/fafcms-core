<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;
use yii\helpers\Html;

/**
 * Class Textarea
 *
 * @package fafcms\fafcms\inputs
 */
class Textarea extends FormInput
{
    /**
     * @var bool
     */
    public bool $counter = false;

    /**
     * @var bool
     */
    public bool $maxlength = false;

    /**
     * {@inheritdoc}
     */
    public function run(): string
    {
        $options = $this->getInputOptions();

        if ($this->counter) {
            Html::addCssClass($options, 'counter');
        }

        $options['maxlength'] = $this->maxlength;

        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->textarea($options);
    }
}
