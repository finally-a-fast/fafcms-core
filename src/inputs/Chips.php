<?php

namespace fafcms\fafcms\inputs;

use Closure;

/**
 * Class Chips
 *
 * @package fafcms\fafcms\inputs
 */
class Chips extends ExtendedDropDownList
{
    /**
     * {@inheritdoc}
     */
    public function run(): string
    {
        $this->options['data-allow-additions'] = 'true';
        $this->options['options']['multiple'] = true;

        return parent::run();
    }
}
