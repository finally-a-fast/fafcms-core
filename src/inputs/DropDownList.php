<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;
use Closure;
use yii\helpers\Url;
use yii\helpers\Html;
use Yii;

/**
 * Class DropDownList
 *
 * @package fafcms\fafcms\inputs
 */
class DropDownList extends FormInput
{
    /**
     * @var array|Closure
     */
    public $items = [];

    /**
     * @var string|null
     */
    public ?string $relationClassName = null;

    /**
     * {@inheritdoc}
     * @return string
     */
    public function run(): string
    {
        if ($this->items instanceof Closure) {
            $this->items = call_user_func($this->items);
        }

        $this->addRelationPopup();

        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->dropDownList($this->items, $this->getInputOptions());
    }

    public function addRelationPopup(): void
    {
        if ($this->relationClassName !== null) {
            $this->options['data-relation-class-name'] = $this->relationClassName;
            Html::addCssClass($this->options, 'fafcms-relation-popup');
        }
    }
}
