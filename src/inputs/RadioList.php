<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;

/**
 * Class RadioList
 *
 * @package fafcms\fafcms\inputs
 */
class RadioList extends FormInput
{
    /**
     * @var array
     */
    public array $items = [];

    /**
     * {@inheritdoc}
     */
    public function run(): string
    {
        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->radioList($this->items, $this->getInputOptions());
    }
}
