<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;
use Closure;
use yii\helpers\Html;

/**
 * Class HiddenInput
 *
 * @package fafcms\fafcms\inputs
 */
class HiddenInput extends FormInput
{
    /**
     * {@inheritdoc}
     */
    public function run(): string
    {
        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->hiddenInput($this->getInputOptions());
    }
}
