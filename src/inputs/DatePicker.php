<?php


namespace fafcms\fafcms\inputs;

/**
 * Class DatePicker
 *
 * @package fafcms\fafcms\inputs
 */
class DatePicker extends DateTimePicker
{
    /**
     * @var string $type
     */
    public string $type = 'date';

    /**
     * @var string $icon
     */
    protected string $icon = 'mdi mdi-calendar';

    /**
     * @var string
     */
    public static string $columnFormat = 'date';
}
