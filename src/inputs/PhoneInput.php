<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;
use yii\widgets\MaskedInput;

/**
 * Class PhoneInput
 *
 * @package fafcms\fafcms\inputs
 */
class PhoneInput extends FormInput
{
    /** @var string|array|\yii\web\JsExpression */
    public $mask = '+#{5,40}';

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function run(): string
    {
        if (!isset($this->options['alias'], $this->options['mask'])) {
            $this->options['mask'] = $this->mask;
        }

        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->widget(MaskedInput::class, $this->getInputOptions(true));
    }
}
