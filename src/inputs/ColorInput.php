<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;
use yii\helpers\Html;

/**
 * Class ColorInput
 *
 * @package fafcms\fafcms\inputs
 */
class ColorInput extends FormInput
{
    /**
     * {@inheritdoc}
     */
    public function run(): string
    {
        Html::addCssClass($this->options, 'color-btn');

        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->input('color', $this->getInputOptions());
    }
}
