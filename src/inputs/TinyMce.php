<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;
use fafcms\fafcms\widgets\TinyMce as TinyMceWidget;

/**
 * Class TinyMce
 *
 * @package fafcms\fafcms\inputs
 */
class TinyMce extends FormInput
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function run(): string
    {
        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->widget(TinyMceWidget::class, $this->getInputOptions(true));
    }
}
