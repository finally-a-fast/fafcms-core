<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;

/**
 * Class Checkbox
 *
 * @package fafcms\fafcms\inputs
 */
class Checkbox extends FormInput
{
    /**
     * @var bool
     */
    public bool $enclosedByLabel = false;

    /**
     * @var array
     */
    public array $checkboxOptions = [];

    /**
     * {@inheritdoc}
     */
    public function run(): string
    {
        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->checkbox($this->getInputOptions(), $this->checkboxOptions, $this->enclosedByLabel);
    }
}
