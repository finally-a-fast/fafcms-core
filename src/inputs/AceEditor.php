<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;
use fafcms\fafcms\widgets\AceEditor as AceEditorWidget;

/**
 * Class AceEditor
 *
 * @package fafcms\fafcms\inputs
 */
class AceEditor extends FormInput
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function run(): string
    {
        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->widget(AceEditorWidget::class, $this->getInputOptions(true));
    }
}
