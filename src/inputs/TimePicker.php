<?php

namespace fafcms\fafcms\inputs;

/**
 * Class TimePicker
 *
 * @package fafcms\fafcms\inputs
 */
class TimePicker extends DateTimePicker
{
    /**
     * @var string $type
     */
    public string $type = 'time';

    /**
     * @var string $icon
     */
    protected string $icon = 'mdi mdi-clock-outline';

    /**
     * @var string
     */
    public static string $columnFormat = 'time';
}
