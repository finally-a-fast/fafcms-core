<?php

namespace fafcms\fafcms\inputs;

use Yii;
use yii\helpers\Html;

/**
 * Class SwitchCheckbox
 *
 * @package fafcms\fafcms\inputs
 */
class SwitchCheckbox extends Checkbox
{
    /**
     * @var array|string
     */
    public $offLabel = ['fafcms-core', 'Off'];

    /**
     * @var array|string
     */
    public $onLabel = ['fafcms-core', 'On'];

    /**
     * {@inheritdoc}
     */
    public function run(): string
    {
        $this->labelOptions['data-label-off'] = Yii::$app->fafcms->getTextOrTranslation($this->offLabel);
        $this->labelOptions['data-label-on'] = Yii::$app->fafcms->getTextOrTranslation($this->onLabel);

        Html::addCssClass($this->checkboxOptions, 'toggle');

        return parent::run();
    }
}
