<?php

namespace fafcms\fafcms\inputs;

/**
 * Class Percent
 *
 * @package fafcms\fafcms\inputs
 */
class Percent extends Slider
{
    /**
     * @var string
     */
    public static string $columnFormat = 'percent';
}
