<?php

namespace fafcms\fafcms\inputs;

use yii\helpers\Html;
use Yii;
use fafcms\helpers\abstractions\FormInput;
use yii\base\UnknownPropertyException;
use fafcms\fafcms\helpers\FormatConverter;

/**
 * Class DateTimePicker
 *
 * @package fafcms\fafcms\inputs
 */
class DateTimePicker extends FormInput
{
    /**
     * @var string $type
     */
    public string $type = 'datetime';

    /**
     * @var string $icon
     */
    protected string $icon = 'mdi mdi-calendar-clock';

    /**
     * @var string
     */
    public static string $columnFormat = 'datetime';

    /**
     * {@inheritdoc}
     * @throws UnknownPropertyException
     */
    public function run(): string
    {
        $format = $this->type . 'Format';
        $type   = 'as'. ucfirst($this->type);

        if (!Yii::$app->formatter->hasMethod($type) || !Yii::$app->formatter->hasProperty($format)) {
            throw new UnknownPropertyException('Unknown format: ' . $this->type);
        }

        $name = $this->name;

        // needed to convert the timezone
        if (!empty($this->model->$name)) {
            $icuPattern = 'php:Y-m-d H:i:s';

            if ($this->type === 'time') {
                $icuPattern = 'php:H:i:s';
            }

            if ($this->type === 'date') {
                $icuPattern = 'php:Y-m-d';
            }

            $this->model->$name = Yii::$app->formatter->$type($this->model->$name, $icuPattern);
        }

        $this->options['data-type'] = $this->type;
        $this->options['data-date-time-format'] = $this->getConvertedIcuFormat(Yii::$app->formatter->datetimeFormat, 'datetime');
        $this->options['data-time-format'] = $this->getConvertedIcuFormat(Yii::$app->formatter->timeFormat, 'time');
        $this->options['data-date-format'] = $this->getConvertedIcuFormat(Yii::$app->formatter->dateFormat, 'date');
        $this->options['icon'] = $this->icon;
        $this->options['inputOptions']['autocomplete'] = 'off';

        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->dateTimeInput($this->getInputOptions());
    }

    /**
     * @param string $pattern
     * @param string $type
     *
     * @return string
     */
    protected function getConvertedIcuFormat(string $pattern, string $type): string
    {
        /**
         * We need to rewrite the pattern for abbreviations and full names because input mask does not support it
         * No leading zero inputs are very buggy so its also rewritten
         * @see https://github.com/RobinHerbots/Inputmask/issues/400 and @see https://github.com/RobinHerbots/Inputmask/issues/1897
         */
        $icuPattern = FormatConverter::convertDateIcuToIcuPattern($pattern, $type);

        // http://userguide.icu-project.org/formatparse/datetime#TOC-Date-Time-Format-Syntax
        // escaped text
        $escaped = [];

        if (preg_match_all('/(?<!\')\'(.*?[^\'])\'(?!\')/', $icuPattern, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                $match[1] = str_replace('\'\'', '\'', $match[1]);
                $escaped[$match[0]] = '\\' . implode('\\', preg_split('//u', $match[1], -1, PREG_SPLIT_NO_EMPTY));
            }
        }

        return strtr($icuPattern, array_merge($escaped, [
            'd' => 'dd',
            'dd' => 'dd',
            'M' => 'MM',
            'MM' => 'MM',
            'MMM' => 'MM',
            'MMMM' => 'MM',
            'MMMMM' => 'MMMMM',
            'L' => 'MM',
            'LL' => 'LL',
            'LLL' => 'MM',
            'LLLL' => 'MM',
            'LLLLL' => 'LLLLL',
            'E' => '',
            'EE' => '',
            'EEE' => '',
            'EEEE' => '',
            'EEEEE' => 'EEEEE',
            'EEEEEE' => 'EEEEEE',
            'e' => 'e',
            'ee' => 'ee',
            'eee' => '',
            'eeee' => '',
            'eeeee' => 'eeeee',
            'eeeeee' => 'eeeeee',
            'c' => 'c',
            'cc' => 'cc',
            'ccc' => '',
            'cccc' => '',
            'ccccc' => 'ccccc',
            'cccccc' => 'cccccc',
        ]));
    }
}
