<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;
use Closure;
use yii\helpers\Html;

/**
 * Class Slider
 *
 * @package fafcms\fafcms\inputs
 * @TODO
 */
class Slider extends FormInput
{
    /**
     * @var bool
     */
    public bool $showCopyButton = false;

    /**
     * @var array|Closure
     */
    public $copyButtonOptions = [];

    /**
     * @param string $formInput
     * @return string
     */
    public function wrapButtons(string $formInput): string
    {
        if ($this->showCopyButton) {
            if ($this->copyButtonOptions instanceof Closure) {
                $options = call_user_func($this->copyButtonOptions, $this);
            } else {
                $options = $this->copyButtonOptions;
            }

            $containerOptions = ['class' => 'input-field-button'];

            Html::addCssClass($options, 'btn-floating waves-effect waves-light copy-text');

            $options['data-copy-target'] = '#' . $this->inputId;

            $formInput = Html::tag(
                'div',
                $formInput.Html::button('<i class="mdi mdi-content-copy"></i>', $options),
                $containerOptions
            );
        }

        return $formInput;
    }

    /**
     * {@inheritdoc}
     */
    public function run(): string
    {
        return $this->wrapButtons($this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->textInput($this->getInputOptions()));
    }
}
