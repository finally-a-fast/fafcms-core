<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;
use yii\widgets\MaskedInput;

/**
 * Class EmailInput
 *
 * @package fafcms\fafcms\inputs
 */
class EmailInput extends FormInput
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function run(): string
    {
        if (!isset($this->options['clientOptions']['alias'])) {
            $this->options['clientOptions']['alias'] = 'email';
        }

        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->widget(MaskedInput::class, $this->getInputOptions(true));
    }
}
