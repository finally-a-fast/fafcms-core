<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;

/**
 * Class NumberInput
 *
 * @package fafcms\fafcms\inputs
 */
class NumberInput extends FormInput
{
    /**
     * {@inheritdoc}
     */
    public function run(): string
    {
        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->input('number', $this->getInputOptions());
    }
}
