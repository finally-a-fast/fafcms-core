<?php

namespace fafcms\fafcms\inputs;

use fafcms\fafcms\widgets\RawInputWidget;
use fafcms\helpers\abstractions\FormInput;

/**
 * Class Raw
 *
 * @package fafcms\fafcms\inputs
 */
class Raw extends FormInput
{
    /**
     * @var string
     */
    public static string $columnFormat = 'raw';

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function run(): string
    {
        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->widget(RawInputWidget::class, $this->getInputOptions(true));
    }
}
