<?php

namespace fafcms\fafcms\inputs;

use fafcms\helpers\abstractions\FormInput;
use yii\widgets\MaskedInput;

/**
 * Class UrlInput
 *
 * @package fafcms\fafcms\inputs
 */
class UrlInput extends FormInput
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function run(): string
    {
        if (!isset($this->options['clientOptions']['alias'])) {
            $this->options['clientOptions']['alias'] = 'url';
        }

        return $this->form->field($this->model, $this->name, $this->fieldOptions)
            ->label(null, $this->labelOptions)
            ->hint($this->description)
            ->widget(MaskedInput::class, $this->getInputOptions(true));
    }
}
