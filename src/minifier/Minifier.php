<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\minifier;

use Less_Parser;
use MatthiasMullie\Minify\CSS;
use Padaliyajay\PHPAutoprefixer\Autoprefixer;
use ScssPhp\ScssPhp\Compiler;
use MatthiasMullie\Minify\JS;
use Yii;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\base\NotSupportedException;

/**
 * Class AssetManagerController
 *
 * @package fafcms\fafcms\controllers
 */
class Minifier extends BaseObject
{
    public const DIST_TYPE_CSS = 'css';
    public const DIST_TYPE_JS = 'js';
    public const DIST_TYPE_HTML = 'html';

    public const SRC_TYPES = [
        'css' => self::DIST_TYPE_CSS,
        'scss' => self::DIST_TYPE_CSS,
        'less' => self::DIST_TYPE_CSS,
        'js' => self::DIST_TYPE_JS,
        'html' => self::DIST_TYPE_HTML
    ];

    /**
     * @var MinifierSrc[]
     */
    public array $src;

    /**
     * @var string|null
     */
    public ?string $dist = null;

    /**
     * @var string
     */
    protected string $content = '';

    /**
     * @var string|null
     */
    protected ?string $distType = null;

    /**
     * @var bool
     */
    public bool $sourceMap = true;

    /**
     * @var bool
     */
    public bool $autoPrefix = true;

    /**
     * @var bool
     */
    public bool $minify = true;

    /**
     * @var array
     */
    protected array $usedPaths = [];

    /**
     * @param string $srcType
     *
     * @throws NotSupportedException
     */
    protected function setDistType(string $srcType): void
    {
        if ($this->distType === null || $this->distType === $srcType) {
            $this->distType = $srcType;
        } else {
            throw new NotSupportedException('You cannot mix content of type "' . $this->distType . '" with content of type "' . $srcType . '".');
        }
    }

    /**
     * @return string|null
     * @throws Exception
     * @throws NotSupportedException
     */
    public function run(): ?string
    {
        foreach ($this->src as $src) {
            if (!isset(self::SRC_TYPES[$src->type])) {
                throw new NotSupportedException('Content of type "' . $src->type . '" is not supported.');
            }

            $this->setDistType(self::SRC_TYPES[$src->type]);

            $src->content = $this->convert($src);
            /** @noinspection SuspiciousAssignmentsInspection */
            $src->content = $this->rewrite($src);

            if ($src->type === 'js') {
                $this->content .= ';';
            }

            $this->content .= $src->content . PHP_EOL;

            if ($src->hasProperty('fileInfo')) {
                /** @noinspection PhpPossiblePolymorphicInvocationInspection */
                $this->usedPaths[] = $src->fileInfo['dirname'];
            }
        }

        if ($this->minify) {
            $this->content = $this->minify($this->distType, $this->content);
        }

        if ($this->dist === null) {
            return $this->content;
        }

        if (!file_put_contents(Yii::getAlias($this->dist), $this->content)) {
            throw new Exception('Cannot write file "' . Yii::getAlias($this->dist) . '".');
        }

        return null;
    }

    /**
     * @param string $type
     * @param string $content
     *
     * @return string
     */
    public function minify(string $type, string $content): string
    {
        if ($type === 'css') {
            return $this->minifyCss($content);
        }

        if ($type === 'js') {
            return $this->minifyJs($content);
        }

        if ($type === 'html') {
            return $this->minifyHtml($content);
        }

        return $content;
    }

    /**
     * @param string $content
     *
     * @return string
     */
    public function minifyCss(string $content): string
    {
        $minifier = new CSS($content);
        return $minifier->minify();
    }

    /**
     * @param string $content
     *
     * @return string
     */
    public function minifyJs(string $content): string
    {
        $minifier = new JS($content);
        return $minifier->minify();
    }

    /**
     * @param MinifierSrc $src
     *
     * @return string
     * @throws \ScssPhp\ScssPhp\Exception\CompilerException
     */
    public function convert(MinifierSrc $src): string
    {
        $content = $src->content;
        $path = [FAFCMS_BASE_PATH];

        if ($src->hasProperty('fileInfo')) {
            /** @noinspection PhpPossiblePolymorphicInvocationInspection */
            $path[] = $src->fileInfo['dirname'];
        }

        if ($src->type === 'scss') {
            $scss = new Compiler();
            $scss->setImportPaths($path);
            $scss->setSourceMap(Compiler::SOURCE_MAP_INLINE);
            $content = $scss->compile($content);
        }

        if ($src->type === 'less') {
            $less = new Less_Parser([
                'sourceMap' => true,
            ]);
            $less->SetImportDirs($path);
            $less->parse($content);
            $content = $less->getCss();
        }

        if ($this->autoPrefix && ($src->type === 'css' || $src->type === 'less' || $src->type === 'scss')) {
            $autoprefixer = new Autoprefixer($content);
            $content = $autoprefixer->compile();
        }

        return $content;
    }

    /**
     * @var array
     */
    protected array $_placeholders = [];

    /**
     * Html minifier is mostly based on https://github.com/mrclay/minify
     * by Stephen Clay <steve@mrclay.org>
     * BSD-3-Clause License
     * https://github.com/mrclay/minify/blob/master/LICENSE.txt
     *
     * @param string $content
     *
     * @return string
     */
    public function minifyHtml(string $content): string
    {
        $content = str_replace("\r\n", "\n", trim($content));

        $this->_placeholders = [];

        // replace SCRIPTs (and minify) with placeholders
        $content = preg_replace_callback('/(\\s*)<script(\\b[^>]*?>)([\\s\\S]*?)<\\/script>(\\s*)/iu',
            function($m) {
                $openScript = '<script' . $m[2];
                $js = $m[3];

                // whitespace surrounding? preserve at least one space
                $ws1 = ($m[1] === '') ? '' : ' ';
                $ws2 = ($m[4] === '') ? '' : ' ';

                // remove CDATA section markers
                $js = $this->removeCdata($js);
                $js = trim($this->minifyJs($js));

                return $this->reservePlace($ws1 . $openScript . $js . '</script>' . $ws2);
            },
            $content
        );

        // replace STYLEs (and minify) with placeholders
        $content = preg_replace_callback('/\\s*<style(\\b[^>]*>)([\\s\\S]*?)<\\/style>\\s*/iu',
            function($m) {
                $openStyle = '<style' . $m[1];
                $css = $m[2];

                // remove HTML comments
                $css = preg_replace('/(?:^\\s*<!--|-->\\s*$)/u', '', $css);

                // remove CDATA section markers
                $css = $this->removeCdata($css);

                return $this->reservePlace( $openScript . $this->minifyCss($css) . '</style>');
            },
            $content
        );

        // remove HTML comments (not containing IE conditional comments).
        $content = preg_replace_callback('/<!--([\\s\\S]*?)-->/u',
            function($m) {
                if (strpos($m[1], '[') === 0 || strpos($m[1], '<![') !== false || strpos($m[1], '#') === 0) {
                    return $m[0];
                }

                return '';
            },
            $content
        );

        // replace PREs with placeholders
        $content = preg_replace_callback('/\\s*<pre(\\b[^>]*?>[\\s\\S]*?<\\/pre>)\\s*/iu',
            function($m) {
                return $this->reservePlace('<pre' . $m[1]);
            },
            $content
        );

        // replace TEXTAREAs with placeholders
        $content = preg_replace_callback('/\\s*<textarea(\\b[^>]*?>[\\s\\S]*?<\\/textarea>)\\s*/iu',
            function($m) {
                return $this->reservePlace('<textarea' . $m[1]);
            },
            $content
        );

        // trim each line.
        // @todo take into account attribute values that span multiple lines.
        $content = preg_replace('/^\\s+|\\s+$/mu', '', $content);

        // remove ws around block/undisplayed elements
        $content = preg_replace('/\\s+(<\\/?(?:area|article|aside|base(?:font)?|blockquote|body'
                                .'|canvas|caption|center|col(?:group)?|dd|dir|div|dl|dt|fieldset|figcaption|figure|footer|form'
                                .'|frame(?:set)?|h[1-6]|head|header|hgroup|hr|html|legend|li|link|main|map|menu|meta|nav'
                                .'|ol|opt(?:group|ion)|output|p|param|section|t(?:able|body|head|d|h||r|foot|itle)'
                                .'|ul|video)\\b[^>]*>)/iu', '$1', $content);

        // remove ws outside of all elements
        $content = preg_replace('/>(\\s(?:\\s*))?([^<]+)(\\s(?:\s*))?</u', '>$1$2$3<', $content);

        // use newlines before 1st attribute in open tags (to limit line lengths)
        $content = preg_replace('/(<[a-z\\-]+)\\s+([^>]+>)/iu', "$1\n$2", $content);

        // fill placeholders
        $content = str_replace(array_keys($this->_placeholders), array_values($this->_placeholders), $content);

        // issue 229: multi-pass to catch scripts that didn't get replaced in textareas
        return str_replace(array_keys($this->_placeholders), array_values($this->_placeholders), $content);
    }

    /**
     * @param $content
     *
     * @return string
     */
    protected function reservePlace($content): string
    {
        $placeholder = '%HTML_MINIFY_PLACEHOLDER_' . count($this->_placeholders) . '%';
        $this->_placeholders[$placeholder] = $content;

        return $placeholder;
    }

    protected function removeCdata($str)
    {
        return (strpos($str, '<![CDATA[') !== false) ? str_replace(array('<![CDATA[', ']]>'), '', $str) : $str;
    }

    /**
     * Rewrite is mostly based on https://github.com/mrclay/minify
     * by Stephen Clay <steve@mrclay.org>
     * BSD-3-Clause License
     * https://github.com/mrclay/minify/blob/master/LICENSE.txt
     * @param MinifierSrc $src
     *
     * @return string
     */
    public function rewrite(MinifierSrc $src): string
    {
        $content = $src->content;

        if ($src->type === 'css' || $src->type === 'less' || $src->type === 'scss') {
            if ($src->hasProperty('fileInfo')) {
                self::$_docRoot = Yii::getAlias('@webroot');
                self::$_currentDir = $src->fileInfo['dirname'];

                $content = self::_trimUrls($content);

                $content = self::_owlifySvgPaths($content);

                // rewrite
                $pattern = '/@import\\s+([\'"])(.*?)[\'"]/';
                $content = preg_replace_callback($pattern, array(self::class, '_processUriCB'), $content);

                $pattern = '/url\\(\\s*([\'"](.*?)[\'"]|[^\\)\\s]+)\\s*\\)/';
                $content = preg_replace_callback($pattern, array(self::class, '_processUriCB'), $content);
                $content = self::_unOwlify($content);
            }
        }

        return $content;
    }

    /**
     * In CSS content, prepend a path to relative URIs
     *
     * @param string $css
     *
     * @param string $path The path to prepend.
     *
     * @return string
     */
    public static function prepend($css, $path)
    {
        self::$_prependPath = $path;

        $css = self::_trimUrls($css);

        $css = self::_owlifySvgPaths($css);

        // append
        $pattern = '/@import\\s+([\'"])(.*?)[\'"]/';
        $css = preg_replace_callback($pattern, array(self::class, '_processUriCB'), $css);

        $pattern = '/url\\(\\s*([\'"](.*?)[\'"]|[^\\)\\s]+)\\s*\\)/';
        $css = preg_replace_callback($pattern, array(self::class, '_processUriCB'), $css);

        $css = self::_unOwlify($css);

        self::$_prependPath = null;

        return $css;
    }

    /**
     * Get a root relative URI from a file relative URI
     *
     * <code>
     * Minify_CSS_UriRewriter::rewriteRelative(
     *       '../img/hello.gif'
     *     , '/home/user/www/css'  // path of CSS file
     *     , '/home/user/www'      // doc root
     * );
     * // returns '/img/hello.gif'
     *
     * // example where static files are stored in a symlinked directory
     * Minify_CSS_UriRewriter::rewriteRelative(
     *       'hello.gif'
     *     , '/var/staticFiles/theme'
     *     , '/home/user/www'
     *     , array('/home/user/www/static' => '/var/staticFiles')
     * );
     * // returns '/static/theme/hello.gif'
     * </code>
     *
     * @param string $uri file relative URI
     *
     * @param string $realCurrentDir realpath of the current file's directory.
     *
     * @param string $realDocRoot realpath of the site document root.
     *
     * @param array $symlinks (default = array()) If the file is stored in
     * a symlink-ed directory, provide an array of link paths to
     * real target paths, where the link paths "appear" to be within the document
     * root. E.g.:
     * <code>
     * array('/home/foo/www/not/real/path' => '/real/target/path') // unix
     * array('C:\\htdocs\\not\\real' => 'D:\\real\\target\\path')  // Windows
     * </code>
     *
     * @return string
     */
    public static function rewriteRelative($uri, $realCurrentDir, $realDocRoot, $symlinks = array())
    {
        // prepend path with current dir separator (OS-independent)
        $path = strtr($realCurrentDir, '/', DIRECTORY_SEPARATOR);
        $path .= DIRECTORY_SEPARATOR . strtr($uri, '/', DIRECTORY_SEPARATOR);

        // "unresolve" a symlink back to doc root
        foreach ($symlinks as $link => $target) {
            if (0 === strpos($path, $target)) {
                // replace $target with $link
                $path = $link . substr($path, strlen($target));

                break;
            }
        }
        // strip doc root
        $path = substr($path, strlen($realDocRoot));

        // fix to root-relative URI
        $uri = strtr($path, '/\\', '//');
        $uri = self::removeDots($uri);

        return $uri;
    }

    /**
     * Remove instances of "./" and "../" where possible from a root-relative URI
     *
     * @param string $uri
     *
     * @return string
     */
    public static function removeDots($uri)
    {
        $uri = str_replace('/./', '/', $uri);
        // inspired by patch from Oleg Cherniy
        do {
            $uri = preg_replace('@/[^/]+/\\.\\./@', '/', $uri, 1, $changed);
        } while ($changed);

        return $uri;
    }

    /**
     * Get realpath with any trailing slash removed. If realpath() fails,
     * just remove the trailing slash.
     *
     * @param string $path
     *
     * @return mixed path with no trailing slash
     */
    protected static function _realpath($path)
    {
        $realPath = realpath($path);
        if ($realPath !== false) {
            $path = $realPath;
        }

        return rtrim($path, '/\\');
    }

    /**
     * Directory of this stylesheet
     *
     * @var string
     */
    private static $_currentDir = '';

    /**
     * DOC_ROOT
     *
     * @var string
     */
    private static $_docRoot = '';

    /**
     * Path to prepend
     *
     * @var string
     */
    private static $_prependPath = null;

    /**
     * @param string $css
     *
     * @return string
     */
    private static function _trimUrls($css)
    {
        $pattern = '/
            url\\(      # url(
            \\s*
            ([^\\)]+?)  # 1 = URI (assuming does not contain ")")
            \\s*
            \\)         # )
        /x';

        return preg_replace($pattern, 'url($1)', $css);
    }

    /**
     * @param array $m
     *
     * @return string
     */
    private static function _processUriCB($m)
    {
        // $m matched either '/@import\\s+([\'"])(.*?)[\'"]/' or '/url\\(\\s*([^\\)\\s]+)\\s*\\)/'
        $isImport = ($m[0][0] === '@');
        // determine URI and the quote character (if any)
        if ($isImport) {
            $quoteChar = $m[1];
            $uri = $m[2];
        } else {
            // $m[1] is either quoted or not
            $quoteChar = ($m[1][0] === "'" || $m[1][0] === '"') ? $m[1][0] : '';

            $uri = ($quoteChar === '') ? $m[1] : substr($m[1], 1, strlen($m[1]) - 2);
        }

        if ($uri === '') {
            return $m[0];
        }

        // if not root/scheme relative and not starts with scheme
        if (!preg_match('~^(/|[a-z]+\:)~', $uri)) {
            // URI is file-relative: rewrite depending on options
            if (self::$_prependPath === null) {
                $uri = self::rewriteRelative($uri, self::$_currentDir, self::$_docRoot);
            } else {
                $uri = self::$_prependPath . $uri;
                if ($uri[0] === '/') {
                    $root = '';
                    $rootRelative = $uri;
                    $uri = $root . self::removeDots($rootRelative);
                } elseif (preg_match('@^((https?\:)?//([^/]+))/@', $uri, $m) && (false !== strpos($m[3], '.'))) {
                    $root = $m[1];
                    $rootRelative = substr($uri, strlen($root));
                    $uri = $root . self::removeDots($rootRelative);
                }
            }
        }

        if ($isImport) {
            return "@import {$quoteChar}{$uri}{$quoteChar}";
        } else {
            return "url({$quoteChar}{$uri}{$quoteChar})";
        }
    }

    /**
     * Mungs some inline SVG URL declarations so they won't be touched
     *
     * @link https://github.com/mrclay/minify/issues/517
     * @see _unOwlify
     *
     * @param string $css
     * @return string
     */
    private static function _owlifySvgPaths($css)
    {
        return preg_replace('~\b((?:clip-path|mask|-webkit-mask)\s*\:\s*)url(\(\s*#\w+\s*\))~', '$1owl$2', $css);
    }

    /**
     * Undo work of _owlify
     *
     * @see _owlifySvgPaths
     *
     * @param string $css
     * @return string
     */
    private static function _unOwlify($css)
    {
        return preg_replace('~\b((?:clip-path|mask|-webkit-mask)\s*\:\s*)owl~', '$1url', $css);
    }
}
