<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\minifier;

/**
 * Class MinifierSrcContent
 *
 * @package fafcms\fafcms\minifier
 */
class MinifierSrcContent extends MinifierSrc
{
}
