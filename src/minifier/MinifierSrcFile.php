<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\minifier;

use Yii;
use yii\base\Exception;

/**
 * Class MinifierSrcFile
 *
 * @package fafcms\fafcms\minifier
 */
class MinifierSrcFile extends MinifierSrc
{
    public array $fileInfo;

    /**
     * @param string $file
     *
     * @throws Exception
     */
    public function setFile(string $file): void
    {
        $file = Yii::getAlias($file);

        if (!file_exists($file)) {
            throw new Exception('File "' . $file . '" not found.');
        }

        $this->file = $file;
        $this->fileInfo = pathinfo($this->file);

        if ($this->type === null) {
            $this->type = strtolower($this->fileInfo['extension']);

            if ($this->type === 'tmp' && ($pos = strrpos($this->fileInfo['filename'], '.')) !== false) {
                $this->type = substr($this->fileInfo['filename'], $pos + 1);
            }
        }

        if ($this->content === null) {
            $this->content = file_get_contents($this->file);
        }
    }
}
