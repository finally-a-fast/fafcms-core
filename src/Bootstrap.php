<?php

namespace fafcms\fafcms;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\models\Customfield;
use fafcms\fafcms\models\Customtable;
use fafcms\fafcms\models\Domain;
use fafcms\fafcms\models\Flag;
use fafcms\fafcms\models\Project;
use fafcms\fafcms\models\Projectlanguage;
use fafcms\fafcms\models\SystemTranslation;
use fafcms\fafcms\models\Tag;
use fafcms\fafcms\models\User;
use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\caching\CacheInterface;
use yii\helpers\Inflector;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use yiiui\yii2flagiconcss\widget\FlagIcon;

/**
 * Class Bootstrap
 * @package fafcms\fafcms
 */
class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-core';
    public static $tablePrefix = 'fafcms-core_';

    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        if (Yii::$app->fafcms->getIsInstalled()) {
            Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_SIDEBAR_LEFT, [
                'login' => [
                    'icon' => 'login-variant',
                    'label' => ['fafcms-core', 'Login'],
                    'url' => ['/main/login'],
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['?'],
                        ],
                    ]
                ],
                'lost-password' => [
                    'icon' => 'account-key-outline',
                    'label' => ['fafcms-core', 'Lost password?'],
                    'url' => ['/main/request-password-reset'],
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['?'],
                        ],
                    ]
                ],
                'project' => [
                    'type' => 'group',
                    'label' => ['fafcms-core', 'Project'],
                    'items' => [
                        'project-dashboard' => [
                            'icon' => 'speedometer',
                            'label' => ['fafcms-core', 'Project dashboard'],
                            'url' => ['/main/project-index'],
                        ],
                        'tags' => [
                            'icon' => Tag::instance()->getEditDataIcon(),
                            'label' => Tag::instance()->getEditDataPlural(),
                            'url' => ['/' . Tag::instance()->getEditDataUrl() . '/index']
                        ],
                        'project-settings' => [
                            'label' => ['fafcms-core', 'Project settings'],
                            'icon' => 'cog-outline',
                            'items' => [
                                'project-domains' => [
                                    'icon' => Domain::instance()->getEditDataIcon(),
                                    'label' => Domain::instance()->getEditDataPlural(),
                                    'url' => ['/' . Domain::instance()->getEditDataUrl() . '/index']
                                ],
                                'project-languages' => [
                                    'icon' => Projectlanguage::instance()->getEditDataIcon(),
                                    'label' => Projectlanguage::instance()->getEditDataPlural(),
                                    'url' => ['/' . Projectlanguage::instance()->getEditDataUrl() . '/index']
                                ],
                            ]
                        ],
                    ],
                ],
                'system' => [
                    'type' => 'group',
                    'label' => ['fafcms-core', 'System'],
                    'items' => [
                        'projects' => [
                            'icon' => Project::instance()->getEditDataIcon(),
                            'label' => Project::instance()->getEditDataPlural(),
                            'url' => ['/' . Project::instance()->getEditDataUrl() . '/index']
                        ],
                        'customisation' => [
                            'label' => ['fafcms-core', 'Customisation'],
                            'icon' => 'shape-outline',
                            'items' => [
                                'custom-table' => [
                                    'label' => Customtable::instance()->getEditDataPlural(),
                                    'icon' => Customtable::instance()->getEditDataIcon(),
                                    'url' => ['/' . Customtable::instance()->getEditDataUrl() . '/index']
                                ],
                                'custom-field' => [
                                    'label' => Customfield::instance()->getEditDataPlural(),
                                    'icon' => Customfield::instance()->getEditDataIcon(),
                                    'url' => ['/' . Customfield::instance()->getEditDataUrl() . '/index']
                                ],
                            ],
                        ],
                        'users' => [
                            'label' => ['fafcms-core', 'User Management'],
                            'icon' => 'account-group-outline',
                            'items' => [
                                'user' => [
                                    'icon'    => 'account-circle-outline',
                                    'visible' => true,
                                    'label'   => ['fafcms-core', 'User'],
                                    'url'     => ['/' . User::instance()->getEditDataUrl() . '/index'],
                                ],
                                'usergroups' => [
                                    'icon'    => 'folder-account-outline',
                                    'visible' => true,
                                    'label'   => ['fafcms-core', 'Usergroups'],
                                    'url'     => ['/usergroup/index'],
                                ],
                            ],
                        ],
                        'settings' => [
                            'label' => ['fafcms-core', 'Settings'],
                            'icon' => 'cog-outline',
                            'items' => [
                                'language-management' => [
                                    'label' => ['fafcms-core', 'Language Management'],
                                    'icon' => 'translate',
                                    'items' => [
                                        'language' => [
                                            'icon' => 'earth',
                                            'label' => ['fafcms-core', 'Languages'],
                                            'url' => ['/language/index']
                                        ],
                                        'countries' => [
                                            'icon' => 'globe-model',
                                            'label' => ['fafcms-core', 'Countries'],
                                            'url' => ['/country/index']
                                        ],
                                        'flags' => [
                                            'icon' => 'flag-variant-outline',
                                            'label' => ['fafcms-core', 'Flags'],
                                            'url' => ['/flags/index']
                                        ],
                                        'system-translations' => [
                                            'label' => ['fafcms-core', 'System translations'],
                                            'icon' => 'translate',
                                            'visible' => true,
                                            'url' => ['/' . SystemTranslation::instance()->getEditDataUrl() . '/index'],
                                        ]
                                    ]
                                ],
                                'installer' => [
                                    'label' => ['fafcms-core', 'Update and install modules'],
                                    'icon' => 'puzzle-outline',
                                    'visible' => true,
                                    'url' => ['/installer/index']
                                ],
                            ],
                        ],
                    ],
                ],
            ]);

            Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_HEADER_LEFT, [
                'sidebar-trigger' => [
                    'html' => '<a class="item sidenav-trigger"><div><div></div><div></div><div></div></div></a>',
                    'rules' => false
                ],
                'dashboard' => [
                    'icon' => 'speedometer',
                    'label' => ['fafcms-core', 'Dashboard'],
                    'url' => ['/main/index'],
                ],
            ]);

            $cacheItems = [
                'all' => [
                    'label' => ['fafcms-core', 'Flush all caches'],
                    'url' => ['/main/flush-cache']
                ],
            ];

            $components = Yii::$app->getComponents();

            foreach (array_keys($components) as $cacheName) {
                if (isset($components[$cacheName], Yii::$app->$cacheName) && Yii::$app->$cacheName instanceof CacheInterface) {
                    $cacheItems[$cacheName] = [
                        'label' => ['fafcms-core', 'Flush {cache}', ['cache' => Inflector::camel2words($cacheName)]],
                        'url' => ['/main/flush-cache', 'cache' => $cacheName]
                    ];
                }
            }

            if (class_exists('DarkGhostHunter\Preloader\Preloader')) {
                $cacheItems['preloader'] = [
                    'label' => ['fafcms-core', 'Generate opcache preloader file'],
                    'url' => ['/main/generate-preloader']
                ];
            }

            if (extension_loaded('Zend OPcache')) {
                $cacheItems['opCache'] = [
                    'label' => ['fafcms-core', 'Flush opcache'],
                    'url' => ['/main/flush-cache', 'cache' => 'opCache']
                ];
            }

            $cacheItems['assetCache'] = [
                'label' => ['fafcms-core', 'Flush asset cache'],
                'url' => ['/main/flush-cache', 'cache' => 'assetCache']
            ];

            $availableLanguages = Yii::$app->fafcms->getAvailableLanguages();
            $languageSelection = [];

            $flags = Yii::$app->dataCache->map(Flag::class);

            foreach ($availableLanguages as $availableLanguage) {
                $languageSelection[] = [
                    'label' => FlagIcon::widget([
                        'countryCode' => $flags[$availableLanguage['flag_id']] ?? ''
                    ]) . ' ' . $availableLanguage['name'],
                    'url' => ['/main/change-language', 'id' => $availableLanguage['id']],
                    'rules' => [
                        [
                            'allow' => true,
                        ],
                    ]
                ];
            }

            Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_HEADER_RIGHT, [
                /*'update' => [
                    'icon' => 'update',
                    'label' => '<span class="badge red">{{count}}</span>',
                    'url' => ['/installer/index'],
                    'listItemOptions' => ['class' => 'full-update-btn']
                ],*/
                'cache' => [
                    'icon' => 'flash',
                    'label' => ['fafcms-core', 'Flush cache'],
                    'items' => $cacheItems
                ],
                'change-language' => [
                    'options' => ['class' => 'search selection'],
                    'label' => FlagIcon::widget([
                        'countryCode' => $flags[Yii::$app->fafcms->getCurrentLanguage()['flag_id']] ?? ''
                    ]) . ' ' . Yii::$app->fafcms->getTextOrTranslation(['fafcms-core', 'Change language']),
                    'items' => $languageSelection,
                    'rules' => [
                        [
                            'allow' => true,
                        ],
                    ]
                ],
                'user' => [
                    'label' => function() {
                        return '<div class="ui avatar image">' . Yii::$app->getUser()->getImage() . '</div>' . Yii::$app->getUser()->getName();
                    },
                    'items' => [
                        'profile' => [
                            'icon' => 'face-profile',
                            'label' => ['fafcms-core', 'Manage account'],
                            'url' => ['/user/change-account']
                        ],
                        'change-password' => [
                            'icon' => 'key',
                            'label' => ['fafcms-core', 'Change password'],
                            'url' => ['/user/change-password']
                        ],
                        'logout' => [
                            'icon' => 'lock',
                            'label' => ['fafcms-core', 'Logout'],
                            'url' => ['/user/logout']
                        ]
                    ]
                ],
            ]);

            /*Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_SIDEBAR_RIGHT, [
                'views' => [
                    'icon' => 'table-multiple',
                    'label' => ['fafcms-core', 'Views'],
                    'url' => '#',
                ],
                'edit-view' => [
                    'icon' => 'table-edit',
                    'label' => ['fafcms-core', 'Edit view'],
                    'url' => '#',
                ],
                'add-view' => [
                    'icon' => 'table-plus',
                    'label' => ['fafcms-core', 'Add view'],
                    'url' => '#',
                ],
                'notes' => [
                    'icon' => 'note-outline',
                    'label' => ['fafcms-core', 'Notes'],
                    'url' => '#',
                ],
                'history' => [
                    'icon' => 'history',
                    'label' => ['fafcms-core', 'History'],
                    'url' => '#',
                ],
                'favorite' => [
                    'icon' => 'star-outline',
                    'label' => ['fafcms-core', 'Mark as favorite'], // index als favorit anzeigen
                    'url' => '#',
                ],
                'bookmark' => [
                    'icon' => 'bookmark-outline',
                    'label' => ['fafcms-core', 'Save to bookmarks'], // global in bookmarks anzeigen
                    'url' => '#',
                ],
            ]);*/

            Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_FOOTER_LEFT, [
                'legal' => [
                    'label' => ['fafcms-core', 'Site Notice'],
                    'rules' => false
                ],
                'privacy' => [
                    'label' => ['fafcms-core', 'Privacy Policy'],
                    'rules' => false
                ]
            ]);

            Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_FOOTER_RIGHT, [
                'powered-by' => [
                    'label' => ['fafcms-core', 'Made with Finally a fast CMS &copy; {year}', ['year' => date('Y')]],
                    'rules' => false,
                    'url' => 'https://www.finally-a-fast.com/',
                ],
            ]);

            if (!Yii::$app->getUser()->getIsGuest() && Yii::$app->getUser()->can('accessBackend')) {
                Yii::$app->fafcms->setJsConfigValue('backend', $app->fafcms->getBackendUrl());
            }

            // TODO
            // Yii::$app->fafcms->setJsConfigValue('api', $app->fafcms->getApiUrl());
        } else {
            $app->fafcms->addSidebarItems('install', [
                'install' => [
                    'label' => 'Setup',
                    'url'   => 'setup'
                ]
            ]);
        }

        return true;
    }
}
