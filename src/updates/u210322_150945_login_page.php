<?php

namespace fafcms\fafcms\updates;

use fafcms\fafcms\updates\migrations\m210322_150945_login_page;
use fafcms\updater\base\Update;

/**
 * Class u210322_150945_login_page
 *
 * @package fafcms\fafcms\updates
 */
class u210322_150945_login_page extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        return $this->migrateUp(m210322_150945_login_page::class);
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        return $this->migrateDown(m210322_150945_login_page::class);
    }
}
