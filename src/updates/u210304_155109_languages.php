<?php

namespace fafcms\fafcms\updates;

use fafcms\fafcms\models\Language;
use fafcms\updater\base\Update;

/**
 * Class u210304_155109_languages
 *
 * @package fafcms\fafcms\updates
 */
class u210304_155109_languages extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        Language::updateAll(['name' => 'German (Italy)'], ['id' => '113']);
        Language::updateAll(['name' => 'German (Belgium)'], ['id' => '110']);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        Language::updateAll(['name' => 'German'], ['id' => '113']);
        Language::updateAll(['name' => 'German'], ['id' => '110']);

        return true;
    }
}
