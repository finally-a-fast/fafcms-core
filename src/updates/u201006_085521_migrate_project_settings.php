<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\updates;

use fafcms\fafcms\updates\migrations\m201005_121303_project_settings;
use fafcms\updater\base\Update;

/**
 * Class u201006_085521_migrate_project_settings
 *
 * @package fafcms\fafcms\updates
 */
class u201006_085521_migrate_project_settings extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        if (!$this->migrateUp(m201005_121303_project_settings::class)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        if (!$this->migrateDown(m201005_121303_project_settings::class)) {
            return false;
        }

        return true;
    }
}
