<?php

namespace fafcms\fafcms\updates;

use fafcms\fafcms\updates\migrations\m210312_075535_domain_project_language;
use fafcms\updater\base\Update;

/**
 * Class u210312_075535_domain_project_language
 *
 * @package fafcms\fafcms\updates
 */
class u210312_075535_domain_project_language extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        return $this->migrateUp(m210312_075535_domain_project_language::class);
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        return $this->migrateDown(m210312_075535_domain_project_language::class);
    }
}
