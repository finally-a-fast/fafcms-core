<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\updates;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\settingmanager\models\Setting;
use fafcms\settingmanager\Module as SettingmanagerModule;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\sitemanager\models\Layout;
use fafcms\sitemanager\models\Site;
use fafcms\updater\base\Update;
use Yii;
use yii\helpers\VarDumper;

/**
 * Class u201006_085550_rewrite_settings
 *
 * @package fafcms\fafcms\updates
 */
class u201006_085550_rewrite_settings extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        if (($currentProject = Yii::$app->fafcms->getCurrentProject()) !== null) {
            $currentProject->site_name = $currentProject->name;
            $currentProject->show_start_page_in_title = (int)SettingmanagerModule::getLoadedModule()->getSettingValue('', 'title_on_start_page', 0, 0, '', 0) !== 0 ? 1 : 0;
            $currentProject->reverse_title = (int)SettingmanagerModule::getLoadedModule()->getSettingValue('', 'title_direction', 0, 0, '', 2) === 2 ? 1 : 0;
            $currentProject->title_separator = SettingmanagerModule::getLoadedModule()->getSettingValue('', 'title_sperator', 0, 0, '', ' - ');
            $currentProject->default_layout_id = SettingmanagerModule::getLoadedModule()->getSettingValue('', 'default_layout', 0, 0, '', null);

            $startSiteId = SettingmanagerModule::getLoadedModule()->getSettingValue('', 'start_page', 0, 0, '');

            if ($startSiteId !== null) {
                $startContentmeta = Contentmeta::find()->where([
                    'model_class' => Site::class,
                    'model_id' => $startSiteId
                ])->one();

                if ($startContentmeta !== null) {
                    $currentProject->start_page_contentmeta_id = $startContentmeta->id;
                }
            }

            $errorSiteId = SettingmanagerModule::getLoadedModule()->getSettingValue('', 'error_page', 0, 0, '');

            if ($errorSiteId !== null) {
                $errorContentmeta = Contentmeta::find()->where([
                    'model_class' => Site::class,
                    'model_id' => $errorSiteId
                ])->one();

                if ($errorContentmeta !== null) {
                    $currentProject->error_page_contentmeta_id = $errorContentmeta->id;
                }
            }

            if (!$currentProject->save()) {
                VarDumper::dump($currentProject->getErrors());
                return false;
            }
        }

        Setting::deleteAll([
            'code' => [
                'title_on_start_page',
                'title_direction',
                'title_sperator',
                'default_layout',
                'start_page',
                'error_page'
            ]
        ]);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        if (($currentProject = Yii::$app->fafcms->getCurrentProject()) !== null) {
            SettingmanagerModule::getLoadedModule()->createOrUpdateSetting('', 'title_on_start_page', 0, 0, '', 'Titel auf der Startseite hinzufügen', '', FafcmsComponent::VALUE_TYPE_BOOL, $currentProject->show_start_page_in_title === true ? 1 : 0);
            SettingmanagerModule::getLoadedModule()->createOrUpdateSetting('', 'title_direction', 0, 0, '', 'Title Reihenfolge', '1 = forward, 2 = backward', FafcmsComponent::VALUE_TYPE_INT, $currentProject->reverse_title === true ? 2 : 1);
            SettingmanagerModule::getLoadedModule()->createOrUpdateSetting('', 'title_sperator', 0, 0, '', 'Title Trennzeichen', '', FafcmsComponent::VALUE_TYPE_VARCHAR, $currentProject->title_separator);
            SettingmanagerModule::getLoadedModule()->createOrUpdateSetting('', 'default_layout', 0, 0, '', 'Standart Layout', '', FafcmsComponent::VALUE_TYPE_MODEL, ['model' => Layout::class, 'id' => $currentProject->default_layout_id]);

            if ($currentProject->start_page_contentmeta_id !== null) {
                $startContentmeta = Contentmeta::find()->where([
                    'id' => $currentProject->start_page_contentmeta_id
                ])->one();

                if ($startContentmeta !== null) {
                    SettingmanagerModule::getLoadedModule()->createOrUpdateSetting('', 'start_page', 0, 0, '', 'Startseite', '', FafcmsComponent::VALUE_TYPE_MODEL, ['model' => $startContentmeta->model_class, 'id' => $startContentmeta->model_id]);
                }
            }

            if ($currentProject->error_page_contentmeta_id !== null) {
                $errorContentmeta = Contentmeta::find()->where([
                    'id' => $currentProject->error_page_contentmeta_id
                ])->one();

                if ($errorContentmeta !== null) {
                    SettingmanagerModule::getLoadedModule()->createOrUpdateSetting('', 'error_page', 0, 0, '', 'Fehlerseite', '', FafcmsComponent::VALUE_TYPE_MODEL, ['model' => $errorContentmeta->model_class, 'id' => $errorContentmeta->model_id]);
                }
            }
        }

        return true;
    }
}
