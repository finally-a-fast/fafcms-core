<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\updates\migrations;

use fafcms\fafcms\models\Project;
use fafcms\fafcms\models\Projectlanguage;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\sitemanager\models\Layout;
use fafcms\updater\base\Migration;

/**
 * Class m201005_121303_project_settings
 *
 * @package fafcms\fafcms\updates\migrations
 */
class m201005_121303_project_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $this->addColumn(Project::tableName(), 'site_name', $this->string(255)->null()->defaultValue(null)->after('description'));
        $this->addColumn(Project::tableName(), 'show_start_page_in_title', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('site_name'));
        $this->addColumn(Project::tableName(), 'show_site_name_in_title', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('show_start_page_in_title'));
        $this->addColumn(Project::tableName(), 'reverse_title', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('show_site_name_in_title'));
        $this->addColumn(Project::tableName(), 'title_separator', $this->string(255)->notNull()->defaultValue(' - ')->after('reverse_title'));
        $this->addColumn(Project::tableName(), 'start_page_contentmeta_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('title_separator'));
        $this->addColumn(Project::tableName(), 'error_page_contentmeta_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('start_page_contentmeta_id'));
        $this->addColumn(Project::tableName(), 'default_layout_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('error_page_contentmeta_id'));

        $this->createIndex('idx-project-start_page_contentmeta_id', Project::tableName(), ['start_page_contentmeta_id'], false);
        $this->createIndex('idx-project-error_page_contentmeta_id', Project::tableName(), ['error_page_contentmeta_id'], false);
        $this->createIndex('idx-project-default_layout_id', Project::tableName(), ['default_layout_id'], false);

        $this->addColumn(Projectlanguage::tableName(), 'site_name', $this->string(255)->null()->defaultValue(null)->after('description'));
        $this->addColumn(Projectlanguage::tableName(), 'show_start_page_in_title', $this->tinyInteger(1)->unsigned()->null()->defaultValue(null)->after('site_name'));
        $this->addColumn(Projectlanguage::tableName(), 'show_site_name_in_title', $this->tinyInteger(1)->unsigned()->null()->defaultValue(null)->after('show_start_page_in_title'));
        $this->addColumn(Projectlanguage::tableName(), 'reverse_title', $this->tinyInteger(1)->unsigned()->null()->defaultValue(null)->after('show_site_name_in_title'));
        $this->addColumn(Projectlanguage::tableName(), 'title_separator', $this->string(255)->null()->defaultValue(null)->after('reverse_title'));
        $this->addColumn(Projectlanguage::tableName(), 'start_page_contentmeta_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('title_separator'));
        $this->addColumn(Projectlanguage::tableName(), 'error_page_contentmeta_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('start_page_contentmeta_id'));
        $this->addColumn(Projectlanguage::tableName(), 'default_layout_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('error_page_contentmeta_id'));

        $this->createIndex('idx-projectlanguage-start_page_contentmeta_id', Projectlanguage::tableName(), ['start_page_contentmeta_id'], false);
        $this->createIndex('idx-projectlanguage-error_page_contentmeta_id', Projectlanguage::tableName(), ['error_page_contentmeta_id'], false);
        $this->createIndex('idx-projectlanguage-default_layout_id', Projectlanguage::tableName(), ['default_layout_id'], false);

        $this->addForeignKey('fk-project-start_page_contentmeta_id', Project::tableName(), 'start_page_contentmeta_id', Contentmeta::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-project-error_page_contentmeta_id', Project::tableName(), 'error_page_contentmeta_id', Contentmeta::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-project-default_layout_id', Project::tableName(), 'default_layout_id', Layout::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-projectlanguage-start_page_contentmeta_id', Projectlanguage::tableName(), 'start_page_contentmeta_id', Contentmeta::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-projectlanguage-error_page_contentmeta_id', Projectlanguage::tableName(), 'error_page_contentmeta_id', Contentmeta::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-projectlanguage-default_layout_id', Projectlanguage::tableName(), 'default_layout_id', Layout::tableName(), 'id', 'SET NULL', 'CASCADE');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->dropForeignKey('fk-project-start_page_contentmeta_id', Project::tableName());
        $this->dropForeignKey('fk-project-error_page_contentmeta_id', Project::tableName());
        $this->dropForeignKey('fk-project-default_layout_id', Project::tableName());

        $this->dropForeignKey('fk-projectlanguage-start_page_contentmeta_id', Projectlanguage::tableName());
        $this->dropForeignKey('fk-projectlanguage-error_page_contentmeta_id', Projectlanguage::tableName());
        $this->dropForeignKey('fk-projectlanguage-default_layout_id', Projectlanguage::tableName());

        $this->dropIndex('idx-project-start_page_contentmeta_id', Project::tableName());
        $this->dropIndex('idx-project-error_page_contentmeta_id', Project::tableName());
        $this->dropIndex('idx-project-default_layout_id', Project::tableName());

        $this->dropIndex('idx-projectlanguage-start_page_contentmeta_id', Projectlanguage::tableName());
        $this->dropIndex('idx-projectlanguage-error_page_contentmeta_id', Projectlanguage::tableName());
        $this->dropIndex('idx-projectlanguage-default_layout_id', Projectlanguage::tableName());

        $this->dropColumn(Project::tableName(), 'site_name');
        $this->dropColumn(Project::tableName(), 'show_start_page_in_title');
        $this->dropColumn(Project::tableName(), 'show_site_name_in_title');
        $this->dropColumn(Project::tableName(), 'reverse_title');
        $this->dropColumn(Project::tableName(), 'title_separator');
        $this->dropColumn(Project::tableName(), 'start_page_contentmeta_id');
        $this->dropColumn(Project::tableName(), 'error_page_contentmeta_id');
        $this->dropColumn(Project::tableName(), 'default_layout_id');

        $this->dropColumn(Projectlanguage::tableName(), 'site_name');
        $this->dropColumn(Projectlanguage::tableName(), 'show_start_page_in_title');
        $this->dropColumn(Projectlanguage::tableName(), 'show_site_name_in_title');
        $this->dropColumn(Projectlanguage::tableName(), 'reverse_title');
        $this->dropColumn(Projectlanguage::tableName(), 'title_separator');
        $this->dropColumn(Projectlanguage::tableName(), 'start_page_contentmeta_id');
        $this->dropColumn(Projectlanguage::tableName(), 'error_page_contentmeta_id');
        $this->dropColumn(Projectlanguage::tableName(), 'default_layout_id');

        return true;
    }
}
