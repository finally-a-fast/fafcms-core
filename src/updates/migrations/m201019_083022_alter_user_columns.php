<?php

namespace fafcms\fafcms\updates\migrations;

use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\models\User;
use fafcms\updater\base\Migration;

/**
 * Class m201019_083022_alter_user_columns
 */
class m201019_083022_alter_user_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $class     = InjectorComponent::getClass(User::class);
        $tableName = $class::tableName();

        $this->alterColumn($tableName, 'notifications_email', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0));
        $this->alterColumn($tableName, 'notificaitons_browser', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0));

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $class     = InjectorComponent::getClass(User::class);
        $tableName = $class::tableName();

        $this->alterColumn($tableName, 'notifications_email', $this->string(255)->notNull()->defaultValue('false'));
        $this->alterColumn($tableName, 'notificaitons_browser', $this->string(255)->notNull()->defaultValue('false'));

        return true;
    }
}
