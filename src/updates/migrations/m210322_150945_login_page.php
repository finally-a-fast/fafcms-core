<?php

namespace fafcms\fafcms\updates\migrations;

use fafcms\fafcms\models\Domain;
use fafcms\fafcms\models\Project;
use fafcms\fafcms\models\Projectlanguage;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\updater\base\Migration;

/**
 * Class m210322_150945_login_page
 *
 * @package fafcms\fafcms\updates\migrations
 */
class m210322_150945_login_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $this->addColumn(Project::tableName(), 'login_page_contentmeta_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('error_page_contentmeta_id'));
        $this->createIndex('idx-project-login_page_contentmeta_id', Project::tableName(), ['login_page_contentmeta_id'], false);
        $this->addForeignKey('fk-project-login_page_contentmeta_id', Project::tableName(), 'login_page_contentmeta_id', Contentmeta::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->addColumn(Projectlanguage::tableName(), 'login_page_contentmeta_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('error_page_contentmeta_id'));
        $this->createIndex('idx-projectlanguage-login_page_contentmeta_id', Projectlanguage::tableName(), ['login_page_contentmeta_id'], false);
        $this->addForeignKey('fk-projectlanguage-login_page_contentmeta_id', Projectlanguage::tableName(), 'login_page_contentmeta_id', Contentmeta::tableName(), 'id', 'SET NULL', 'CASCADE');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->dropForeignKey('fk-projectlanguage-login_page_contentmeta_id', Projectlanguage::tableName());
        $this->dropIndex('idx-projectlanguage-login_page_contentmeta_id', Projectlanguage::tableName());
        $this->dropColumn(Projectlanguage::tableName(), 'login_page_contentmeta_id');

        $this->dropForeignKey('fk-project-login_page_contentmeta_id', Project::tableName());
        $this->dropIndex('idx-project-login_page_contentmeta_id', Project::tableName());
        $this->dropColumn(Project::tableName(), 'login_page_contentmeta_id');

        return true;
    }
}
