<?php

namespace fafcms\fafcms\updates\migrations;

use fafcms\fafcms\models\Domain;
use fafcms\fafcms\models\Projectlanguage;
use fafcms\updater\base\Migration;

/**
 * Class m210312_075535_domain_project_language
 *
 * @package fafcms\fafcms\updates
 */
class m210312_075535_domain_project_language extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $this->addColumn(Domain::tableName(), 'projectlanguage_id', $this->integer(10)->unsigned()->null()->defaultValue(null)->after('project_id'));
        $this->createIndex('idx-domain-projectlanguage_id', Domain::tableName(), ['projectlanguage_id'], false);
        $this->addForeignKey('fk-domain-projectlanguage_id', Domain::tableName(), 'projectlanguage_id', Projectlanguage::tableName(), 'id', 'SET NULL', 'CASCADE');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->dropForeignKey('fk-domain-projectlanguage_id', Domain::tableName());
        $this->dropIndex('idx-domain-projectlanguage_id', Domain::tableName());
        $this->dropColumn(Domain::tableName(), 'projectlanguage_id');

        return true;
    }
}
