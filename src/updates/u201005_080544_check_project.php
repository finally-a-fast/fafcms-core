<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\updates;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\models\Domain;
use fafcms\fafcms\models\Language;
use fafcms\fafcms\models\Project;
use fafcms\fafcms\models\Projectlanguage;
use fafcms\fafcms\updates\migrations\m201005_121303_update;
use fafcms\settingmanager\models\Setting;
use fafcms\settingmanager\Module as SettingmanagerModule;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\sitemanager\models\Layout;
use fafcms\sitemanager\models\Site;
use fafcms\updater\base\Update;
use Yii;

/**
 * Class u201005_080544_check_project
 *
 * @package fafcms\fafcms\updates
 */
class u201005_080544_check_project extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        $projectCount = (int)Project::find()->count();

        if ($projectCount === 0) {
            $currentProject = new Project();
            $currentProject->name = SettingmanagerModule::getLoadedModule()->getSetting('app_name', Yii::t('fafcms-core', 'Default Project'));

            if (!$currentProject->save(false)) {
                throw new \Exception('Cannot save project: ' . print_r($currentProject->getErrors(), true));
            }

            $projectDomain = new Domain();
            $projectDomain->domain = 'localhost';
            $projectDomain->project_id = $currentProject->id;

            if (!$projectDomain->save(false)) {
                throw new \Exception('Cannot save project domain: ' . print_r($projectDomain->getErrors(), true));
            }

            $language = Language::find()->where([
                'ietfcode' => Yii::$app->language
            ])->one();

            $projectLanguage = new Projectlanguage();
            $projectLanguage->project_id = $currentProject->id;
            $projectLanguage->domain_id = $projectDomain->id;
            $projectLanguage->language_id = $language->id;
            $projectLanguage->path = '/';
            $projectLanguage->name = Yii::t('fafcms-core-language', $language->name);

            if (!$projectLanguage->save(false)) {
                throw new \Exception('Cannot save project language: ' . print_r($projectLanguage->getErrors(), true));
            }

            $currentProject->primary_projectlanguage_id = $projectLanguage->id;

            if (!$currentProject->save(false)) {
                throw new \Exception('Cannot update primary_projectlanguage_id of project: ' . print_r($currentProject->getErrors(), true));
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        return true;
    }
}
