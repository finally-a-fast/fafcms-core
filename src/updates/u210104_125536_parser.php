<?php

namespace fafcms\fafcms\updates;

use fafcms\sitemanager\models\Layout;
use fafcms\sitemanager\models\Site;
use fafcms\sitemanager\models\Snippet;
use fafcms\updater\base\Update;
use IvoPetkov\HTML5DOMElement;
use IvoPetkov\HTML5DOMDocument;

/**
 * Class u210104_125536_parser
 *
 * @package fafcms\fafcms\updates
 */
class u210104_125536_parser extends Update
{
    /**
     * {@inheritdoc}
     */
    public function up(): bool
    {
        $items = array_merge(
            Snippet::find()->all(),
            Site::find()->all(),
            Layout::find()->all(),
        );

        foreach ($items as $item) {
            if ($item->content === null) {
                continue;
            }

            $dom = new HTML5DOMDocument();
            $dom->loadHTML(
                '<!DOCTYPE html><html lang=""><body><temp-tag-fafcms-temp-tag>' .
                $this->getSafeHtml($item->content) .
                '</temp-tag-fafcms-temp-tag></body></html>',
                LIBXML_NONET | HTML5DOMDocument::ALLOW_DUPLICATE_IDS
            );

            $xPath = new \DOMXPath($dom);
            $domNodes = $xPath->query('//fafcms-get|//fafcms-set');

            foreach ($domNodes as $domNode) {
                /**
                 * @var \DOMNode $domNode
                 */
                foreach ($domNode->attributes as $attr) {
                    if (
                        ($domNode->nodeName === 'fafcms-get' && $attr->nodeName === 'format') ||
                        $attr->nodeValue[0] === '\'' ||
                        $attr->nodeValue[0] === '"' ||
                        $attr->nodeValue[0] === '.' ||
                        $attr->nodeValue[0] === 'true' ||
                        $attr->nodeValue[0] === 't' ||
                        $attr->nodeValue[0] === 'false' ||
                        $attr->nodeValue[0] === 'f' ||
                        is_numeric($attr->nodeValue[0])
                    ) {
                        continue;
                    }

                    $attr->nodeValue = '.' . $attr->nodeValue;
                }
            }

            $domNodes = $xPath->query('//fafcms-loop');

            foreach ($domNodes as $domNode) {
                /**
                 * @var \DOMNode $domNode
                 */
                foreach ($domNode->attributes as $attr) {
                    if ($attr->nodeName !== 'each' || $attr->nodeValue[0] === '\'' || $attr->nodeValue[0] === '"' || $attr->nodeValue[0] === '.') {
                        continue;
                    }

                    $attr->nodeValue = '.' . $attr->nodeValue;
                }
            }

            $domNodes = $xPath->query('//fafcms-json-encode');

            foreach ($domNodes as $domNode) {
                /**
                 * @var \DOMNode $domNode
                 */
                foreach ($domNode->attributes as $attr) {
                    if ($attr->nodeName !== 'data' || $attr->nodeValue[0] === '\'' || $attr->nodeValue[0] === '"' || $attr->nodeValue[0] === '.') {
                        continue;
                    }

                    $attr->nodeValue = '.' . $attr->nodeValue;
                }
            }

            $node = $xPath->query('//temp-tag-fafcms-temp-tag');
            $result = $node[0]->innerHTML;

            $result = str_ireplace(
                ['<temp-tag-fafcms-temp-tag-special>', '</temp-tag-fafcms-temp-tag-special>'],
                ['<!', '>'],
                $result
            );

            $result = str_ireplace(
                array_values($this->specialTagMap),
                array_keys($this->specialTagMap),
                $result
            );

            $item->content = $result;

            if (!$item->save(false)) {
                var_dump($item->getErrors());
                return false;
            }
        }

        return true;
    }

    private $specialTagMap = [
        '<body' => '<temp-tag-fafcms-temp-tag-body',
        '</body' => '</temp-tag-fafcms-temp-tag-body',
        '<head' => '<temp-tag-fafcms-temp-tag-head',
        '</head' => '</temp-tag-fafcms-temp-tag-head',
        '<html' => '<temp-tag-fafcms-temp-tag-html',
        '</html' => '</temp-tag-fafcms-temp-tag-html',
    ];

    protected function getSafeHtml(string $string): string
    {
        $string = preg_replace(
            '/<!(?<tag>[^\->]+)>/mi',
            '<temp-tag-fafcms-temp-tag-special>$1</temp-tag-fafcms-temp-tag-special>',
            $string
        );

        return str_ireplace(
            array_keys($this->specialTagMap),
            array_values($this->specialTagMap),
            $string
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down(): bool
    {
        return false;
    }
}
