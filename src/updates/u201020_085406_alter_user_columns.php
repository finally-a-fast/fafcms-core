<?php


namespace fafcms\fafcms\updates;


use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\models\User;
use fafcms\fafcms\updates\migrations\m201019_083022_alter_user_columns;
use fafcms\updater\base\Update;

/**
* Handles the Update
*/
class u201020_085406_alter_user_columns extends Update
{
    /**
    * {@inheritdoc}
    */
    public function up(): bool
    {
        $class = InjectorComponent::getClass(User::class);

        $class::updateAll(['notifications_email' => '1'], ['notifications_email' => 'true']);
        $class::updateAll(['notifications_email' => '0'], ['notifications_email' => 'false']);

        $class::updateAll(['notificaitons_browser' => '1'], ['notificaitons_browser' => 'true']);
        $class::updateAll(['notificaitons_browser' => '0'], ['notificaitons_browser' => 'false']);

        if ($this->migrateUp(m201019_083022_alter_user_columns::class)) {
            return true;
        }

        return false;
    }

    /**
    * {@inheritdoc}
    */
    public function down(): bool
    {
        if ($this->migrateDown(m201019_083022_alter_user_columns::class)) {
            $class = InjectorComponent::getClass(User::class);

            $class::updateAll(['notifications_email' => 'true'], ['notifications_email' => '1']);
            $class::updateAll(['notifications_email' => 'false'], ['notifications_email' => '0']);

            $class::updateAll(['notificaitons_browser' => 'true'], ['notificaitons_browser' => '1']);
            $class::updateAll(['notificaitons_browser' => 'false'], ['notificaitons_browser' => '0']);

            return true;
        }

        return false;
    }
}
