<?php

namespace fafcms\fafcms\assets;

use yii\web\AssetBundle;

/**
 * Class LuxonAsset
 *
 * @package fafcms\fafcms\assets
 */
class LuxonAsset extends AssetBundle
{
    public $sourcePath = '@npm/luxon/build/global';

    public $js = [
        'luxon.min.js'
    ];
}
