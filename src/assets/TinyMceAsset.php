<?php

namespace fafcms\fafcms\assets;

use yii\web\AssetBundle;

/**
 * Class TinyMceAsset
 *
 * @package fafcms\fafcms\assets
 */
class TinyMceAsset extends AssetBundle
{
    public $sourcePath = '@vendor/tinymce/tinymce';

    public $js = [
        'tinymce.min.js'
    ];
}
