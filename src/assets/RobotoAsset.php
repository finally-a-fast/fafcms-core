<?php

namespace fafcms\fafcms\assets;

use yii\web\AssetBundle;

/**
 * Class RobotoAsset
 *
 * @package fafcms\fafcms\assets
 */
class RobotoAsset extends AssetBundle
{
    public $sourcePath = '@bower/roboto-fontface';

    public $css = [
        'css/roboto/roboto-fontface.css'
    ];

    public $cssOptions = [
        'fafcmsAsync' => true
    ];
}
