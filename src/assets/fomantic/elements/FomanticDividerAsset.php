<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\fafcms\assets\fomantic\elements;

use fafcms\fafcms\assets\fomantic\globals\FomanticResetAsset;
use fafcms\helpers\classes\AssetComponentBundle;

/**
 * Class FomanticDividerAsset
 *
 * @package fafcms\fafcms\assets\fomantic
 */
class FomanticDividerAsset extends AssetComponentBundle
{
    public $sourcePath = '@runtime/fomantic-ui';

    public $css = [
        'components/divider.css',
        // 'definitions/elements/divider.css',
    ];

    public array $variables = [
        'elements/divider.variables'
    ];

    public $depends = [
        FomanticResetAsset::class,
    ];
}
