<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\fafcms\assets\fomantic\elements;

use fafcms\fafcms\assets\fomantic\globals\FomanticResetAsset;
use fafcms\helpers\classes\AssetComponentBundle;

/**
 * Class FomanticPlaceholderAsset
 *
 * @package fafcms\fafcms\assets\fomantic
 */
class FomanticPlaceholderAsset extends AssetComponentBundle
{
    public $sourcePath = '@runtime/fomantic-ui';

    public $css = [
        'components/placeholder.css',
        // 'definitions/elements/placeholder.css',
    ];

    public array $variables = [
        'elements/placeholder.variables'
    ];

    public $depends = [
        FomanticResetAsset::class,
    ];
}
