<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\fafcms\assets\fomantic\modules;

use fafcms\fafcms\assets\fomantic\elements\FomanticHeaderAsset;
use fafcms\fafcms\assets\fomantic\globals\FomanticResetAsset;
use fafcms\helpers\classes\AssetComponentBundle;
use yii\web\JqueryAsset;

/**
 * Class FomanticModalAsset
 *
 * @package fafcms\fafcms\assets\fomantic
 */
class FomanticModalAsset extends AssetComponentBundle
{
    public $sourcePath = '@runtime/fomantic-ui';

    public $js = [
        'components/modal.js',
        // 'definitions/modules/modal.js',
    ];

    public $css = [
        'components/modal.css',
        // 'definitions/modules/modal.css',
    ];

    public array $variables = [
        'modules/modal.variables'
    ];

    public $depends = [
        JqueryAsset::class,
        FomanticResetAsset::class,
        FomanticHeaderAsset::class,
    ];
}
