<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\fafcms\assets\fomantic\modules;

use fafcms\fafcms\assets\fomantic\globals\FomanticResetAsset;
use fafcms\helpers\classes\AssetComponentBundle;
use yii\web\JqueryAsset;

/**
 * Class FomanticSidebarAsset
 *
 * @package fafcms\fafcms\assets\fomantic
 */
class FomanticSidebarAsset extends AssetComponentBundle
{
    public $sourcePath = '@runtime/fomantic-ui';

    public $js = [
        'components/sidebar.js',
        // 'definitions/modules/sidebar.js',
    ];

    public $css = [
        'components/sidebar.css',
        // 'definitions/modules/sidebar.css',
    ];

    public array $variables = [
        'modules/sidebar.variables'
    ];

    public $depends = [
        JqueryAsset::class,
        FomanticResetAsset::class,
    ];
}
