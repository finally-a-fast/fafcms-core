<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\fafcms\assets\fomantic\modules;

use fafcms\fafcms\assets\fomantic\globals\FomanticResetAsset;
use fafcms\helpers\classes\AssetComponentBundle;
use yii\web\JqueryAsset;

/**
 * Class FomanticSearchAsset
 *
 * @package fafcms\fafcms\assets\fomantic
 */
class FomanticSearchAsset extends AssetComponentBundle
{
    public $sourcePath = '@runtime/fomantic-ui';

    public $js = [
        'components/search.js',
        // 'definitions/modules/search.js',
    ];

    public $css = [
        'components/search.css',
        // 'definitions/modules/search.css',
    ];

    public array $variables = [
        'modules/search.variables'
    ];

    public $depends = [
        JqueryAsset::class,
        FomanticResetAsset::class,
    ];
}
