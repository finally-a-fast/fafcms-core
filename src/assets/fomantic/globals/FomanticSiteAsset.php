<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\fafcms\assets\fomantic\globals;

use fafcms\helpers\classes\AssetComponentBundle;
use yii\web\JqueryAsset;

/**
 * Class FomanticSiteAsset
 *
 * @package fafcms\fafcms\assets\fomantic
 */
class FomanticSiteAsset extends AssetComponentBundle
{
    public $sourcePath = '@runtime/fomantic-ui';

    public $js = [
        'components/site.js',
        // 'definitions/globals/site.js',
    ];

    public $css = [
        'components/site.css',
        // 'definitions/globals/site.css',
    ];

    public array $variables = [
        'globals/site.variables'
    ];

    public $depends = [
        JqueryAsset::class
    ];
}
