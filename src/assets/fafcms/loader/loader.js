window.faf = Object.assign(window.faf || {}, {
  events: {
    'scroll': [],
    'ready': [],
    'windowLoad': [],
    'windowBeforeUnload': [],
    'windowFocus': [],
    'windowBlur': [],
    'windowUnload': [],
    'windowDragenter': [],
    'windowResize': [],
    'init': [],
    'initPlugins': [],
  },
  components: {
    store: [],
    count: 0,
    register: function (name, requires, className) {
      window.faf.components.count++

      window.faf.components.store[name] = Object.assign(window.faf.components.store[name] || {}, {
        'requires': requires,
      })

      if (typeof className !== 'undefined') {
        window.faf.components.store[name].className = className
      }
    },
    loaded: function (name) {
      window.faf.components.store[name].loaded = true

      if (name === 'FafcmsApp') {
        window.faf.app = new window[window.faf.components.store[name].className]()
        window.faf.app.init()
      } else if (typeof window.faf.app !== 'undefined') {
        window.faf.app.componentLoaded(name)
      }
    },
  },
})
