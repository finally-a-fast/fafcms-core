function FafcmsApp () {
  let cmsComponent = this
  let allComponentsLoaded = false
  let loadedComponentCount = 0

  let checkAllComponents = function () {
    for (let componentName in window.faf.components.store) {
      if (
        componentName !== 'FafcmsApp' &&
        typeof window.faf.components.store[componentName].loaded !== 'undefined' &&
        window.faf.components.store[componentName].loaded === true
      ) {
        checkComponent(componentName)
      }
    }
  }

  let checkIfAllComponentsAreLoaded = function () {
    if (!allComponentsLoaded && loadedComponentCount === window.faf.components.count - 1) {
      allComponentsLoaded = true

      addEventHandler()

      cmsComponent.runEvents(window.faf.events, 'init')
    }
  }

  let domLoaded = function (e) {
    if (window.faf.events['ready'].length > 0) {
      cmsComponent.runEvents(window.faf.events, 'ready', e)
    }

    if (window.faf.events['initPlugins'].length > 0) {
      cmsComponent.runEvents(window.faf.events, 'initPlugins', document.getElementsByTagName('html'))
    }

    document.addEventListener('pjax:success', function (e) {
      cmsComponent.runEvents(window.faf.events, 'initPlugins', e.target)
    })
  }

  let addEventHandler = function () {
    window.addEventListener('scroll', function (e) {//todo change with request animation frame
      cmsComponent.runEvents(window.faf.events, 'scroll', e)
    })

    if (document.readyState === 'loading') {
      document.addEventListener('DOMContentLoaded', domLoaded)
    } else {
      domLoaded()
    }

    window.addEventListener('dragenter', function (e) {
      cmsComponent.runEvents(window.faf.events, 'windowDragenter', e)
    })

    window.addEventListener('resize', function (e) {
      cmsComponent.runEvents(window.faf.events, 'windowResize', e)
    })

    window.addEventListener('load', function (e) {
      cmsComponent.runEvents(window.faf.events, 'windowLoad', e)
    })

    window.addEventListener('beforeunload', function (e) {
      cmsComponent.runEvents(window.faf.events, 'windowBeforeUnload', e)
    })

    window.addEventListener('unload', function (e) {
      cmsComponent.runEvents(window.faf.events, 'windowUnload', e)
    })

    window.addEventListener('focus', function (e) {
      cmsComponent.runEvents(window.faf.events, 'windowFocus', e)
    })

    window.addEventListener('blur', function (e) {
      cmsComponent.runEvents(window.faf.events, 'windowBlur', e)
    })
  }

  let checkComponent = function (name) {
    let requirementsLoaded = true

    if (window.faf.components.store[name].requires.length > 0) {
      for (let requiredIndex in window.faf.components.store[name].requires) {
        let required = window.faf.components.store[name].requires[requiredIndex]

        if (typeof window.faf.components.store[required] === 'undefined') {
          window.faf.components.store[required] = {}
        }

        if (typeof window.faf.components.store[required].requiredBy === 'undefined') {
          window.faf.components.store[required].requiredBy = {}
        }

        window.faf.components.store[required].requiredBy[name] = true

        if (
          required !== 'FafcmsApp' &&
          (
            typeof window.faf.components.store[required].initialized === 'undefined' ||
            window.faf.components.store[required].initialized === false
          )
        ) {
          requirementsLoaded = false
        }
      }
    }

    if (
      requirementsLoaded &&
      (
        typeof window.faf.components.store[name].initialized === 'undefined' ||
        window.faf.components.store[name].initialized === false
      )
    ) {
      loadedComponentCount++
      window.faf.components.store[name].initialized = true

      if (
        typeof window.faf.components.store[name].className !== 'undefined' &&
        typeof window[window.faf.components.store[name].className] !== 'undefined'
      ) {
        window.faf[name] = new window[window.faf.components.store[name].className]()

        if (typeof window.faf[name].init !== 'undefined') {
          window.faf[name].init()
        }
      }

      for (let requriedName in window.faf.components.store[name].requiredBy) {
        checkComponent(requriedName)
      }

      checkIfAllComponentsAreLoaded()
    }
  }

  cmsComponent.init = function () {
    checkAllComponents()
  }

  cmsComponent.runEvents = function (events, eventType, e) {
    if (typeof events[eventType] !== 'undefined' && events[eventType].length > 0) {
      events[eventType].forEach(function (eventAction) {
        if (typeof eventAction === 'function') {
          eventAction(e)
        } else {
          console.warn('Event of type ' + eventType + ' is not a function.', eventAction)
        }
      })
    }
  }

  cmsComponent.findChildren = function(elements, selector) {
    let children = [];

    if (typeof elements.length === 'undefined') {
      elements = [elements]
    }

    for (let i = 0; i < elements.length; i++) {
      let childElements = elements[i].querySelectorAll(selector)

      for (let x = 0; x < childElements.length; x++) {
        if (!children.includes(childElements[x])) {
          children.push(childElements[x])
        }
      }
    }

    return children;
  }

  cmsComponent.checkForAjaxDupes = function (target) {
    let elements = cmsComponent.findChildren(target, '.check-ajax-dupe')

    for (let i = 0; i < elements.length; i++) {
      let scriptSrc = elements[i].dataset.src
      let noSrc = typeof scriptSrc === 'undefined'

      if (noSrc || document.querySelector('script[src="' + scriptSrc + '"]') === null) {
        var newScript = document.createElement('script')
        let attributes = Array.prototype.slice.call(elements[i].attributes)
        let attr

        while (attr = attributes.pop()) {
          newScript.setAttribute(attr.nodeName, attr.nodeValue)
        }

        newScript.classList.remove('check-ajax-dupe')

        if (noSrc) {
          newScript.innerHTML = elements[i].innerHTML + '()'
        } else {
          newScript.removeAttribute('data-src')
          newScript.src = scriptSrc
        }

        elements[i].after(newScript)
        console.warn('dupe check new', newScript)
      }

      elements[i].remove()
    }
  }

  window.faf.events['initPlugins'].push(function (target) {
    cmsComponent.checkForAjaxDupes(target)
  })

  cmsComponent.componentLoaded = function (name) {
    checkComponent(name)
  }

  cmsComponent.t = function (category, message, params, language) {
    let translatedMessage = message;

    if (typeof language === 'undefined') {
      language =  document.documentElement.lang
    }

    if (
      typeof window.faf.translations !== 'undefined' &&
      typeof window.faf.translations[language] !== 'undefined' &&
      typeof window.faf.translations[language][category] !== 'undefined' &&
      typeof window.faf.translations[language][category][message] !== 'undefined'
    ) {
      translatedMessage = window.faf.translations[language][category][message];
    }

    if (typeof params !== 'undefined') {
      Object.keys(params).map(function (key) {
        // TODO replace with cleaner way
        // https://stackoverflow.com/a/6969486/4223982

        let escapedParam = key.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&')
        let regExp = new RegExp('\\\{' + escapedParam + '\\\}', 'g')
        translatedMessage = translatedMessage.replace(regExp, params[key])
      })
    }

    return translatedMessage
  }
}
