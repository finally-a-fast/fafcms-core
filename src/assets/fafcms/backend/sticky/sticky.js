function FafcmsSticky() {
  let cmsModule = this

  let initStickies = function(target) {
    let elements = window.faf.app.findChildren(target, '.sticky:not(.sticky-initialized)')

    for (let i = 0; i < elements.length; i++) {
      let options = {}
      let dataset = elements[i].dataset

      if (typeof dataset.context !== 'undefined') {
        options.context = dataset.context
      }

      if (typeof dataset.offset !== 'undefined') {
        options.offset = parseInt(dataset.offset, 10)
      }

      if (typeof dataset.scrollContext !== 'undefined') {
        options.scrollContext = dataset.scrollContext
      }

      elements[i].classList.add('sticky-initialized')
      $(elements[i]).sticky(options)
    }
  }

  cmsModule.init = function () {
    initStickies(document.getElementsByTagName('html'))
  }

  window.faf.events['initPlugins'].push(function (target) {
    initStickies(target)
  })
}
