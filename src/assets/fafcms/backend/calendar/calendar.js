function FafcmsCalendar() {
  let cmsModule = this
  let fafApp = window.faf.app

  let initDropdowns = function(target) {
    let elements = fafApp.findChildren(target, '.ui.calendar:not(.popup):not(.calendar-initialized)')

    for (let i = 0; i < elements.length; i++) {
      let options = {}
      let dataset = elements[i].dataset

      options.today = true

      if (typeof dataset.today !== 'undefined') {
        options.today = dataset.today === 'true'
      }

      options.showWeekNumbers = true

      if (typeof dataset.showWeekNumbers !== 'undefined') {
        options.showWeekNumbers = dataset.showWeekNumbers === 'true'
      }

      options.ampm = false

      if (typeof dataset.ampm !== 'undefined') {
        options.ampm = dataset.ampm === 'true'
      }

      if (typeof dataset.type !== 'undefined') {
        options.type = dataset.type
      }

      let formatter = {}

      if (typeof dataset.timeFormat !== 'undefined') {
        formatter.time = function (date, settings, forCalendar) {
          if (!date) {
            return ''
          }

          return luxon.DateTime.fromJSDate(date).toFormat(dataset.timeFormat)
        }
      }

      if (typeof dataset.dateTimeFormat !== 'undefined') {
        formatter.datetime = function (date, settings) {
          if (!date) {
            return '';
          }

          if (settings.type === 'datetime') {
            return luxon.DateTime.fromJSDate(date).toFormat(dataset.dateTimeFormat)
          } else if (settings.type === 'time') {
            return settings.formatter.time(date, settings, false)
          }

          return settings.formatter.date(date, settings)
        }
      }

      if (typeof dataset.dateFormat !== 'undefined') {
        formatter.date = function (date, settings) {
          if (!date) {
            return ''
          }

          return luxon.DateTime.fromJSDate(date).toFormat(dataset.dateFormat)
        }
      }

      if (Object.keys(formatter).length > 0) {
        options.formatter = formatter
      }

      options.text = {
        days: [
          fafApp.t('fafcms-core-date', 'SUNDAY_ONE_LETTER'),
          fafApp.t('fafcms-core-date', 'SUNDAY_ONE_LETTER'),
          fafApp.t('fafcms-core-date', 'MONDAY_ONE_LETTER'),
          fafApp.t('fafcms-core-date', 'TUESDAY_ONE_LETTER'),
          fafApp.t('fafcms-core-date', 'WEDNESDAY_ONE_LETTER'),
          fafApp.t('fafcms-core-date', 'THURSDAY_ONE_LETTER'),
          fafApp.t('fafcms-core-date', 'FRIDAY_ONE_LETTER'),
          fafApp.t('fafcms-core-date', 'SATURDAY_ONE_LETTER'),
        ],
        months: [
          fafApp.t('fafcms-core-date', 'JANUARY_FULL'),
          fafApp.t('fafcms-core-date', 'FEBRUARY_FULL'),
          fafApp.t('fafcms-core-date', 'MARCH_FULL'),
          fafApp.t('fafcms-core-date', 'APRIL_FULL'),
          fafApp.t('fafcms-core-date', 'MAY_FULL'),
          fafApp.t('fafcms-core-date', 'JUNE_FULL'),
          fafApp.t('fafcms-core-date', 'JULY_FULL'),
          fafApp.t('fafcms-core-date', 'AUGUST_FULL'),
          fafApp.t('fafcms-core-date', 'SEPTEMBER_FULL'),
          fafApp.t('fafcms-core-date', 'OCTOBER_FULL'),
          fafApp.t('fafcms-core-date', 'NOVEMBER_FULL'),
          fafApp.t('fafcms-core-date', 'DECEMBER_FULL'),
        ],
        monthsShort: [
          fafApp.t('fafcms-core-date', 'JANUARY_THREE_LETTER'),
          fafApp.t('fafcms-core-date', 'FEBRUARY_THREE_LETTER'),
          fafApp.t('fafcms-core-date', 'MARCH_THREE_LETTER'),
          fafApp.t('fafcms-core-date', 'APRIL_THREE_LETTER'),
          fafApp.t('fafcms-core-date', 'MAY_THREE_LETTER'),
          fafApp.t('fafcms-core-date', 'JUNE_THREE_LETTER'),
          fafApp.t('fafcms-core-date', 'JULY_THREE_LETTER'),
          fafApp.t('fafcms-core-date', 'AUGUST_THREE_LETTER'),
          fafApp.t('fafcms-core-date', 'SEPTEMBER_THREE_LETTER'),
          fafApp.t('fafcms-core-date', 'OCTOBER_THREE_LETTER'),
          fafApp.t('fafcms-core-date', 'NOVEMBER_THREE_LETTER'),
          fafApp.t('fafcms-core-date', 'DECEMBER_THREE_LETTER'),
        ],
        today: fafApp.t('fafcms-core-date', 'SELECT_TODAY'),
        now: fafApp.t('fafcms-core-date', 'SELECT_NOW'),
        am: fafApp.t('fafcms-core-date', 'TIME_AM'),
        pm: fafApp.t('fafcms-core-date', 'TIME_PM'),
        weekNo: fafApp.t('fafcms-core-date', 'WEEK_NO')
      }

      elements[i].classList.add('calendar-initialized')
      $(elements[i]).calendar(options)
    }
  }

  cmsModule.init = function () {
    initDropdowns(document.getElementsByTagName('html'))
  }

  window.faf.events['initPlugins'].push(function (target) {
    initDropdowns(target)
  })
}
