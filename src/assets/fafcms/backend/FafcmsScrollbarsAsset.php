<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\fafcms\assets\fafcms\backend;

use fafcms\helpers\classes\AssetComponentBundle;
use yiiui\yii2overlayscrollbars\assets\OverlayScrollbarsAsset;

/**
 * Class FafcmsScrollbarsAsset
 *
 * @package fafcms\fafcms\assets\fafcms
 */
class FafcmsScrollbarsAsset extends AssetComponentBundle
{
    public $sourcePath = __DIR__ . '/scrollbars';

    public $js = [
        'scrollbars.js',
    ];

    public $css = [
        'scrollbars.scss',
    ];

    public $depends = [
        OverlayScrollbarsAsset::class
    ];
}
