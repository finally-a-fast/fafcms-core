function FafcmsScrollbars () {
  let initSidebars = function(target) {
    let scrollbars = window.faf.app.findChildren(target, '.fafcms-scrollbars')

    for (let i = 0; i < scrollbars.length; i++) {
      let scrollbar = scrollbars[i]
      let readyEvent = new Event('fafcms-scrollbar-ready')
      let sizeChangeEvent = new Event('fafcms-scrollbar-size-change')
      let contentSizeChangeEvent = new Event('fafcms-scrollbar-content-size-change')
      let hostSizeChangeEvent = new Event('fafcms-scrollbar-host-size-change')

      let callbacks = {
        onInitialized: function (e) {
          let scrollbarHost = this.getElements().host
          scrollbarHost.classList.add('fafcms-scrollbar-ready')
          scrollbarHost.dispatchEvent(readyEvent)
        },
        onContentSizeChanged: function () {
          let scrollbarHost = this.getElements().host
          scrollbarHost.dispatchEvent(sizeChangeEvent)
          scrollbarHost.dispatchEvent(contentSizeChangeEvent)
        },
        onHostSizeChanged: function () {
          let scrollbarHost = this.getElements().host
          scrollbarHost.dispatchEvent(sizeChangeEvent)
          scrollbarHost.dispatchEvent(hostSizeChangeEvent)
        },
      }

      /* todo scrollbars on maincontainer, currently the sticky tabs are not working
      if (scrollbars[i].classList.contains('content-main-container')) {
        callbacks.onScroll = function(e) {
          fafcms.prototype.runEvents(fafcms.prototype.events, 'scroll', e)
        }
      }
      */

      OverlayScrollbars(scrollbar, {
        className: 'os-theme-fafcms',
        paddingAbsolute: true,
        scrollbars: {
          clickScrolling: true,
        },
        callbacks: callbacks,
      })
    }
  }

  window.faf.events['initPlugins'].push(function (target) {
    initSidebars(target)
  })

  window.faf.events['windowLoad'].push(function () {
    initSidebars(document.getElementsByTagName('html'))

    document.querySelector('.content-main-container').addEventListener('scroll', function () {
      let scrollEvent = new Event('scroll')
      window.dispatchEvent(scrollEvent)
    })
  })
}
