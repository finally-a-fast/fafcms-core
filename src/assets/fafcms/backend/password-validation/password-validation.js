function FafcmsPasswordValidation() {
  let cmsModule = this

  cmsModule.isEmpty = function(value, trim) {
    return value === null || value === undefined || value.length === 0 || trim && value.trim() === ''
  }

  cmsModule.addMessage = function(messages, message, value) {
    let val = this.isEmpty(value) ? 'none' : value
    messages.push(message.replace(/\{value}/g, val))
  }

  cmsModule.addError = function(messages, message, valueRequired, valueFound) {
    let self = this
    let msg = message.replace(/\{found}/g, valueFound)

    self.addMessage(messages, msg, valueRequired)
  }

  cmsModule.findPatterns = function(str) {
    let self = this, isEmpty = self.isEmpty
    let lower = str.match(/[a-z]/g)
    let upper = str.match(/[A-Z]/g)
    let digit = str.match(/\d/g)
    let special = str.match(/\W/g)

    return {
      lower: isEmpty(lower) ? 0 : lower.length,
      upper: isEmpty(upper) ? 0 : upper.length,
      digit: isEmpty(digit) ? 0 : digit.length,
      special: isEmpty(special) ? 0 : special.length
    }
  }

  cmsModule.compare = function(from, operator, to) {
    let chk = (from !== undefined) && (to !== undefined)

    if (operator === '<') {
      return chk && (from < to)
    }

    if (operator === '>') {
      return chk && (from > to)
    }

    return chk && (from === to)
  }

  cmsModule.validate = function(value, messages, options) {
    let self = this, compare = self.compare

    if (self.isEmpty(value)) {
      return
    }

    if (typeof value !== 'string') {
      self.addMessage(messages, options.strError, value)
      return
    }

    let patterns = self.findPatterns(value), len = value.length || 0, username = document.querySelector(options.userField).value

    if (compare(len, '<', options.min)) {
      self.addError(messages, options.minError, options.min, len)
    }

    if (compare(len, '>', options.max)) {
      self.addError(messages, options.maxError, options.max, len)
    }

    if (compare(len, '>', options['length'])) {
      self.addError(messages, options.lengthError, options['length'], len)
    }

    if (options.allowSpaces === false && value.indexOf(' ') !== -1) {
      self.addMessage(messages, options.allowSpacesError, value)
    }

    if (options.hasUser && username && value.toLowerCase().match(username.toLowerCase())) {
      self.addMessage(messages, options.hasUserError, value)
    }

    if (options.hasEmail && value.match(/^([\w!#$%&'\*\+\-\/=\?\^`{\|}~]+\.)*[\w!#$%&'\*\+\-\/=\?\^`{\|}~]+@((((([a-z0-9]{1}[a-z0-9\-]{0,62}[a-z0-9]{1})|[a-z])\.)+[a-z]{2,6})|(\d{1,3}\.){3}\d{1,3}(:\d{1,5})?)$/i)) {
      self.addMessage(messages, options.hasEmailError, value)
    }

    if (compare(patterns.lower, '<', options.lower)) {
      self.addError(messages, options.lowerError, options.lower, patterns.lower)
    }

    if (compare(patterns.upper, '<', options.upper)) {
      self.addError(messages, options.upperError, options.upper, patterns.upper)
    }

    if (compare(patterns.digit, '<', options.digit)) {
      self.addError(messages, options.digitError, options.digit, patterns.digit)
    }

    if (compare(patterns.special, '<', options.special)) {
      self.addError(messages, options.specialError, options.special, patterns.special)
    }
  }
}
