function FafcmsForm() {
  let cmsModule = this
  let fafToast = window.faf.FafcmsToast
  let fafApp = window.faf.app
  let fafAjaxModal = window.faf.FafcmsAjaxModal

  cmsModule.removeLoaderFromButtons = function(submitButtons) {
    for (let i = 0; i < submitButtons.length; i++) {
      submitButtons[i].classList.remove('disabled')
      submitButtons[i].classList.remove('loading')
    }
  }

  cmsModule.addLoaderToButtons = function(submitButtons) {
    for (let i = 0; i < submitButtons.length; i++) {
      submitButtons[i].classList.add('disabled')
      submitButtons[i].classList.add('loading')
    }
  }

  cmsModule.dynamicActions = []

  cmsModule.handleAjaxResponse = function(response) {
    if (typeof response === 'string') {
      response = JSON.parse(response)
    }

    if (typeof response.message !== 'undefined' && response.message !== '') {
      fafToast.toast({message: response.message, type: response.type, displayTime: (typeof response.timeout !== 'undefined' ? response.timeout : 'auto')})
    }

    if (typeof response.unload !== 'undefined') {
      for (let i in response.unload) {
        $(response.unload[i]).attr('data-loaded', false).data('loaded', false)
      }
    }

    if (response.close === true) {
      $('#ajax-modal').modal('close')
    } else if (response.reload === true) {
      window.location.reload()
    }

    if (typeof response.url !== 'undefined') {
      window.location.href = response.url
    }

    return response
  }

  cmsModule.sendAjaxForm = function(form, submitButtons) {
    let $form = $(form)
    let beforeSubmit = form.dataset.beforeSubmit
    let afterSubmit = form.dataset.afterSubmit

    if (typeof beforeSubmit !== 'undefined' && !cmsModule.dynamicActions[beforeSubmit]()) {
      cmsModule.removeLoaderFromButtons(submitButtons)
      return false
    }

    $.ajax({
      url: form.getAttribute('action'),
      headers: {
        'X-Ie-Redirect-Compatibility': true
      },
      type: 'post',
      data: $form.serialize(),
      error: function(e) {
        cmsModule.removeLoaderFromButtons(submitButtons)

        if (typeof e.responseJSON !== 'undefined' && typeof e.responseJSON.message !== 'undefined') {
          if (e.status === 403 && e.responseJSON.code === 42) {
            fafAjaxModal.openModal(JSON.parse(e.responseJSON.message))
            return false
          }

          fafToast.toast({message: e.responseJSON.message, type: 'error'})
        } else {
          fafToast.toast({message: fafApp.t('fafcms-core', 'An error has occurred, please try again.'), type: 'error'})
        }
      },
      success: function (response) {
        formSaved(form)
        cmsModule.removeLoaderFromButtons(submitButtons)

        if (typeof afterSubmit !== 'undefined' && !cmsModule.dynamicActions[afterSubmit]()) {
          return false
        }

        cmsModule.handleAjaxResponse(response)
      }
    })
  }

  let initAjaxForm = function(target) {
    let elements = window.faf.app.findChildren(target, 'form:not(.fafcms-ajax-form-initialized)')

    for (let i = 0; i < elements.length; i++) {
      elements[i].addEventListener('submit', function (e) {
        let form = this
        e.preventDefault()

        let submitButtons = []
        let insideSubmitButtons = form.querySelectorAll('button[type="submit"], input[type="submit"]')
        let outsideSubmitButtons = document.querySelectorAll('button[type="submit"][form="' + form.getAttribute('id') + '"')

        for (let x = 0; x < insideSubmitButtons.length; x++) {
          submitButtons.push(insideSubmitButtons[x])
        }

        for (let x = 0; x < outsideSubmitButtons.length; x++) {
          submitButtons.push(outsideSubmitButtons[x])
        }

        if (!form.classList.contains('no-loader')) {
          cmsModule.addLoaderToButtons(submitButtons)
        }

        if (form.classList.contains('ajax-form')) {
          e.preventDefault()
          cmsModule.sendAjaxForm(form, submitButtons)
          return false
        } else {
          formSaved(form)
        }
      }, true)

      elements[i].classList.add('fafcms-ajax-form-initialized')
    }
  }

  let formSaved = function (target) {
    let elements = target.querySelectorAll('.fafcms-has-unsaved-data')

    for (let i = 0; i < elements.length; i++) {
      elements[i].classList.remove('fafcms-has-unsaved-data')
    }

    window.faf.FafcmsBackendApp.hasChanges = false
  }

  let initChangeWatcher = function(target) {
    let elements = window.faf.app.findChildren(target, 'form input:not(.fafcms-change-watcher-initialized), form textarea:not(.fafcms-change-watcher-initialized), form select:not(.fafcms-change-watcher-initialized)')

    for (let i = 0; i < elements.length; i++) {
      elements[i].addEventListener('change', function (e) {
        let input = this
        e.preventDefault()

        input.classList.add('fafcms-has-unsaved-data')
        window.faf.FafcmsBackendApp.hasChanges = true
      })

      elements[i].classList.add('fafcms-change-watcher-initialized')
    }
  }

  cmsModule.init = function () {
    //todo check why form is always submitted when the jquery listener is removed
    $(document).on('beforeSubmit', 'form', function (e) {
      if ($(this).hasClass('ajax-form')) {
        e.preventDefault()
        return false
      }
    })

    initAjaxForm(document.getElementsByTagName('html'))
    initChangeWatcher(document.getElementsByTagName('html'))
  }

  window.faf.events['initPlugins'].push(function (target) {
    initAjaxForm(target)
    initChangeWatcher(target)
  })
}
