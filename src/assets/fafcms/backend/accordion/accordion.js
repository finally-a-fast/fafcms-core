function FafcmsAccordion() {
    let cmsModule = this

    cmsModule.init = function () {
        $('.ui.accordion:not(.ui.accordion .ui.accordion)').accordion()
    }
}
