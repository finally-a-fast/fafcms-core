function FafcmsActionBar() {
  let cmsModule = this

  let initActionToggler = function(target) {
    let elements = window.faf.app.findChildren(target, '.toggle-action-bar:not(.toggle-action-bar-initialized)')

    for (let i = 0; i < elements.length; i++) {
      elements[i].addEventListener('click', function (e) {
        this.blur()

        e.preventDefault()
        let actionBar = document.getElementsByClassName('action-bar')[0]

        if (actionBar.classList.contains('active')) {
          actionBar.classList.remove('active')
        } else {
          actionBar.classList.add('active')
        }
      })

      elements[i].classList.add('toggle-action-bar-initialized')
    }
  }

  cmsModule.init = function () {
    initActionToggler(document.getElementsByTagName('html'))
  }

  window.faf.events['initPlugins'].push(function (target) {
    initActionToggler(target)
  })
}
