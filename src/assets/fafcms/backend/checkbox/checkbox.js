function FafcmsCheckbox() {
  let cmsModule = this

  let initCheckboxes = function(target) {
    let elements = window.faf.app.findChildren(target, '.ui.checkbox')

    for (let i = 0; i < elements.length; i++) {
      $(elements[i]).checkbox()
    }
  }

  cmsModule.init = function () {
    initCheckboxes(document.getElementsByTagName('html'))
  }

  window.faf.events['initPlugins'].push(function (target) {
    initCheckboxes(target)
  })
}
