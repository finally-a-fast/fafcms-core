function FafcmsCopy() {
  let cmsModule = this
  let fafToast = window.faf.FafcmsToast
  let fafApp = window.faf.app

  cmsModule.copy = function (text) {
    let input = document.createElement('textarea')
    input.value = text

    document.body.appendChild(input)
    input.select()
    document.execCommand('copy')
    document.body.removeChild(input)

    fafToast.toast({message: fafApp.t('fafcms-core', 'Text has been copied!'), displayTime: 3750, type: 'info'})
  }

  let initCopyButtons = function(target) {
    let elements = fafApp.findChildren(target, '.copy-text:not(.copy-text-initialized)')

    for (let i = 0; i < elements.length; i++) {
      let dataset = elements[i].dataset

      elements[i].addEventListener('click', function () {
        if (typeof dataset.copyText !== 'undefined') {
          cmsModule.copy(dataset.copyText)
        }

        if (typeof dataset.copyTarget !== 'undefined') {
          let element = document.querySelector(dataset.copyTarget)

          if (typeof element !== 'undefined') {
            cmsModule.copy(element.value)
          }
        }
      })

      elements[i].classList.add('copy-text-initialized')
    }
  }

  cmsModule.init = function () {
    initCopyButtons(document.getElementsByTagName('html'))
  }

  window.faf.events['initPlugins'].push(function (target) {
    initCopyButtons(target)
  })
}
