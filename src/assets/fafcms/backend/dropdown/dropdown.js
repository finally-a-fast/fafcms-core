function FafcmsDropdown() {
  let cmsModule = this

  let initDropdowns = function(target) {
    let elements = window.faf.app.findChildren(target, '.ui.dropdown:not(.dropdown-initialized)')

    for (let i = 0; i < elements.length; i++) {
      let options = {}
      let dataset = elements[i].dataset

      if (typeof dataset.clearable !== 'undefined') {
        options.clearable = dataset.clearable === 'true'
      }

      if (typeof dataset.match !== 'undefined') {
        options.match = dataset.match
      }

      if (typeof dataset.direction !== 'undefined') {
        options.direction = dataset.direction
      }

      if (typeof dataset.forceSelection !== 'undefined') {
        options.forceSelection = dataset.forceSelection === 'true'
      }

      if (typeof dataset.fullTextSearch !== 'undefined') {
        options.fullTextSearch = dataset.fullTextSearch
      } else {
        options.fullTextSearch = 'exact'
      }

      if (typeof dataset.allowAdditions !== 'undefined') {
        options.allowAdditions = dataset.allowAdditions === 'true'
      }

      elements[i].classList.add('dropdown-initialized')
      $(elements[i]).dropdown(options)
    }
  }

  cmsModule.init = function () {
    initDropdowns(document.getElementsByTagName('html'))
  }

  window.faf.events['initPlugins'].push(function (target) {
    initDropdowns(target)
  })
}
