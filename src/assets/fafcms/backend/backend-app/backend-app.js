function FafcmsBackendApp () {
  let cmsModule = this

  window.faf.events['windowLoad'].push(function () {
    document.getElementById('page-load-dimmer').classList.remove('active')
  })

  cmsModule.hasChanges = false

  window.faf.events['windowBeforeUnload'].push(function (e) {
    if (cmsModule.hasChanges) {
      e.returnValue = window.faf.app.t('fafcms-core', 'Not all changes have been saved yet, do you want to leave the page anyway?')
    } else {
      document.getElementById('page-load-dimmer').classList.add('active')

      window.setTimeout(function() {
        document.getElementById('page-load-dimmer').classList.remove('active')
      }, 20000)
    }
  })

  window.faf.events['windowUnload'].push(function () {
    document.getElementById('page-load-dimmer').classList.add('active')
  })
}
