function FafcmsTab() {
  let cmsModule = this

  let initTabs = function(target) {
    let elements = window.faf.app.findChildren(target, '.tabs:not(.tabs-initialized)')

    for (let i = 0; i < elements.length; i++) {
      let tabItemElements = window.faf.app.findChildren(elements[i], '.item')

      elements[i].classList.add('tabs-initialized')
      $(tabItemElements).tab()
    }
  }

  cmsModule.init = function () {
    initTabs(document.getElementsByTagName('html'))
  }

  window.faf.events['initPlugins'].push(function (target) {
    initTabs(target)
  })
}
