function FafcmsGridView() {
  let cmsModule = this

/*
  let checkTableHeaderPos = function (actionWrapper) {
    let $actionWrapper = $(actionWrapper)
    let maxTopPos = $('.page-header').height()
    let $pinnedItems = $('.pinned:visible')

    if ($pinnedItems.length > 0) {
      $pinnedItems.each(function () {
        let pinnedPos = Number.parseInt($(this).css('top').slice(0, -2)) + $(this).height()

        if (pinnedPos > maxTopPos) {
          maxTopPos = pinnedPos
        }
      })
    }

    let newTableHeaderPos = $(window).scrollTop() - $actionWrapper.offset().top + maxTopPos
    let maxTableHeaderPos = $actionWrapper.offset().top + $actionWrapper.height() - maxTopPos - $actionWrapper.find('thead').height() - 250

    if (newTableHeaderPos < maxTableHeaderPos) {
      $actionWrapper.find('thead tr > * > div').css('top', (newTableHeaderPos > 0 ? newTableHeaderPos : 0))
    } else {
      $actionWrapper.find('thead tr > * > div').css('top', (maxTableHeaderPos > 0 ? maxTableHeaderPos : 0))
    }
  }
*/

  let setActionColumnSize = function (actionWrapper) {
    let rows = actionWrapper.querySelectorAll('tr')

    for (let i = 0; i < rows.length; i++) {
      let column = rows[i].querySelector('.action-column')
      column.style.height = rows[i].clientHeight + 'px'
    }
  }

  let checkScrollBarPos = function (actionWrapper) {
    let $actionWrapper = $(actionWrapper)
    let scrollHeight = actionWrapper.querySelector('.os-scrollbar.os-scrollbar-horizontal').offsetHeight
    let newScrollBarPos = $(window).scrollTop() + $(window).height() - $actionWrapper.offset().top - scrollHeight
    let maxScrollBarPos = actionWrapper.clientHeight - scrollHeight

    if (newScrollBarPos < maxScrollBarPos) {
      actionWrapper.querySelector('.os-scrollbar.os-scrollbar-horizontal').style.top = (newScrollBarPos - 10) + 'px'
    } else {
      actionWrapper.querySelector('.os-scrollbar.os-scrollbar-horizontal').style.top = maxScrollBarPos + 'px'
    }
  }

  let initActionWrapperTable = function (target) {
    let elements = window.faf.app.findChildren(target, '.table-action-wrapper:not(.table-action-wrapper-initialized)')

    for (let i = 0; i < elements.length; i++) {
      let afterScrollbarInit = function () {
        checkScrollBarPos(elements[i])
        //checkTableHeaderPos(elements[i])
      }

      elements[i].addEventListener('fafcms-scrollbar-size-change', function () {
        setActionColumnSize(elements[i])
      })

      elements[i].classList.add('table-action-wrapper-initialized')

      if (elements[i].classList.contains('fafcms-scrollbar-ready')) {
        setActionColumnSize(elements[i])
        afterScrollbarInit()
      } else {
        elements[i].addEventListener('fafcms-scrollbar-ready', afterScrollbarInit)
      }
    }
  }

  cmsModule.init = function () {
    initActionWrapperTable(document.getElementsByTagName('html'))
  }

  window.faf.events['initPlugins'].push(function (target) {
    initActionWrapperTable(target)
  })

  window.faf.events['scroll'].push(function () {
    let elements = document.querySelectorAll('.table-action-wrapper')

    for (let i = 0; i < elements.length; i++) {
      checkScrollBarPos(elements[i])
      //checkTableHeaderPos($actionWrapper)
    }
  })
}
