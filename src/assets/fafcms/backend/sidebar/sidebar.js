function FafcmsSidebar () {
  let cmsModule = this

  cmsModule.init = function () {
    $('.sidebar-main').sidebar({
      context: $('.content-main'),
      dimPage: false,
      closable: false,
      transition: 'overlay',
      onVisible: function () {
        document.getElementsByTagName('body')[0].classList.add('sidebar-main-visible')
        // fafcms.prototype.saveUserSetting('sidebar', 'main', ($('body').hasClass('has-sidenav-fixed')?1:0))
      },
      onHide: function () {
        document.getElementsByTagName('body')[0].classList.remove('sidebar-main-visible')
        // fafcms.prototype.saveUserSetting('sidebar', 'main', ($('body').hasClass('has-sidenav-fixed')?1:0))
      },
    }).sidebar('attach events', '.sidenav-trigger')
  }
}
