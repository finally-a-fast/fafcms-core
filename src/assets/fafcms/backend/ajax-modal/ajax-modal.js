function FafcmsAjaxModal() {
  let cmsModule = this
  let fafApp = window.faf.app
  let fafToast = window.faf.FafcmsToast

  /***
   * Add events for specific modal by adding an object with your modal id based on the object
   * @type {{modalID: {openStart: Array, openEnd: Array, closeStart: Array, closeEnd: Array, contentLoaded: Array}}}
   */
  cmsModule.modalEvents = {}

  cmsModule.openModal = function (options) {
    let ajaxModal = document.getElementById(options.modalId)
    let $ajaxModal = $(ajaxModal)

    $ajaxModal.modal('hide')

    if (typeof options.modalEvents === 'undefined') {
      options.modalEvents = {
        openStart: [],
        openEnd: [],
        closeStart: [],
        closeEnd: [],
        contentLoaded: [],
      }

      if (typeof options.modalId !== 'undefined' && typeof cmsModule.modalEvents[options.modalId] !== 'undefined') {
        options.modalEvents = {
          openStart: cmsModule.modalEvents[options.modalId].openStart,
          openEnd: cmsModule.modalEvents[options.modalId].openEnd,
          closeStart: cmsModule.modalEvents[options.modalId].closeStart,
          closeEnd: cmsModule.modalEvents[options.modalId].closeEnd,
          contentLoaded: cmsModule.modalEvents[options.modalId].contentLoaded,
        }
      }
    }

    if (typeof options.headerSelector === 'undefined') {
      options.headerSelector = '.modal-header, header'
    }

    if (typeof options.headerRemoveFromContent === 'undefined') {
      options.headerRemoveFromContent = true
    }

    if (typeof options.footerSelector === 'undefined') {
      options.footerSelector = '.modal-footer, .card-action'
    }

    if (typeof options.footerRemoveFromContent === 'undefined') {
      options.footerRemoveFromContent = true
    }

    if (ajaxModal.getElementsByClassName('modal-footer').length > 0) {
      ajaxModal.getElementsByClassName('modal-footer')[0].remove()
    }

    if (ajaxModal.getElementsByClassName('modal-header').length > 0) {
      ajaxModal.getElementsByClassName('modal-header')[0].remove()
    }

    $ajaxModal.modal({
      onShow: function (e) {
        fafApp.runEvents(options.modalEvents, 'openStart', e)
      },
      onVisible: function (e) {
        fafApp.runEvents(options.modalEvents, 'openEnd', e)
      },
      onHide: function (e) {
        fafApp.runEvents(options.modalEvents, 'closeStart', e)
      },
      onHidden: function (e) {
        if (typeof options.modalClass !== 'undefined') {
          $ajaxModal.removeClass(options.modalClass)
        }

        fafApp.runEvents(options.modalEvents, 'closeEnd', e)
      },
    }).modal('show')

    $.get(options.url, options.data).done(function (response) {
      let content
      let footer
      let header

      if (typeof options.modalSelector !== 'undefined') {
        content = $('<div>' + response + '<div>').find(options.modalSelector).html()
      } else {
        content = response
      }

      if (typeof options.modalClass !== 'undefined') {
        $ajaxModal.addClass(options.modalClass)
      }

      if (options.headerSelector !== 'false' && options.headerSelector !== false) {
        header = $('<div>' + response + '<div>').find(options.headerSelector).html()
      }

      if (options.footerSelector !== 'false' && options.footerSelector !== false) {
        footer = $('<div>' + response + '<div>').find(options.footerSelector).html()
      }

      ajaxModal.getElementsByClassName('modal-content')[0].innerHTML = content

      if (typeof header !== 'undefined') {
        $ajaxModal.prepend('<header class="ui inverted header modal-header">' + header + '</header>')
      } else {
        $ajaxModal.find('.modal-header').remove()
      }

      if (typeof footer !== 'undefined') {
        $ajaxModal.append('<div class="actions modal-footer">' + footer + '</div>')
      } else {
        $ajaxModal.find('.modal-footer').remove()
      }

      if (
        options.headerSelector !== 'false' && options.headerSelector !== false &&
        (options.headerRemoveFromContent === true || options.headerRemoveFromContent === 'true')
      ) {
        $ajaxModal.find('.modal-content').find(options.headerSelector).remove()
      }

      if (
        options.footerSelector !== 'false' && options.footerSelector !== false &&
        (options.footerRemoveFromContent === true || options.footerRemoveFromContent === 'true')
      ) {
        $ajaxModal.find('.modal-content').find(options.footerSelector).remove()
      }

      if (!ajaxModal.classList.contains('visible')) {
        $ajaxModal.modal('open')
      }

      ajaxModal.getElementsByClassName('modal-loader')[0].classList.remove('active')

      if (typeof options.modalSelector !== 'undefined') {
        let scripts = $('<div>' + response + '<div>').find('script')
        let script = ''

        for (let i = 0; i < scripts.length; i++) {
          script += scripts[i].outerHTML
        }

        $ajaxModal.find('.modal-content').append(script)
      }

      fafApp.runEvents(window.faf.events, 'initPlugins', ajaxModal)
      fafApp.runEvents(options.modalEvents, 'contentLoaded')
    }).fail(function (e) {
      if (typeof e.responseJSON !== 'undefined' &&
        typeof e.responseJSON.message !== 'undefined') {
        if (e.status === 403 && e.responseJSON.code === 42) { // requires login
          cmsModule.openModal(JSON.parse(e.responseJSON.message))
          return false
        }

        fafToast.toast({message: e.responseJSON.message, type: 'error'})
      } else {
        fafToast.toast({message: fafApp.t('fafcms-core', 'An error has occurred, please try again.'), type: 'error'})
      }

      $ajaxModal.modal('hide')
    })
  }

  let body = document.getElementsByTagName('body')[0]
  let ajaxModalId = 0

  cmsModule.initModal = function(modalId, size) {
    if (document.getElementById(modalId) === null) {
      if (typeof size === 'undefined') {
        size = 'fullscreen'
      }

      body.insertAdjacentHTML('beforeend',
        '<div id="' + modalId + '" class="ui modal ' + size + ' ajax-modal-modal">\n' +
        '  <i class="close icon inside"></i>\n' +
        '  <div class="scrolling content">\n' +
        '    <div class="ui active inverted dimmer modal-loader">\n' +
        '      <div class="ui text loader">' + fafApp.t('fafcms-core', 'Loading') + '</div>\n' +
        '    </div>' +
        '    <div class="modal-content"></div>' +
        '  </div>\n' +
        '</div>')
    }
  }

  let initAjaxModals = function(target) {
    let elements = window.faf.app.findChildren(target, '.ajax-modal:not(.ajax-modal-initialized)')

    for (let i = 0; i < elements.length; i++) {
      let dataset = elements[i].dataset
      let modalId = dataset.ajaxModalId

      if (typeof modalId === 'undefined') {
        ajaxModalId++
        modalId = 'ajax-modal-generated-' + ajaxModalId
      }

      dataset.ajaxModalId = modalId

      elements[i].addEventListener('click', function (e) {
        e.preventDefault()

        cmsModule.initModal(modalId, dataset.ajaxModalSize)

        let ajaxModal = document.getElementById(modalId)
        ajaxModal.getElementsByClassName('modal-loader')[0].classList.add('active')
        ajaxModal.getElementsByClassName('modal-content')[0].innerHTML = ''

        let url

        if (typeof dataset.ajaxModalUrl !== 'undefined') {
          url = dataset.ajaxModalUrl
        } else if (typeof elements[i].attributes['xlink:href'] !== 'undefined') {
          url = elements[i].attributes['xlink:href'].value
        } else if (typeof elements[i].attributes.href !== 'undefined') {
          url = elements[i].attributes.href.value
        }

        let modalData

        if (typeof dataset.ajaxModalData !== 'undefined') {
          modalData = JSON.parse(dataset.ajaxModalData)
        }

        if (typeof cmsModule.modalEvents[modalId] === 'undefined') {
          cmsModule.modalEvents[modalId] = {}
        }

        cmsModule.openModal({
          url: url,
          data: modalData,
          modalSelector: dataset.ajaxModalContent,
          headerSelector: dataset.ajaxModalHeader,
          headerRemoveFromContent: dataset.ajaxModalHeaderRemove,
          footerSelector: dataset.ajaxModalFooter,
          footerRemoveFromContent: dataset.ajaxModalFooterRemove,
          modalId: modalId,
          modalClass: dataset.ajaxModalClass,
          modalEvents: {
            openStart: cmsModule.modalEvents[modalId].openStart,
            openEnd: cmsModule.modalEvents[modalId].openEnd,
            closeStart: cmsModule.modalEvents[modalId].closeStart,
            closeEnd: cmsModule.modalEvents[modalId].closeEnd,
            contentLoaded: cmsModule.modalEvents[modalId].contentLoaded,
          },
        })
      })

      elements[i].classList.add('ajax-modal-initialized')
    }
  }

  cmsModule.init = function () {
    initAjaxModals(document.getElementsByTagName('html'))
  }

  window.faf.events['windowBeforeUnload'].push(function () {
    let elements = document.getElementsByClassName('ajax-modal-modal')

    for (let i = 0; i < elements.length; i++) {
      $(elements[i]).modal('hide')
    }
  })

  window.faf.events['initPlugins'].push(function (target) {
    initAjaxModals(target)
  })
}
