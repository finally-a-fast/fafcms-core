;(function (factory) {
    'use strict'
    new factory(window.jQuery, window.fafcms)
})(function ($, fafcms) {
    'use strict'

  let realModalInit = M.Modal.init
  /*let magicModal = function(elems, options) {
    var realOnOpenStart = options.onOpenStart
    var realOnOpenEnd = options.onOpenEnd

    var setModalMargin = function($modal) {
      var $header = $modal.find('.modal-header')
      var marginTop = 0

      if ($header.length > 0) {
        marginTop = $modal.find('.modal-header').height()
      }

      $modal.find('.modal-content').css('margin-top', marginTop)

      if ($modal.hasClass('modal-fixed-footer')) {
        var footerHeight = $modal.find('.modal-footer').outerHeight()
        $modal.find('.modal-content').css('height', 'calc(100% - ' + (footerHeight + marginTop) + 'px)')
      }
    }*/

    var addModalCloseButton = function($modal, options) {
      if ((typeof options.modalClose === 'undefined' || options.modalClose) && $modal.find('.modal-header-close').length === 0) {
        $modal.prepend('<button class="btn waves-effect waves-light modal-close modal-header-close"><i class="close icon"></i></button>')
      }
    }

    $(elems).each(function () {
      var $modal = $(this)

      $modal.on('magic-modal-content-change', function () {
        setModalMargin($modal)
      })
    })

    options.onOpenStart = function() {
      if (typeof realOnOpenStart === 'function') {
        realOnOpenStart.call(this, this.el)
      }
    }

    options.onOpenEnd = function() {
      var $modal = $(this.el)

        setModalMargin($modal)
        addModalCloseButton($modal, options)

      if (typeof realOnOpenEnd === 'function') {
        realOnOpenEnd.call(this, this.el, this._openingTrigger)
      }
    }

    return options
  }

  M.Modal.init = function (elems, options) {
    return realModalInit.call(this, elems, magicModal.call(this, elems, options))
  }

  fafcms.prototype.events['initPlugins'].push(function ($target) {
    $target.find('.modal').each(function () {
      if (typeof M.Modal.getInstance(this) === 'undefined') {
        $(this).modal({
          opacity: .7,
        })
      }
    })
  })

  /*$('.fullscreen.modal')
  .modal('show')
;*/
    return fafcms
})
