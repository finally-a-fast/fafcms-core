<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\fafcms\assets\fafcms\backend;

use fafcms\fafcms\assets\fomantic\collections\FomanticMenuAsset;
use fafcms\fafcms\assets\fomantic\elements\FomanticButtonAsset;
use fafcms\helpers\classes\AssetComponentBundle;

/**
 * Class FafcmsActionBarAsset
 *
 * @package fafcms\fafcms\assets\fafcms\backend
 */
class FafcmsActionBarAsset extends AssetComponentBundle
{
    public $sourcePath = __DIR__ . '/action-bar';

    public $js = [
        'action-bar.js',
    ];

    public $css = [
        'action-bar.scss',
    ];

    public $depends = [
        FomanticMenuAsset::class,
        FomanticButtonAsset::class,
        FafcmsDropdownAsset::class
    ];
}
