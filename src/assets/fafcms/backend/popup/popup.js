function FafcmsPopup() {
  let cmsModule = this
  let fafApp = window.faf.app

  let initPopups = function(target) {
    let elements = fafApp.findChildren(target, '.popup-hover:not(.popup-initialized), .popup-click:not(.popup-initialized)')

    for (let i = 0; i < elements.length; i++) {
      let options = {}

      if (elements[i].classList.contains('popup-click')) {
        options.on = 'click'
      }

      elements[i].classList.add('popup-initialized')

      window.setTimeout(function () {
        $(elements[i]).popup(options)
      })
    }
  }

  cmsModule.init = function () {
    initPopups(document.getElementsByTagName('html'))
  }

  window.faf.events['initPlugins'].push(function (target) {
    initPopups(target)
  })
}
