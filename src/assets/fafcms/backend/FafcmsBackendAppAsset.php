<?php

/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\fafcms\assets\fafcms\backend;

use fafcms\fafcms\assets\fafcms\FafcmsAppAsset;
use fafcms\fafcms\assets\fomantic\{
    behaviors\FomanticApiAsset,
    behaviors\FomanticStateAsset,
    behaviors\FomanticVisibilityAsset,
    collections\FomanticBreadcrumbAsset,
    collections\FomanticGridAsset,
    collections\FomanticMenuAsset,
    collections\FomanticMessageAsset,
    collections\FomanticTableAsset,
    elements\FomanticButtonAsset,
    elements\FomanticContainerAsset,
    elements\FomanticDividerAsset,
    elements\FomanticEmojiAsset,
    elements\FomanticHeaderAsset,
    elements\FomanticIconAsset,
    elements\FomanticImageAsset,
    elements\FomanticInputAsset,
    elements\FomanticLabelAsset,
    elements\FomanticListAsset,
    elements\FomanticLoaderAsset,
    elements\FomanticPlaceholderAsset,
    elements\FomanticRailAsset,
    elements\FomanticRevealAsset,
    elements\FomanticSegmentAsset,
    elements\FomanticStepAsset,
    elements\FomanticTextAsset,
    globals\FomanticSiteAsset,
    modules\FomanticDimmerAsset,
    modules\FomanticProgressAsset,
    modules\FomanticRatingAsset,
    modules\FomanticSearchAsset,
    modules\FomanticShapeAsset,
    modules\FomanticSidebarAsset,
    modules\FomanticSliderAsset,
    modules\FomanticTransitionAsset,
    views\FomanticCommentAsset,
    views\FomanticFeedAsset,
    views\FomanticItemAsset,
    views\FomanticStatisticAsset
};
use fafcms\helpers\classes\AssetComponentBundle;

use fafcms\fafcms\assets\RobotoAsset;
use yiiui\yii2materialdesignicons\MaterialDesignIconsAsset;


/**
 * Class FafcmsBackendAppAsset
 *
 * @package fafcms\fafcms\assets\fafcms
 */
class FafcmsBackendAppAsset extends AssetComponentBundle
{
    public $sourcePath = __DIR__ . '/backend-app';

    public $js = [
        'backend-app.js',
    ];

    public $css = [
        'backend-app.scss',
    ];

    public $depends = [
        RobotoAsset::class,
        MaterialDesignIconsAsset::class,
        FafcmsAppAsset::class,
        FafcmsTextScrollerAsset::class,
        FafcmsAccordionAsset::class,
        FafcmsDropdownAsset::class,
        FafcmsPopupAsset::class,
        FafcmsSidebarAsset::class,
        FafcmsTabAsset::class,
        FafcmsAjaxModalAsset::class,
        FafcmsCardAsset::class,
        FafcmsScrollbarsAsset::class,
        FafcmsStickyAsset::class,
        FafcmsToastAsset::class,
        FafcmsCopyAsset::class,
        FafcmsFormAsset::class,
        FomanticApiAsset::class,
        FomanticStateAsset::class,
        FomanticVisibilityAsset::class,
        FomanticBreadcrumbAsset::class,
        FomanticGridAsset::class,
        FomanticMenuAsset::class,
        FomanticMessageAsset::class,
        FomanticTableAsset::class,
        FomanticButtonAsset::class,
        FomanticContainerAsset::class,
        FomanticDividerAsset::class,
        FomanticEmojiAsset::class, //optional
        FomanticHeaderAsset::class,
        FomanticIconAsset::class,
        FomanticImageAsset::class,
        FomanticInputAsset::class,
        FomanticLabelAsset::class,
        FomanticListAsset::class,
        FomanticLoaderAsset::class,
        FomanticPlaceholderAsset::class,
        FomanticRailAsset::class, //optional
        FomanticRevealAsset::class, //optional
        FomanticSegmentAsset::class,
        FomanticStepAsset::class, //optional
        FomanticTextAsset::class,
        FomanticDimmerAsset::class,
        FomanticProgressAsset::class,
        FomanticRatingAsset::class, //optional
        FomanticSearchAsset::class, //optional
        FomanticShapeAsset::class, //optional
        FomanticSidebarAsset::class, //optional
        FomanticSliderAsset::class,
        FomanticTransitionAsset::class,
        FomanticSiteAsset::class,
        FomanticCommentAsset::class, //optional
        FomanticFeedAsset::class, //optional
        FomanticItemAsset::class,
        FomanticStatisticAsset::class, //optional
    ];
}
