function FafcmsTextScroller() {
  let root = document.documentElement
  let $body = $('body')

  let scrollAnimation = function (e) {
    let $textScroller = $(this)

    window.setTimeout(function () {
      if ($textScroller.hasClass('text-scroll-animation')) {
        $textScroller.removeClass('text-scroll-animation')
      } else {
        $textScroller.addClass('text-scroll-animation')
      }
    }, 500)
  }

  let scrollStart = function ($textScroller) {
    let scrollLength = Math.ceil(
      $textScroller.width() - $textScroller.find('span').width())
    let scrollSpeed = (Math.ceil(Math.abs(scrollLength / 40) * 10) / 10)

    if (scrollLength > 0) {
      scrollLength = 0
    }

    if (scrollSpeed < .5) {
      scrollSpeed = .5
    }

    root.style.setProperty('--text-scroller-position', scrollLength + 'px')
    root.style.setProperty('--text-scroller-speed', scrollSpeed + 's')

    $textScroller.on('transitionend', scrollAnimation)
    $textScroller.addClass('default-overflow text-scroll-animation')
  }

  let scrollEnd = function ($textScroller) {
    $textScroller.unbind('transitionend', scrollAnimation)
    $textScroller.removeClass('default-overflow text-scroll-animation')
  }

  window.faf.events['ready'].push(function () {
    $body.on('mouseenter', '.text-scroller', function () {
      scrollStart($(this))
    })

    $body.on('focus', '.text-scroller', function () {
      scrollStart($(this))
    })

    $body.on('mouseleave', '.text-scroller', function () {
      scrollEnd($(this))
    })

    $body.on('blur', '.text-scroller', function () {
      scrollEnd($(this))
    })
  })
}
