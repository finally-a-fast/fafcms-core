<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 - 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since File available since Release 1.0.0
 */

declare(strict_types=1);

namespace fafcms\fafcms\assets\fafcms\backend;

use fafcms\fafcms\assets\fomantic\modules\FomanticAccordionAsset;
use fafcms\fafcms\assets\fomantic\modules\FomanticCheckboxAsset;
use fafcms\helpers\classes\AssetComponentBundle;

/**
 * Class FafcmsCheckboxAsset
 *
 * @package fafcms\fafcms\assets\fafcms
 */
class FafcmsCheckboxAsset extends AssetComponentBundle
{
    public $sourcePath = __DIR__ . '/checkbox';

    public $js = [
        'checkbox.js',
    ];

    public $css = [
        'checkbox.scss',
    ];

    public $depends = [
        FomanticCheckboxAsset::class
    ];
}
