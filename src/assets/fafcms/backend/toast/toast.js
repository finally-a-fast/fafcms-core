function FafcmsToast() {
  let cmsModule = this
  let fafApp = window.faf.app

  cmsModule.notificationTypeMapping = {
    'default': {},
    'error': {
      class: 'red',
      icon: 'mdi mdi-alert-decagram-outline',
      title: fafApp.t('fafcms-core', 'Error'),
    },
    'warning': {
      class: 'orange',
      icon: 'mdi mdi-alert-circle-outline',
      title: fafApp.t('fafcms-core', 'Warning'),
    },
    'success': {
      class: 'green',
      icon: 'mdi mdi-check-circle-outline',
      title: fafApp.t('fafcms-core', 'Success'),
    },
    'info': {
      class: 'blue',
      icon: 'mdi mdi-information-outline',
      title: fafApp.t('fafcms-core', 'Information'),
    },
  }

  cmsModule.toast = function (config) {
    let notificationType = cmsModule.notificationTypeMapping['default']

    if (typeof cmsModule.notificationTypeMapping[config.type] !== 'undefined') {
      notificationType = cmsModule.notificationTypeMapping[config.type]
    }

    delete config.type

    $('body').toast(Object.assign({
      showProgress: 'top',
      classProgress: notificationType.class,
      title: notificationType.title,
      showIcon: notificationType.icon,
      displayTime: 'auto',
      minDisplayTime: 5000,
      actions: [
        /*{
        text: 'Yes',
        icon: 'check',
        class: 'green',
        click: function() {
          //todo mark as read
        }
      },*/{
        icon: 'mdi mdi-content-copy',
        class: 'icon',
        click: function() {
          window.faf.FafcmsCopy.copy(config.message)
          return false
        }
      }, {
        icon: 'mdi mdi-trash-can-outline red',
        class: 'icon',
        click: function() {
          return true
        }
      }]
    }, config))
  }

  window.faf.events['ready'].push(function () {
    let flashedNotifications = JSON.parse(document.getElementsByTagName('body')[0].dataset.flashedNotifications)

    for (let notificationType in flashedNotifications) {
      let notificationMessage = flashedNotifications[notificationType]

      if (typeof notificationMessage === 'object') {
        notificationMessage = notificationMessage.join('<hr>')
      }

      cmsModule.toast({message: notificationMessage, type: notificationType})
    }
  })
}
