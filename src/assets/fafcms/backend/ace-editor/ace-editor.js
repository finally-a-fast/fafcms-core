function FafcmsAceEditor() {
  let cmsModule = this


  let initAceEditors = function() {
    window.faf.config.aceEditor = window.faf.config.aceEditor || {}
    window.faf.config.aceEditor.instances = window.faf.config.aceEditor.instances || []

    if (typeof window.faf.config.aceEditor.baseUrl !== 'undefined') {
      ace.config.set('basePath', window.faf.config.aceEditor.baseUrl)
    }

    for (let inputId in window.faf.config.aceEditor.instances) {
      let text = document.getElementById(inputId)

      if (!text.classList.contains('ace-editor-initialized')) {
        let editor = ace.edit(inputId + '-container')

        editor.session.setValue(text.value)
        editor.session.on('change', function() {
          text.value = editor.getSession().getValue()
        })

        window.faf.config.aceEditor.instances[inputId](editor)
        text.classList.add('ace-editor-initialized')
      }
    }
  }

  cmsModule.init = function () {
    initAceEditors()
  }

  window.faf.events['initPlugins'].push(function () {
    initAceEditors()
  })
}

