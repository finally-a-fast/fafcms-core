;(function (factory) {
    'use strict'
    new factory(window.jQuery, window.fafcms)
})(function ($, fafcms) {
    'use strict'

    var isTrigger = false

    var openElement = function($element) {
        var elementId = $element.attr('id')

        if ($element.hasClass('card')) {
            $element.trigger('toggle', 'open')
        }
        else if ($element.hasClass('tab')) {
            var tabInstance = $element.data('tab-instance')
            $('[data-target="#' + elementId + '"]').trigger('tab-active')

            if (typeof tabInstance !== 'undefined' && !$element.hasClass('active')) {
                $('#' + tabInstance).tabs('select', elementId)
            }
        }
    }

    var checkUrl = function(url, scroll, e) {
        if (typeof url !== 'undefined') {
            if (typeof scroll !== 'undefined') {
                scroll = true
            }

            var urlParts = url.split('#')

            if (urlParts.length > 1 && (urlParts[0] === '' || urlParts[0] === window.location.pathname + window.location.search)) {
                var targetId = urlParts[urlParts.length - 1]
                var $target = null

                if (targetId === '') {
                    $target = $('body')
                }
                else {
                    try {
                        $target = $('#' + targetId)
                    } catch (e) {
                        $target = null
                    }
                }

                if ($target !== null && $target.length > 0) {
                    if (typeof e !== 'undefined') {
                        e.preventDefault()
                    }

                    $target.parents('[id]:hidden, .collapsed-collapsible-card').each(function() {
                        openElement($(this))
                    })

                    openElement($target)

                    if (scroll) {
                        $('html, body').animate({
                            scrollTop: $target.offset().top - $('header').height() - 50
                        }, 'slow')
                    }
                }
            }
        }
    }

    fafcms.prototype.events['init'].push(function () {
        $('body').on('click', 'a', function(e) {
            if (isTrigger) {
                return
            }

            isTrigger = true

            var $a = $(this)
            var url = $a.attr('href')

            if (typeof url === 'undefined') {
                url = $a.attr('xlink:href')
            }

            checkUrl(url, !$a.hasClass('no-scroll'), e)

            isTrigger = false
        })
    })

    fafcms.prototype.events['ready'].push(function () {
        setTimeout(function () {
            checkUrl(window.location.hash)
        }, 100)
    })

    return fafcms
})
