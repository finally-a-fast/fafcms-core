
$target.find('.color-btn').spectrum({
  preferredFormat: 'hex',
  showInput: true,
  showPalette: false,
  //palette: ['#ffffff', '#000000', '#DB0720'],
})


  var addActionButtonValue = function($actionButton, formSelector) {
    if (typeof $actionButton.data('submit-name') !== 'undefined') {
      var name = $actionButton.data('submit-name')
    }
    else if (typeof $actionButton.attr('name') !== 'undefined') {
      var name = $actionButton.attr('name')
    }

    if (typeof $actionButton.data('submit-value') !== 'undefined') {
      var value = $actionButton.data('submit-value')
    }
    else if (typeof $actionButton.attr('value') !== 'undefined') {
      var value = $actionButton.attr('value')
    }

    if (typeof name !== 'undefined' && typeof value !== 'undefined') {
      $(formSelector).append('<input type="hidden" name="' + name + '" value="' + value + '" />')
    }
  }

  fafcms.prototype.events['init'].push(function () {
    $('body').on('click', 'button[type="submit"][form]', function(e) {
      e.preventDefault()

      var $actionButton = $(this)
      var formSelector = $actionButton.attr('form')

      if (typeof formSelector === 'undefined') {
        formSelector = $actionButton.data('form-selector')
      } else {
        formSelector = '#' + formSelector
      }

      var title = $actionButton.data('confirm-modal')

      if (typeof title === 'undefined') {
        title = $actionButton.data('confirm')
      }

      if (typeof title !== 'undefined') {
        var cancel = $actionButton.data('confirm-modal-cancel')
        var confirm = $actionButton.data('confirm-modal-confirm')
        var message = $actionButton.data('message')

        fafcms.prototype.openConfirmModal({
          title: title,
          cancel: cancel,
          confirm: confirm,
          message: message,
          actionConfirmed: function () {
            addActionButtonValue($actionButton, formSelector)
            $(formSelector).submit()
          }
        })
      }
      else {
        addActionButtonValue($actionButton, formSelector)
        $(formSelector).submit()
      }
    })
  })


  let eggString = ''
  let eggInitialized = false
  const eggCode = 'ArrowUpArrowUpArrowDownArrowDownArrowLeftArrowRightArrowLeftArrowRightba'

  fafcms.prototype.events['ready'].push(function () {
    $(window).on('keyup', function (event) {
      if (eggCode.indexOf(eggString) === 0) {
        eggString += event.key
      } else {
        eggString = ''
      }

      if (eggString === eggCode) {
        eggInitialized = true
        //$('body').append('<iframe id="easter-egg" src="http://localhost/finally-a-fast/egg/" height="500" width="500" frameborder="0" style="position:fixed;top:25%;left:25%;z-index:99999;width:50%;height:50%;"></iframe>')
      }

      if (event.key === 'Escape') {
        eggInitialized = false
        $('#easter-egg').remove()
      }
    })
  })













  fafcms.prototype.events['init'].push(function () {
    $('body').on('click', 'input[name="showDependencies"]', function () {
      window.location.search = (window.location.search === ''?'?':window.location.search + '&') + 'showDependencies=' + ($(this).is(':checked')?1:0)
    })

    /*
    var updater = setInterval(function () {
        $.getJSON(document.baseURI + '/installer/module-outdated', function(result) {
            if (result.status === 'done' || result.status === 'cached') {
                clearInterval(updater)

                var outdatedPackages = JSON.parse(result.result)
                var updateCount = 0

                outdatedPackages.installed.forEach(function (outdatedPackage) {
                    updateCount++
                    $('#' + outdatedPackage.name.replace('/', '\\/') + ' .update-btn').removeClass('hide')
                })

                $('.full-update-btn').each(function() {
                    $(this).html($(this).html().replace('{{count}}', updateCount))
                })

                $('.full-update-btn').removeClass('hide')
            }
        }).fail(function() {
            clearInterval(updater)
        })
    }, 2000)*/
  })








fafcms.prototype.events['initPlugins'].push(function ($target) {
  $target.find('.card:not(.not-collapsible)').each(function () {
    var $card = $(this)

    $card.find('> .card-content > .card-title').prepend('<button type="button" class="btn-floating waves-effect toggle-collapsible-card grey lighten-2"><i class="mdi mdi-chevron-up grey-text text-darken-2"></i></button>')

    if ($card.hasClass('collapsed-collapsible-card')) {
      var maxHeight = $card.find('> .card-content > .card-title').outerHeight(true) + parseInt($card.find('> .card-content').css('padding-top').slice(0, -2), 10)

      $card.css({
        'overflow': 'hidden',
        'max-height': (maxHeight < 64?64:maxHeight)
      })
    }
  })
})

fafcms.prototype.events['ready'].push(function () {
  $('body').on('toggle', '.card', function(e, type) {
    if (e.target !== this) {
      return
    }

    var $card = $(this)

    if (typeof type === 'undefined') {
      type = ($card.hasClass('collapsed-collapsible-card')?'open':'close')
    }

    if ($card.css('max-height') === 'none') {
      $card.css({'max-height': $card.height()})
    }

    if (type === 'open') {
      var maxHeight = $card.css('max-height')
      $card.css({'max-height': 'none'})

      var height = $card.height()
      $card.css({'max-height': maxHeight})

      M.anime({
        targets: $card[0],
        'max-height': height,
        duration: 300,
        easing: 'easeInOutCubic',
        complete: function() {
          $card.css({'max-height': 'none', 'overflow': 'visible'})
        }
      })

      $card.find('.tabs').tabs('updateTabIndicator')
      $card.removeClass('collapsed-collapsible-card')
    } else {
      $card.css({'overflow': 'hidden'})

      M.anime({
        targets: $card[0],
        'max-height': $card.find('> .card-content > .card-title').outerHeight(true) + parseInt($card.find('> .card-content').css('padding-top').slice(0,-2), 10),
        duration: 300,
        easing: 'easeInOutCubic'
      })

      $card.addClass('collapsed-collapsible-card')
    }
  })

  $('body').on('click', '.toggle-collapsible-card', function () {
    $($(this).parents('.card')[0]).trigger('toggle')
  })
})


