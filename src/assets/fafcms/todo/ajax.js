;(function (factory) {
    'use strict'
    new factory(window.jQuery, window.fafcms)
})(function ($, fafcms) {
    'use strict'

    let $body = $('body')
    let contentCache = []

    let setContent = function(src, $ajaxContent, $ajaxContentTarget, loaded) {
        let pageContent
        let replaceCurrentSite = $ajaxContent.data('ajax-content-replace-state') === true || $ajaxContent.data('ajax-content-replace-state') === 'true'

        if (replaceCurrentSite) {
            let currentSite = window.location.pathname + window.location.hash

            if (typeof contentCache[currentSite] === 'undefined') {
                contentCache[currentSite] = {
                    title: $('title').html(),
                    breadcrumb: $('.breadcrumb-wrapper').html(),
                    content: $ajaxContentTarget.html(),
                }
            }

            window.history.replaceState({} , '', src)
        }

        if (typeof contentCache[src] === 'string') {
            pageContent = contentCache[src]
        } else {
            if (replaceCurrentSite) {
                $('title').html(contentCache[src].title)
                $('.breadcrumb-wrapper').html(contentCache[src].breadcrumb)
            }

            pageContent = contentCache[src].content
        }

        if (!loaded) {
            $ajaxContentTarget.html(pageContent)
            fafcms.prototype.runEvents(fafcms.prototype.events, 'initPlugins', $ajaxContentTarget)
        }
    }

    let loadAjaxContent = function($ajaxContent) {
        let $ajaxContentTarget

        if (typeof $ajaxContent.data('ajax-content-target') === 'undefined') {
            if (typeof $ajaxContent.data('target') === 'undefined') {
                $ajaxContentTarget = $ajaxContent
            } else {
                $ajaxContentTarget = $($ajaxContent.data('target'))
            }
        } else {
            $ajaxContentTarget = $($ajaxContent.data('ajax-content-target'))
        }

        let src = $ajaxContent.data('src')

        if (typeof src === 'undefined') {
            src = $ajaxContent.attr('href')
        }

        if (typeof src === 'undefined') {
            return
        }

        if (!$ajaxContent.data('loaded')) {
            if ($ajaxContentTarget.is('ul')) {
                $ajaxContentTarget.html('<li>' + fafcms.prototype.loader + '</li>')
            } else {
                $ajaxContentTarget.html(fafcms.prototype.loader)
            }

            let data = $ajaxContent.data()

            let ajaxData = Object.keys(data)
            .filter(function(key) {
                return key.indexOf('ajaxData') === 0
            })
            .reduce(function(obj, key) {
                obj[key] = data[key]
                return obj
            }, {})

            $.get({
                url: src,
                data: {'render-mode': 'ajax', 'type': $body.data('type'), 'id': $body.data('id'), url: window.location.pathname, data: ajaxData}
            }, function(content) {
                $ajaxContent.attr('data-loaded', true).data('loaded', true)
                contentCache[src] = content
                setContent(src, $ajaxContent, $ajaxContentTarget, false)
            })
            .fail(function(e) {
                M.toast({html: '<div>' + yii.t('app', 'An error has occurred, please try again.') + '</div>', classes: 'red', displayLength: 10000})
            })
        } else {
            setContent(src, $ajaxContent, $ajaxContentTarget, true)
        }
    }

  fafcms.prototype.saveUserSetting('collapsible', $collapsible.attr('id'), 0)
  fafcms.prototype.saveUserSetting('collapsible', $collapsible.attr('id'), 1)
  fafcms.prototype.saveUserSetting('sidebar', 'main', ($('body').hasClass('has-sidenav-fixed') ? 1 : 0))

    fafcms.prototype.saveUserSetting = function(type, variation, value) {
        $.get({
            //todo get backend url
            url: '/backend/save-user-setting',
            data: {
                type: type,
                variation: variation,
                value: value
            }
        })
    }

    fafcms.prototype.events['initPlugins'].push(function($target) {
        $target.find('.ajax-content').each(function() {
            let $ajaxContent = $(this)
            let trigger = $ajaxContent.data('ajax-content-trigger')

            if (typeof trigger === 'undefined') {
                loadAjaxContent($ajaxContent)
            } else {
                $ajaxContent.on(trigger, function () {
                    loadAjaxContent($ajaxContent)
                })
            }

            $ajaxContent.on('load', function () {
                $ajaxContent.attr('data-loaded', false).data('loaded', false)
                loadAjaxContent($ajaxContent)
            })
        })
    })

    return fafcms
})
