<?php

namespace fafcms\fafcms\assets;

use fafcms\assets\{
    ajaxmodal\AjaxModalAsset,
    copytoclipboard\CopyToClipboardAsset,
    datetimepicker\DateTimePickerAsset,
    formhelper\FormHelperAsset
};

use Yii;

use yii\{
    grid\GridViewAsset,
    helpers\FileHelper,
    validators\ValidationAsset,
    web\AssetBundle,
    web\JqueryAsset,
    web\YiiAsset,
    widgets\ActiveFormAsset,
    widgets\PjaxAsset
};

use yiiui\yii2advancedgridview\assets\AdvancedGridViewAsset;

class FafcmsAsset extends AssetBundle
{
    public $extras = [
        __DIR__ . '/fafcms/img' => '@webroot/img',
    ];

    public $sourcePath = __DIR__ . '/fafcms';

    public $depends = [
        FormHelperAsset::class,
        CopyToClipboardAsset::class,
        DateTimePickerAsset::class,
        AjaxModalAsset::class,
        CardAsset::class,
        FormAsset::class,
        FrameActionAsset::class,
        LinkCheckAsset::class,
        //TODO MATERIALIZE
       // ModalAsset::class,
        AjaxAsset::class,
        AjaxFormAsset::class,
        NavigationAsset::class,
        TabAsset::class,
        //TODO MATERIALIZE
       // ToastAsset::class,
        SpectrumAsset::class,
        InstallerAsset::class,
        AdvancedGridViewAsset::class,
        YiiAsset::class,
        GridViewAsset::class,
        PjaxAsset::class,
        ActiveFormAsset::class,
        ValidationAsset::class,
    ];

    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function publish($am): void
    {
        var_dump('a'); die();
        parent::publish($am);

        foreach ($this->extras as $from => $to) {
            $to = Yii::getAlias($to);

            if ($am->forceCopy || !file_exists($to)) {
                $from = Yii::getAlias($from);

                if (is_dir($from)) {
                    FileHelper::copyDirectory($from, $to);
                } else {
                    if (!is_dir(dirname($to))) {
                        FileHelper::createDirectory(dirname($to));
                    }

                    copy($from, $to);
                }
            }
        }
    }
}
