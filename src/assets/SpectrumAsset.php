<?php

namespace fafcms\fafcms\assets;

use yii\web\AssetBundle;

/**
 * Class SpectrumAsset
 *
 * @package fafcms\fafcms\assets
 */
class SpectrumAsset extends AssetBundle
{
    public $sourcePath = '@npm/spectrum-colorpicker';

    public $js = [
        'spectrum.js'
    ];

    public $css = [
        'spectrum.css'
    ];
/*
    public function init()
    {
        parent::init();

        $this->js[] = 'i18n/jquery.spectrum-' . Yii::$app->language . '.js';
    }*/
}
