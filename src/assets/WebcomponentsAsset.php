<?php

namespace fafcms\fafcms\assets;

use fafcms\fafcms\components\ViewComponent;
use yii\web\AssetBundle;

class WebcomponentsAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@npm/webcomponents--webcomponentsjs';

    /**
     * @var string[]
     */
    public $js = [
        'webcomponents-loader.js',
    ];

    public $jsOptions = [
        'position' => ViewComponent::POS_HEAD,
        'fafcmsAsync' => true
    ];
}
