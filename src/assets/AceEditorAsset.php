<?php

namespace fafcms\fafcms\assets;

use yii\web\AssetBundle;

class AceEditorAsset extends AssetBundle
{
    public $sourcePath = '@npm/ace-builds/src-min-noconflict';

    public $js = [
        'ace.js'
    ];
}
