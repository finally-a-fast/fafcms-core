<?php

namespace fafcms\fafcms\migrations;

use yii\db\Migration;

/**
 * Class m190913_140742_projects
 * @package fafcms\fafcms\migrations
 */
class m190913_140742_projects extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%project}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'name' => $this->string(255)->notNull(),
            'media' => $this->string(255)->null()->defaultValue(null),
            'description' => $this->text()->null()->defaultValue(null),
            'primary_projectlanguage_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-project-primary_projectlanguage_id', '{{%project}}', ['primary_projectlanguage_id'], false);
        $this->createIndex('idx-project-created_by', '{{%project}}', ['created_by'], false);
        $this->createIndex('idx-project-updated_by', '{{%project}}', ['updated_by'], false);
        $this->createIndex('idx-project-activated_by', '{{%project}}', ['activated_by'], false);
        $this->createIndex('idx-project-deactivated_by', '{{%project}}', ['deactivated_by'], false);
        $this->createIndex('idx-project-deleted_by', '{{%project}}', ['deleted_by'], false);

        $this->createTable('{{%projectlanguage}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text()->null()->defaultValue(null),
            'path' => $this->string(255)->notNull(),
            'project_id' => $this->integer(10)->unsigned()->notNull(),
            'domain_id' => $this->integer(10)->unsigned()->notNull(),
            'language_id' => $this->integer(10)->unsigned()->notNull(),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-projectlanguage-path-domain_id', '{{%projectlanguage}}', ['path', 'domain_id'], false);
        $this->createIndex('idx-projectlanguage-path', '{{%projectlanguage}}', ['path'], false);
        $this->createIndex('idx-projectlanguage-project_id', '{{%projectlanguage}}', ['project_id'], false);
        $this->createIndex('idx-projectlanguage-domain_id', '{{%projectlanguage}}', ['domain_id'], false);
        $this->createIndex('idx-projectlanguage-language_id', '{{%projectlanguage}}', ['language_id'], false);
        $this->createIndex('idx-projectlanguage-created_by', '{{%projectlanguage}}', ['created_by'], false);
        $this->createIndex('idx-projectlanguage-updated_by', '{{%projectlanguage}}', ['updated_by'], false);
        $this->createIndex('idx-projectlanguage-activated_by', '{{%projectlanguage}}', ['activated_by'], false);
        $this->createIndex('idx-projectlanguage-deactivated_by', '{{%projectlanguage}}', ['deactivated_by'], false);
        $this->createIndex('idx-projectlanguage-deleted_by', '{{%projectlanguage}}', ['deleted_by'], false);

        $this->createTable('{{%domain}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'domain' => $this->string(255)->notNull(),
            'project_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'is_wildcard' => $this->tinyInteger(1)->null()->defaultValue(null),
            'is_validated' => $this->tinyInteger(1)->null()->defaultValue(null),
            'last_validation_at' => $this->datetime()->null()->defaultValue(null),
            'is_https_validated' => $this->tinyInteger(1)->null()->defaultValue(null),
            'last_https_validation_at' => $this->datetime()->null()->defaultValue(null),
            'force_https' => $this->tinyInteger(1)->null()->defaultValue(null),
            'is_redirect' => $this->tinyInteger(1)->null()->defaultValue(null),
            'redirect_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-domain-project_id', '{{%domain}}', ['project_id'], false);
        $this->createIndex('idx-domain-redirect_id', '{{%domain}}', ['redirect_id'], false);
        $this->createIndex('idx-domain-created_by', '{{%domain}}', ['created_by'], false);
        $this->createIndex('idx-domain-updated_by', '{{%domain}}', ['updated_by'], false);
        $this->createIndex('idx-domain-activated_by', '{{%domain}}', ['activated_by'], false);
        $this->createIndex('idx-domain-deactivated_by', '{{%domain}}', ['deactivated_by'], false);
        $this->createIndex('idx-domain-deleted_by', '{{%domain}}', ['deleted_by'], false);

        $this->addForeignKey('fk-project-primary_projectlanguage_id', '{{%project}}', 'primary_projectlanguage_id', '{{%projectlanguage}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-project-created_by', '{{%project}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-project-updated_by', '{{%project}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-project-activated_by', '{{%project}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-project-deactivated_by', '{{%project}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-project-deleted_by', '{{%project}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-projectlanguage-project_id', '{{%projectlanguage}}', 'project_id', '{{%project}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-projectlanguage-domain_id', '{{%projectlanguage}}', 'domain_id', '{{%domain}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-projectlanguage-language_id', '{{%projectlanguage}}', 'language_id', '{{%language}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-projectlanguage-created_by', '{{%projectlanguage}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-projectlanguage-updated_by', '{{%projectlanguage}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-projectlanguage-activated_by', '{{%projectlanguage}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-projectlanguage-deactivated_by', '{{%projectlanguage}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-projectlanguage-deleted_by', '{{%projectlanguage}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-domain-project_id', '{{%domain}}', 'project_id', '{{%project}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-domain-created_by', '{{%domain}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-domain-updated_by', '{{%domain}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-domain-activated_by', '{{%domain}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-domain-deactivated_by', '{{%domain}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-domain-deleted_by', '{{%domain}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-project-primary_projectlanguage_id', '{{%project}}');
        $this->dropForeignKey('fk-project-created_by', '{{%project}}');
        $this->dropForeignKey('fk-project-updated_by', '{{%project}}');
        $this->dropForeignKey('fk-project-activated_by', '{{%project}}');
        $this->dropForeignKey('fk-project-deactivated_by', '{{%project}}');
        $this->dropForeignKey('fk-project-deleted_by', '{{%project}}');

        $this->dropForeignKey('fk-projectlanguage-project_id', '{{%projectlanguage}}');
        $this->dropForeignKey('fk-projectlanguage-domain_id', '{{%projectlanguage}}');
        $this->dropForeignKey('fk-projectlanguage-language_id', '{{%projectlanguage}}');
        $this->dropForeignKey('fk-projectlanguage-created_by', '{{%projectlanguage}}');
        $this->dropForeignKey('fk-projectlanguage-updated_by', '{{%projectlanguage}}');
        $this->dropForeignKey('fk-projectlanguage-activated_by', '{{%projectlanguage}}');
        $this->dropForeignKey('fk-projectlanguage-deactivated_by', '{{%projectlanguage}}');
        $this->dropForeignKey('fk-projectlanguage-deleted_by', '{{%projectlanguage}}');

        $this->dropForeignKey('fk-domain-project_id', '{{%domain}}');
        $this->dropForeignKey('fk-domain-created_by', '{{%domain}}');
        $this->dropForeignKey('fk-domain-updated_by', '{{%domain}}');
        $this->dropForeignKey('fk-domain-activated_by', '{{%domain}}');
        $this->dropForeignKey('fk-domain-deactivated_by', '{{%domain}}');
        $this->dropForeignKey('fk-domain-deleted_by', '{{%domain}}');

        $this->dropTable('{{%project}}');
        $this->dropTable('{{%projectlanguage}}');
        $this->dropTable('{{%domain}}');
    }
}
