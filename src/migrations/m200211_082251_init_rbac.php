<?php

namespace fafcms\fafcms\migrations;

use fafcms\fafcms\models\AuthModel;
use fafcms\fafcms\models\User;
use fafcms\fafcms\rbac\BackendPermission;
use fafcms\fafcms\rbac\CreatePermission;
use fafcms\fafcms\rbac\DeletePermission;
use fafcms\fafcms\rbac\EditPermission;
use fafcms\fafcms\rbac\ViewPermission;
use Yii;
use yii\db\Migration;

/**
 * Class m200211_082251_init_rbac
 * @package fafcms\fafcms\migrations
 */
class m200211_082251_init_rbac extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $backendPermission = new BackendPermission();
        $auth->add($backendPermission);

        $viewPermission = new ViewPermission();
        $auth->add($viewPermission);

        $createPermission = new CreatePermission();
        $auth->add($createPermission);

        $editPermission = new EditPermission();
        $auth->add($editPermission);

        $deletePermission = new DeletePermission();
        $auth->add($deletePermission);

        $guest = $auth->createRole('visitor');
        $guest->description = ['fafcms-core', 'Visitor'];
        $guest->data = ['system' => true];
        $auth->add($guest);

        $author = $auth->createRole('author');
        $author->description = ['fafcms-core', 'Author'];
        $author->data = ['system' => true];
        $auth->add($author);
        $auth->addChild($author, $backendPermission);

        $admin = $auth->createRole('admin');
        $admin->description = ['fafcms-core', 'Admin'];
        $admin->data = ['system' => true];
        $auth->add($admin);
        $auth->addChild($admin, $backendPermission);
        $auth->addChild($admin, $viewPermission);
        $auth->addChild($admin, $createPermission);
        $auth->addChild($admin, $editPermission);
        $auth->addChild($admin, $deletePermission);

        $modelClasses = array_keys(Yii::$app->fafcms->getModelClasses());

        foreach ($modelClasses as $modelClass) {
            $authModel = new AuthModel([
                'type' => AuthModel::TYPE_ROLE,
                'permission_item_name' => $viewPermission->name,
                'role_item_name' => $admin->name,
                'model_type' => AuthModel::MODEL_TYPE_TABLE,
                'model_class' => $modelClass
            ]);

            if (!$authModel->save()) {
                return false;
            }

            $authModel = new AuthModel([
                'type' => AuthModel::TYPE_ROLE,
                'permission_item_name' => $createPermission->name,
                'role_item_name' => $admin->name,
                'model_type' => AuthModel::MODEL_TYPE_TABLE,
                'model_class' => $modelClass
            ]);

            if (!$authModel->save()) {
                return false;
            }

            $authModel = new AuthModel([
                'type' => AuthModel::TYPE_ROLE,
                'permission_item_name' => $editPermission->name,
                'role_item_name' => $admin->name,
                'model_type' => AuthModel::MODEL_TYPE_TABLE,
                'model_class' => $modelClass
            ]);

            if (!$authModel->save()) {
                return false;
            }

            $authModel = new AuthModel([
                'type' => AuthModel::TYPE_ROLE,
                'permission_item_name' => $deletePermission->name,
                'role_item_name' => $admin->name,
                'model_type' => AuthModel::MODEL_TYPE_TABLE,
                'model_class' => $modelClass
            ]);

            if (!$authModel->save()) {
                return false;
            }
        }

        $userIds = User::find()->select('id')->column();

        foreach ($userIds as $userId) {
            $auth->assign($admin, $userId);
        }
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $this->truncateTable(AuthModel::tableName());
    }
}
