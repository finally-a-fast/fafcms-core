<?php

namespace fafcms\fafcms\migrations;

use yii\db\Migration;

/**
 * Class m190905_121220_tags
 * @package fafcms\fafcms\migrations
 */
class m190905_121220_tags extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tag}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'display_start' => $this->datetime()->null()->defaultValue(null),
            'display_end' => $this->datetime()->null()->defaultValue(null),
            'site_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'name' => $this->string(255)->notNull(),
            'content' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-tag-site_id', '{{%tag}}', ['site_id'], false);
        $this->createIndex('idx-tag-created_by', '{{%tag}}', ['created_by'], false);
        $this->createIndex('idx-tag-updated_by', '{{%tag}}', ['updated_by'], false);
        $this->createIndex('idx-tag-activated_by', '{{%tag}}', ['activated_by'], false);
        $this->createIndex('idx-tag-deactivated_by', '{{%tag}}', ['deactivated_by'], false);
        $this->createIndex('idx-tag-deleted_by', '{{%tag}}', ['deleted_by'], false);

        $this->createTable('{{%tagrealation}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'tag_id' => $this->integer(10)->unsigned()->notNull(),
            'model_class' => $this->string(255)->notNull(),
            'model_id' => $this->integer(10)->unsigned()->notNull(),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-tagrealation-tag_id', '{{%tagrealation}}', ['tag_id'], false);
        $this->createIndex('idx-tagrealation-model_class', '{{%tagrealation}}', ['model_class'], false);
        $this->createIndex('idx-tagrealation-model_id', '{{%tagrealation}}', ['model_id'], false);
        $this->createIndex('idx-tagrealation-relation', '{{%tagrealation}}', ['tag_id', 'model_class', 'model_id'], false);
        $this->createIndex('idx-tagrealation-created_by', '{{%tagrealation}}', ['created_by'], false);
        $this->createIndex('idx-tagrealation-deleted_by', '{{%tagrealation}}', ['deleted_by'], false);

        $this->addForeignKey('fk-tag-site_id', '{{%tag}}', 'site_id', '{{%site}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-tag-created_by', '{{%tag}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-tag-updated_by', '{{%tag}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-tag-activated_by', '{{%tag}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-tag-deactivated_by', '{{%tag}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-tag-deleted_by', '{{%tag}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-tagrealation-tag_id', '{{%tagrealation}}', 'tag_id', '{{%tag}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-tagrealation-created_by', '{{%tagrealation}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-tagrealation-deleted_by', '{{%tagrealation}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-tag-site_id', '{{%tag}}');
        $this->dropForeignKey('fk-tag-created_by', '{{%tag}}');
        $this->dropForeignKey('fk-tag-updated_by', '{{%tag}}');
        $this->dropForeignKey('fk-tag-activated_by', '{{%tag}}');
        $this->dropForeignKey('fk-tag-deactivated_by', '{{%tag}}');
        $this->dropForeignKey('fk-tag-deleted_by', '{{%tag}}');

        $this->dropForeignKey('fk-tagrealation-tag_id', '{{%tagrealation}}');
        $this->dropForeignKey('fk-tagrealation-created_by', '{{%tagrealation}}');
        $this->dropForeignKey('fk-tagrealation-deleted_by', '{{%tagrealation}}');

        $this->dropTable('{{%tag}}');
        $this->dropTable('{{%tagrealation}}');
    }
}
