<?php

namespace fafcms\fafcms\migrations;

use fafcms\fafcms\models\User;
use yii\db\Migration;

/**
 * Class m200326_103739_usersettings
 * @package fafcms\fafcms\migrations
 */
class m200326_103739_usersettings extends Migration
{
    public function safeUp()
    {
        $this->addColumn(User::tableName(), 'animation_auto', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('notificaitons_browser'));
        $this->addColumn(User::tableName(), 'animation_focus', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('animation_auto'));
        $this->addColumn(User::tableName(), 'animation_action', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('animation_focus'));
        $this->addColumn(User::tableName(), 'animation_system', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('animation_action'));
        $this->addColumn(User::tableName(), 'video_animated_preview', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('animation_system'));
        $this->addColumn(User::tableName(), 'video_muted', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('video_animated_preview'));
        $this->addColumn(User::tableName(), 'video_default_resolution', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('video_muted'));
        $this->addColumn(User::tableName(), 'video_default_resolution_mobile', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('video_default_resolution'));
        $this->addColumn(User::tableName(), 'video_autoplay', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1)->after('video_default_resolution_mobile'));
        $this->addColumn(User::tableName(), 'video_autoplay_mobile', $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0)->after('video_autoplay'));
    }

    public function safeDown()
    {
        $this->dropColumn(User::tableName(), 'animation_auto');
        $this->dropColumn(User::tableName(), 'animation_focus');
        $this->dropColumn(User::tableName(), 'animation_action');
        $this->dropColumn(User::tableName(), 'animation_system');
        $this->dropColumn(User::tableName(), 'video_animated_preview');
        $this->dropColumn(User::tableName(), 'video_muted');
        $this->dropColumn(User::tableName(), 'video_default_resolution');
        $this->dropColumn(User::tableName(), 'video_default_resolution_mobile');
        $this->dropColumn(User::tableName(), 'video_autoplay');
        $this->dropColumn(User::tableName(), 'video_autoplay_mobile');
    }
}
