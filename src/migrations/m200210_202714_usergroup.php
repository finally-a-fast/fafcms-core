<?php

namespace fafcms\fafcms\migrations;

use fafcms\fafcms\models\AuthModel;
use fafcms\fafcms\models\User;
use yii\db\Migration;
use Yii;

/**
 * Class m200210_202714_usergroup
 * @package fafcms\fafcms\migrations
 */
class m200210_202714_usergroup extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk-user-usergroup_id', User::tableName());
        $this->dropColumn(User::tableName(), 'usergroup_id');

        $this->dropForeignKey('fk-usergroup-created_by', '{{%fafcms-core_usergroup}}');
        $this->dropForeignKey('fk-usergroup-updated_by', '{{%fafcms-core_usergroup}}');
        $this->dropForeignKey('fk-usergroup-activated_by', '{{%fafcms-core_usergroup}}');
        $this->dropForeignKey('fk-usergroup-deactivated_by', '{{%fafcms-core_usergroup}}');
        $this->dropForeignKey('fk-usergroup-deleted_by', '{{%fafcms-core_usergroup}}');

        $this->dropTable('{{%fafcms-core_usergroup}}');
    }

    public function safeDown()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%fafcms-core_usergroup}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('inactive'),
            'name' => $this->string(255)->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-usergroup-created_by', '{{%fafcms-core_usergroup}}', ['created_by'], false);
        $this->createIndex('idx-usergroup-updated_by', '{{%fafcms-core_usergroup}}', ['updated_by'], false);
        $this->createIndex('idx-usergroup-activated_by', '{{%fafcms-core_usergroup}}', ['activated_by'], false);
        $this->createIndex('idx-usergroup-deactivated_by', '{{%fafcms-core_usergroup}}', ['deactivated_by'], false);
        $this->createIndex('idx-usergroup-deleted_by', '{{%fafcms-core_usergroup}}', ['deleted_by'], false);

        $this->addForeignKey('fk-usergroup-created_by', '{{%fafcms-core_usergroup}}', 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-usergroup-updated_by', '{{%fafcms-core_usergroup}}', 'updated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-usergroup-activated_by', '{{%fafcms-core_usergroup}}', 'activated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-usergroup-deactivated_by', '{{%fafcms-core_usergroup}}', 'deactivated_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-usergroup-deleted_by', '{{%fafcms-core_usergroup}}', 'deleted_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');


        $this->addColumn(User::tableName(), 'usergroup_id', $this->integer(10)->unsigned()->notNull()->after('status'));

        $this->createIndex('idx-user-usergroup_id', User::tableName(), ['usergroup_id'], false);
        $this->addForeignKey('fk-user-usergroup_id', User::tableName(), 'usergroup_id', '{{%fafcms-core_usergroup}}', 'id', 'RESTRICT', 'CASCADE');
    }
}
