<?php

namespace fafcms\fafcms\migrations;

use fafcms\fafcms\models\QueueHelper;
use yii\db\Migration;
use Yii;

/**
 * Class m200217_222739_queue_helper
 * @package fafcms\fafcms\migrations
 */
class m200217_222739_queue_helper extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(QueueHelper::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'queue_id' => $this->integer(11)->notNull(),
            'action' => $this->string(255)->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk-queuehelper-queue_id', QueueHelper::tableName(), 'queue_id', '{{%queue}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-queuehelper-queue_id', QueueHelper::tableName());
        $this->dropTable(QueueHelper::tableName());
    }
}
