<?php

namespace fafcms\fafcms\migrations;

use fafcms\fafcms\models\Country;
use fafcms\fafcms\models\Customfield;
use fafcms\fafcms\models\Customtable;
use fafcms\fafcms\models\Domain;
use fafcms\fafcms\models\Flag;
use fafcms\fafcms\models\Language;
use fafcms\fafcms\models\Project;
use fafcms\fafcms\models\Projectlanguage;
use fafcms\fafcms\models\Tag;
use fafcms\fafcms\models\Tagrealation;
use fafcms\fafcms\models\Translation;
use fafcms\fafcms\models\User;
use fafcms\fafcms\models\Usergroup;
use fafcms\sitemanager\models\Snippet;
use yii\db\Migration;

/**
 * Class m200120_211853_prefix
 * @package fafcms\fafcms\migrations
 */
class m200120_211853_prefix extends Migration
{
    public function safeUp()
    {
        $this->renameTable('{{%country}}', Country::tableName());
        $this->renameTable('{{%flag}}', Flag::tableName());
        $this->renameTable('{{%language}}', Language::tableName());
        $this->renameTable('{{%snippet}}', Snippet::tableName());
        $this->renameTable('{{%translation}}', Translation::tableName());
        $this->renameTable('{{%user}}', User::tableName());
        $this->renameTable('{{%usergroup}}', '{{%fafcms-core_usergroup}}');
        $this->renameTable('{{%tag}}', Tag::tableName());
        $this->renameTable('{{%tagrealation}}', Tagrealation::tableName());
        $this->renameTable('{{%project}}', Project::tableName());
        $this->renameTable('{{%projectlanguage}}', Projectlanguage::tableName());
        $this->renameTable('{{%domain}}', Domain::tableName());
        $this->renameTable('{{%customtable}}', Customtable::tableName());
        $this->renameTable('{{%customfield}}', Customfield::tableName());
    }

    public function safeDown()
    {
        $this->renameTable(Country::tableName(), '{{%country}}');
        $this->renameTable(Flag::tableName(), '{{%flag}}');
        $this->renameTable(Language::tableName(), '{{%language}}');
        $this->renameTable(Snippet::tableName(), '{{%snippet}}');
        $this->renameTable(Translation::tableName(), '{{%translation}}');
        $this->renameTable(User::tableName(), '{{%user}}');
        $this->renameTable('{{%fafcms-core_usergroup}}', '{{%usergroup}}');
        $this->renameTable(Tag::tableName(), '{{%tag}}');
        $this->renameTable(Tagrealation::tableName(), '{{%tagrealation}}');
        $this->renameTable(Project::tableName(), '{{%project}}');
        $this->renameTable(Projectlanguage::tableName(), '{{%projectlanguage}}');
        $this->renameTable(Domain::tableName(), '{{%domain}}');
        $this->renameTable(Customtable::tableName(), '{{%customtable}}');
        $this->renameTable(Customfield::tableName(), '{{%customfield}}');
    }
}
