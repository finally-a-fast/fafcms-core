<?php

namespace fafcms\fafcms\migrations;

use fafcms\fafcms\models\Project;
use fafcms\fafcms\models\Projectlanguage;
use yii\db\Migration;

/**
 * Class m200922_130412_open_graph
 *
 * @package fafcms\fafcms\migrations
 */
class m200922_130412_open_graph extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Projectlanguage::tableName(), 'og_site_name', $this->string(255)->null()->defaultValue(null)->after('description'));
        $this->addColumn(Projectlanguage::tableName(), 'fb_app_id', $this->string(255)->null()->defaultValue(null)->after('og_site_name'));

        $this->addColumn(Project::tableName(), 'og_site_name', $this->string(255)->null()->defaultValue(null)->after('description'));
        $this->addColumn(Project::tableName(), 'fb_app_id', $this->string(255)->null()->defaultValue(null)->after('og_site_name'));
    }

    public function safeDown()
    {
        $this->dropColumn(Projectlanguage::tableName(), 'og_site_name');
        $this->dropColumn(Projectlanguage::tableName(), 'fb_app_id');

        $this->dropColumn(Project::tableName(), 'og_site_name');
        $this->dropColumn(Project::tableName(), 'fb_app_id');
    }
}
