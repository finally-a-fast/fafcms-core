<?php

namespace fafcms\fafcms\migrations;

use yii\db\Migration;

/**
 * Class m190925_065950_customtable_customfield
 * @package fafcms\fafcms\migrations
 */
class m190925_065950_customtable_customfield extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%customtable}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'project_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'model_class' => $this->string(255)->notNull(),
            'name' => $this->string(20)->notNull(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-customtable-project_id', '{{%customtable}}', ['project_id'], false);
        $this->createIndex('idx-customfield-model_class', '{{%customtable}}', ['model_class'], false);
        $this->createIndex('idx-customtable-created_by', '{{%customtable}}', ['created_by'], false);
        $this->createIndex('idx-customtable-updated_by', '{{%customtable}}', ['updated_by'], false);
        $this->createIndex('idx-customtable-activated_by', '{{%customtable}}', ['activated_by'], false);
        $this->createIndex('idx-customtable-deactivated_by', '{{%customtable}}', ['deactivated_by'], false);
        $this->createIndex('idx-customtable-deleted_by', '{{%customtable}}', ['deleted_by'], false);

        $this->createTable('{{%customfield}}', [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'project_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'customtable_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'model_class' => $this->string(255)->notNull(),
            'type' => $this->string(255)->notNull(),
            'name' => $this->string(20)->notNull(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-customfield-project_id', '{{%customfield}}', ['project_id'], false);
        $this->createIndex('idx-customfield-customtable_id', '{{%customfield}}', ['customtable_id'], false);
        $this->createIndex('idx-customfield-model_class', '{{%customfield}}', ['model_class'], false);
        $this->createIndex('idx-customfield-created_by', '{{%customfield}}', ['created_by'], false);
        $this->createIndex('idx-customfield-updated_by', '{{%customfield}}', ['updated_by'], false);
        $this->createIndex('idx-customfield-activated_by', '{{%customfield}}', ['activated_by'], false);
        $this->createIndex('idx-customfield-deactivated_by', '{{%customfield}}', ['deactivated_by'], false);
        $this->createIndex('idx-customfield-deleted_by', '{{%customfield}}', ['deleted_by'], false);

        $this->addForeignKey('fk-customtable-project_id', '{{%customtable}}', 'project_id', '{{%project}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-customtable-created_by', '{{%customtable}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-customtable-updated_by', '{{%customtable}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-customtable-activated_by', '{{%customtable}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-customtable-deactivated_by', '{{%customtable}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-customtable-deleted_by', '{{%customtable}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-customfield-project_id', '{{%customfield}}', 'project_id', '{{%project}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-customfield-customtable_id', '{{%customfield}}', 'customtable_id', '{{%domain}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-customfield-created_by', '{{%customfield}}', 'created_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-customfield-updated_by', '{{%customfield}}', 'updated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-customfield-activated_by', '{{%customfield}}', 'activated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-customfield-deactivated_by', '{{%customfield}}', 'deactivated_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-customfield-deleted_by', '{{%customfield}}', 'deleted_by', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-customtable-project_id', '{{%customtable}}');
        $this->dropForeignKey('fk-customtable-created_by', '{{%customtable}}');
        $this->dropForeignKey('fk-customtable-updated_by', '{{%customtable}}');
        $this->dropForeignKey('fk-customtable-activated_by', '{{%customtable}}');
        $this->dropForeignKey('fk-customtable-deactivated_by', '{{%customtable}}');
        $this->dropForeignKey('fk-customtable-deleted_by', '{{%customtable}}');

        $this->dropForeignKey('fk-customfield-project_id', '{{%customfield}}');
        $this->dropForeignKey('fk-customfield-customtable_id', '{{%customfield}}');
        $this->dropForeignKey('fk-customfield-created_by', '{{%customfield}}');
        $this->dropForeignKey('fk-customfield-updated_by', '{{%customfield}}');
        $this->dropForeignKey('fk-customfield-activated_by', '{{%customfield}}');
        $this->dropForeignKey('fk-customfield-deactivated_by', '{{%customfield}}');
        $this->dropForeignKey('fk-customfield-deleted_by', '{{%customfield}}');

        $this->dropTable('{{%customtable}}');
        $this->dropTable('{{%customfield}}');
    }
}
