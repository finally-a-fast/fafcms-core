<?php

namespace fafcms\fafcms\migrations;

use fafcms\fafcms\models\AuthModel;
use fafcms\fafcms\models\User;
use yii\db\Migration;
use Yii;

/**
 * Class m200210_190721_auth_model
 * @package fafcms\fafcms\migrations
 */
class m200210_190721_auth_model extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(AuthModel::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'permission_item_name' => $this->string(255)->notNull(),
            'type' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'role_item_name' => $this->string(255)->null(),
            'user_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'model_type' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'model_class' => $this->string(255)->notNull(),
            'model_id' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-auth_model-permission_item_name', AuthModel::tableName(), ['permission_item_name'], false);
        $this->createIndex('idx-auth_model-role_item_name', AuthModel::tableName(), ['role_item_name'], false);
        $this->createIndex('idx-auth_model-user_id', AuthModel::tableName(), ['user_id'], false);
        $this->createIndex('idx-auth_model-model_class', AuthModel::tableName(), ['model_class'], false);
        $this->createIndex('idx-auth_model-model_id', AuthModel::tableName(), ['model_id'], false);
        $this->createIndex('idx-auth_model-created_by', AuthModel::tableName(), ['created_by'], false);

        $this->addForeignKey('fk-auth_model-permission_item_name', AuthModel::tableName(), 'permission_item_name', '{{%auth_item}}', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-auth_model-role_item_name', AuthModel::tableName(), 'role_item_name', '{{%auth_item}}', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-auth_model-user_id', AuthModel::tableName(), 'user_id', User::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-auth_model-created_by', AuthModel::tableName(), 'created_by', User::tableName(), 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-auth_model-permission_item_name', AuthModel::tableName());
        $this->dropForeignKey('fk-auth_model-role_item_name', AuthModel::tableName());
        $this->dropForeignKey('fk-auth_model-user_id', AuthModel::tableName());
        $this->dropForeignKey('fk-auth_model-created_by', AuthModel::tableName());

        $this->dropTable(AuthModel::tableName());
    }
}
