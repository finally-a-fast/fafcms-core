<?php
return [
    'Lost password?' => 'Passwort vergessen?',
    'Login' => 'Einloggen',
    'Email Address' => 'E-Mail-Adresse',
    'Password' => 'Passwort',
    'Remember Me' => 'Angemeldet bleiben',
];
