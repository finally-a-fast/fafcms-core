<?php
return [
    'min' => '{attribute} muss mindestens {n, plural, one{ein Zeichen} other{# Zeichen}} enthalten ({found} gefunden)!',
    'max' => '{attribute} darf höchstens {n, plural, one{ein Zeichen} other{# Zeichen}} enthalten ({found} gefunden)!',
    'length' => '{attribute} muss genau {n, plural, one{ein Zeichen} other{# Zeichen}} enthalten ({found} gefunden)!',
    'allowSpaces' => '{attribute} kann keine Leerzeichen enthalten!',
    'hasUser' => '{attribute} darf den Benutzernamen nicht enthalten!',
    'hasEmail' => '{attribute} darf keine E-Mail-Adresse enthalten!',
    'lower' => '{attribute} muss mindestens {n, plural, one{einen Kleinbuchstaben enthalten} other{# Kleinbuchstaben enthalten}} ({found} gefunden)!',
    'upper' => '{attribute} muss mindestens {n, plural, one{ein Großbuchstaben enthalten} other{# Großbuchstaben enthalten}} ({found} gefunden)!',
    'digit' => '{attribute} muss mindestens {n, plural, one{eine Ziffer enthalten} other{# Ziffern enthalten}} ({found} gefunden)!',
    'special' => '{attribute} muss mindestens {n, plural, one{ein Sonderzeichen enthalten} other{# Sonderzeichen enthalten}} ({found} gefunden)!',
    'string' => '{attribute} muss eine Zeichenfogle sein!'
];
