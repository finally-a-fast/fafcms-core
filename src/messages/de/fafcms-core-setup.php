<?php
return [
    'setup' => 'Einrichtung',
    'thanks_for_download' => 'Vielen Dank für das Herunterladen von Finally a fast CMS!',
    'step_{current}_of_{max}' => 'Schritt {current} von {max}',
    'system_check' => 'Systemprüfung'
];
