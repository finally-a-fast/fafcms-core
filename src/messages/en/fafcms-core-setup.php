<?php
return [
    'setup' => 'Setup',
    'thanks_for_download' => 'Thank you for downloading Finally a fast CMS!',
    'step_{current}_of_{max}' => 'Step {current} of {max}',
    'system_check' => 'System check'
];
