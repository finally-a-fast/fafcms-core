<?php
return [
    'no_js_warning' => 'For full functionality of this site it is necessary to enable JavaScript.<br>Here are the <a href="https://www.enable-javascript.com/en" target="_blank">instructions how to enable JavaScript in your web browser</a>.',
    'old_browser_warning' => 'You are using an <b>outdated</b> browser. Here are the <a href="https://browsehappy.com/?locale=en" target="_blank">instructions how to update your web browser</a>.',
    'back' => 'Back',
    'next' => 'Next',
    'choose_language' => 'Choose language',
];
