<?php

namespace fafcms\fafcms\models;

use Yii;

/**
 * This is the model class for table "queue".
 *
 * @property int $id
 * @property string $channel
 * @property resource $job
 * @property int $pushed_at
 * @property int $ttr
 * @property int $delay
 * @property int $priority
 * @property int|null $reserved_at
 * @property int|null $attempt
 * @property int|null $done_at
 *
 * @property Queuehelper[] $queuehelpers
 */
class Queue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['channel', 'job', 'pushed_at', 'ttr'], 'required'],
            [['job'], 'string'],
            [['pushed_at', 'ttr', 'delay', 'priority', 'reserved_at', 'attempt', 'done_at'], 'integer'],
            [['channel'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('fafcms-core', 'ID'),
            'channel' => Yii::t('fafcms-core', 'Channel'),
            'job' => Yii::t('fafcms-core', 'Job'),
            'pushed_at' => Yii::t('fafcms-core', 'Pushed At'),
            'ttr' => Yii::t('fafcms-core', 'Ttr'),
            'delay' => Yii::t('fafcms-core', 'Delay'),
            'priority' => Yii::t('fafcms-core', 'Priority'),
            'reserved_at' => Yii::t('fafcms-core', 'Reserved At'),
            'attempt' => Yii::t('fafcms-core', 'Attempt'),
            'done_at' => Yii::t('fafcms-core', 'Done At'),
        ];
    }

    /**
     * Gets query for [[Queuehelpers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQueuehelpers()
    {
        return $this->hasMany(Queuehelper::className(), ['queue_id' => 'id']);
    }
}
