<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\abstracts\models\BaseDomain;
use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\classes\OptionProvider;

/**
 * This is the model class for table "{{%domain}}".
 *
 * @package fafcms\fafcms\models
 */
class Domain extends BaseDomain
{
    public static string $searchModelClass = DomainSearch::class;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'wan';
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['domain'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.domain'
            ])
            ->setSort([static::tableName() . '.domain' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => ['fafcms-core', 'Master data'],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => ['fafcms-core', 'Master data'],
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                'field-domain' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'domain',
                                                    ],
                                                ],
                                                'field-project_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'project_id',
                                                    ],
                                                ],
                                                'field-projectlanguage_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'projectlanguage_id',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'access-restrictions-card' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => ['fafcms-core', 'Access restrictions'],
                                                'icon' => 'timetable',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-is_redirect' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_redirect',
                                                    ],
                                                ],
                                                'field-redirect_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'redirect_id',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-3' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'access-restrictions-card' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => ['fafcms-core', 'Preview'],
                                                'icon' => 'file-eye-outline',
                                            ],
                                            'contents' => [
                                                'field-force_https' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'force_https',
                                                    ],
                                                ],
                                                'field-is_https_validated' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_https_validated',
                                                    ],
                                                ],
                                                'field-last_https_validation_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_https_validation_at',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ]
                            ],
                        ],
                        'row-2' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'content-card' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => ['fafcms-core', 'Web access'],
                                                'icon' => 'web',
                                            ],
                                            'contents' => [
                                                'field-is_validated' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_validated',
                                                    ],
                                                ],
                                                'field-last_validation_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_validation_at',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'content-card' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => ['fafcms-core', 'Status'],
                                                'icon' => 'card-text-outline',
                                            ],
                                            'contents' => [
                                                'field-is_wildcard' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_wildcard',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                            ],
                        ]
                    ]
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * @param Domain $model
     *
     * @return string
     */
    public static function getUrl(Domain $model): string
    {
        return 'http' . (Yii::$app->getRequest()->getIsSecureConnection() || ($model->is_https_validated && $model->force_https) ? 's' : '') . '://' . $model->domain;
    }

    /**
     * @return string
     */
    public function getAbsoluteUrl(): string
    {
        return self::getUrl($this);
    }
}
