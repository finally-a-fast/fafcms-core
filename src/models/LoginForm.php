<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\interfaces\EditViewInterface;
use fafcms\helpers\interfaces\FieldConfigInterface;
use fafcms\helpers\Model;
use Yii;

/**
 * User login form
 * Class LoginForm
 *
 * @package fafcms\fafcms\models
 */
class LoginForm extends Model implements FieldConfigInterface, EditViewInterface
{
    /**
     * @var bool
     */
    public static bool $checkAccess = false;

    /**
     * @var string|null
     */
    public ?string $username = null;

    /**
     * @var string|null
     */
    public ?string $password = null;

    /**
     * @var bool
     */
    public bool $rememberMe = true;

    /**
     * @var bool
     */
    public bool $allowUsername = true;

    /**
     * @var bool
     */
    public bool $allowEmail = true;

    /**
     * @var User|null
     */
    protected ?User $foundUser = null;

    /**
     * @var User[]|null
     */
    private ?array $users = null;

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'username' => [
                'type' => TextInput::class,
                'options' => [
                    'autofocus' => true
                ]
            ],
            'password' => [
                'type'    => TextInput::class,
                'options' => ['type' => 'password'],
            ],
            'rememberMe' => [
                'type' => Checkbox::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'buttons' => [
                    static function($buttons, $model, $form, $editView) {
                        $buttons['lost-password'] = [
                            'icon' => 'account-key-outline',
                            'label' => ['fafcms-core', 'Lost password?'],
                            'url' => ['request-password-reset'],
                        ];

                        $buttons['edit'] = [
                            'form' => $form->id,
                            'options' => [
                                'class' => ['primary'],
                            ],
                            'type' => 'submit',
                            'icon' => 'login-variant',
                            'label' => ['fafcms-core', 'Login'],
                        ];

                        return $buttons;
                    }
                ],
                'contents' => [
                    'tab-1' => [
                        'class' => Tab::class,
                        'settings' => [
                            'label' => [
                                'fafcms-core',
                                'Login',
                            ],
                        ],
                        'contents' => [
                            'row-1' => [
                                'class' => Row::class,
                                'contents' => [
                                    'column-1' => [
                                        'class' => Column::class,
                                        'settings' => [
                                            'm' => 4,
                                        ],
                                    ],
                                    'column-2' => [
                                        'class' => Column::class,
                                        'settings' => [
                                            'm' => 8,
                                        ],
                                        'contents' => [
                                            'card-1' => [
                                                'class' => Card::class,
                                                'settings' => [
                                                    'title' => [
                                                        'fafcms-core',
                                                        'Login',
                                                    ],
                                                    'icon' => 'login-variant',
                                                ],
                                                'buttons' => [
                                                    'lost-password' => [
                                                        'icon' => 'account-key-outline',
                                                        'label' => ['fafcms-core', 'Lost password?'],
                                                        'url' => ['request-password-reset'],
                                                        'rules' => false
                                                    ],
                                                    'edit' => [
                                                        'icon' => 'login-variant',
                                                        'label' => ['fafcms-core', 'Login'],
                                                        'options' => [
                                                            'class' => ['primary'],
                                                        ],
                                                        'type' => 'submit',
                                                        'rules' => false
                                                    ]
                                                ],
                                                'contents' => [
                                                    'field-username' => [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'username',
                                                        ],
                                                    ],
                                                    'field-password' => [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'password',
                                                        ],
                                                    ],
                                                    'field-rememberMe' => [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'rememberMe',
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * @inheritdoc
     */
    public function loadDefaultValues($skipIfSet = true): self
    {
        $result = parent::loadDefaultValues($skipIfSet);

        if (!$skipIfSet || $result->username === null) {
            $result->username = Yii::$app->request->getQueryParam('email', '');
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-username' => ['username', 'required'],
            'required-password' => ['password', 'required'],
            'boolean-rememberMe' => ['rememberMe', 'boolean'],
            'validatePassword-password' => ['password', 'validatePassword'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'username' => Yii::t('fafcms-core-login',  'Email Address'),
            'password' => Yii::t('fafcms-core-login',  'Password'),
            'rememberMe' => Yii::t('fafcms-core-login',  'Remember Me'),
        ]);
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->foundUser;
    }

    /**
     * @param $attribute
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function validatePassword($attribute): void
    {
        if (!$this->hasErrors()) {
            $users = $this->getUsers();
            $found = false;

            foreach ($users as $user) {
                if ($user->validatePassword($this->password)) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $this->addError($attribute, Yii::t('fafcms-core', 'Invalid credentials.'));
            }
        }
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function save($runValidation = true, $attributeNames = null): bool
    {
        if ($this->validate()) {
            $users = $this->getUsers();
            $foundUser = null;

            foreach ($users as $user) {
                if ($user->validatePassword($this->password)) {
                    $foundUser = $user;

                    break;
                }
            }

            if (!is_null($foundUser)) {
                $this->foundUser = $foundUser;
                return Yii::$app->user->login($foundUser, $this->rememberMe ? 3600 * 24 * 30 : 0);
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getRedirectAfterSave(): bool
    {
        return true;
    }

    /**
     * @return string
     */
    public function getRedirectUrlAfterSave(): string
    {
        return Yii::$app->getUser()->getReturnUrl();
    }

    /**
     * @return string
     */
    public function getErrorMessageAfterSave(): string
    {
        return Yii::$app->fafcms->getTextOrTranslation(['fafcms-core', 'Invalid credentials.']);
    }

    /**
     * @return string
     */
    public function getSuccessMessageAfterSave(): string
    {
        if (method_exists($this->foundUser, 'getExtendedLabel')) {
            return Yii::$app->fafcms->getTextOrTranslation(['fafcms-core', 'Welcome back {name}!', ['name' => $this->foundUser->getExtendedLabel(false)]]);
        }

        return Yii::$app->fafcms->getTextOrTranslation(['fafcms-core', 'Welcome back!']);
    }

    /**
     * @return User[]|null
     * @throws \yii\base\InvalidConfigException
     */
    protected function getUsers(): ?array
    {
        if ($this->users === null) {
            $userFilter = [];

            if ($this->allowUsername && User::instance()->hasAttribute('username')) {
                $userFilter[] = ['username' => $this->username];
            }

            if ($this->allowEmail && User::instance()->hasAttribute('email')) {
                $userFilter[] = ['email' => $this->username];
            }

            if (count($userFilter) > 0) {
                $this->users = User::find()->where(array_merge(
                    ['or'],
                    $userFilter
                ))->all();
            }
        }

        return $this->users;
    }
}

/*
namespace fafcms\models;

use fafcms\fafcms\models\User;
use Yii;
use yii\base\Model;
use yii\web\Cookie;
use DateTime;
use DateTimeZone;
use DateInterval;

class LoginForm extends Model
{
    public function login()
    {
        if ($this->validate() && $user = User::find()->where(['email' => $this->email])->one()) {
            $blocked = false;

            if (!empty($user->wrong_password_blocked_until)) {
                $now = new DateTime('now', new DateTimeZone('Europe/Berlin'));
                $blockTime = DateTime::createFromFormat('Y-m-d H:i:s', $user->wrong_password_blocked_until, new DateTimeZone('Europe/Berlin'));

                if ($now->diff($blockTime)->invert === 0) {
                    $blocked = true;
                }
            }

            if (!$blocked) {
                if ($user->validatePassword($this->password)) {
                    $user->wrong_password_count = 0;
                    $user->wrong_password_blocked_count = null;
                    $user->wrong_password_blocked_until = null;
                    $user->save();

                    return Yii::$app->user->login($user, $this->rememberMe? 3600 * 24 * 30: 0);
                } else {
                    if (empty($user->wrong_password_count)) {
                        $user->wrong_password_count = 0;
                    }

                    $user->wrong_password_count++;

                    if ($user->wrong_password_count >= Yii::$app->getModule('settingmanager')->getSetting('wrong_password_block_count', 5)) {
                        $newBlockCount = floor($user->wrong_password_count / Yii::$app->getModule('settingmanager')->getSetting('wrong_password_block_count', 5));

                        if ($newBlockCount < 1) {
                            $newBlockCount = 1;
                        }

                        if ($newBlockCount > $user->wrong_password_blocked_count) {
                            $blockTime = new DateTime('now', new DateTimeZone('Europe/Berlin'));
                            $blockTime->add(new DateInterval('PT'.Yii::$app->getModule('settingmanager')->getSetting('wrong_login_block_time_'.($newBlockCount > 5?5:$newBlockCount), 60).'S'));

                            $user->wrong_password_blocked_until = $blockTime->format('Y-m-d H:i:s');
                            $blocked = true;

                            $message = '<pre>';
                            $message .= 'User: ' . print_r($user->getAttributes(), true) . "<br>\n";
                            $message .= '$_SERVER: ' . print_r($_SERVER, true) . "<br>\n";
                            $message .= '</pre>';

                            Yii::$app->mailer->compose(null)
                                              ->setFrom(\Yii::$app->params['log'][YII_DEBUG?'debug':'error']['from'])
                                              ->setTo(\Yii::$app->params['log'][YII_DEBUG?'debug':'error']['to'])
                                              ->setHtmlBody($message)
                                              ->setTextBody($message)
                                              ->setSubject('USER BLOCK - toyota-media.de (user id: '.$user->id.', block level: '.$newBlockCount.')')
                                              ->send();
                        }

                        $user->wrong_password_blocked_count = $newBlockCount;
                    }

                    $user->save();
                }
            }

            if ($blocked) {
                Yii::$app->session->setFlash('error', 'Da mehrmals erfolglos versucht wurde sich mit dem Benutzer einzuloggen, wurde dieser aus Sicherheitsgründen bis '.Yii::$app->formatter->asDatetime($user->wrong_password_blocked_until).' Uhr gesperrt.');
                return false;
            }
        }

        Yii::$app->session->setFlash('error', 'Ungültige Zugangsdaten.');

        return false;
    }
}
*/
