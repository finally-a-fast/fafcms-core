<?php

namespace fafcms\fafcms\models;

use fafcms\helpers\ActiveRecord;
use fafcms\helpers\traits\BeautifulModelTrait;
use fafcms\helpers\traits\OptionTrait;
use Yii;

/**
 * This is the model class for table "customtable".
 *
 * @property int $id
 * @property string $status
 * @property int $project_id
 * @property string $model_class
 * @property string $name
 * @property string $title
 * @property string $description
 * @property int $created_by
 * @property int $updated_by
 * @property int $activated_by
 * @property int $deactivated_by
 * @property int $deleted_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $activated_at
 * @property string $deactivated_at
 * @property string $deleted_at
 *
 * @property User $activatedBy
 * @property User $createdBy
 * @property User $deactivatedBy
 * @property User $deletedBy
 * @property Project $project
 * @property User $updatedBy
 */
class Customtable extends ActiveRecord
{
    use BeautifulModelTrait;
    use OptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'custom-table';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'table-large';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-core', 'Custom Tables');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-core', 'Custom Table');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['title'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            self::class,
            array_merge([
                self::tableName().'.id',
                self::tableName().'.title'
            ], $select??[]),
            'id',
            function ($item) {
                return self::extendedLabel($item);
            },
            null,
            $sort??[self::tableName().'.title' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%customtable}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['project_id', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['model_class', 'name', 'title'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
            [['status', 'model_class', 'title'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 20],
            [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['activated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['deactivated_by' => 'id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['deleted_by' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('fafcms-core', 'ID'),
            'status' => Yii::t('fafcms-core', 'Status'),
            'project_id' => Yii::t('fafcms-core', 'Project ID'),
            'model_class' => Yii::t('fafcms-core', 'Model Class'),
            'name' => Yii::t('fafcms-core', 'Name'),
            'title' => Yii::t('fafcms-core', 'Title'),
            'description' => Yii::t('fafcms-core', 'Description'),
            'created_by' => Yii::t('fafcms-core', 'Created By'),
            'updated_by' => Yii::t('fafcms-core', 'Updated By'),
            'activated_by' => Yii::t('fafcms-core', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-core', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-core', 'Deleted By'),
            'created_at' => Yii::t('fafcms-core', 'Created At'),
            'updated_at' => Yii::t('fafcms-core', 'Updated At'),
            'activated_at' => Yii::t('fafcms-core', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-core', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-core', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'activated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeactivatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'deactivated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'deleted_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
