<?php

namespace fafcms\fafcms\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use fafcms\fafcms\models\Tag;
use yii\data\BaseDataProvider;

/**
 * TagSearch represents the model behind the search form of `fafcms\fafcms\models\Tag`.
 */
class TagSearch extends Tag
{
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'site_id', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['status', 'display_start', 'display_end', 'name', 'content', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        $query = Tag::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 9999,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'display_start' => $this->display_start,
            'display_end' => $this->display_end,
            'site_id' => $this->site_id,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'activated_by' => $this->activated_by,
            'deactivated_by' => $this->deactivated_by,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'activated_at' => $this->activated_at,
            'deactivated_at' => $this->deactivated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
