<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 * @since File available since Release 1.0.0
 */

namespace fafcms\fafcms\models;

use fafcms\fafcms\components\FafcmsComponent;
use Yii;
use fafcms\helpers\classes\PluginSetting;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class SettingForm
 * @package fafcms\fafcms\models
 */
class SettingForm extends Model
{
    /**
     * @var array
     */
    private $settingAttributes = [];

    /**
     * @var array
     */
    private $settingLabels = [];

    /**
     * @var array
     */
    private $settings = [];

    /**
     * @var array
     */
    private $settingRules = [];

    /**
     * @var int
     */
    public $projectId = null;

    /**
     * @var int
     */
    public $languageId = null;

    /**
     * SettingForm constructor.
     * @param PluginSetting[] $settings
     * @param array $settingRules
     * @param array $config
     * @throws InvalidConfigException
     */
    public function __construct(array $settings, array $settingRules, $config = [])
    {
        $this->settings = $settings;
        $this->settingRules = $settingRules;

        foreach ($this->settings as $setting) {
            $this->settingAttributes[$setting->name] = $setting->getValue($this->projectId, $this->languageId);
            $this->settingLabels[$setting->name] = $setting->label;
        }

        parent::__construct($config);
    }

    public function attributes(): array
    {
        return array_merge(parent::attributes(), array_keys($this->settingAttributes));
    }

    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->settingAttributes)) {
            return $this->settingAttributes[$name] = $value;
        }

        return parent::__set($name, $value);
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->settingAttributes)) {
            return $this->settingAttributes[$name];
        }

        return parent::__get($name);
    }

    public function rules(): array
    {
        return $this->settingRules;
    }

    public function attributeLabels(): array
    {
        return $this->settingLabels;
    }

    public function save($runValidation = true, $attributeNames = null): bool
    {
        foreach ($this->settings as $setting) {
            $newValue = $this->{$setting->name};

            switch ($setting->valueType) {
                case FafcmsComponent::VALUE_TYPE_INT:
                    $newValue = (int)$newValue;
                    break;
                case FafcmsComponent::VALUE_TYPE_BOOL:
                    $newValue = (bool)$newValue;
                    break;
            }

            if ($newValue !== $setting->getValue($this->projectId, $this->languageId)) {
                if (!$setting->setValue($newValue, $this->projectId, $this->languageId)) {
                    $this->addError($setting->name, Yii::t('fafcms-core', 'Cannot save {settingName} ({settingErrors}).', [
                        'settingName' => $setting->label,
                        'settingErrors' => (is_array($setting->errors)?implode(' ', array_merge(...array_values($setting->errors))):'')
                    ]));
                }
            }
        }

        return count($this->getErrors()) === 0;
    }
}
