<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\interfaces\ContentmetaInterface;
use fafcms\sitemanager\traits\ContentmetaTrait;
use fafcms\sitemanager\models\Site;
use Yii;
use fafcms\fafcms\Bootstrap as FafcmsBootstrap;
use fafcms\fafcms\abstracts\models\BaseTag;

/**
 * This is the model class for table "{{%tag}}".
 *
 * @package fafcms\fafcms\models
 */
class Tag extends BaseTag implements ContentmetaInterface
{
    use ContentmetaTrait;

    public static string $searchModelClass = TagSearch::class;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'tag-text-outline';
    }
    //endregion BeautifulModelTrait implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => ['fafcms-core', 'Master data'],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => ['fafcms-core', 'Master data'],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-site_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'site_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'access-restrictions-card' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => ['fafcms-core', 'Access restrictions'],
                                                'icon' => 'timetable',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-display_start' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                'field-display_end' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-2' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'contents' => [
                                        'content-card' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => ['fafcms-core', 'Content'],
                                                'icon' => 'card-text-outline',
                                            ],
                                            'contents' => [
                                                'field-content' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'content',
                                                        'hideLabel' => true,
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                            ],
                        ]
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    //region ContentmetaInterface implementation
    /**
     * @return string
     */
    public static function contentmetaName(): string
    {
        return 'Tag';
    }

    /**
     * @return string
     */
    public static function contentmetaId(): string
    {
        return 'id';
    }

    /**
     * @return string
     */
    public static function contentmetaSiteId(): string
    {
        return 'site_id';
    }
    //endregion ContentmetaInterface implementation

    public function loadDefaultValues($skipIfSet = true): self
    {
        parent::loadDefaultValues($skipIfSet);

        if (!$skipIfSet || $this->site_id === null) {
            $this->site_id = Yii::$app->getModule(FafcmsBootstrap::$id)->getPluginSettingValue('tag_site_id');

            if (!$this->validate('site_id')) {
                $this->site_id = null;
            }
        }

        return $this;
    }
}
