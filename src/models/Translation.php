<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\components\InjectorComponent;
use Yii;
use fafcms\fafcms\abstracts\models\BaseTranslation;
use yii\base\ErrorException;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\BaseDataProvider;
use yii\helpers\ArrayHelper;
use yii\i18n\MessageSource;

/**
 * This is the model class for table "{{%translation}}".
 *
 * @package fafcms\fafcms\models
 */
class Translation extends BaseTranslation
{
    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        $indexViews = parent::indexView();

        unset($indexViews['default']['id']);
        unset($indexViews['default']['language']);
        unset($indexViews['default']['category']);
        return $indexViews;
    }
    //endregion IndexViewInterface implementation

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Yii::$app->translationCache->delete('js-translation');
        Yii::$app->translationCache->delete($this->category . '-' . $this->language . '-' . $this->message);
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        $idParams = explode('_', $params['id']);

        $category = $idParams[0];
        $language = $idParams[1];

        if (!isset(Yii::$app->fafcms->getAvailableLanguages()[$language])) {
            throw new ErrorException('Unknown language: ' . $language);
        }

        if (!isset(Yii::$app->i18n->translations[$category])) {
            throw new ErrorException('Unknown category: ' . $category);
        }

        $translationModels = Translation::find()->all();
        $manualTranslations = ArrayHelper::index($translationModels, 'message', ['language', 'category']);

        /**
         * @var MessageSource $messageSource
         */
        $messageSource = Yii::$app->i18n->getAllMessageCategories()[$category];

        if (is_array($messageSource)) {
            $messageSource = Yii::createObject($messageSource);
        }

        //trigger loadMessages method
        $messageSource->translate($category, 'LOAD MESSAGES', $language);

        foreach ($messageSource->getMessages($category, $language) as $message => $translation) {
            if (isset($manualTranslations[$language][$category][$message])) {
                continue;
            }

            $id = $category . '_' . $language . '_' . $message;

            $translationModels[] = new Translation([
               'id'          => $id,
               'language'    => $language,
               'category'    => $category,
               'message'     => $message,
               'translation' => $translation,
            ]);
        }

        return InjectorComponent::createObject([
            'class' => ArrayDataProvider::class,
            'key' => 'id',
            'allModels' => $translationModels,
            'pagination' => false,
            'sort' => [
                'attributes' => [
                    'message',
                    'translation'
                ],
                'defaultOrder' => [
                    'message' => SORT_ASC,
                ]
            ],
        ]);
    }
}
