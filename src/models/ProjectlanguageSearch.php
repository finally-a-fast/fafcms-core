<?php

namespace fafcms\fafcms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use fafcms\fafcms\models\Projectlanguage;
use yii\data\BaseDataProvider;

/**
 * ProjectlanguageSearch represents the model behind the search form of `\fafcms\fafcms\models\Projectlanguage`.
 */
class ProjectlanguageSearch extends Projectlanguage
{
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'project_id', 'domain_id', 'language_id', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['status', 'name', 'description', 'og_site_name', 'fb_app_id', 'path', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        $query = Projectlanguage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'project_id' => $this->project_id,
            'domain_id' => $this->domain_id,
            'language_id' => $this->language_id,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'activated_by' => $this->activated_by,
            'deactivated_by' => $this->deactivated_by,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'activated_at' => $this->activated_at,
            'deactivated_at' => $this->deactivated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'og_site_name', $this->og_site_name])
            ->andFilterWhere(['like', 'fb_app_id', $this->fb_app_id])
            ->andFilterWhere(['like', 'path', $this->path]);

        $query->where(['project_id' => Yii::$app->view->params['model']->id]);

        return $dataProvider;
    }
}
