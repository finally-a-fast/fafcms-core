<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\components\InjectorComponent;
use fafcms\helpers\ActiveRecord;
use fafcms\helpers\traits\BeautifulModelTrait;
use fafcms\helpers\traits\OptionTrait;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%language}}".
 *
 * @property int $id
 * @property string $name
 * @property string $ietfcode
 * @property int $flag_id
 * @property int $country_id
 *
 * @property Flag $flag
 * @property Country $country
 */
class Language extends ActiveRecord
{
    use BeautifulModelTrait;
    use OptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'language';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'wan';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-core', 'Languages');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-core', 'Language');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            self::class,
            array_merge([
                            self::tableName().'.id',
                            self::tableName().'.name'
                        ], $select??[]),
            'id',
            function ($item) {
                return self::extendedLabel($item);
            },
            null,
            $sort??[self::tableName().'.name' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%language}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name', 'ietfcode'], 'required'],
            [['flag_id', 'country_id'], 'integer'],
            [['name', 'ietfcode'], 'string', 'max' => 255],
            [['ietfcode'], 'unique'],
            [['flag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Flag::class, 'targetAttribute' => ['flag_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id'         => Yii::t('fafcms-core', 'ID'),
            'name'       => Yii::t('fafcms-core', 'Name'),
            'ietfcode'   => Yii::t('fafcms-core', 'Ietfcode'),
            'flag_id'    => Yii::t('fafcms-core', 'Flag ID'),
            'country_id' => Yii::t('fafcms-core', 'Country ID'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getFlag(): ActiveQuery
    {
        return $this->hasOne(Flag::class, ['id' => 'flag_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCountry(): ActiveQuery
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }
}
