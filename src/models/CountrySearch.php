<?php

namespace fafcms\fafcms\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;

/**
 * CountrySearch represents the model behind the search form of `fafcms\fafcms\models\Country`.
 */
class CountrySearch extends Country
{
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'iso3166numericcode'], 'integer'],
            [['name', 'capitalname', 'continent', 'currencycode', 'currencyname', 'fipscode', 'iso3166alpha2code', 'iso3166alpha3code', 'phoneprefix', 'postalcodemask', 'postalcoderegex'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        $query = Country::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'iso3166numericcode' => $this->iso3166numericcode,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'capitalname', $this->capitalname])
            ->andFilterWhere(['like', 'continent', $this->continent])
            ->andFilterWhere(['like', 'currencycode', $this->currencycode])
            ->andFilterWhere(['like', 'currencyname', $this->currencyname])
            ->andFilterWhere(['like', 'fipscode', $this->fipscode])
            ->andFilterWhere(['like', 'iso3166alpha2code', $this->iso3166alpha2code])
            ->andFilterWhere(['like', 'iso3166alpha3code', $this->iso3166alpha3code])
            ->andFilterWhere(['like', 'phoneprefix', $this->phoneprefix])
            ->andFilterWhere(['like', 'postalcodemask', $this->postalcodemask])
            ->andFilterWhere(['like', 'postalcoderegex', $this->postalcoderegex]);

        return $dataProvider;
    }
}
