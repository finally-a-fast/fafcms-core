<?php

namespace fafcms\fafcms\models;

use fafcms\helpers\ActiveRecord;
use Yii;

/**
 * This is the model class for table "tagrealation".
 *
 * @property int $id
 * @property string $status
 * @property int $tag_id
 * @property string $model_class
 * @property int $model_id
 * @property int $created_by
 * @property int $deleted_by
 * @property string $created_at
 * @property string $deleted_at
 *
 * @property User $createdBy
 * @property User $deletedBy
 * @property Tag $tag
 */
class Tagrealation extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%tagrealation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['tag_id', 'model_class', 'model_id'], 'required'],
            [['tag_id', 'model_id', 'created_by', 'deleted_by'], 'integer'],
            [['created_at', 'deleted_at'], 'safe'],
            [['status', 'model_class'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['deleted_by' => 'id']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tag::className(), 'targetAttribute' => ['tag_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('fafcms-core', 'ID'),
            'status' => Yii::t('fafcms-core', 'Status'),
            'tag_id' => Yii::t('fafcms-core', 'Tag ID'),
            'model_class' => Yii::t('fafcms-core', 'Model Class'),
            'model_id' => Yii::t('fafcms-core', 'Model ID'),
            'created_by' => Yii::t('fafcms-core', 'Created By'),
            'deleted_by' => Yii::t('fafcms-core', 'Deleted By'),
            'created_at' => Yii::t('fafcms-core', 'Created At'),
            'deleted_at' => Yii::t('fafcms-core', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'deleted_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tag::className(), ['id' => 'tag_id']);
    }
}
