<?php

namespace fafcms\fafcms\models;

use fafcms\helpers\ActiveRecord;
use Yii;

/**
 * This is the model class for table "permission".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $description
 *
 * @property UsergroupPermission[] $agentgroupPermissions
 */
class Permission extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function prefixableTableName(): string
    {
        return '{{%permission}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['code', 'name'], 'string', 'max' => 255],
            [['code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('fafcms-core',  'ID'),
            'code' => Yii::t('fafcms-core',  'Code'),
            'name' => Yii::t('fafcms-core',  'Name'),
            'description' => Yii::t('fafcms-core',  'Description'),
        ];
    }

    /**
     * @return \common\helpers\ActiveQuery
     */
    public function getUsergroupPermissions()
    {
        return $this->hasMany(UsergroupPermission::className(), ['permission_id' => 'id']);
    }
}
