<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 * @since File available since Release 1.0.0
 */

namespace fafcms\fafcms\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;
use fafcms\fafcms\components\ViewComponent;

/**
 * Class InstallerForm
 * @package fafcms\fafcms\models
 */
class InstallerForm extends Model
{
    public $language;

    public bool $check = true;

    public bool $databaseAdvanced = false;
    public bool $run_migrations = true;
    public string $databaseDsn = 'mysql:host=localhost;dbname=fafcms';
    public string $databaseType = 'mysql';
    public string $databaseHost = 'localhost';
    public string $databaseName = 'fafcms';
    public string $databaseTablePrefix = '';
    public string $databaseUser = '';
    public string $databasePassword = '';

    public string $backendUrl = 'backend';
    public string $adminEmail = '';
    public string $adminUser = '';
    public string $adminPassword = '';

    public string $name = 'My Project';
    public string $projectDescription = '';
    public string $domain = '';

    public bool $insertDefaultSettings = true;
    public bool $insertDefaultLayout   = true;
    public bool $insertDefaultSites    = true;
    public bool $insertExampleArticles = true;

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function rules(): array
    {
        $js = <<<JS
 function (typeAttribute, typeValue) {
    var \$form = typeAttribute.\$form

    var inputIds = {
        1: [\$form.yiiActiveForm('find', 'installerform-databasedsn')],
        0: [
            \$form.yiiActiveForm('find', 'installerform-databasetype'),
            \$form.yiiActiveForm('find', 'installerform-databasename'),
            \$form.yiiActiveForm('find', 'installerform-databasehost')
        ],
    }

    $.each(inputIds, function(i, attributeGroup) {
        $.each(attributeGroup, function(i, attribute) {
            $(attribute.container).addClass('hide')
        })
    })

    $.each(inputIds[typeValue], function(i, attribute) {
        $(attribute.container).removeClass('hide')
    })

    return true
}
JS;

        Yii::$app->getView()->registerJs('
            window.setTimeout(function() {
                var $form = $(\'#'.strtolower($this->formName()).'-form\')
                var typeAttribute = $form.yiiActiveForm(\'find\', \'installerform-databaseadvanced\')
                typeAttribute.$form = $form;
                ('.$js.')(typeAttribute, typeAttribute.value);
            }, 0);
        ',
        ViewComponent::POS_READY);

        return [
            ['language', 'required', 'on' => ['step_0']],
            ['language', 'in', 'range' => array_keys($this->attributeOptions()['language']), 'on' => ['step_0']],
            ['check', 'required', 'on' => ['step_1']],
            ['databaseType', 'in', 'range' => array_keys($this->attributeOptions()['databaseType']), 'on' => ['step_2']],
            ['databaseName', 'string', 'max' => 255, 'on' => ['step_2']],
            [['databaseDsn', 'databaseHost','databaseUser', 'databasePassword', 'databaseTablePrefix'], 'string', 'on' => ['step_2']],
            ['databaseName', 'match', 'pattern' => '/^[\w-]+$/', 'on' => ['step_2']],
            ['databaseAdvanced', 'required', 'whenClient' => $js, 'on' => ['step_2']],
            ['databaseHost', 'required', 'when' => function () {
                return !$this->databaseAdvanced;
            }, 'whenClient' => 'function (attribute, value) {
                return attribute.$form.yiiActiveForm(\'find\', \'installerform-databaseadvanced\').value === 0
            }', 'on' => ['step_2']],
            ['databaseDsn', 'required', 'when' => function () {
                return $this->databaseAdvanced;
            }, 'whenClient' => 'function (attribute, value) {
                return attribute.$form.yiiActiveForm(\'find\', \'installerform-databaseadvanced\').value === 1
            }', 'on' => ['step_2']],
            ['databaseTablePrefix', 'validateDbConnection', 'on' => ['step_2']],
            ['run_migrations', 'boolean', 'on' => ['step_2', 'step_3']],
            [['backendUrl', 'adminUser'], 'string', 'max' => 255, 'on' => ['step_4']],
            [['adminPassword'], 'string', 'on' => ['step_4']],
            ['adminEmail', 'email'],
            [['backendUrl', 'adminUser', 'adminPassword', 'adminEmail'], 'required', 'on' => ['step_4']],
            [['name', 'domain'], 'string', 'max' => 255,  'on' => ['step_5']],
            [['projectDescription'], 'string', 'on' => ['step_5']],
            [['name', 'domain', ], 'required', 'on' => ['step_5']],
            [
                [
                    'insertDefaultSettings',
                    'insertDefaultLayout',
                    'insertDefaultSites',
                    'insertExampleArticles',
                ],
                'boolean',
                'on' => ['step_6'],
            ],
        ];
    }

    /**
     * @param $attribute
     */
    public function validateDbConnection($attribute)
    {
        $dbConnection = [
            'dsn' => ($this->databaseAdvanced?$this->databaseDsn:$this->databaseType.':host='.$this->databaseHost),
            'username' => $this->databaseUser,
            'password' => $this->databasePassword,
        ];

        if ($dbConnection['dsn'] === '') {
            return;
        }

        $connection = new \yii\db\Connection($dbConnection);

        try {
            $connection->open();

            if ($connection->isActive) {
                $connection->createCommand('CREATE DATABASE IF NOT EXISTS '.$this->databaseName.' CHARACTER SET utf8')->execute();
            }
        } catch (Exception $e) {
            $this->addError($attribute, Yii::t('fafcms-core-setup', 'Cannot connect to the database.').'<br>'.$e->getMessage());
        }
    }

    /**
     * @return bool
     */
    public function validateMigration(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'language'              => Yii::t('fafcms-core', 'choose_language'),
            'databaseAdvanced'      => Yii::t('fafcms-core-setup', 'Advanced mode'),
            'run_migrations'        => Yii::t('fafcms-core-setup', 'Crate Database tables'),
            'insertDefaultSettings' => Yii::t('fafcms-core-setup', 'Insert Default Settings (recommend)'),
            'insertDefaultLayout'   => Yii::t('fafcms-core-setup', 'Insert Default Layout'),
            'insertDefaultSites'    => Yii::t('fafcms-core-setup', 'Insert Default Sites'),
            'insertExampleArticles' => Yii::t('fafcms-core-setup', 'Insert Default Articles'),
        ];
    }

    /**
     * @return array
     */
    public function attributeOptions(): array
    {
        return [
            'language' => [
                'en' => 'English',
                'de' => 'Deutsch',
                'ja' => '日本の',
            ],
            'databaseType' => [// TODO filter options by system check
                //http://php.net/manual/en/pdo.construct.php
                'mysql' => 'MySQL',
                'pgsql' => 'PostgreSQL',
                'oci' => 'Oracle',
                'sqlsrv' => 'MSSQL (sqlsrv)',
                'mssql' => 'MSSQL',
                'sqlite' => 'SQLite',
                'cubrid' => 'CUBRID',
            ],
        ];
    }

    /**
     * @return void
     */
    public function loadDefaultValues(): void
    {
        $defaultValues = [
            'step_5' => [
                'domain' => Yii::$app->getRequest()->getHostName(),
            ],
        ];

        $this->setAttributes($defaultValues[$this->getScenario()] ?? []);
    }
}
