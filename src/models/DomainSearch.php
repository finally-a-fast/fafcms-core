<?php

namespace fafcms\fafcms\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use fafcms\fafcms\models\Domain;
use yii\data\BaseDataProvider;

/**
 * DomainSearch represents the model behind the search form of `fafcms\fafcms\models\Domain`.
 */
class DomainSearch extends Domain
{
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'project_id', 'is_wildcard', 'is_validated', 'is_https_validated', 'force_https', 'is_redirect', 'redirect_id', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by'], 'integer'],
            [['status', 'domain', 'last_validation_at', 'last_https_validation_at', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        $query = Domain::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'project_id' => $this->project_id,
            'is_wildcard' => $this->is_wildcard,
            'is_validated' => $this->is_validated,
            'last_validation_at' => $this->last_validation_at,
            'is_https_validated' => $this->is_https_validated,
            'last_https_validation_at' => $this->last_https_validation_at,
            'force_https' => $this->force_https,
            'is_redirect' => $this->is_redirect,
            'redirect_id' => $this->redirect_id,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'activated_by' => $this->activated_by,
            'deactivated_by' => $this->deactivated_by,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'activated_at' => $this->activated_at,
            'deactivated_at' => $this->deactivated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'domain', $this->domain]);

        return $dataProvider;
    }
}
