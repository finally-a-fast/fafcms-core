<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\classes\OptionProvider;
use Yii;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use fafcms\fafcms\abstracts\models\BaseCustomfield;

/**
 * This is the model class for table "{{%customfield}}".
 */
class Customfield extends BaseCustomfield
{
    public static string $searchModelClass = CustomfieldSearch::class;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'table-column';
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['title'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.title'
            ])
            ->setSort([static::tableName() . '.title' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return array_merge(parent::attributeOptions(), [
            'model_class' => ArrayHelper::map(Yii::$app->fafcms->getModelClasses(), 'fullyQualifiedClassName', 'fullyQualifiedClassName'),
            'customtable_id' => Customtable::getOptions(),
            'type' => [
                FafcmsComponent::VALUE_TYPE_INT => Yii::t('fafcms-core', 'int'),
                FafcmsComponent::VALUE_TYPE_VARCHAR => Yii::t('fafcms-core', 'varchar'),
                FafcmsComponent::VALUE_TYPE_TEXT => Yii::t('fafcms-core', 'text'),
                FafcmsComponent::VALUE_TYPE_BOOL => Yii::t('fafcms-core', 'bool'),
                //FafcmsComponent::VALUE_TYPE_MODEL => Yii::t('fafcms-core', 'model'),
                FafcmsComponent::VALUE_TYPE_DATE => Yii::t('fafcms-core', 'date'),
                FafcmsComponent::VALUE_TYPE_TIME => Yii::t('fafcms-core', 'time'),
                FafcmsComponent::VALUE_TYPE_DATETIME => Yii::t('fafcms-core', 'datetime')
            ]
        ]);
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    /**
     * @inheritDoc
     */
    public function getFieldConfig(): array
    {
        $fieldConfig = parent::getFieldConfig();

        $fieldConfig['model_class'] = [
            'type' => ExtendedDropDownList::class,
            'items' => $this->attributeOptions()['model_class'],
        ];

        $fieldConfig['type'] = [
            'type' => ExtendedDropDownList::class,
            'items' => $this->attributeOptions()['type']
        ];

        $fieldConfig['name']['description'] = Yii::t('fafcms-core', 'Used to help you to identify the field in the database.');

        return $fieldConfig;
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    /**
     * @inheritDoc
     */
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'access-restrictions-card' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Access restrictions'),
                                                'icon' => 'timetable',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-project_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'project_id',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                                'column-3' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'page-properties-card' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Field properties'),
                                                'icon' => 'card-bulleted-settings-outline',
                                            ],
                                            'contents' => [
                                                'field-type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'type',
                                                    ],
                                                ],
                                                'field-model_class' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'model_class',
                                                    ],
                                                ],
                                                'field-customtable_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'customtable_id',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                            ],
                        ],
                        'row-2' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'contents' => [
                                        'content-card' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Description'),
                                                'icon' => 'card-text-outline',
                                            ],
                                            'contents' => [
                                                'field-description' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'description',
                                                    ],
                                                ],
                                            ]
                                        ],
                                    ]
                                ],
                            ],
                        ]
                    ]
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * @inheritDoc
     */
    public function beforeDelete(): bool
    {
        if (isset($this->attributeOptions()['model_class'][$this->model_class]) && class_exists($this->model_class)) {
            $table = $this->model_class::tableName();
            $tableSchema = Yii::$app->db->schema->getTableSchema($table);
            $column = 'cf_' . $this->id . '_' . $this->name;

            try {
                if ($tableSchema->getColumn($column) !== null) {
                    Yii::$app->db->createCommand()
                        ->dropColumn($table, $column)
                        ->execute();
                }
            } catch (\Throwable $e) {
                return false;
            }
        }

        return parent::beforeDelete();
    }

    /**
     * @inheritDoc
     */
    public function save($runValidation = true, $attributeNames = null): bool
    {
        if (!isset($this->attributeOptions()['model_class'][$this->model_class]) || !class_exists($this->model_class)) {
            throw new ErrorException('The model class "' . $this->model_class . '" does not exist.');
        }

        $nameChanged = $this->isAttributeChanged('name');
        $typeChanged = $this->isAttributeChanged('type');
        $modelClassChanged = $this->isAttributeChanged('model_class');

        $oldModelClass = $this->getOldAttribute('model_class');
        $oldName = $this->getOldAttribute('name');

        $result = parent::save($runValidation, $attributeNames);

        if ($result) {
            $oldColumn = 'cf_' . $this->id . '_' . $oldName;

            $table = $this->model_class::tableName();
            $tableSchema = Yii::$app->db->schema->getTableSchema($table);
            $columnNotExists = $tableSchema->getColumn($oldColumn) === null;

            try {
                if ($modelClassChanged && isset($this->attributeOptions()['model_class'][$oldModelClass]) && class_exists($oldModelClass)) {
                    $oldTable = $oldModelClass::tableName();
                    $oldTableSchema = Yii::$app->db->schema->getTableSchema($oldTable);

                    if ($oldTableSchema->getColumn($oldColumn) !== null) {
                        Yii::$app->db->createCommand()
                            ->dropColumn($oldTable, $oldColumn)
                            ->execute();
                    }
                }

                if ($nameChanged || $typeChanged || $columnNotExists) {
                    $newColumn = 'cf_'.$this->id.'_'.$this->name;
                    $newType = $this->type === 'varchar'?'string':$this->type;

                    if ($columnNotExists) {
                        Yii::$app->db->createCommand()
                            ->addColumn($table, $newColumn, $newType)
                            ->execute();
                    } else {
                        if ($nameChanged) {
                            Yii::$app->db->createCommand()
                                ->renameColumn($table, $oldColumn, $newColumn)
                                ->execute();
                        }

                        if ($typeChanged) {
                            Yii::$app->db->createCommand()
                                ->alterColumn($table, $newColumn, $newType)
                                ->execute();
                        }
                    }
                }
            } catch (\Throwable $e) {
                $this->addError('name', $e->getMessage());
                $this->addError('type', $e->getMessage());
                return false;
            }

            return true;
        }

        return false;
    }
}
