<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\inputs\TextInput;
use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\fafcms\Module;
use fafcms\helpers\interfaces\EditViewInterface;
use fafcms\helpers\interfaces\FieldConfigInterface;
use fafcms\helpers\Model;
use Yii;

/**
 * Password reset request form
 * Class PasswordRequestForm
 *
 * @package fafcms\fafcms\models
 */
class PasswordRequestForm extends Model implements FieldConfigInterface, EditViewInterface
{
    /**
     * @var bool
     */
    public static bool $checkAccess = false;

    /**
     * @var string|null
     */
    public ?string $username = null;

    /**
     * @var bool
     */
    public bool $allowUsername = true;

    /**
     * @var bool
     */
    public bool $allowEmail = true;

    /**
     * @var array
     */
    public array $mailView = [
        'html' => 'password-reset-html',
        'text' => 'password-reset-text'
    ];

    /**
     * @var array
     */
    public array $mailSubject = ['fafcms-core', 'Your access'];

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'username' => [
                'type' => TextInput::class,
                'options' => [
                    'autofocus' => true
                ]
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'buttons' => [
                    static function($buttons, $model, $form, $editView) {
                        $buttons['edit'] = [
                            'form' => $form->id,
                            'options' => [
                                'class' => ['primary'],
                            ],
                            'type' => 'submit',
                            'icon' => 'account-key-outline',
                            'label' => ['fafcms-core', 'Request new password'],
                        ];

                        return $buttons;
                    }
                ],
                'contents' => [
                    'tab-1' => [
                        'class' => Tab::class,
                        'settings' => [
                            'label' => [
                                'fafcms-core',
                                'Login',
                            ],
                        ],
                        'contents' => [
                            'row-1' => [
                                'class' => Row::class,
                                'contents' => [
                                    'column-1' => [
                                        'class' => Column::class,
                                        'settings' => [
                                            'm' => 4,
                                        ],
                                    ],
                                    'column-2' => [
                                        'class' => Column::class,
                                        'settings' => [
                                            'm' => 8,
                                        ],
                                        'contents' => [
                                            'card-1' => [
                                                'class' => Card::class,
                                                'settings' => [
                                                    'title' => [
                                                        'fafcms-core',
                                                        'Request new password',
                                                    ],
                                                    'icon' => 'account-key-outline'
                                                ],
                                                'buttons' => [
                                                    'back' => [
                                                        'icon' => 'arrow-left',
                                                        'label' => ['fafcms-core', 'Back'],
                                                        'url' => ['login'],
                                                        'rules' => false
                                                    ],
                                                    'edit' => [
                                                        'icon' => 'account-key-outline',
                                                        'label' => ['fafcms-core', 'Request new password'],
                                                        'options' => [
                                                            'class' => ['primary'],
                                                        ],
                                                        'type' => 'submit',
                                                        'rules' => false
                                                    ]
                                                ],
                                                'contents' => [
                                                    'field-username' => [
                                                        'class' => FormField::class,
                                                        'settings' => [
                                                            'field' => 'username',
                                                        ],
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * @inheritdoc
     */
    public function loadDefaultValues($skipIfSet = true): self
    {
        $result = parent::loadDefaultValues($skipIfSet);

        if (!$skipIfSet || $result->username === null) {
            $result->username = Yii::$app->request->getQueryParam('email', '');
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'username' => Yii::t('fafcms-core', 'Email address'),
        ]);
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function save($runValidation = true, $attributeNames = null): bool
    {
        if ($this->validate()) {
            $userFilter = [];
            $userClass  = InjectorComponent::getClass(User::class);

            if ($this->allowUsername && $userClass::instance()->hasAttribute('username')) {
                $userFilter[] = ['username' => $this->username];
            }

            if ($this->allowEmail && $userClass::instance()->hasAttribute('email')) {
                $userFilter[] = ['email' => $this->username];
            }

            if (count($userFilter) > 0) {
                $user = $userClass::find()->where(array_merge(
                    ['or'],
                    $userFilter
                ))->one();

                if ($user !== null) {
                    $user->setScenario($userClass::SCENARIO_PASSWORD_CHANGE_REQUEST);
                    $user->generateResetToken();

                    if ($user->validate() && $user->save()) {
                        $from = Module::getLoadedModule()->getPluginSettingValue('password_reset_email_from_address');
                        $label = Module::getLoadedModule()->getPluginSettingValue('password_reset_email_from_label');

                        if ($label !== null) {
                            $from = [$from => $label];
                        }

                        // this is needed to get clean date for the email template (e.g. user date is encrypted otherwise)
                        $user = $userClass::find()->where(['id' => $user->id])->one();

                        return Yii::$app->mailer->compose($this->mailView, ['user' => $user])
                        ->setFrom($from)
                        ->setTo($user->email)
                        ->setSubject(Yii::$app->fafcms->getTextOrTranslation($this->mailSubject))
                        ->send();
                    }
                }
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getErrorMessageAfterSave(): string
    {
        return Yii::$app->fafcms->getTextOrTranslation(['fafcms-core', 'The e-mail could not be sent at the moment. Please try again later.']);
    }

    /**
     * @return string
     */
    public function getSuccessMessageAfterSave(): string
    {
        return Yii::$app->fafcms->getTextOrTranslation(['fafcms-core', 'Please check your email inbox.<br><br>Provided that a user account "{username}" exists, you will receive an e-mail with further information shortly. Please also check your spam folder in this regard.<br><br>If you have not received an e-mail within a few minutes, try a different e-mail address if necessary.', ['username' => $this->username]]);
    }
}
