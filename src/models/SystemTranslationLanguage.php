<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\inputs\Percent;
use fafcms\fafcms\inputs\Raw;
use fafcms\fafcms\inputs\TextInput;
use fafcms\fafcms\items\ActionColumn;
use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\DataColumn;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\IndexView;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\ActiveRecord;
use fafcms\helpers\interfaces\EditViewInterface;
use fafcms\helpers\interfaces\FieldConfigInterface;
use fafcms\helpers\interfaces\IndexViewInterface;
use fafcms\helpers\Model;
use fafcms\helpers\traits\BeautifulModelTrait;
use Yii;
use yii\data\ArrayDataProvider;
use yii\data\BaseDataProvider;

/**
 * Class SystemTranslationLanguage
 *
 * @package fafcms\fafcms\models
 */
class SystemTranslationLanguage extends Model implements FieldConfigInterface, EditViewInterface
{
    use BeautifulModelTrait;

    public string $id;
    public string $category;
    public string $language;

    public int $messageCount;
    public int $translatedCount;
    public float $translatedPercent;

    public array $messages;

    public static ?string $authModelClass = SystemTranslation::class;

    /**
     * @inheritdoc
     */
    public static function getRecords(): array
    {
        $translationMessages = Yii::$app->i18n->searchTranslationMessages();
        $records = [];

        foreach ($translationMessages as $translationMessage) {
            foreach (array_keys($translationMessage['translations']) as $language) {
                $records[] = [
                    'id'                 => $translationMessage['id'] . '_' . $language,
                    'category'           => $translationMessage['category'],
                    'language'           => $language,
                    'messageCount'       => $translationMessage['translations'][$language]['messageCount'],
                    'translatedCount'    => $translationMessage['translations'][$language]['translatedCount'],
                    'translatedPercent'  => $translationMessage['translations'][$language]['translatedPercent'],
                    'messages'           => $translationMessage['translations'][$language]['messages'],
                ];
            }
        }

        return $records;
    }

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'system-translation';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'translate';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('app', 'System translation languages');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('app', 'System translation language');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['headline'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'category' => [
                'type' => TextInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'language' => [
                'type' => TextInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'translatedPercent' => [
                'type' => Percent::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-category' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'category',
                                                    ],
                                                ],
                                                'field-language' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'language',
                                                    ],
                                                ],
                                                'field-translatedPercent' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'translatedPercent',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'row-2' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'contents' => [
                                        'index-1' => [
                                            'class' => IndexView::class,
                                            'settings' => [
                                                'modelClass' => Translation::class,
                                                'isWidget' => true,
                                            ],
                                            'contents' => Translation::indexView()['default']
                                        ],
                                    ]
                                ],
                            ]
                        ]
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation
}
