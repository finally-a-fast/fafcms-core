<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\filemanager\inputs\FileSelect;
use Yii;
use fafcms\fafcms\abstracts\models\BaseProject;

/**
 * This is the model class for table "{{%project}}".
 *
 * @package fafcms\fafcms\models
 */
class Project extends BaseProject
{
    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'scatter-plot-outline';
    }

    /**
     * @inheritDoc
     */
    public function getFieldConfig(): array
    {
        return array_merge(parent::getFieldConfig(), [
            'media' => [
                'type' => FileSelect::class,
                'mediatypes' => 'image'
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-description' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'description',
                                                    ],
                                                ],
                                                'field-primary_projectlanguage_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'primary_projectlanguage_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Media'),
                                                'icon' => 'file-outline',
                                            ],
                                            'contents' => [
                                                'field-media' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'media',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ]
                            ]
                        ],
                        'row-2' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-site_name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'site_name',
                                                    ],
                                                ],
                                                'field-og_site_name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_site_name',
                                                    ],
                                                ],
                                                'field-fb_app_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'fb_app_id',
                                                    ],
                                                ],
                                                'field-show_start_page_in_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'show_start_page_in_title',
                                                    ],
                                                ],
                                                'field-show_site_name_in_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'show_site_name_in_title',
                                                    ],
                                                ],
                                                'field-reverse_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'reverse_title',
                                                    ],
                                                ],
                                                'field-title_separator' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title_separator',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-start_page_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'start_page_contentmeta_id',
                                                    ],
                                                ],
                                                'field-error_page_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'error_page_contentmeta_id',
                                                    ],
                                                ],
                                                'field-login_page_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'login_page_contentmeta_id',
                                                    ],
                                                ],
                                                'field-default_layout_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'default_layout_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ]
                            ]
                        ],
                        'row-3' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'contents' => [
                                        'index-1' => [
                                            'class' => \fafcms\fafcms\items\IndexView::class,
                                            'settings' => [
                                                'modelClass' => Projectlanguage::class,
                                                'searchModelClass' => ProjectlanguageSearch::class,
                                                'isWidget' => true,
                                            ],
                                            'contents' => Projectlanguage::indexView()['default']
                                        ],
                                    ]
                                ],
                            ]
                        ]
                    ],
                ],
            ],
        ];
    }
}
