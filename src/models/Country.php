<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\{
    items\Tab,
    items\Row,
    items\Card,
    items\Column,
    items\FormField,
    items\DataColumn,
    items\ActionColumn,
    inputs\TextInput,
};

use fafcms\helpers\{
    ActiveRecord,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\OptionTrait,
    traits\BeautifulModelTrait
};

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%country}}".
 *
 * @property int $id
 * @property string $name
 * @property string $capitalname
 * @property string $continent
 * @property string $currencycode
 * @property string $currencyname
 * @property string $fipscode
 * @property string $iso3166alpha2code
 * @property string $iso3166alpha3code
 * @property int $iso3166numericcode
 * @property string $phoneprefix
 * @property string $postalcodemask
 * @property string $postalcoderegex
 *
 * @property Flag[] $flags
 * @property Language[] $languages
 * @property User[] $users
 */
class Country extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use OptionTrait;
    use BeautifulModelTrait;

    public static string $searchModelClass = CountrySearch::class;

    //region BeautifulModelTrait implementation
    /**
     * {@inheritDoc}
     */
    public static function editDataUrl($model): string
    {
        return 'country';
    }

    /**
     * {@inheritDoc}
     */
    public static function editDataIcon($model): string
    {
        return 'globe-model';
    }

    /**
     * {@inheritDoc}
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-core', 'Countries');
    }

    /**
     * {@inheritDoc}
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-core', 'Country');
    }

    /**
     * {@inheritDoc}
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(Yii::t('fafcms-core-country', $model['iso3166alpha2code']));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * {@inheritDoc}
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.name',
                static::tableName() . '.iso3166alpha2code',
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.name' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region FieldConfigInterface implementation
    /**
     * @return array|string[][]
     */
    public function getFieldConfig(): array
    {
        return [
            'name' => [
                'type' => TextInput::class,
            ],
            'capitalname' => [
                'type' => TextInput::class,
            ],
            'continent' => [
                'type' => TextInput::class,
            ],
            'currencycode' => [
                'type' => TextInput::class,
            ],
            'currencyname' => [
                'type' => TextInput::class,
            ],
            'fipscode' => [
                'type' => TextInput::class,
            ],
            'iso3166alpha2code' => [
                'type' => TextInput::class,
            ],
            'iso3166alpha3code' => [
                'type' => TextInput::class,
            ],
            'iso3166numericcode' => [
                'type' => TextInput::class,
            ],
            'phoneprefix' => [
                'type' => TextInput::class,
            ],
            'postalcodemask' => [
                'type' => TextInput::class,
            ],
            'postalcoderegex' => [
                'type' => TextInput::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    /**
     * @return array
     */
    public static function indexView(): array
    {
        return [
            'default'      => [
                [
                    'class'    => DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'sort'  => 1,
                    ],
                ],
                [
                    'class'    => DataColumn::class,
                    'settings' => [
                        'field' => 'capitalname',
                        'sort'  => 2,
                    ],
                ],
                [
                    'class'    => DataColumn::class,
                    'settings' => [
                        'field' => 'currencyname',
                        'sort'  => 3,
                    ],
                ],
                [
                    'class'    => DataColumn::class,
                    'settings' => [
                        'field' => 'iso3166alpha3code',
                        'sort'  => 4,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    /**
     * @return array|array[][]
     */
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class'    => Tab::class,
                    'settings' => [
                        'label' => Yii::t('fafcms-core', 'Master data'),
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'capitalname',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'continent',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Currency'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'currencycode',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'currencyname',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-3' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Country Code'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'fipscode',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'iso3166alpha2code',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'iso3166alpha3code',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'iso3166numericcode',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                        ],
                        'row-2' => [
                            'class'    => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Additional Informationen'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'phoneprefix',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'postalcodemask',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'postalcoderegex',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                        ]
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {{@inheritDoc}}
     */
    public static function prefixableTableName(): string
    {
        return '{{%country}}';
    }

    /**
     * {{@inheritDoc}}
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['iso3166numericcode'], 'integer'],
            [['name', 'capitalname', 'continent', 'currencycode', 'currencyname', 'fipscode', 'iso3166alpha2code', 'iso3166alpha3code', 'phoneprefix', 'postalcodemask', 'postalcoderegex'], 'string', 'max' => 255],
            [['fipscode'], 'unique'],
            [['iso3166alpha2code'], 'unique'],
            [['iso3166alpha3code'], 'unique'],
            [['iso3166numericcode'], 'unique'],
        ];
    }

    /**
     * {{@inheritDoc}}
     */
    public function attributeLabels(): array
    {
        return [
            'id'                 => Yii::t('fafcms-core', 'ID'),
            'name'               => Yii::t('fafcms-core', 'Name'),
            'capitalname'        => Yii::t('fafcms-core', 'Capitalname'),
            'continent'          => Yii::t('fafcms-core', 'Continent'),
            'currencycode'       => Yii::t('fafcms-core', 'Currencycode'),
            'currencyname'       => Yii::t('fafcms-core', 'Currencyname'),
            'fipscode'           => Yii::t('fafcms-core', 'Fipscode'),
            'iso3166alpha2code'  => Yii::t('fafcms-core', 'Iso3166alpha2code'),
            'iso3166alpha3code'  => Yii::t('fafcms-core', 'Iso3166alpha3code'),
            'iso3166numericcode' => Yii::t('fafcms-core', 'Iso3166numericcode'),
            'phoneprefix'        => Yii::t('fafcms-core', 'Phoneprefix'),
            'postalcodemask'     => Yii::t('fafcms-core', 'Postalcodemask'),
            'postalcoderegex'    => Yii::t('fafcms-core', 'Postalcoderegex'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getFlags(): ActiveQuery
    {
        return $this->hasMany(Flag::class, ['country_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getLanguages(): ActiveQuery
    {
        return $this->hasMany(Language::class, ['country_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUsers(): ActiveQuery
    {
        return $this->hasMany(User::class, ['country_id' => 'id']);
    }
}
