<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\Bootstrap as FafcmsBootstrap;
use fafcms\helpers\validators\StrengthValidator;
use yii\base\Model;
use Yii;

/**
 * Class ChangePasswordForm
 *
 * @package fafcms\fafcms\models
 */
class ChangePasswordForm extends Model
{
    const TYPE_TOKEN = 1;
    const TYPE_USER = 2;

    public $password;
    public $password_confirmation;
    public $email;
    private $_user;
    public $type = self::TYPE_TOKEN;

    /**
     * ChangePasswordForm constructor.
     * @param string|User $tokenOrUser
     * @param array $config name-value pairs that will be used to initialize the object properties
     */
    public function __construct($tokenOrUser, $config = [])
    {
        if ($tokenOrUser instanceof \yii\web\User) {
            $this->_user = $tokenOrUser->identity;
            $this->type = self::TYPE_USER;
        } else {
            $passwordResetTokenExpirationTime = Yii::$app->getModule(FafcmsBootstrap::$id)->getPluginSettingValue('password_reset_token_expiration_time');

            if (empty($tokenOrUser) || !is_string($tokenOrUser) || (int)substr($tokenOrUser, strrpos($tokenOrUser, '_') + 1) + $passwordResetTokenExpirationTime < time() ||
                !($this->_user = User::find()->where(['reset_token' => $tokenOrUser])->one())) {
                Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'An invalid code was used. Please try again.'), false);
                return Yii::$app->response->redirect(Yii::$app->user->loginUrl);
            }
        }

        $this->email = $this->_user->email;

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'password' => Yii::t('fafcms-core', 'Password'),
            'password_confirmation' => Yii::t('fafcms-core', 'Repeat password'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['password', 'password_confirmation'], 'required'],
            [['password', 'password_confirmation'], 'filter', 'filter' => 'trim'],
            [['password'],
                StrengthValidator::class,
                'userAttribute' => 'email',
                'min' => 6,
                'upper' => 0,
                'lower' => 1,
                'digit' => 1,
                'special' => 0,
                'hasUser' => true,
                'hasEmail' => false
            ],
            ['password_confirmation', 'compare', 'compareAttribute'=> 'password', 'message' => Yii::t('fafcms-core', 'The entered passwords do not match.')],
        ];
    }

    /**
     * Resets password.
     * @return bool if password was reset.
     */
    public function resetPassword(): bool
    {
        $user = $this->_user;
        $user->setPassword($this->password);

        if ($this->type === self::TYPE_TOKEN) {
            $user->reset_token = null;
        }

        return $user->save(false);
    }
}
