<?php

namespace fafcms\fafcms\models;

use fafcms\helpers\ActiveRecord;
use Yii;

/**
 * This is the model class for table "queuehelper".
 *
 * @property int $id
 * @property int $queue_id
 * @property string $action
 *
 * @property Queue $queue
 */
class QueueHelper extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%queuehelper}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['queue_id', 'action'], 'required'],
            [['queue_id'], 'integer'],
            [['action'], 'string', 'max' => 255],
            [['queue_id'], 'exist', 'skipOnError' => true, 'targetClass' => Queue::className(), 'targetAttribute' => ['queue_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('fafcms-core', 'ID'),
            'queue_id' => Yii::t('fafcms-core', 'Queue ID'),
            'action' => Yii::t('fafcms-core', 'Action'),
        ];
    }

    /**
     * Gets query for [[Queue]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQueue()
    {
        return $this->hasOne(Queue::className(), ['id' => 'queue_id']);
    }

    /**
     * @param string $job
     * @param array  $data
     * @param int    $priority
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public static function runJob(string $job, array $data, int $priority = 1024): bool
    {
        $action = hash('sha256', $job . json_encode($data));

        if (!QueueHelper::find()->where(['action' => $action])->exists()) {
            $id = Yii::$app->queue->priority($priority)->push(new $job($data));

            $queueHelper = new QueueHelper([
                'queue_id' => $id,
                'action' => $action
            ]);

            return $queueHelper->save();
        }

        return true;
    }
}
