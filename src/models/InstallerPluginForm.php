<?php

namespace fafcms\fafcms\models;

use Yii;
use yii\base\Model;

/**
 * Class InstallerPluginForm
 * @package fafcms\fafcms\models
 */
class InstallerPluginForm extends Model
{
    /**
     * @var string
     */
    public $search = '';

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            ['search', 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'search' => Yii::t('fafcms-core', 'Search and install plugins')
        ];
    }
}
