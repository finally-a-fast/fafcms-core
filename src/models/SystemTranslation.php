<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\inputs\Percent;
use fafcms\fafcms\inputs\Raw;
use fafcms\fafcms\inputs\TextInput;
use fafcms\fafcms\items\ActionColumn;
use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\DataColumn;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\fafcms\queries\ArrayQuery;
use fafcms\fafcms\queries\DefaultQuery;
use fafcms\helpers\ActiveRecord;
use fafcms\helpers\interfaces\EditViewInterface;
use fafcms\helpers\interfaces\FieldConfigInterface;
use fafcms\helpers\interfaces\IndexViewInterface;
use fafcms\helpers\Model;
use fafcms\helpers\traits\BeautifulModelTrait;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\BaseDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;
use yiiui\yii2flagiconcss\widget\FlagIcon;

/**
 * Class SystemTranslation
 *
 * @package fafcms\fafcms\models
 */
class SystemTranslation extends Model implements FieldConfigInterface, IndexViewInterface
{
    use BeautifulModelTrait;

    public string $id;
    public string $category;

    public int $messageCount;
    public int $totalMessageCount;
    public int $translatedCount;
    public float $translatedPercent;

    public array $languages;

    public static bool $checkAccess = false;

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getLanguageSelection(): string
    {
        $languageSelection = '';

        $flags = Yii::$app->dataCache->index(Flag::class);

        foreach ($this->languages as $language) {
            $languageSelection .= Html::tag('div',
                Html::tag('div',
                    FlagIcon::widget([
                        'countryCode' => $flags[$language['flag_id']]['name'] ?? '',
                        'tagName' => 'a',
                        'options' => [
                            'href' => Url::to(['/' . SystemTranslationLanguage::instance()->getEditDataUrl() . '/update', 'id' => $language['id']]),
                        ]
                    ]),
                    [
                        'data-content' => Yii::t('fafcms-core', $language['name']) . ' (' . $language['ietfcode'] . ') ' . Yii::$app->getFormatter()->asPercent($language['translatedPercent']),
                        'class' => 'popup-hover'
                    ]
                ),
                ['class' => 'item']
            );
        }

        return '<div class="ui mini horizontal divided list">' . $languageSelection . '</div>';
    }

    /**
     * @inheritDoc
     */
    public static function getRecords(): array
    {
        return Yii::$app->i18n->searchTranslationMessages();
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        if (!$this->searchInternal($dataProvider, $params)) {
            return $dataProvider;
        }

        $dataProvider->sort =  [
            'attributes' => [
                'category',
                'translatedPercent',
            ],
            'defaultOrder' => [
                'category' => SORT_ASC,
            ]
        ];

        return $dataProvider;
    }

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'system-translation';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'translate';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('app', 'System translations');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('app', 'System translation');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['headline'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'category' => [
                'type' => TextInput::class
            ],
            'translatedCount' => [
                'type' => TextInput::class
            ],
            'translatedPercent' => [
                'type' => Percent::class
            ],
            'languageSelection' => [
                'type' => Raw::class
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'category',
                        'sort' => 1,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'translatedPercent',
                        'sort' => 2,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'languageSelection',
                        'sort' => 3,
                    ],
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation
}
