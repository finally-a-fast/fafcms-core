<?php

namespace fafcms\fafcms\models;

use fafcms\helpers\ActiveRecord;
use Yii;

/**
 * This is the model class for table "auth_model".
 *
 * @property int $id
 * @property string $permission_item_name
 * @property int $type
 * @property string|null $role_item_name
 * @property int|null $user_id
 * @property int $model_type
 * @property string $model_class
 * @property int|null $model_id
 * @property int|null $created_by
 * @property string|null $created_at
 *
 * @property User $createdBy
 * @property AuthItem $permissionItemName
 * @property AuthItem $roleItemName
 * @property User $user
 */
class AuthModel extends ActiveRecord
{
    const TYPE_ROLE = 1;
    const TYPE_USER = 2;

    const MODEL_TYPE_TABLE = 1;
    const MODEL_TYPE_RECORD = 2;

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%auth_model}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['permission_item_name', 'model_class'], 'required'],
            [['type', 'user_id', 'model_type', 'model_id', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['permission_item_name', 'role_item_name', 'model_class'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('fafcms-core', 'ID'),
            'permission_item_name' => Yii::t('fafcms-core', 'Permission Item Name'),
            'type' => Yii::t('fafcms-core', 'Type'),
            'role_item_name' => Yii::t('fafcms-core', 'Role Item Name'),
            'user_id' => Yii::t('fafcms-core', 'User ID'),
            'model_type' => Yii::t('fafcms-core', 'Model Type'),
            'model_class' => Yii::t('fafcms-core', 'Model Class'),
            'model_id' => Yii::t('fafcms-core', 'Model ID'),
            'created_by' => Yii::t('fafcms-core', 'Created By'),
            'created_at' => Yii::t('fafcms-core', 'Created At'),
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
