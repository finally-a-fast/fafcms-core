<?php

namespace fafcms\fafcms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;

/**
 * UserSearch represents the model behind the search form about `fafcms\fafcms\models\User`.
 *
 * @deprecated
 */
class UserSearch extends User
{
    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'wrong_password_count', 'wrong_password_blocked_count', 'country_id', 'language_id', 'animation_auto', 'animation_focus', 'animation_action', 'animation_system', 'video_animated_preview', 'video_muted', 'video_default_resolution', 'video_default_resolution_mobile', 'video_autoplay', 'video_autoplay_mobile', 'created_by', 'updated_by', 'activated_by', 'deactivated_by', 'deleted_by', 'last_change_id'], 'integer'],
            [['status', 'email', 'auth_key', 'activation_token', 'password_hash', 'reset_token', 'wrong_password_blocked_until', 'title', 'sex', 'firstname', 'lastname', 'company', 'position', 'department', 'address', 'zip', 'city', 'phone', 'mobile', 'fax', 'website', 'notifications_email', 'notificaitons_browser', 'created_at', 'updated_at', 'activated_at', 'deactivated_at', 'deleted_at', 'last_change_type', 'last_change_at', 'last_logout_at', 'last_login_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'                              => $this->id,
            'wrong_password_count'            => $this->wrong_password_count,
            'wrong_password_blocked_count'    => $this->wrong_password_blocked_count,
            'wrong_password_blocked_until'    => $this->wrong_password_blocked_until,
            'country_id'                      => $this->country_id,
            'language_id'                     => $this->language_id,
            'animation_auto'                  => $this->animation_auto,
            'animation_focus'                 => $this->animation_focus,
            'animation_action'                => $this->animation_action,
            'animation_system'                => $this->animation_system,
            'video_animated_preview'          => $this->video_animated_preview,
            'video_muted'                     => $this->video_muted,
            'video_default_resolution'        => $this->video_default_resolution,
            'video_default_resolution_mobile' => $this->video_default_resolution_mobile,
            'video_autoplay'                  => $this->video_autoplay,
            'video_autoplay_mobile'           => $this->video_autoplay_mobile,
            'created_by'                      => $this->created_by,
            'updated_by'                      => $this->updated_by,
            'activated_by'                    => $this->activated_by,
            'deactivated_by'                  => $this->deactivated_by,
            'deleted_by'                      => $this->deleted_by,
            'created_at'                      => $this->created_at,
            'updated_at'                      => $this->updated_at,
            'activated_at'                    => $this->activated_at,
            'deactivated_at'                  => $this->deactivated_at,
            'deleted_at'                      => $this->deleted_at,
            'last_change_id'                  => $this->last_change_id,
            'last_change_at'                  => $this->last_change_at,
            'last_logout_at'                  => $this->last_logout_at,
            'last_login_at'                   => $this->last_login_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'activation_token', $this->activation_token])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'reset_token', $this->reset_token])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'department', $this->department])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'zip', $this->zip])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'notifications_email', $this->notifications_email])
            ->andFilterWhere(['like', 'notificaitons_browser', $this->notificaitons_browser])
            ->andFilterWhere(['like', 'last_change_type', $this->last_change_type]);

        return $dataProvider;
    }
}
