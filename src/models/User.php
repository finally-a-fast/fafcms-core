<?php

namespace fafcms\fafcms\models;

use fafcms\helpers\{
    traits\OptionTrait,
    validators\StrengthValidator,
};
use fafcms\fafcms\{
    abstracts\models\BaseUser,
    inputs\DropDownList,
    inputs\HiddenInput,
    inputs\SwitchCheckbox,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab};
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;
use yii\db\ActiveQuery;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 */
class User extends BaseUser implements IdentityInterface
{
    public const SCENARIO_PASSWORD_CHANGE_REQUEST = 'password_change_request';
    public const SCENARIO_PASSWORD_CHANGE         = 'password_change';

    public static bool $hasCreateScenario = true;
    public static array $defaultOrder     = ['lastname' => SORT_ASC, 'firstname' => SORT_ASC];

    public string $password              = '';
    public string $password_confirmation = '';
    public string $real_last_login_at    = '';

    /**@var array */
    public array $userRoles = [];

    /** @var array */
    protected array $newUserRoles = [];

    //region BeautifulModelTrait implementation

    /**
     * {@inheritdoc}
     */
    public static function editDataIcon($model): string
    {
        return 'text-subject';
    }

    /**
     * {@inheritdoc}
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        $extendedLabel = trim(($model['email'] ?? ''));

        if ($html) {
            $extendedLabel .= '<br><small><b>';
        }

        $extendedLabel .= trim(($model['firstname'] ?? '')) . ' ' . trim(($model['lastname'] ?? ''));

        if ($html) {
            $extendedLabel .= '</b></small>';
        }

        return $extendedLabel;
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * {@inheritdoc}
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.firstname',
                static::tableName() . '.lastname',
                static::tableName() . '.email',
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [
                static::tableName() . '.lastname' => SORT_ASC,
                static::tableName() . '.firstname' => SORT_ASC,
            ],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region FieldConfigInterface implementation
    /**
     * {@inheritdoc}
     */
    public function getFieldConfig(): array
    {
        return array_merge(parent::getFieldConfig(), [
            'password' => [
                'type'    => TextInput::class,
                'options' => ['type' => 'password'],
            ],
            'password_confirmation' => [
                'type'    => TextInput::class,
                'options' => ['type' => 'password'],
            ],
            'sex' => [
                'type'  => DropDownList::class,
                'items' => $this->attributeOptions()['sex'],
            ],
            'notifications_email' => [
                'type'     => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'notificaitons_browser' => [
                'type'     => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'video_default_resolution' => [
                'type'  => DropDownList::class,
                'items' => $this->attributeOptions()['video_default_resolution'],
            ],
            'video_default_resolution_mobile' =>[
                'type'  => DropDownList::class,
                'items' => $this->attributeOptions()['video_default_resolution_mobile'],
            ],
            // region disabled
            'password_hash'        => [
                'type'    => TextInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'wrong_password_count' => [
                'type'    => TextInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'wrong_password_blocked_count' => [
                'type'    => TextInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'wrong_password_blocked_until' => [
                'type'    => TextInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'last_login_at' => [
                'type'    => TextInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'last_change_id' => [
                'type'    => TextInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'last_change_at' => [
                'type'    => TextInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'last_change_type' => [
                'type'    => TextInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'last_logout_at' => [
                'type'    => TextInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            // endregion disabled
           'userRoles' => [
                'type'    => DropDownList::class,
                'items'   => $this->attributeOptions()['userRoles'],
                'options' => [
                    'multiple' => true,
                    'selected' => $this->getUserRoles()
                ],
            ],
        ]);
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    /**
     * @return array
     */
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class'    => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort'  => 1,
                    ],
                ],
                [
                    'class'    => DataColumn::class,
                    'settings' => [
                        'field' => 'email',
                        'sort'  => 2,
                    ],
                ],
                [
                    'class'    => DataColumn::class,
                    'settings' => [
                        'field' => 'title',
                        'sort'  => 3,
                    ],
                ],
                [
                    'class'    => DataColumn::class,
                    'settings' => [
                        'field' => 'sex',
                        'sort'  => 4,
                    ],
                ],
                [
                    'class'    => DataColumn::class,
                    'settings' => [
                        'field' => 'firstname',
                        'sort'  => 5,
                    ],
                ],
                [
                    'class'    => DataColumn::class,
                    'settings' => [
                        'field' => 'lastname',
                        'sort'  => 6,
                    ],
                ],
                [
                    'class'    => DataColumn::class,
                    'settings' => [
                        'field' => 'company',
                        'sort'  => 7,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ],
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    /**
     * @return array|array[][]
     */
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class'    => Tab::class,
                    'settings' => [
                        'label' => Yii::t('fafcms-core', 'Master data'),
                    ],
                    'contents' => [
                        'row-1' => [
                            'class'    => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'master-data-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sex',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'firstname',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'lastname',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'image-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Image'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [],
                                        ],
                                    ]
                                ],
                                'column-3' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'access-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Access'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'password',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'password_confirmation',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                        ],
                        'row-2' => [
                            'class'    => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'additional-data-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Additional Informationen'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'company',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'position',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'department',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'address',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'zip',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'city',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'country_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'contact-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Contact'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'email',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'phone',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'mobile',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'fax',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'website',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-3' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'user-roles-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'User Roles'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'userRoles',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                        ],
                    ],
                ],
                'settings-tab' => [
                    'class'    => Tab::class,
                    'settings' => [
                        'label' => Yii::t('fafcms-core', 'Settings'),
                    ],
                    'contents' => [
                        'row-1' => [
                            'class'    => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'notifications-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Notifications'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'notifications_email',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'notificaitons_browser',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'animation-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Animations'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'animation_auto',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'animation_focus',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'animation_action',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'animation_system',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-3' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'video-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Video Settings'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_animated_preview',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_muted',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_autoplay',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_autoplay_mobile',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_default_resolution',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_default_resolution_mobile',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                        ],
                    ],
                ],
                'login-tab' => [
                    'class'    => Tab::class,
                    'settings' => [
                        'label' => Yii::t('fafcms-core', 'Activity'),
                    ],
                    'contents' => [
                        'row-1' => [
                            'class'    => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'last-changes-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Last Changes'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_change_id',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_change_type',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_change_at',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'login-log-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Login Log'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_login_at',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_logout_at',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-3' => [
                                    'class'    => Column::class,
                                    'settings' => [
                                        'm' => 5,
                                    ],
                                    'contents' => [
                                        'password-card' => [
                                            'class'    => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Wrong Password'),
                                                'icon' => 'id-card',
                                            ],
                                            'contents' => [
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'wrong_password_count',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'wrong_password_blocked_count',
                                                    ],
                                                ],
                                                [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'wrong_password_blocked_until',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                            ],
                        ],
                    ],
                ]
            ],
            'changePasswordView' => [
                'label' => ['fafcms-core', 'Change password'],
                'scenario' => self::SCENARIO_PASSWORD_CHANGE,
                'buttons' => [
                    static function($buttons) {
                        unset($buttons['export']);
                        return $buttons;
                    }
                ],
                'contents' => [
                    'row-1' => [
                        'class'    => Row::class,
                        'contents' => [
                            'column-1' => [
                                'class'    => Column::class,
                                'contents' => [
                                    'access-card' => [
                                        'class'    => Card::class,
                                        'settings' => [
                                            'title' => Yii::t('fafcms-core', 'Change password'),
                                            'icon' => 'key',
                                        ],
                                        'contents' => [
                                            [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'email',
                                                    'fieldDefinition' => [
                                                        'fieldType' => HiddenInput::class,
                                                    ],
                                                ],
                                            ],
                                            [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'password',
                                                ],
                                            ],
                                            [
                                                'class' => FormField::class,
                                                'settings' => [
                                                    'field' => 'password_confirmation',
                                                ],
                                            ],
                                        ],
                                    ],
                                ]
                            ],
                        ],
                    ],
                ]
            ],
        ];
    }
    //endregion EditViewInterface implementation

    //region AttributeOptionTrait implementation
    /**
     * @return array
     */
    public function attributeOptions(): array
    {
        return array_merge(parent::attributeOptions(), [
            'video_default_resolution' => [
                1 => Yii::t('fafcms-core', 'Automatic'),
                2 => '144p',
                3 => '240p',
                4 => '360p',
                5 => '480p',
                6 => '720p (HD)',
                7 => '1080p (Full HD)',
                8 => '1440p (Quad HD)',
                9 => '2160p (4K Ultra HD)',
            ],
            'video_default_resolution_mobile' => [
                1 => Yii::t('fafcms-core', 'Automatic'),
                2 => '144p',
                3 => '240p',
                4 => '360p',
                5 => '480p',
                6 => '720p (HD)',
                7 => '1080p (Full HD)',
                8 => '1440p (Quad HD)',
                9 => '2160p (4K Ultra HD)',
            ],
            'sex' => [
                'female' => Yii::t('fafcms-core',  'Miss'),
                'male'   => Yii::t('fafcms-core',  'Mister'),
                'misc' => Yii::t('fafcms-core', 'Miscellaneous'),
            ],
            'country_id' => static function (...$parmas) {
                return Country::getOptions(...$parmas);
            },
            'language_id' => static function (...$parmas) {
                return Language::getOptions(...$parmas);
            },
            'userRoles' => static function() {
                return static::userRoles(false);
            },
        ]);
    }
    //endregion AttributeOptionTrait implementation

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function beforeValidate(): bool
    {
        if ($this->password !== '' && $this->password === $this->password_confirmation && !$this->isAttributeChanged('password_hash')) {
            $this->setPassword($this->password);
        }

        return parent::beforeValidate();
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert): bool
    {
        $newUserRoles = $this->userRoles;
        if ($newUserRoles !== $this->getUserRoles()) {
            $this->newUserRoles = $newUserRoles;
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritDoc}
     *
     * @throws \Exception
     */
    public function afterSave($insert, $changedAttributes): void
    {
        parent::afterSave($insert, $changedAttributes);

        if (!empty($this->newUserRoles)) {
            $auth = Yii::$app->getAuthManager();
            $roles = array_flip($this->userRoles);

            foreach ($this->newUserRoles as $userRole) {
                if (!isset($roles[$userRole])) {
                    $auth->assign($auth->getRole($userRole), $this->getPrimaryKey());
                }

                unset($roles[$userRole]);
            }

            foreach ($roles as $roleName => $value) {
                $auth->revoke($auth->getRole($roleName), $this->getPrimaryKey());
            }

            $this->getUserRoles();
        }

        $this->password = '';
        $this->password_confirmation = '';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        $rules = parent::rules();
        unset(
            $rules['string-notifications_email'],
            $rules['string-notificaitons_browser'],
            $rules['integer-animation_auto'],
            $rules['integer-animation_focus'],
            $rules['integer-animation_action'],
            $rules['integer-animation_system'],
            $rules['integer-video_animated_preview'],
            $rules['integer-video_muted'],
            $rules['integer-video_autoplay'],
            $rules['integer-video_autoplay_mobile'],
        );

        return array_merge($rules, [
            'boolean-notifications_email'          => ['notifications_email', 'boolean',],
            'boolean-notificaitons_browser'        => ['notificaitons_browser', 'boolean',],
            'boolean-animation_auto'               => ['animation_auto', 'boolean'],
            'boolean-animation_focus'              => ['animation_focus', 'boolean'],
            'boolean-animation_action'             => ['animation_action', 'boolean'],
            'boolean-animation_system'             => ['animation_system', 'boolean'],
            'boolean-video_animated_preview'       => ['video_animated_preview', 'boolean'],
            'boolean-video_muted'                  => ['video_muted', 'boolean'],
            'boolean-video_autoplay'               => ['video_autoplay', 'boolean'],
            'boolean-video_autoplay_mobile'        => ['video_autoplay_mobile', 'boolean'],
            'safe-userRoles'                       => ['userRoles', 'safe'],
            'unique-auth_key'                      => ['auth_key', 'unique'],
            'unique-activation_token'              => ['activation_token', 'unique'],
            'required-password'                    => ['password', 'required', 'on' => [static::SCENARIO_CREATE, static::SCENARIO_PASSWORD_CHANGE]],
            'required-password_confirmation'       => ['password_confirmation', 'required', 'on' => [static::SCENARIO_CREATE, static::SCENARIO_PASSWORD_CHANGE]],
            'filter-password'                      => ['password', 'filter', 'filter' => 'trim'],
            'filter-password_confirmation'         => ['password_confirmation', 'filter', 'filter' => 'trim'],
            'compare-password_confirmation'        => ['password_confirmation', 'compare', 'compareAttribute' => 'password', 'message' => 'Die eingegebenen Kennwörter stimmen nicht überein.'],
            StrengthValidator::class . '-password' => [
                ['password'],
                StrengthValidator::class,
                'userAttribute' => 'email',
                'min'           => 6,
                'upper'         => 0,
                'lower'         => 1,
                'digit'         => 1,
                'special'       => 1,
                'hasUser'       => true,
                'hasEmail'      => false,
            ],
        ]);
    }

    public function scenarios(): array
    {
        $scenarios = parent::scenarios();
        $scenarios[static::SCENARIO_PASSWORD_CHANGE_REQUEST] = ['email'];

        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'password'              => Yii::t('fafcms-core', 'Password'),
            'password_hash'         => Yii::t('fafcms-core', 'Password'),
            'password_confirmation' => Yii::t('fafcms-core', 'Repeat password'),
            'userAuthItems'         => Yii::t('fafcms-core', 'Usergroups'),
        ]);
    }

    // region IdentityInterface
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey(): ?string
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey): bool
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id): ?self
    {
        return static::findOne(['id' => $id, 'status' => static::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    // endregion IdentityInterface

    // TODO
    // public static function findByUsername($username)
    // {
    //     return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    // }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    /*TODO
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }*/

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     *
     * @return static|null
     */
    public static function findByPasswordResetToken(string $token): ?self
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'reset_token' => $token,
            'status'      => static::STATUS_ACTIVE,
        ]);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return bool if password provided is valid for current user
     */
    public function validatePassword(string $password): bool
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     *
     * @throws Exception
     */
    public function setPassword(string $password): void
    {
        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     *
     * @throws Exception
     */
    public function generateAuthKey(): void
    {
        do {
            $this->auth_key = Yii::$app->getSecurity()->generateRandomString(255);
        } while (!$this->validate(['auth_key']));
    }

    /**
     * Generates activation key
     *
     * @throws Exception
     */
    public function generateActivationToken(): void
    {
        do {
            $this->activation_token = Yii::$app->getSecurity()->generateRandomString(32);
        } while (!$this->validate(['activation_token']));
    }

    /**
     * Generates new password reset token
     *
     * @throws Exception
     */
    public function generateResetToken(): void
    {
        do {
            $this->reset_token = Yii::$app->getSecurity()->generateRandomString(32) . '_' . time();
        } while (!$this->validate(['reset_token']));
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken(): void
    {
        $this->reset_token = null;
    }

    //region search
    /**
     * @return array
     */
    public function searchRules(): array
    {
        return [
            'integer-id'                              => ['id', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-wrong_password_count'            => ['wrong_password_count', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-wrong_password_blocked_count'    => ['wrong_password_blocked_count', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-country_id'                      => ['country_id', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-language_id'                     => ['language_id', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-animation_auto'                  => ['animation_auto', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-animation_focus'                 => ['animation_focus', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-animation_action'                => ['animation_action', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-animation_system'                => ['animation_system', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-video_animated_preview'          => ['video_animated_preview', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-video_muted'                     => ['video_muted', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-video_default_resolution'        => ['video_default_resolution', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-video_default_resolution_mobile' => ['video_default_resolution_mobile', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-video_autoplay'                  => ['video_autoplay', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-video_autoplay_mobile'           => ['video_autoplay_mobile', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-created_by'                      => ['created_by', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-updated_by'                      => ['updated_by', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-activated_by'                    => ['activated_by', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-deactivated_by'                  => ['deactivated_by', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-deleted_by'                      => ['deleted_by', 'integer', 'on' => static::SCENARIO_SEARCH],
            'integer-last_change_id'                  => ['last_change_id', 'integer', 'on' => static::SCENARIO_SEARCH],
            'status'                                  => ['status', 'safe', 'on' => static::SCENARIO_SEARCH],
            'email'                                   => ['email', 'safe', 'on' => static::SCENARIO_SEARCH],
            'auth_key'                                => ['auth_key', 'safe', 'on' => static::SCENARIO_SEARCH],
            'activation_token'                        => ['activation_token', 'safe', 'on' => static::SCENARIO_SEARCH],
            'password_hash'                           => ['password_hash', 'safe', 'on' => static::SCENARIO_SEARCH],
            'reset_token'                             => ['reset_token', 'safe', 'on' => static::SCENARIO_SEARCH],
            'wrong_password_blocked_until'            => ['wrong_password_blocked_until', 'safe', 'on' => static::SCENARIO_SEARCH],
            'title'                                   => ['title', 'safe', 'on' => static::SCENARIO_SEARCH],
            'sex'                                     => ['sex', 'safe', 'on' => static::SCENARIO_SEARCH],
            'firstname'                               => ['firstname', 'safe', 'on' => static::SCENARIO_SEARCH],
            'lastname'                                => ['lastname', 'safe', 'on' => static::SCENARIO_SEARCH],
            'company'                                 => ['company', 'safe', 'on' => static::SCENARIO_SEARCH],
            'position'                                => ['position', 'safe', 'on' => static::SCENARIO_SEARCH],
            'department'                              => ['department', 'safe', 'on' => static::SCENARIO_SEARCH],
            'address'                                 => ['address', 'safe', 'on' => static::SCENARIO_SEARCH],
            'zip'                                     => ['zip', 'safe', 'on' => static::SCENARIO_SEARCH],
            'city'                                    => ['city', 'safe', 'on' => static::SCENARIO_SEARCH],
            'phone'                                   => ['phone', 'safe', 'on' => static::SCENARIO_SEARCH],
            'mobile'                                  => ['mobile', 'safe', 'on' => static::SCENARIO_SEARCH],
            'fax'                                     => ['fax', 'safe', 'on' => static::SCENARIO_SEARCH],
            'website'                                 => ['website', 'safe', 'on' => static::SCENARIO_SEARCH],
            'notifications_email'                     => ['notifications_email', 'safe', 'on' => static::SCENARIO_SEARCH],
            'notificaitons_browser'                   => ['notificaitons_browser', 'safe', 'on' => static::SCENARIO_SEARCH],
            'created_at'                              => ['created_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'updated_at'                              => ['updated_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'activated_at'                            => ['activated_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'deactivated_at'                          => ['deactivated_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'deleted_at'                              => ['deleted_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'last_change_type'                        => ['last_change_type', 'safe', 'on' => static::SCENARIO_SEARCH],
            'last_change_at'                          => ['last_change_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'last_logout_at'                          => ['last_logout_at', 'safe', 'on' => static::SCENARIO_SEARCH],
            'last_login_at'                           => ['last_login_at', 'safe', 'on' => static::SCENARIO_SEARCH],
        ];
    }

    /**
     * @param array $params
     *
     * @return BaseDataProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params): BaseDataProvider
    {
        if (!$this->searchInternal($dataProvider, $params)) {
            return $dataProvider;
        }

        $dataProvider->query->andFilterWhere([
            'id'                              => $this->id,
            'wrong_password_count'            => $this->wrong_password_count,
            'wrong_password_blocked_count'    => $this->wrong_password_blocked_count,
            'wrong_password_blocked_until'    => $this->wrong_password_blocked_until,
            'country_id'                      => $this->country_id,
            'language_id'                     => $this->language_id,
            'animation_auto'                  => $this->animation_auto,
            'animation_focus'                 => $this->animation_focus,
            'animation_action'                => $this->animation_action,
            'animation_system'                => $this->animation_system,
            'video_animated_preview'          => $this->video_animated_preview,
            'video_muted'                     => $this->video_muted,
            'video_default_resolution'        => $this->video_default_resolution,
            'video_default_resolution_mobile' => $this->video_default_resolution_mobile,
            'video_autoplay'                  => $this->video_autoplay,
            'video_autoplay_mobile'           => $this->video_autoplay_mobile,
            'created_by'                      => $this->created_by,
            'updated_by'                      => $this->updated_by,
            'activated_by'                    => $this->activated_by,
            'deactivated_by'                  => $this->deactivated_by,
            'deleted_by'                      => $this->deleted_by,
            'created_at'                      => $this->created_at,
            'updated_at'                      => $this->updated_at,
            'activated_at'                    => $this->activated_at,
            'deactivated_at'                  => $this->deactivated_at,
            'deleted_at'                      => $this->deleted_at,
            'last_change_id'                  => $this->last_change_id,
            'last_change_at'                  => $this->last_change_at,
            'last_logout_at'                  => $this->last_logout_at,
            'last_login_at'                   => $this->last_login_at,
        ]);

        $dataProvider->query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'activation_token', $this->activation_token])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'reset_token', $this->reset_token])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'department', $this->department])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'zip', $this->zip])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'notifications_email', $this->notifications_email])
            ->andFilterWhere(['like', 'notificaitons_browser', $this->notificaitons_browser])
            ->andFilterWhere(['like', 'last_change_type', $this->last_change_type]);

        return $dataProvider;
    }
    //endregion search

    // region AuthManager
    /**
     * @return string
     */
    public function getUserGroupLabel(): string
    {
        $groupLabels = '';

        foreach (Yii::$app->getAuthManager()->getRolesByUser($this->id) as $role) {
            $groupLabels .= ($groupLabels !== ''?', ':'').Yii::$app->fafcms->getTextOrTranslation($role->description);
        }

        return $groupLabels;
    }

    /**
     * @param bool $empty
     *
     * @return array
     */
    public static function userRoles($empty = false): array
    {
        $roles = Yii::$app->getAuthManager()->getRoles();

        foreach ($roles as $role) {
            $options[$role->name] = Yii::t('fafcms-core', ucfirst($role->name));
        }

        return OptionTrait::getOptions($empty) + ($options ?? []);
    }

    /**
     * @return array
     */
    public function getUserRoles(): array
    {
        $this->userRoles = [];
        $roles = Yii::$app->getAuthManager()->getRolesByUser($this->getPrimaryKey());

        foreach ($roles as $role) {
            $this->userRoles[] = $role->name;
        }

        return $this->userRoles;
    }
    // endregion AuthManager

    //region relations
    /**
     * @return ActiveQuery
     */
    public function getCountry(): ActiveQuery
    {
        return parent::getCountry()->inverseOf('user');
    }

    /**
     * @return ActiveQuery
     */
    public function getLanguage(): ActiveQuery
    {
        return parent::getLanguage()->inverseOf('user');
    }
    //endregion relations

    // region deprecated
    /**
     * @return array
     * @deprecated
     */
    public function getAttributesOptions()
    {
        return $this->attributeOptions();
    }

    // endregion deprecated
}
