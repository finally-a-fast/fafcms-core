<?php

namespace fafcms\fafcms\models;

use fafcms\fafcms\abstracts\models\BaseProjectlanguage;
use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\classes\OptionProvider;
use Yii;
use yiiui\yii2flagiconcss\widget\FlagIcon;

/**
 * This is the model class for table "{{%projectlanguage}}".
 *
 * @package fafcms\fafcms\models
 */
class Projectlanguage extends BaseProjectlanguage
{
    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return 'translate';
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        $name = trim(($model['name'] ?? ''));

        if (!$html) {
            return $name;
        }

        if ($model instanceof self) {
            $language = $model->language;
        } else {
            $language = Yii::$app->dataCache->index(Language::class)[$model['language_id']];
        }

        return FlagIcon::widget([
           'countryCode' => $language->flag->name,
       ]) . ' ' . $name;
    }
    //endregion BeautifulModelTrait implementation

    /**
     * @inheritDoc
     */
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-description' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'description',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 6
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Media'),
                                                'icon' => 'file-outline',
                                            ],
                                            'contents' => [
                                                'field-project_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'project_id',
                                                    ],
                                                ],
                                                'field-path' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'path',
                                                    ],
                                                ],
                                                'field-domain_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'domain_id',
                                                    ],
                                                ],
                                                'field-language_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'language_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ]
                            ]
                        ],
                        'row-2' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 6
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-site_name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'site_name',
                                                    ],
                                                ],
                                                'field-og_site_name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_site_name',
                                                    ],
                                                ],
                                                'field-fb_app_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'fb_app_id',
                                                    ],
                                                ],
                                                'field-show_start_page_in_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'show_start_page_in_title',
                                                    ],
                                                ],
                                                'field-show_site_name_in_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'show_site_name_in_title',
                                                    ],
                                                ],
                                                'field-reverse_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'reverse_title',
                                                    ],
                                                ],
                                                'field-title_separator' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title_separator',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ],
                                'column-2' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 6
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => Yii::t('fafcms-core', 'Master data'),
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-start_page_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'start_page_contentmeta_id',
                                                    ],
                                                ],
                                                'field-error_page_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'error_page_contentmeta_id',
                                                    ],
                                                ],
                                                'field-login_page_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'login_page_contentmeta_id',
                                                    ],
                                                ],
                                                'field-default_layout_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'default_layout_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]
                                ]
                            ]
                        ],
                    ],
                ],
            ],
        ];
    }

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.name',
                static::tableName() . '.language_id'
            ])
            ->setSort([static::tableName() . '.name' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation
}
