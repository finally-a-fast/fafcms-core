<?php

namespace fafcms\fafcms\models;

use fafcms\helpers\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%flag}}".
 *
 * @property int $id
 * @property string $name
 * @property int $country_id
 *
 * @property Country $country
 * @property Language[] $languages
 */
class Flag extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%flag}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['country_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('fafcms-core', 'ID'),
            'name' => Yii::t('fafcms-core', 'Name'),
            'country_id' => Yii::t('fafcms-core', 'Country ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages()
    {
        return $this->hasMany(Language::className(), ['flag_id' => 'id']);
    }
}
