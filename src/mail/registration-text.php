<?php
    /* @var $this fafcms\fafcms\components\ViewComponent */
    /* @var $user common\models\User */

    $activationLink = Yii::$app->urlManager->createAbsoluteUrl(['benutzer/aktivierung', 'k' => $user->activation_token]);
?>
Benutzerkonto aktivieren

Guten Tag, <?=$user->getAttributeOptions('sex')[$user->sex]??'Herr/Frau';?> <?=$user->lastname;?>!

Bitte rufen Sie folgenden Link in Ihrem Browser auf, um Ihr Benutzerkonto zu
aktivieren:

<?=$activationLink;?>
