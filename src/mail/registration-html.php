<?php
    use yii\helpers\Html;

    /* @var $this fafcms\fafcms\components\ViewComponent */
    /* @var $user common\models\User */

    $activationLink = Yii::$app->urlManager->createAbsoluteUrl(['benutzer/aktivierung', 'k' => $user->activation_token]);
?>
<div id="content">
    <h1 style="font-size: 12px;">Benutzerkonto aktivieren</h1>
    <p>
        Guten Tag, <?=$user->getAttributeOptions('sex')[$user->sex]??'Herr/Frau' ?> <?=Html::encode($user->lastname) ?>!
    </p>
    <p>
        Bitte rufen Sie folgenden Link in Ihrem Browser auf, um Ihr Benutzerkonto zu aktivieren:
    </p>
    <p>
        <?=Html::a(Html::encode($activationLink), $activationLink, ['style' => 'color: #808080;']);?>
    </p>
</div>
