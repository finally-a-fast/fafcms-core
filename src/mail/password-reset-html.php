<?php
use yii\helpers\Html;

/* @var $this fafcms\fafcms\components\ViewComponent */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['main/reset-password', 'k' => $user->reset_token]);
?>
<h1>Neues Kennwort anfordern</h1>
<p>
    Guten Tag, <?=$user->getAttributeOptions('sex')[$user->sex]??'Herr/Frau' ?> <?=Html::encode($user->lastname) ?>!
</p>
<p>
    Bitte rufen Sie folgenden Link in Ihrem Browser auf, um ein neues Kennwort für Ihr Benutzerkonto zu erstellen.<br>
    Sollten Sie diesen Vorgang nicht ausgelöst haben, können Sie diese E-Mail löschen - es wird dann nichts unternommen.
</p>
<p>
    <?=Html::a(Html::encode($resetLink), $resetLink, ['style' => 'color: #000000;']);?>
</p>
