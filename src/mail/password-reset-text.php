<?php
/* @var $this fafcms\fafcms\components\ViewComponent */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['main/reset-password', 'k' => $user->reset_token]);
?>
Neues Kennwort anfordern

Guten Tag, <?=$user->getAttributeOptions('sex')[$user->sex]??'Herr/Frau';?> <?=$user->lastname;?>!

Bitte rufen Sie folgenden Link in Ihrem Browser auf, um ein neues Kennwort für
Ihr Benutzerkonto zu erstellen.
Sollten Sie diesen Vorgang nicht ausgelöst haben, können Sie diese E-Mail
löschen - es wird dann nichts unternommen.

<?=$resetLink;?>
