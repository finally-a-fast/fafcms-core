<?php

namespace fafcms\fafcms\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\Textarea,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\Domain,
    models\Project,
    models\User,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%customfield}}".
 *
 * @package fafcms\fafcms\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property int|null $project_id
 * @property int|null $customtable_id
 * @property string $model_class
 * @property string $type
 * @property string $name
 * @property string $title
 * @property string|null $description
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property User $activatedBy
 * @property User $createdBy
 * @property Domain $customtable
 * @property User $deactivatedBy
 * @property User $deletedBy
 * @property Project $project
 * @property User $updatedBy
 */
abstract class BaseCustomfield extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'customfield';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'customfield';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-core', 'Customfields');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-core', 'Customfield');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.name'
            ])
            ->setSort([static::tableName() . '.name' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
            'project_id' => static function($properties = []) {
                return Project::getOptionProvider($properties)->getOptions();
            },
            'customtable_id' => static function($properties = []) {
                return Domain::getOptionProvider($properties)->getOptions();
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'activated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deactivated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('status', false),
            ],
            'project_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('project_id', false),
                'relationClassName' => Project::class,
            ],
            'customtable_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('customtable_id', false),
                'relationClassName' => Domain::class,
            ],
            'model_class' => [
                'type' => TextInput::class,
            ],
            'type' => [
                'type' => TextInput::class,
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'title' => [
                'type' => TextInput::class,
            ],
            'description' => [
                'type' => Textarea::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('created_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('updated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('activated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deactivated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deleted_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                'status' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                'name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'project_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'project_id',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                'customtable_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'customtable_id',
                        'sort' => 5,
                        'link' => true,
                    ],
                ],
                'model_class' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'model_class',
                        'sort' => 6,
                    ],
                ],
                'type' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'type',
                        'sort' => 7,
                    ],
                ],
                'title' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'title',
                        'sort' => 8,
                    ],
                ],
                'description' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'description',
                        'sort' => 9,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-project_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'project_id',
                                                    ],
                                                ],
                                                'field-customtable_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'customtable_id',
                                                    ],
                                                ],
                                                'field-model_class' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'model_class',
                                                    ],
                                                ],
                                                'field-type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'type',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title',
                                                    ],
                                                ],
                                                'field-description' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'description',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%customfield}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'integer-project_id' => ['project_id', 'integer'],
            'integer-customtable_id' => ['customtable_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'required-model_class' => ['model_class', 'required'],
            'required-type' => ['type', 'required'],
            'required-name' => ['name', 'required'],
            'required-title' => ['title', 'required'],
            'string-description' => ['description', 'string'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-model_class' => ['model_class', 'string', 'max' => 255],
            'string-type' => ['type', 'string', 'max' => 255],
            'string-title' => ['title', 'string', 'max' => 255],
            'string-name' => ['name', 'string', 'max' => 20],
            'exist-activated_by' => [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['activated_by' => 'id']],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-customtable_id' => [['customtable_id'], 'exist', 'skipOnError' => true, 'targetClass' => Domain::class, 'targetAttribute' => ['customtable_id' => 'id']],
            'exist-deactivated_by' => [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deactivated_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-project_id' => [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-core', 'ID'),
            'status' => Yii::t('fafcms-core', 'Status'),
            'project_id' => Yii::t('fafcms-core', 'Project ID'),
            'customtable_id' => Yii::t('fafcms-core', 'Customtable ID'),
            'model_class' => Yii::t('fafcms-core', 'Model Class'),
            'type' => Yii::t('fafcms-core', 'Type'),
            'name' => Yii::t('fafcms-core', 'Name'),
            'title' => Yii::t('fafcms-core', 'Title'),
            'description' => Yii::t('fafcms-core', 'Description'),
            'created_by' => Yii::t('fafcms-core', 'Created By'),
            'updated_by' => Yii::t('fafcms-core', 'Updated By'),
            'activated_by' => Yii::t('fafcms-core', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-core', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-core', 'Deleted By'),
            'created_at' => Yii::t('fafcms-core', 'Created At'),
            'updated_at' => Yii::t('fafcms-core', 'Updated At'),
            'activated_at' => Yii::t('fafcms-core', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-core', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-core', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[ActivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getActivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'activated_by',
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[Customtable]].
     *
     * @return ActiveQuery
     */
    public function getCustomtable(): ActiveQuery
    {
        return $this->hasOne(Domain::class, [
            'id' => 'customtable_id',
        ]);
    }

    /**
     * Gets query for [[DeactivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeactivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deactivated_by',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[Project]].
     *
     * @return ActiveQuery
     */
    public function getProject(): ActiveQuery
    {
        return $this->hasOne(Project::class, [
            'id' => 'project_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
