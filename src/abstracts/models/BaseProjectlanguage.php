<?php

namespace fafcms\fafcms\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\Domain,
    models\Language,
    models\Project,
    models\User,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\sitemanager\{
    models\Contentmeta,
    models\Layout,
    models\Menu,
    models\Menuitem,
    models\Site,
    models\Snippet,
    models\Topic,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%projectlanguage}}".
 *
 * @package fafcms\fafcms\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property string $name
 * @property string|null $description
 * @property string|null $site_name
 * @property int|null $show_start_page_in_title
 * @property int|null $show_site_name_in_title
 * @property int|null $reverse_title
 * @property string|null $title_separator
 * @property int|null $start_page_contentmeta_id
 * @property int|null $error_page_contentmeta_id
 * @property int|null $login_page_contentmeta_id
 * @property int|null $default_layout_id
 * @property string|null $og_site_name
 * @property string|null $fb_app_id
 * @property string $path
 * @property int $project_id
 * @property int $domain_id
 * @property int $language_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property User $activatedBy
 * @property User $createdBy
 * @property User $deactivatedBy
 * @property Layout $defaultLayout
 * @property User $deletedBy
 * @property Domain $domain
 * @property Contentmeta $errorPageContentmeta
 * @property Language $language
 * @property Contentmeta $loginPageContentmeta
 * @property Project[] $primaryProjectlanguageProjects
 * @property Project $project
 * @property Contentmeta[] $projectlanguageContentmetas
 * @property Domain[] $projectlanguageDomains
 * @property Layout[] $projectlanguageLayouts
 * @property Menuitem[] $projectlanguageMenuitems
 * @property Menu[] $projectlanguageMenus
 * @property Site[] $projectlanguageSites
 * @property Snippet[] $projectlanguageSnippets
 * @property Topic[] $projectlanguageTopics
 * @property Contentmeta $startPageContentmeta
 * @property User $updatedBy
 */
abstract class BaseProjectlanguage extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'projectlanguage';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'projectlanguage';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-core', 'Projectlanguages');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-core', 'Projectlanguage');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.name'
            ])
            ->setSort([static::tableName() . '.name' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
            'start_page_contentmeta_id' => static function($properties = []) {
                return Contentmeta::getOptionProvider($properties)->getOptions();
            },
            'error_page_contentmeta_id' => static function($properties = []) {
                return Contentmeta::getOptionProvider($properties)->getOptions();
            },
            'login_page_contentmeta_id' => static function($properties = []) {
                return Contentmeta::getOptionProvider($properties)->getOptions();
            },
            'default_layout_id' => static function($properties = []) {
                return Layout::getOptionProvider($properties)->getOptions();
            },
            'project_id' => static function($properties = []) {
                return Project::getOptionProvider($properties)->getOptions();
            },
            'domain_id' => static function($properties = []) {
                return Domain::getOptionProvider($properties)->getOptions();
            },
            'language_id' => static function(...$params) {
                return Language::getOptions(...$params);
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'activated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deactivated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('status', false),
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'description' => [
                'type' => Textarea::class,
            ],
            'site_name' => [
                'type' => TextInput::class,
            ],
            'show_start_page_in_title' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'show_site_name_in_title' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'reverse_title' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'title_separator' => [
                'type' => TextInput::class,
            ],
            'start_page_contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('start_page_contentmeta_id', false),
                'relationClassName' => Contentmeta::class,
            ],
            'error_page_contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('error_page_contentmeta_id', false),
                'relationClassName' => Contentmeta::class,
            ],
            'login_page_contentmeta_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('login_page_contentmeta_id', false),
                'relationClassName' => Contentmeta::class,
            ],
            'default_layout_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('default_layout_id', false),
                'relationClassName' => Layout::class,
            ],
            'og_site_name' => [
                'type' => TextInput::class,
            ],
            'fb_app_id' => [
                'type' => TextInput::class,
            ],
            'path' => [
                'type' => TextInput::class,
            ],
            'project_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('project_id', false),
                'relationClassName' => Project::class,
            ],
            'domain_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('domain_id', false),
                'relationClassName' => Domain::class,
            ],
            'language_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('language_id', false),
                'relationClassName' => Language::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('created_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('updated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('activated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deactivated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deleted_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                'status' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                'name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'description' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'description',
                        'sort' => 4,
                    ],
                ],
                'site_name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'site_name',
                        'sort' => 5,
                    ],
                ],
                'show_start_page_in_title' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'show_start_page_in_title',
                        'sort' => 6,
                    ],
                ],
                'show_site_name_in_title' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'show_site_name_in_title',
                        'sort' => 7,
                    ],
                ],
                'reverse_title' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'reverse_title',
                        'sort' => 8,
                    ],
                ],
                'title_separator' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'title_separator',
                        'sort' => 9,
                    ],
                ],
                'start_page_contentmeta_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'start_page_contentmeta_id',
                        'sort' => 10,
                        'link' => true,
                    ],
                ],
                'error_page_contentmeta_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'error_page_contentmeta_id',
                        'sort' => 11,
                        'link' => true,
                    ],
                ],
                'login_page_contentmeta_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'login_page_contentmeta_id',
                        'sort' => 12,
                        'link' => true,
                    ],
                ],
                'default_layout_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'default_layout_id',
                        'sort' => 13,
                        'link' => true,
                    ],
                ],
                'og_site_name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'og_site_name',
                        'sort' => 14,
                    ],
                ],
                'fb_app_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'fb_app_id',
                        'sort' => 15,
                    ],
                ],
                'path' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'path',
                        'sort' => 16,
                    ],
                ],
                'project_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'project_id',
                        'sort' => 17,
                        'link' => true,
                    ],
                ],
                'domain_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'domain_id',
                        'sort' => 18,
                        'link' => true,
                    ],
                ],
                'language_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'language_id',
                        'sort' => 19,
                        'link' => true,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-description' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'description',
                                                    ],
                                                ],
                                                'field-site_name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'site_name',
                                                    ],
                                                ],
                                                'field-show_start_page_in_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'show_start_page_in_title',
                                                    ],
                                                ],
                                                'field-show_site_name_in_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'show_site_name_in_title',
                                                    ],
                                                ],
                                                'field-reverse_title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'reverse_title',
                                                    ],
                                                ],
                                                'field-title_separator' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title_separator',
                                                    ],
                                                ],
                                                'field-start_page_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'start_page_contentmeta_id',
                                                    ],
                                                ],
                                                'field-error_page_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'error_page_contentmeta_id',
                                                    ],
                                                ],
                                                'field-login_page_contentmeta_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'login_page_contentmeta_id',
                                                    ],
                                                ],
                                                'field-default_layout_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'default_layout_id',
                                                    ],
                                                ],
                                                'field-og_site_name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'og_site_name',
                                                    ],
                                                ],
                                                'field-fb_app_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'fb_app_id',
                                                    ],
                                                ],
                                                'field-path' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'path',
                                                    ],
                                                ],
                                                'field-project_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'project_id',
                                                    ],
                                                ],
                                                'field-domain_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'domain_id',
                                                    ],
                                                ],
                                                'field-language_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'language_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%projectlanguage}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-name' => ['name', 'required'],
            'required-path' => ['path', 'required'],
            'required-project_id' => ['project_id', 'required'],
            'required-domain_id' => ['domain_id', 'required'],
            'required-language_id' => ['language_id', 'required'],
            'string-description' => ['description', 'string'],
            'boolean-show_start_page_in_title' => ['show_start_page_in_title', 'boolean'],
            'boolean-show_site_name_in_title' => ['show_site_name_in_title', 'boolean'],
            'boolean-reverse_title' => ['reverse_title', 'boolean'],
            'integer-start_page_contentmeta_id' => ['start_page_contentmeta_id', 'integer'],
            'integer-error_page_contentmeta_id' => ['error_page_contentmeta_id', 'integer'],
            'integer-login_page_contentmeta_id' => ['login_page_contentmeta_id', 'integer'],
            'integer-default_layout_id' => ['default_layout_id', 'integer'],
            'integer-project_id' => ['project_id', 'integer'],
            'integer-domain_id' => ['domain_id', 'integer'],
            'integer-language_id' => ['language_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-name' => ['name', 'string', 'max' => 255],
            'string-site_name' => ['site_name', 'string', 'max' => 255],
            'string-title_separator' => ['title_separator', 'string', 'max' => 255],
            'string-og_site_name' => ['og_site_name', 'string', 'max' => 255],
            'string-fb_app_id' => ['fb_app_id', 'string', 'max' => 255],
            'string-path' => ['path', 'string', 'max' => 255],
            'exist-activated_by' => [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['activated_by' => 'id']],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deactivated_by' => [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deactivated_by' => 'id']],
            'exist-default_layout_id' => [['default_layout_id'], 'exist', 'skipOnError' => true, 'targetClass' => Layout::class, 'targetAttribute' => ['default_layout_id' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-domain_id' => [['domain_id'], 'exist', 'skipOnError' => true, 'targetClass' => Domain::class, 'targetAttribute' => ['domain_id' => 'id']],
            'exist-error_page_contentmeta_id' => [['error_page_contentmeta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contentmeta::class, 'targetAttribute' => ['error_page_contentmeta_id' => 'id']],
            'exist-language_id' => [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language_id' => 'id']],
            'exist-login_page_contentmeta_id' => [['login_page_contentmeta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contentmeta::class, 'targetAttribute' => ['login_page_contentmeta_id' => 'id']],
            'exist-project_id' => [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            'exist-start_page_contentmeta_id' => [['start_page_contentmeta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contentmeta::class, 'targetAttribute' => ['start_page_contentmeta_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-core', 'ID'),
            'status' => Yii::t('fafcms-core', 'Status'),
            'name' => Yii::t('fafcms-core', 'Name'),
            'description' => Yii::t('fafcms-core', 'Description'),
            'site_name' => Yii::t('fafcms-core', 'Site Name'),
            'show_start_page_in_title' => Yii::t('fafcms-core', 'Show Start Page In Title'),
            'show_site_name_in_title' => Yii::t('fafcms-core', 'Show Site Name In Title'),
            'reverse_title' => Yii::t('fafcms-core', 'Reverse Title'),
            'title_separator' => Yii::t('fafcms-core', 'Title Separator'),
            'start_page_contentmeta_id' => Yii::t('fafcms-core', 'Start Page Contentmeta ID'),
            'error_page_contentmeta_id' => Yii::t('fafcms-core', 'Error Page Contentmeta ID'),
            'login_page_contentmeta_id' => Yii::t('fafcms-core', 'Login Page Contentmeta ID'),
            'default_layout_id' => Yii::t('fafcms-core', 'Default Layout ID'),
            'og_site_name' => Yii::t('fafcms-core', 'Og Site Name'),
            'fb_app_id' => Yii::t('fafcms-core', 'Fb App ID'),
            'path' => Yii::t('fafcms-core', 'Path'),
            'project_id' => Yii::t('fafcms-core', 'Project ID'),
            'domain_id' => Yii::t('fafcms-core', 'Domain ID'),
            'language_id' => Yii::t('fafcms-core', 'Language ID'),
            'created_by' => Yii::t('fafcms-core', 'Created By'),
            'updated_by' => Yii::t('fafcms-core', 'Updated By'),
            'activated_by' => Yii::t('fafcms-core', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-core', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-core', 'Deleted By'),
            'created_at' => Yii::t('fafcms-core', 'Created At'),
            'updated_at' => Yii::t('fafcms-core', 'Updated At'),
            'activated_at' => Yii::t('fafcms-core', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-core', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-core', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[ActivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getActivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'activated_by',
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[DeactivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeactivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deactivated_by',
        ]);
    }

    /**
     * Gets query for [[DefaultLayout]].
     *
     * @return ActiveQuery
     */
    public function getDefaultLayout(): ActiveQuery
    {
        return $this->hasOne(Layout::class, [
            'id' => 'default_layout_id',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[Domain]].
     *
     * @return ActiveQuery
     */
    public function getDomain(): ActiveQuery
    {
        return $this->hasOne(Domain::class, [
            'id' => 'domain_id',
        ]);
    }

    /**
     * Gets query for [[ErrorPageContentmeta]].
     *
     * @return ActiveQuery
     */
    public function getErrorPageContentmeta(): ActiveQuery
    {
        return $this->hasOne(Contentmeta::class, [
            'id' => 'error_page_contentmeta_id',
        ]);
    }

    /**
     * Gets query for [[Language]].
     *
     * @return ActiveQuery
     */
    public function getLanguage(): ActiveQuery
    {
        return $this->hasOne(Language::class, [
            'id' => 'language_id',
        ]);
    }

    /**
     * Gets query for [[LoginPageContentmeta]].
     *
     * @return ActiveQuery
     */
    public function getLoginPageContentmeta(): ActiveQuery
    {
        return $this->hasOne(Contentmeta::class, [
            'id' => 'login_page_contentmeta_id',
        ]);
    }

    /**
     * Gets query for [[PrimaryProjectlanguageProjects]].
     *
     * @return ActiveQuery
     */
    public function getPrimaryProjectlanguageProjects(): ActiveQuery
    {
        return $this->hasMany(Project::class, [
            'primary_projectlanguage_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[Project]].
     *
     * @return ActiveQuery
     */
    public function getProject(): ActiveQuery
    {
        return $this->hasOne(Project::class, [
            'id' => 'project_id',
        ]);
    }

    /**
     * Gets query for [[ProjectlanguageContentmetas]].
     *
     * @return ActiveQuery
     */
    public function getProjectlanguageContentmetas(): ActiveQuery
    {
        return $this->hasMany(Contentmeta::class, [
            'projectlanguage_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ProjectlanguageDomains]].
     *
     * @return ActiveQuery
     */
    public function getProjectlanguageDomains(): ActiveQuery
    {
        return $this->hasMany(Domain::class, [
            'projectlanguage_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ProjectlanguageLayouts]].
     *
     * @return ActiveQuery
     */
    public function getProjectlanguageLayouts(): ActiveQuery
    {
        return $this->hasMany(Layout::class, [
            'projectlanguage_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ProjectlanguageMenuitems]].
     *
     * @return ActiveQuery
     */
    public function getProjectlanguageMenuitems(): ActiveQuery
    {
        return $this->hasMany(Menuitem::class, [
            'projectlanguage_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ProjectlanguageMenus]].
     *
     * @return ActiveQuery
     */
    public function getProjectlanguageMenus(): ActiveQuery
    {
        return $this->hasMany(Menu::class, [
            'projectlanguage_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ProjectlanguageSites]].
     *
     * @return ActiveQuery
     */
    public function getProjectlanguageSites(): ActiveQuery
    {
        return $this->hasMany(Site::class, [
            'projectlanguage_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ProjectlanguageSnippets]].
     *
     * @return ActiveQuery
     */
    public function getProjectlanguageSnippets(): ActiveQuery
    {
        return $this->hasMany(Snippet::class, [
            'projectlanguage_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ProjectlanguageTopics]].
     *
     * @return ActiveQuery
     */
    public function getProjectlanguageTopics(): ActiveQuery
    {
        return $this->hasMany(Topic::class, [
            'projectlanguage_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[StartPageContentmeta]].
     *
     * @return ActiveQuery
     */
    public function getStartPageContentmeta(): ActiveQuery
    {
        return $this->hasOne(Contentmeta::class, [
            'id' => 'start_page_contentmeta_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
