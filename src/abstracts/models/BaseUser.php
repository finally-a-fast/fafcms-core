<?php

namespace fafcms\fafcms\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\EmailInput,
    inputs\NumberInput,
    inputs\PhoneInput,
    inputs\ExtendedDropDownList,
    inputs\SwitchCheckbox,
    inputs\TextInput,
    inputs\UrlInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\Country,
    models\Language,
    models\Project,
};
use fafcms\helpers\{
    ActiveRecord,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionTrait,
};
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * This is the abstract model class for table "{{%user}}".
 *
 * @package fafcms\fafcms\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property string $email
 * @property string|null $auth_key
 * @property string|null $activation_token
 * @property string $password_hash
 * @property string|null $reset_token
 * @property int|null $wrong_password_count
 * @property int|null $wrong_password_blocked_count
 * @property string|null $wrong_password_blocked_until
 * @property string|null $title
 * @property string|null $sex
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string|null $company
 * @property string|null $position
 * @property string|null $department
 * @property string|null $address
 * @property string|null $zip
 * @property string|null $city
 * @property int|null $country_id
 * @property int|null $language_id
 * @property string|null $phone
 * @property string|null $mobile
 * @property string|null $fax
 * @property string|null $website
 * @property string $notifications_email
 * @property string $notificaitons_browser
 * @property int $animation_auto
 * @property int $animation_focus
 * @property int $animation_action
 * @property int $animation_system
 * @property int $video_animated_preview
 * @property int $video_muted
 * @property int $video_default_resolution
 * @property int $video_default_resolution_mobile
 * @property int $video_autoplay
 * @property int $video_autoplay_mobile
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 * @property string|null $last_change_type
 * @property int|null $last_change_id
 * @property string|null $last_change_at
 * @property string|null $last_logout_at
 * @property string|null $last_login_at
 *
 * @property User $activatedBy
 * @property Project[] $activatedProjects
 * @property User[] $activatedUsers
 * @property Country $country
 * @property User $createdBy
 * @property User[] $createdUsers
 * @property User $deactivatedBy
 * @property User[] $deactivatedUsers
 * @property User $deletedBy
 * @property User[] $deletedUsers
 * @property Language $language
 * @property User $updatedBy
 * @property User[] $updatedUsers
 */
abstract class BaseUser extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'user';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'user';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-core', 'Users');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-core', 'User');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['firstname'] ?? '') . ' ' . ($model['lastname'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.firstname', static::tableName() . '.lastname'
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.firstname' => SORT_ASC, static::tableName() . '.lastname' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
            'country_id' => static function(...$params) {
                return Country::getOptions(...$params);
            },
            'language_id' => static function(...$params) {
                return Language::getOptions(...$params);
            },
            'created_by' => static function(...$params) {
                return static::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return static::getOptions(...$params);
            },
            'activated_by' => static function(...$params) {
                return static::getOptions(...$params);
            },
            'deactivated_by' => static function(...$params) {
                return static::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return static::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->attributeOptions()['status'],
            ],
            'email' => [
                'type' => EmailInput::class,
            ],
            'auth_key' => [
                'type' => TextInput::class,
            ],
            'activation_token' => [
                'type' => TextInput::class,
            ],
            'password_hash' => [
                'type' => TextInput::class,
            ],
            'reset_token' => [
                'type' => TextInput::class,
            ],
            'wrong_password_count' => [
                'type' => NumberInput::class,
            ],
            'wrong_password_blocked_count' => [
                'type' => NumberInput::class,
            ],
            'wrong_password_blocked_until' => [
                'type' => DateTimePicker::class,
            ],
            'title' => [
                'type' => TextInput::class,
            ],
            'sex' => [
                'type' => TextInput::class,
            ],
            'firstname' => [
                'type' => TextInput::class,
            ],
            'lastname' => [
                'type' => TextInput::class,
            ],
            'company' => [
                'type' => TextInput::class,
            ],
            'position' => [
                'type' => TextInput::class,
            ],
            'department' => [
                'type' => TextInput::class,
            ],
            'address' => [
                'type' => TextInput::class,
            ],
            'zip' => [
                'type' => TextInput::class,
            ],
            'city' => [
                'type' => TextInput::class,
            ],
            'country_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['country_id'],
                'relationClassName' => Country::class,
            ],
            'language_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['language_id'],
                'relationClassName' => Language::class,
            ],
            'phone' => [
                'type' => PhoneInput::class,
            ],
            'mobile' => [
                'type' => PhoneInput::class,
            ],
            'fax' => [
                'type' => PhoneInput::class,
            ],
            'website' => [
                'type' => UrlInput::class,
            ],
            'notifications_email' => [
                'type' => TextInput::class,
            ],
            'notificaitons_browser' => [
                'type' => TextInput::class,
            ],
            'animation_auto' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'animation_focus' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'animation_action' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'animation_system' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'video_animated_preview' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'video_muted' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'video_default_resolution' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'video_default_resolution_mobile' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'video_autoplay' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'video_autoplay_mobile' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['created_by'],
                'relationClassName' => static::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['updated_by'],
                'relationClassName' => static::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['activated_by'],
                'relationClassName' => static::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['deactivated_by'],
                'relationClassName' => static::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['deleted_by'],
                'relationClassName' => static::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'last_change_type' => [
                'type' => TextInput::class,
            ],
            'last_change_id' => [
                'type' => NumberInput::class,
            ],
            'last_change_at' => [
                'type' => DateTimePicker::class,
            ],
            'last_logout_at' => [
                'type' => DateTimePicker::class,
            ],
            'last_login_at' => [
                'type' => DateTimePicker::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'firstname',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'lastname',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'email',
                        'sort' => 5,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'auth_key',
                        'sort' => 6,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'activation_token',
                        'sort' => 7,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'password_hash',
                        'sort' => 8,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'reset_token',
                        'sort' => 9,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'wrong_password_count',
                        'sort' => 10,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'wrong_password_blocked_count',
                        'sort' => 11,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'wrong_password_blocked_until',
                        'sort' => 12,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'title',
                        'sort' => 13,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sex',
                        'sort' => 14,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'company',
                        'sort' => 15,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'position',
                        'sort' => 16,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'department',
                        'sort' => 17,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'address',
                        'sort' => 18,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'zip',
                        'sort' => 19,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'city',
                        'sort' => 20,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'country_id',
                        'sort' => 21,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'language_id',
                        'sort' => 22,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'phone',
                        'sort' => 23,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'mobile',
                        'sort' => 24,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'fax',
                        'sort' => 25,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'website',
                        'sort' => 26,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'notifications_email',
                        'sort' => 27,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'notificaitons_browser',
                        'sort' => 28,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'animation_auto',
                        'sort' => 29,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'animation_focus',
                        'sort' => 30,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'animation_action',
                        'sort' => 31,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'animation_system',
                        'sort' => 32,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'video_animated_preview',
                        'sort' => 33,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'video_muted',
                        'sort' => 34,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'video_default_resolution',
                        'sort' => 35,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'video_default_resolution_mobile',
                        'sort' => 36,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'video_autoplay',
                        'sort' => 37,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'video_autoplay_mobile',
                        'sort' => 38,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_change_type',
                        'sort' => 39,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_change_id',
                        'sort' => 40,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_change_at',
                        'sort' => 41,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_logout_at',
                        'sort' => 42,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_login_at',
                        'sort' => 43,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-email' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'email',
                                                    ],
                                                ],
                                                'field-auth_key' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'auth_key',
                                                    ],
                                                ],
                                                'field-activation_token' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'activation_token',
                                                    ],
                                                ],
                                                'field-password_hash' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'password_hash',
                                                    ],
                                                ],
                                                'field-reset_token' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'reset_token',
                                                    ],
                                                ],
                                                'field-wrong_password_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'wrong_password_count',
                                                    ],
                                                ],
                                                'field-wrong_password_blocked_count' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'wrong_password_blocked_count',
                                                    ],
                                                ],
                                                'field-wrong_password_blocked_until' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'wrong_password_blocked_until',
                                                    ],
                                                ],
                                                'field-title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title',
                                                    ],
                                                ],
                                                'field-sex' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sex',
                                                    ],
                                                ],
                                                'field-firstname' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'firstname',
                                                    ],
                                                ],
                                                'field-lastname' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'lastname',
                                                    ],
                                                ],
                                                'field-company' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'company',
                                                    ],
                                                ],
                                                'field-position' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'position',
                                                    ],
                                                ],
                                                'field-department' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'department',
                                                    ],
                                                ],
                                                'field-address' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'address',
                                                    ],
                                                ],
                                                'field-zip' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'zip',
                                                    ],
                                                ],
                                                'field-city' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'city',
                                                    ],
                                                ],
                                                'field-country_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'country_id',
                                                    ],
                                                ],
                                                'field-language_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'language_id',
                                                    ],
                                                ],
                                                'field-phone' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'phone',
                                                    ],
                                                ],
                                                'field-mobile' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'mobile',
                                                    ],
                                                ],
                                                'field-fax' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'fax',
                                                    ],
                                                ],
                                                'field-website' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'website',
                                                    ],
                                                ],
                                                'field-notifications_email' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'notifications_email',
                                                    ],
                                                ],
                                                'field-notificaitons_browser' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'notificaitons_browser',
                                                    ],
                                                ],
                                                'field-animation_auto' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'animation_auto',
                                                    ],
                                                ],
                                                'field-animation_focus' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'animation_focus',
                                                    ],
                                                ],
                                                'field-animation_action' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'animation_action',
                                                    ],
                                                ],
                                                'field-animation_system' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'animation_system',
                                                    ],
                                                ],
                                                'field-video_animated_preview' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_animated_preview',
                                                    ],
                                                ],
                                                'field-video_muted' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_muted',
                                                    ],
                                                ],
                                                'field-video_default_resolution' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_default_resolution',
                                                    ],
                                                ],
                                                'field-video_default_resolution_mobile' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_default_resolution_mobile',
                                                    ],
                                                ],
                                                'field-video_autoplay' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_autoplay',
                                                    ],
                                                ],
                                                'field-video_autoplay_mobile' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_autoplay_mobile',
                                                    ],
                                                ],
                                                'field-last_change_type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_change_type',
                                                    ],
                                                ],
                                                'field-last_change_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_change_id',
                                                    ],
                                                ],
                                                'field-last_change_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_change_at',
                                                    ],
                                                ],
                                                'field-last_logout_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_logout_at',
                                                    ],
                                                ],
                                                'field-last_login_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_login_at',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-email' => ['email', 'required'],
            'required-password_hash' => ['password_hash', 'required'],
            'integer-wrong_password_count' => ['wrong_password_count', 'integer'],
            'integer-wrong_password_blocked_count' => ['wrong_password_blocked_count', 'integer'],
            'integer-country_id' => ['country_id', 'integer'],
            'integer-language_id' => ['language_id', 'integer'],
            'integer-animation_auto' => ['animation_auto', 'integer'],
            'integer-animation_focus' => ['animation_focus', 'integer'],
            'integer-animation_action' => ['animation_action', 'integer'],
            'integer-animation_system' => ['animation_system', 'integer'],
            'integer-video_animated_preview' => ['video_animated_preview', 'integer'],
            'integer-video_muted' => ['video_muted', 'integer'],
            'integer-video_default_resolution' => ['video_default_resolution', 'integer'],
            'integer-video_default_resolution_mobile' => ['video_default_resolution_mobile', 'integer'],
            'integer-video_autoplay' => ['video_autoplay', 'integer'],
            'integer-video_autoplay_mobile' => ['video_autoplay_mobile', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'integer-last_change_id' => ['last_change_id', 'integer'],
            'safe-wrong_password_blocked_until' => ['wrong_password_blocked_until', 'safe'],
            'safe-created_at' => ['created_at', 'safe'],
            'safe-updated_at' => ['updated_at', 'safe'],
            'safe-activated_at' => ['activated_at', 'safe'],
            'safe-deactivated_at' => ['deactivated_at', 'safe'],
            'safe-deleted_at' => ['deleted_at', 'safe'],
            'safe-last_change_at' => ['last_change_at', 'safe'],
            'safe-last_logout_at' => ['last_logout_at', 'safe'],
            'safe-last_login_at' => ['last_login_at', 'safe'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-email' => ['email', 'string', 'max' => 255],
            'string-auth_key' => ['auth_key', 'string', 'max' => 255],
            'string-activation_token' => ['activation_token', 'string', 'max' => 255],
            'string-password_hash' => ['password_hash', 'string', 'max' => 255],
            'string-reset_token' => ['reset_token', 'string', 'max' => 255],
            'string-title' => ['title', 'string', 'max' => 255],
            'string-sex' => ['sex', 'string', 'max' => 255],
            'string-firstname' => ['firstname', 'string', 'max' => 255],
            'string-lastname' => ['lastname', 'string', 'max' => 255],
            'string-company' => ['company', 'string', 'max' => 255],
            'string-position' => ['position', 'string', 'max' => 255],
            'string-department' => ['department', 'string', 'max' => 255],
            'string-address' => ['address', 'string', 'max' => 255],
            'string-zip' => ['zip', 'string', 'max' => 255],
            'string-city' => ['city', 'string', 'max' => 255],
            'string-phone' => ['phone', 'string', 'max' => 255],
            'string-mobile' => ['mobile', 'string', 'max' => 255],
            'string-fax' => ['fax', 'string', 'max' => 255],
            'string-website' => ['website', 'string', 'max' => 255],
            'string-notifications_email' => ['notifications_email', 'string', 'max' => 255],
            'string-notificaitons_browser' => ['notificaitons_browser', 'string', 'max' => 255],
            'string-last_change_type' => ['last_change_type', 'string', 'max' => 255],
            'exist-activated_by' => [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['activated_by' => 'id']],
            'exist-country_id' => [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deactivated_by' => [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deactivated_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-language_id' => [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-core', 'ID'),
            'status' => Yii::t('fafcms-core', 'Status'),
            'email' => Yii::t('fafcms-core', 'Email'),
            'auth_key' => Yii::t('fafcms-core', 'Auth Key'),
            'activation_token' => Yii::t('fafcms-core', 'Activation Token'),
            'password_hash' => Yii::t('fafcms-core', 'Password Hash'),
            'reset_token' => Yii::t('fafcms-core', 'Reset Token'),
            'wrong_password_count' => Yii::t('fafcms-core', 'Wrong Password Count'),
            'wrong_password_blocked_count' => Yii::t('fafcms-core', 'Wrong Password Blocked Count'),
            'wrong_password_blocked_until' => Yii::t('fafcms-core', 'Wrong Password Blocked Until'),
            'title' => Yii::t('fafcms-core', 'Title'),
            'sex' => Yii::t('fafcms-core', 'Sex'),
            'firstname' => Yii::t('fafcms-core', 'Firstname'),
            'lastname' => Yii::t('fafcms-core', 'Lastname'),
            'company' => Yii::t('fafcms-core', 'Company'),
            'position' => Yii::t('fafcms-core', 'Position'),
            'department' => Yii::t('fafcms-core', 'Department'),
            'address' => Yii::t('fafcms-core', 'Address'),
            'zip' => Yii::t('fafcms-core', 'Zip'),
            'city' => Yii::t('fafcms-core', 'City'),
            'country_id' => Yii::t('fafcms-core', 'Country ID'),
            'language_id' => Yii::t('fafcms-core', 'Language ID'),
            'phone' => Yii::t('fafcms-core', 'Phone'),
            'mobile' => Yii::t('fafcms-core', 'Mobile'),
            'fax' => Yii::t('fafcms-core', 'Fax'),
            'website' => Yii::t('fafcms-core', 'Website'),
            'notifications_email' => Yii::t('fafcms-core', 'Notifications Email'),
            'notificaitons_browser' => Yii::t('fafcms-core', 'Notificaitons Browser'),
            'animation_auto' => Yii::t('fafcms-core', 'Animation Auto'),
            'animation_focus' => Yii::t('fafcms-core', 'Animation Focus'),
            'animation_action' => Yii::t('fafcms-core', 'Animation Action'),
            'animation_system' => Yii::t('fafcms-core', 'Animation System'),
            'video_animated_preview' => Yii::t('fafcms-core', 'Video Animated Preview'),
            'video_muted' => Yii::t('fafcms-core', 'Video Muted'),
            'video_default_resolution' => Yii::t('fafcms-core', 'Video Default Resolution'),
            'video_default_resolution_mobile' => Yii::t('fafcms-core', 'Video Default Resolution Mobile'),
            'video_autoplay' => Yii::t('fafcms-core', 'Video Autoplay'),
            'video_autoplay_mobile' => Yii::t('fafcms-core', 'Video Autoplay Mobile'),
            'created_by' => Yii::t('fafcms-core', 'Created By'),
            'updated_by' => Yii::t('fafcms-core', 'Updated By'),
            'activated_by' => Yii::t('fafcms-core', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-core', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-core', 'Deleted By'),
            'created_at' => Yii::t('fafcms-core', 'Created At'),
            'updated_at' => Yii::t('fafcms-core', 'Updated At'),
            'activated_at' => Yii::t('fafcms-core', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-core', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-core', 'Deleted At'),
            'last_change_type' => Yii::t('fafcms-core', 'Last Change Type'),
            'last_change_id' => Yii::t('fafcms-core', 'Last Change ID'),
            'last_change_at' => Yii::t('fafcms-core', 'Last Change At'),
            'last_logout_at' => Yii::t('fafcms-core', 'Last Logout At'),
            'last_login_at' => Yii::t('fafcms-core', 'Last Login At'),
        ]);
    }

    /**
     * Gets query for [[ActivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getActivatedBy(): ActiveQuery
    {
        return $this->hasOne(static::class, [
            'id' => 'activated_by',
        ]);
    }

    /**
     * Gets query for [[ActivatedProjects]].
     *
     * @return ActiveQuery
     */
    public function getActivatedProjects(): ActiveQuery
    {
        return $this->hasMany(Project::class, [
            'activated_by' => 'id',
        ]);
    }

    /**
     * Gets query for [[ActivatedUsers]].
     *
     * @return ActiveQuery
     */
    public function getActivatedUsers(): ActiveQuery
    {
        return $this->hasMany(static::class, [
            'activated_by' => 'id',
        ]);
    }

    /**
     * Gets query for [[Country]].
     *
     * @return ActiveQuery
     */
    public function getCountry(): ActiveQuery
    {
        return $this->hasOne(Country::class, [
            'id' => 'country_id',
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(static::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[CreatedUsers]].
     *
     * @return ActiveQuery
     */
    public function getCreatedUsers(): ActiveQuery
    {
        return $this->hasMany(static::class, [
            'created_by' => 'id',
        ]);
    }

    /**
     * Gets query for [[DeactivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeactivatedBy(): ActiveQuery
    {
        return $this->hasOne(static::class, [
            'id' => 'deactivated_by',
        ]);
    }

    /**
     * Gets query for [[DeactivatedUsers]].
     *
     * @return ActiveQuery
     */
    public function getDeactivatedUsers(): ActiveQuery
    {
        return $this->hasMany(static::class, [
            'deactivated_by' => 'id',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(static::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[DeletedUsers]].
     *
     * @return ActiveQuery
     */
    public function getDeletedUsers(): ActiveQuery
    {
        return $this->hasMany(static::class, [
            'deleted_by' => 'id',
        ]);
    }

    /**
     * Gets query for [[Language]].
     *
     * @return ActiveQuery
     */
    public function getLanguage(): ActiveQuery
    {
        return $this->hasOne(Language::class, [
            'id' => 'language_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(static::class, [
            'id' => 'updated_by',
        ]);
    }

    /**
     * Gets query for [[UpdatedUsers]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedUsers(): ActiveQuery
    {
        return $this->hasMany(static::class, [
            'updated_by' => 'id',
        ]);
    }
}
