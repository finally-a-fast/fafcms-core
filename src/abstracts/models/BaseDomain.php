<?php

namespace fafcms\fafcms\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\Customfield,
    models\Project,
    models\Projectlanguage,
    models\User,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%domain}}".
 *
 * @package fafcms\fafcms\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property string $domain
 * @property int|null $project_id
 * @property int|null $projectlanguage_id
 * @property int|null $is_wildcard
 * @property int|null $is_validated
 * @property string|null $last_validation_at
 * @property int|null $is_https_validated
 * @property string|null $last_https_validation_at
 * @property int|null $force_https
 * @property int|null $is_redirect
 * @property int|null $redirect_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property User $activatedBy
 * @property User $createdBy
 * @property Customfield[] $customtableCustomfields
 * @property User $deactivatedBy
 * @property User $deletedBy
 * @property Projectlanguage[] $domainProjectlanguages
 * @property Project $project
 * @property Projectlanguage $projectlanguage
 * @property User $updatedBy
 */
abstract class BaseDomain extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'domain';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'domain';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-core', 'Domains');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-core', 'Domain');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
            'project_id' => static function($properties = []) {
                return Project::getOptionProvider($properties)->getOptions();
            },
            'projectlanguage_id' => static function($properties = []) {
                return Projectlanguage::getOptionProvider($properties)->getOptions();
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'activated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deactivated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('status', false),
            ],
            'domain' => [
                'type' => TextInput::class,
            ],
            'project_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('project_id', false),
                'relationClassName' => Project::class,
            ],
            'projectlanguage_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('projectlanguage_id', false),
                'relationClassName' => Projectlanguage::class,
            ],
            'is_wildcard' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'is_validated' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'last_validation_at' => [
                'type' => DateTimePicker::class,
            ],
            'is_https_validated' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'last_https_validation_at' => [
                'type' => DateTimePicker::class,
            ],
            'force_https' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'is_redirect' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'redirect_id' => [
                'type' => NumberInput::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('created_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('updated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('activated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deactivated_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('deleted_by', false),
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'status' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                'domain' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'domain',
                        'sort' => 3,
                    ],
                ],
                'project_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'project_id',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                'projectlanguage_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'projectlanguage_id',
                        'sort' => 5,
                        'link' => true,
                    ],
                ],
                'is_wildcard' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'is_wildcard',
                        'sort' => 6,
                    ],
                ],
                'is_validated' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'is_validated',
                        'sort' => 7,
                    ],
                ],
                'last_validation_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_validation_at',
                        'sort' => 8,
                    ],
                ],
                'is_https_validated' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'is_https_validated',
                        'sort' => 9,
                    ],
                ],
                'last_https_validation_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'last_https_validation_at',
                        'sort' => 10,
                    ],
                ],
                'force_https' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'force_https',
                        'sort' => 11,
                    ],
                ],
                'is_redirect' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'is_redirect',
                        'sort' => 12,
                    ],
                ],
                'redirect_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'redirect_id',
                        'sort' => 13,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-domain' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'domain',
                                                    ],
                                                ],
                                                'field-project_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'project_id',
                                                    ],
                                                ],
                                                'field-projectlanguage_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'projectlanguage_id',
                                                    ],
                                                ],
                                                'field-is_wildcard' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_wildcard',
                                                    ],
                                                ],
                                                'field-is_validated' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_validated',
                                                    ],
                                                ],
                                                'field-last_validation_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_validation_at',
                                                    ],
                                                ],
                                                'field-is_https_validated' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_https_validated',
                                                    ],
                                                ],
                                                'field-last_https_validation_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'last_https_validation_at',
                                                    ],
                                                ],
                                                'field-force_https' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'force_https',
                                                    ],
                                                ],
                                                'field-is_redirect' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_redirect',
                                                    ],
                                                ],
                                                'field-redirect_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'redirect_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%domain}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-domain' => ['domain', 'required'],
            'integer-project_id' => ['project_id', 'integer'],
            'integer-projectlanguage_id' => ['projectlanguage_id', 'integer'],
            'integer-redirect_id' => ['redirect_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'boolean-is_wildcard' => ['is_wildcard', 'boolean'],
            'boolean-is_validated' => ['is_validated', 'boolean'],
            'boolean-is_https_validated' => ['is_https_validated', 'boolean'],
            'boolean-force_https' => ['force_https', 'boolean'],
            'boolean-is_redirect' => ['is_redirect', 'boolean'],
            'date-last_validation_at' => ['last_validation_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'last_validation_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-last_https_validation_at' => ['last_https_validation_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'last_https_validation_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-domain' => ['domain', 'string', 'max' => 255],
            'exist-activated_by' => [['activated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['activated_by' => 'id']],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deactivated_by' => [['deactivated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deactivated_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-project_id' => [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            'exist-projectlanguage_id' => [['projectlanguage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projectlanguage::class, 'targetAttribute' => ['projectlanguage_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-core', 'ID'),
            'status' => Yii::t('fafcms-core', 'Status'),
            'domain' => Yii::t('fafcms-core', 'Domain'),
            'project_id' => Yii::t('fafcms-core', 'Project ID'),
            'projectlanguage_id' => Yii::t('fafcms-core', 'Projectlanguage ID'),
            'is_wildcard' => Yii::t('fafcms-core', 'Is Wildcard'),
            'is_validated' => Yii::t('fafcms-core', 'Is Validated'),
            'last_validation_at' => Yii::t('fafcms-core', 'Last Validation At'),
            'is_https_validated' => Yii::t('fafcms-core', 'Is Https Validated'),
            'last_https_validation_at' => Yii::t('fafcms-core', 'Last Https Validation At'),
            'force_https' => Yii::t('fafcms-core', 'Force Https'),
            'is_redirect' => Yii::t('fafcms-core', 'Is Redirect'),
            'redirect_id' => Yii::t('fafcms-core', 'Redirect ID'),
            'created_by' => Yii::t('fafcms-core', 'Created By'),
            'updated_by' => Yii::t('fafcms-core', 'Updated By'),
            'activated_by' => Yii::t('fafcms-core', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-core', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-core', 'Deleted By'),
            'created_at' => Yii::t('fafcms-core', 'Created At'),
            'updated_at' => Yii::t('fafcms-core', 'Updated At'),
            'activated_at' => Yii::t('fafcms-core', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-core', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-core', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[ActivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getActivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'activated_by',
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[CustomtableCustomfields]].
     *
     * @return ActiveQuery
     */
    public function getCustomtableCustomfields(): ActiveQuery
    {
        return $this->hasMany(Customfield::class, [
            'customtable_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[DeactivatedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeactivatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deactivated_by',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[DomainProjectlanguages]].
     *
     * @return ActiveQuery
     */
    public function getDomainProjectlanguages(): ActiveQuery
    {
        return $this->hasMany(Projectlanguage::class, [
            'domain_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[Project]].
     *
     * @return ActiveQuery
     */
    public function getProject(): ActiveQuery
    {
        return $this->hasOne(Project::class, [
            'id' => 'project_id',
        ]);
    }

    /**
     * Gets query for [[Projectlanguage]].
     *
     * @return ActiveQuery
     */
    public function getProjectlanguage(): ActiveQuery
    {
        return $this->hasOne(Projectlanguage::class, [
            'id' => 'projectlanguage_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
