<?php

namespace fafcms\fafcms\abstracts\models;

use fafcms\fafcms\{
    inputs\NumberInput,
    inputs\Textarea,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use Yii;

/**
 * This is the abstract model class for table "{{%translation}}".
 *
 * @package fafcms\fafcms\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $language
 * @property string $category
 * @property string $message
 * @property string|null $translation
 */
abstract class BaseTranslation extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'translation';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'translation';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-core', 'Translations');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-core', 'Translation');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'language' => [
                'type' => TextInput::class,
            ],
            'category' => [
                'type' => TextInput::class,
            ],
            'message' => [
                'type' => Textarea::class,
            ],
            'translation' => [
                'type' => Textarea::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'language' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'language',
                        'sort' => 2,
                    ],
                ],
                'category' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'category',
                        'sort' => 3,
                    ],
                ],
                'message' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'message',
                        'sort' => 4,
                    ],
                ],
                'translation' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'translation',
                        'sort' => 5,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 6,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-language' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'language',
                                                    ],
                                                ],
                                                'field-category' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'category',
                                                    ],
                                                ],
                                                'field-message' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'message',
                                                    ],
                                                ],
                                                'field-translation' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'translation',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%translation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-language' => ['language', 'required'],
            'required-category' => ['category', 'required'],
            'required-message' => ['message', 'required'],
            'string-message' => ['message', 'string'],
            'string-translation' => ['translation', 'string'],
            'string-language' => ['language', 'string', 'max' => 255],
            'string-category' => ['category', 'string', 'max' => 255],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-core', 'ID'),
            'language' => Yii::t('fafcms-core', 'Language'),
            'category' => Yii::t('fafcms-core', 'Category'),
            'message' => Yii::t('fafcms-core', 'Message'),
            'translation' => Yii::t('fafcms-core', 'Translation'),
        ]);
    }
}
