<?php
use yii\helpers\Html;
use fafcms\fafcms\widgets\ActiveForm;
use common\widgets\daterangepicker\DateRangePicker;
use yii\helpers\ArrayHelper;
use fafcms\fafcms\models\Usergroup;

$datetimeFormat = \yii\helpers\FormatConverter::convertDateIcuToPhp(Yii::$app->formatter->datetimeFormat, 'datetime');
?>
<?php $form = ActiveForm::begin(['id' => 'previewmode-form', 'options' => ['action' => '']]) ?>
    <div id="previewModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span aria-hidden="true">&times;</span></button>
                    <h2>Seitenvorschau</h2>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <?= Html::hiddenInput('preview_change', true); ?>
                        <?= Html::label('Vorschau für den', 'preview_show_date', ['class' => 'control-label']); ?>
                        <?= DateRangePicker::widget([
                            'id' => 'preview_show_date',
                            'name' => 'preview_show_date',
                            'value' => Yii::$app->session->get('preview_show_date'),
                            'convertFormat' => true,
                            'language' => 'de',
                            'pluginOptions' => [
                                'locale' => ['format' => $datetimeFormat],
                                'showDropdowns' => 'true',
                                'timePicker' => true,
                                'timePickerIncrement' => 5,
                                'timePicker24Hour' => true,
                                'singleDatePicker' => true,
                                'drops' => 'down'
                            ],
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => 'Bitte Zeitpunkt auswählen',
                            ],
                        ]); ?>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="preview_show_inactive">
                                <?= Html::checkbox('preview_show_inactive', Yii::$app->session->get('preview_show_inactive'), ['id' => 'preview_show_inactive']);?>
                                <span>Inaktive Einträge zeigen</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="preview_guest">
                                <?= Html::checkbox('preview_guest', Yii::$app->session->get('preview_guest'), ['id' => 'preview_guest']);?>
                                <span>Gastmodus</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group hide-when-guest">
                        <?= Html::label('Benutzergruppe', 'usergroup_id', ['class' => 'control-label']); ?>
                        <?= DROPDOWN::widget([
                            'name' => 'preview_usergroup_id',
                            'value' => Yii::$app->user->identity->usergroup_id??null,
                            'data' => ArrayHelper::map(Usergroup::find()->select(['id', 'name'])->orderBy(Usergroup::$defaultOrder)->asArray()->all(), 'id', 'name'),
                            'options' => [
                                'id' => 'usergroup_id',
                                'placeholder' => 'Benutzergruppe',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);?>
                    </div>
                    <div class="form-group hide-when-guest">
                        <div class="checkbox">
                            <label for="usergroup_confirmation_status">
                                <?= Html::checkbox('preview_usergroup_confirmation_status', Yii::$app->user->identity->usergroup_confirmation_status === 'approved', ['id' => 'usergroup_confirmation_status']);?>
                                <span>VIP</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <?= Html::submitButton('Übernehmen', ['class'=>'btn btn-primary'])?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end() ?>
