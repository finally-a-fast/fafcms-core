<?php

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\widgets\ActiveForm;
use yii\helpers\Html;

$isWidget = $isWidget ?? false;

if (!isset($buttons)) {
    $buttons = [];
}

$formOptions = array_merge(['id' => strtolower($model->formName()) . '-form'], $formOptions ?? []);

echo Html::beginTag('div', ['id' => $formOptions['id'] . '-wrapper']);

$form = ActiveForm::begin($formOptions);

if ($buttons instanceof Closure) {
    $buttons = $buttons($model, $form, $this);
}

echo $content($model, $form, $this);

if (!$isWidget) {
    Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_ACTION_BAR, $buttons);
}

if ($isWidget && ($buttonCard ?? true)) {
    $buttonCardOptions = array_merge(['buttons' => $buttons], $buttonCardOptions ?? []);

    echo $this->render('rows', [
        'rows' => [[[
            'options' => ['class' => 'col s12'],
            'content' => $this->render('card', $buttonCardOptions),
        ]]],
    ]);
}

ActiveForm::end();

echo Html::endTag('div');
