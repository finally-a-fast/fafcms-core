<?php

use yii\helpers\Html;

/**
 * @var $rows array
 * @var $rowOptions array|null
 */

$rowOptions = $rowOptions ?? [];

Html::addCssClass($rowOptions, [
    'row'
]);

foreach ($rows as $row) {
    echo Html::beginTag('div', $rowOptions);

    foreach ($row as $column) {
        /**
         * @var $column array
         */
        if (($column['raw'] ?? false) === true) {
            echo $column['content'];
        } else {
            echo $this->render('column', $column);
        }
    }

    echo Html::endTag('div');
}
