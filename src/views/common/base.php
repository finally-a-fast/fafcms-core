<?php

use fafcms\fafcms\components\FafcmsComponent;

$isWidget = $isWidget ?? false;

if (!isset($buttons)) {
    $buttons = [];
}

if ($buttons instanceof Closure) {
    $buttons = $buttons($this);
}

if (!$isWidget) {
    echo '<div class="content-main-container-spacing">';
}

echo $content;

if (!$isWidget) {
    echo '</div>';
}

if (!$isWidget) {
    Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_ACTION_BAR, $buttons);
}

if ($isWidget && ($buttonCard ?? true)) {
    $buttonCardOptions = array_merge(['buttons' => $buttons], $buttonCardOptions??[]);

    echo $this->render('rows', [
        'rows' => [[[
            'options' => ['class' => 'col s12'],
            'content' => $this->render('card', $buttonCardOptions),
        ]]],
    ]);
}
