<?php

use yii\helpers\Html;

/**
 * @var $rows array
 * @var $gridOptions array|null
 * @var $rowOptions array|null
 */
$gridOptions = $gridOptions ?? [];
$rowOptions = $rowOptions ?? [];

Html::addCssClass($gridOptions, [
    'ui grid compact stretched'
]);

echo Html::tag(
    'div',
    $this->render('rows', [
        'rows' => $rows,
        'rowOptions' => $rowOptions,
    ]),
    $gridOptions
);
