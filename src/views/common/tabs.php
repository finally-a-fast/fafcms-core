<?php

use yii\helpers\Html;
use yii\helpers\Url;

$isWidget = $isWidget ?? false;

if ($tabs instanceof Closure) {
    $tabs = call_user_func($tabs, $model, (isset($form) ? $form : $this), $this);
}

if (!($singleTab ?? false) && count($tabs) === 1) {
    if (!$isWidget) {
        echo '<div class="content-main-container-spacing">';
    }

    echo $tabs[0]['content'];

    if (!$isWidget) {
        echo '</div>';
    }

    return;
}

if (!isset($id) || $id === null) {
    $id = Yii::$app->security->generateRandomString(10);
}

if (!isset($tabContainerOptions)) {
    $tabContainerOptions = [];
}

if (!isset($tabContainerOptions['id'])) {
    $tabContainerOptions['id'] = $id;
}

Html::addCssClass($tabContainerOptions, 'ui pointing secondary menu tabs');

if (!$isWidget) {
    echo Html::beginTag('div', [
        'class' => 'ui sticky',
        'data-contect' => 'body',
        'data-offset' => 72,
        'data-scroll-context' => '.content-main-container'
    ]);
}

echo Html::beginTag('div', $tabContainerOptions);

$hasActive = false;

foreach ($tabs as $i => $tab) {
    if (($tab['active'] ?? false)) {
        $hasActive = true;
        break;
    }
}

$tabKeys = array_flip(array_keys($tabs));

foreach ($tabs as $i => $tab) {
    $tabLabelOptions = [
        'class' => [
            'item',
            (($tab['active'] ?? false) || (!$hasActive && $tabKeys[$i] === 0)) ? 'active' : '',
        ],
    ];

    $tabId = $tab['id'] ?? $id . '-' . $i;
    $targetUrl = [Yii::$app->request->getUrl(), '#' => $tabId];

    if (isset($tab['ajaxContent'])) {
        Html::addCssClass($tabLabelOptions, 'ajax-content');
        $tabLabelOptions['data-ajax-content-trigger'] = 'tab-active';
        $tabLabelOptions['data-target'] = $tabId;
        $tabLabelOptions['data-src'] = Url::to($tab['ajaxContent']);
    }

    if (isset($tab['url']) && !empty($tab['url'])) {
        $targetUrl = Url::to($tab['url']);
        $tabLabelOptions['target'] = $tab['target'] ?? '_self';
    }

    $tabLabelOptions['data-tab'] = $tabId;

    echo Html::a( $tab['label'], $targetUrl, $tabLabelOptions);
}

echo Html::endTag('div');

if (!$isWidget) {
    echo Html::endTag('div');
}

foreach ($tabs as $i => $tab) {
    if (!isset($tab['url']) || empty($tab['url']) || $tab['active'] === true) {
        $tabId = $tab['id'] ?? $id . '-' . $i;

        echo Html::tag(
            'div',
            ($tab['content'] ?? ''),
            [
                'id'                => $tabId,
                'data-tab'          => $tabId,
                'data-tab-instance' => $id,
                'class'             => [
                    'ui tab',
                    (($tab['active'] ?? false) || (!$hasActive && $tabKeys[$i] === 0)) ? 'active' : '',
                    ($isWidget ? '' : 'content-main-container-spacing')
                ]
            ]
        );
    }
}
