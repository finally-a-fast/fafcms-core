<div class="row">
    <div class="col l4 m6 s12">
        <div class="card">
            <div class="card-content">
            <span class="card-title"><?=Yii::t('fafcms-core',  'Changes')?></span>
                <?=$model->hasAttribute('created_by')?'<div class="form-group"><label class="control-label">'.$model->getAttributeLabel('created_by').'</label><div class="form-control-static">'.Yii::$app->getUser()->getNameById($model->created_by, true).'</div></div>':''?>

                <?=$model->hasAttribute('updated_by')?'<div class="form-group"><label class="control-label">'.$model->getAttributeLabel('updated_by').'</label><div class="form-control-static">'.Yii::$app->getUser()->getNameById($model->updated_by, true).'</div></div>':''?>

                <?=$model->hasAttribute('activated_by')?'<div class="form-group"><label class="control-label">'.$model->getAttributeLabel('activated_by').'</label><div class="form-control-static">'.Yii::$app->getUser()->getNameById($model->activated_by, true).'</div></div>':''?>

                <?=$model->hasAttribute('deactivated_by')?'<div class="form-group"><label class="control-label">'.$model->getAttributeLabel('deactivated_by').'</label><div class="form-control-static">'.Yii::$app->getUser()->getNameById($model->deactivated_by, true).'</div></div>':''?>
                <?=$model->hasAttribute('created_at')?'<div class="form-group"><label class="control-label">'.$model->getAttributeLabel('created_at').'</label><div class="form-control-static">'.Yii::$app->formatter->asDatetime($model->created_at).'</div></div>':''?>

                <?=$model->hasAttribute('updated_at')?'<div class="form-group"><label class="control-label">'.$model->getAttributeLabel('updated_at').'</label><div class="form-control-static">'.Yii::$app->formatter->asDatetime($model->updated_at).'</div></div>':''?>

                <?=$model->hasAttribute('activated_at')?'<div class="form-group"><label class="control-label">'.$model->getAttributeLabel('activated_at').'</label><div class="form-control-static">'.Yii::$app->formatter->asDatetime($model->activated_at).'</div></div>':''?>

                <?=$model->hasAttribute('deactivated_at')?'<div class="form-group"><label class="control-label">'.$model->getAttributeLabel('deactivated_at').'</label><div class="form-control-static">'.Yii::$app->formatter->asDatetime($model->deactivated_at).'</div></div>':''?>
            </div>
        </div>
    </div>
</div>
