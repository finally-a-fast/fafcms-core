<?php

use yii\helpers\Html;

/**
 * @var $content string
 * @var $options array|null
 */

$options = $options ?? [];

Html::addCssClass($options, [
    'column'
]);

echo Html::tag('div', $content, $options);
