<?php

use fafcms\fafcms\components\FafcmsComponent;
use yii\helpers\Html;

if (isset($buttons) && $buttons instanceof \Closure) {
    $buttons = call_user_func($buttons, $model ?? null, $form ?? null, $this);
}

$cardOptions = $cardOptions ?? [];
Html::addCssClass($cardOptions, ['card' => 'ui card']);
echo Html::beginTag('div', $cardOptions);

if (isset($image) || isset($imageContent)) {
    $cardImageOptions = $cardImageOptions ?? [];
    Html::addCssClass($cardImageOptions, 'card-image');

    if (isset($image)) {
        $imageOptions = $imageOptions ?? [];
        $imageContent = Html::img($image, $imageOptions);
    }

    echo Html::tag('div', $imageContent, $cardImageOptions);
}
/*div class="ui card">
  <div class="content">
    <i class="right floated like icon"></i>
    <i class="right floated star icon"></i>
    <div class="header">Cute Dog</div>
    <div class="description">
      <p></p>
    </div>
  </div>
  <div class="extra content">
    <span class="left floated like">
      <i class="like icon"></i>
      Like
    </span>
    <span class="right floated star">
      <i class="star icon"></i>
      Favorite
    </span>
  </div>
</div>*/

if (isset($title) || isset($content)) {
    $cardContentOptions = $cardContentOptions ?? [];

    if (isset($title)) {
        echo '<div class="content card-header-content"><div class="header">';

        if (($buttonsInHeader ?? true) && isset($buttons) && count($buttons) > 0) {
            foreach ($buttons as $button) {
                if (!($button['visible'] ?? true) || !($button['visibleInHeader'] ?? false)) {
                    continue;
                }

                $options = $button['options'] ?? [];

                if (($button['type'] ?? null) === null) {
                    $button['type'] = 'link';
                }

                Html::addCssClass(
                    $options,
                    [
                        'right floated',
                    ]
                );

                if ($buttonsInHeaderTooltipped ?? true) {
                    Html::addCssClass(
                        $options,
                        [
                            'popup-hover',
                        ]
                    );

                    $options['data-position'] = 'bottom';
                    $options['data-content'] = Yii::$app->fafcms->getTextOrTranslation($button['label']) . (isset($options['data-tooltip']) ? '<br><br>' . $options['data-tooltip'] : '');
                }

                switch ($button['type']) {
                    case 'button':
                        if (isset($button['url'])) {
                            $options['data-url'] = $button['url'];
                        }

                        $buttonHtml = Html::button('<i class="icon ' . $button['icon'] . '"></i>', $options);
                        break;
                    case 'reset':
                        if (isset($button['form'])) {
                            $options['form'] = $button['form'];
                        }

                        $buttonHtml = Html::resetButton('<i class="icon ' . $button['icon'] . '"></i>', $options);
                        break;
                    case 'submit':
                        if (isset($button['form'])) {
                            $options['form'] = $button['form'];
                        }

                        $buttonHtml = Html::submitButton('<i class="icon ' . $button['icon'] . '"></i>', $options);
                        break;
                    default:
                        $buttonHtml = Html::a('<i class="icon ' . $button['icon'] . '"></i>', $button['url'], $options);
                        break;
                }

                echo $buttonHtml;
            }
        }

        if (isset($icon)) {
            echo '<i class="icon mdi mdi-' . $icon . '"></i>';
        }

        echo Html::tag('div', Html::tag('span', $title), ['class' => 'text-scroller']);

        echo '</div></div>';
    }

    Html::addCssClass($cardContentOptions, 'content');
    echo Html::beginTag('div', $cardContentOptions);

    if (isset($content)) {
        echo $content;
    }

    echo Html::endTag('div');
}

echo $this->render('buttons', [
    'buttons' => $buttons ?? [],
    'model' => $model ?? null,
    'form' => $form ?? null,
    'cardActionOptions' => $cardActionOptions ?? []
]);

echo Html::endTag('div');
