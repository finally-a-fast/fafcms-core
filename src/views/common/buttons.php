<?php

use fafcms\fafcms\components\FafcmsComponent;
use yii\helpers\Html;

if ($buttons instanceof \Closure) {
    $buttons = call_user_func($buttons, $model ?? null, $form ?? null, $this);
}

if (count($buttons) > 0) {
    $buttonHtml = [];

    $buttonKeys = array_flip(array_keys($buttons));

    uksort($buttons, static function($posA, $posB) use ($buttons, $buttonKeys) {
        $positionA = $buttons[$posA]['position'] ?? 'default';
        $positionB = $buttons[$posB]['position'] ?? 'default';

        if ($positionB === 'first' || $positionA === 'last') {
            return 1;
        }

        $posAButton = $buttonKeys[$posA];

        if (isset($buttons[$posA]['after'], $buttonKeys[$buttons[$posA]['after']])) {
            $posAButton = $buttonKeys[$buttons[$posA]['after']];
        }

        $posBButton = $buttonKeys[$posB];

        if (isset($buttons[$posB]['after'], $buttonKeys[$buttons[$posB]['after']])) {
            $posBButton = $buttonKeys[$buttons[$posB]['after']];
        }

        return $posAButton > $posBButton;
    });

    $buttonHtml = Yii::$app->view->renderNavigationItems('card-buttons', $buttons, ['class' => array_merge($cardActionOptions ?? [], ['menu' => false, 'fluid-buttons']), 'renderEmpty' => false], ['class' => ['item' => false, 'ui button']], 'dropdown', false);

    if (!empty($buttonHtml)) {
        echo '<div class="content card-action">' . $buttonHtml . '</div>';
    }

    /*
    foreach ($buttons as $button) {
        if (!Yii::$app->view->canAccessNavItem($button)) {
            continue;
        }

        $options = $button['options'] ?? [];

        if (($button['type'] ?? null) === null) {
            $button['type'] = (isset($button['url']) ? 'link' : 'button');
        }

        Html::addCssClass($options, 'ui button');

        $button['label'] = $button['label'] ?? '';

        $icon = '';

        if (($button['icon'] ?? null) !== null) {
            $icon = '<i class="icon mdi mdi-' . $button['icon'] . '"></i>';
        }

        switch ($button['type']) {
            case 'button':
                if (isset($button['url'])) {
                    $options['data-url'] = $button['url'];
                }

                $buttonHtml[] = Html::button($icon . Yii::$app->fafcms->getTextOrTranslation($button['label']), $options);
                break;
            case 'reset':
                if (isset($button['form'])) {
                    $options['form'] = $button['form'];
                }

                $buttonHtml[] = Html::resetButton($icon . Yii::$app->fafcms->getTextOrTranslation($button['label']), $options);
                break;
            case 'submit':
                if (isset($button['form'])) {
                    $options['form'] = $button['form'];
                }

                $buttonHtml[] = Html::submitButton($icon . Yii::$app->fafcms->getTextOrTranslation($button['label']), $options);
                break;
            default:
                $buttonHtml[] = Html::a($icon . Yii::$app->fafcms->getTextOrTranslation($button['label']), $button['url'], $options);
                break;
        }
    }

    $buttonCount = count($buttonHtml);

    if ($buttonCount > 0) {
        $cardActionOptions = $cardActionOptions ?? [];

        Html::addCssClass($cardActionOptions, [
            'fluid-buttons'
        ]);

        echo '<div class="extra content card-action">'. Html::tag('div', implode('', $buttonHtml), $cardActionOptions) . '</div>';
    }*/
}
