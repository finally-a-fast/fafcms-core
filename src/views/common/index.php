<?php

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\widgets\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

$isWidget = $isWidget ?? false;

if (!isset($buttons)) {
    $buttons = [];
}

if ($buttons instanceof Closure) {
    $buttons = $buttons($dataProvider, $filterModel, $this);
}

foreach ($buttons as &$button) {
    if (!isset($button['options']['data-pjax'])) {
        $button['options']['data-pjax'] = '0';
    }
}

if (!isset($sort) || $sort) {
    ArrayHelper::multisort($columns, 'sort');
}

$gridViewOptions = array_merge([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => $columns
], $gridViewOptions ?? []);

if (!$isWidget) {
    echo '<div class="content-main-container-spacing">';
}

Pjax::begin();

if (!$isWidget) {
    Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_ACTION_BAR, $buttons);
}

echo $this->render('grid', [
    'rows' => [[[
        'content' => $this->render('card', [
            'title' => $title ?? null,
            'icon' => $icon ?? null,
            'content' => GridView::widget($gridViewOptions),
            'buttons' => ($isWidget ? $buttons : [])
        ]),
    ]]],
]);

Pjax::end();

if (!$isWidget) {
    echo '</div>';
}
