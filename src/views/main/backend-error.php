<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->params['breadcrumbs'][] = $name;
$this->title = $this->params['breadcrumbs'];
?>
<div class="row">
    <div class="col s12 m6 offset-m3">
        <div class="card">
            <div class="card-content">
                <span class="card-title"><?= $name ?></span>
                <p><?= nl2br($message) ?></p>
            </div>
        </div>
    </div>
</div>
