<?php
/* @var $name string */
/* @var $message string */
/* @var $this fafcms\fafcms\components\ViewComponent */

use fafcms\fafcms\controllers\FrontendController;
use fafcms\settingmanager\Bootstrap as SettingmanagerBootstrap;
use fafcms\sitemanager\models\Contentmeta;
use yii\web\NotFoundHttpException;
use yii\web\Response;

$errorContentmetaId = Yii::$app->fafcms->getCurrentProjectLanguage()->error_page_contentmeta_id ?? Yii::$app->fafcms->getCurrentProject()->error_page_contentmeta_id;

if ($errorContentmetaId !== null) {
    $contentmeta = Contentmeta::find()->andWhere(['id' => $errorContentmetaId])->one();
    $contentOptions = Yii::$app->fafcms->getContentClass($contentmeta['model_class']);

    if ($contentOptions !== null) {
        $contentModel = FrontendController::getContentModel($contentmeta, $contentOptions);

        FrontendController::setCurrentContentMeta($contentmeta, $contentOptions['name'], $contentModel);
        FrontendController::setSiteMeta($contentmeta);

        $id = FrontendController::getSiteId($contentModel, $contentOptions);

        $siteContent = FrontendController::getSiteContent($id, [
            'errorName' => $name,
            'errorMessage' => nl2br($message),
        ]);
    }
}

echo $siteContent ?? Yii::t('fafcms-core', nl2br($message));
