<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $form yii\helpers\ActiveForm */
/* @var $model \fafcms\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
?>
    <div class="card">
        <div class="card-content">
            <span class="card-title"><?=Yii::t('fafcms-core',  'Request new password')?></span>
            <p><?=Yii::t('fafcms-core',  'Please enter your username or email address. You will receive a link to create a new password via email.')?></p>
            <?=$form->field($model, 'username')->textInput(['autofocus' => true]);?>
        </div>
        <div class="card-action">
            <div class="row">
                <div class="col m6 s12">
                    <?=Html::a(Yii::t('fafcms-core', 'Go back'), Yii::$app->getUser()->getReturnUrl(Yii::$app->homeUrl), ['class' => 'btn-flat waves-effect full-width']);?>
                </div>
                <div class="col m6 s12">
                    <?=Html::submitButton(Yii::t('fafcms-core', 'Request new password'), ['class'=>'btn waves-effect waves-light full-width']); ?>
                </div>
            </div>
        </div>
    </div>
<?php
ActiveForm::end();
