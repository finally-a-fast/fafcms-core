<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $model \common\models\ResetPasswordForm */

use yii\helpers\Html;
use fafcms\fafcms\widgets\ActiveForm;

$this->title = ['Passwortänderung'];
$form = ActiveForm::begin();
?>
<div class="content-main-container-spacing">
    <div class="ui grid compact stretched">
        <div class="row">
            <div class="column sixteen wide mobile four wide tablet four wide computer four wide large screen four wide widescreen"></div>
            <div class="column sixteen wide mobile eight wide tablet eight wide computer eight wide large screen eight wide widescreen">
                <div class="ui card">
                    <div class="content card-header-content">
                        <div class="header"><i class="icon mdi mdi-account-key-outline"></i>
                            <div class="text-scroller"><span>Zugriff zur Website erhalten</span></div>
                        </div>
                    </div>
                    <div class="content">
                        <p>Bitte geben Sie folgend Ihr neues Kennwort ein und bestätigen Sie es.</p>
                        <?=$form->field($model, 'email')->label(false)->hiddenInput();?>
                        <?=$form->field($model, 'password')->passwordInput();?>
                        <?=$form->field($model, 'password_confirmation')->passwordInput();?>
                    </div>
                    <div class="extra content card-action">
                        <div class="fluid-buttons">
                            <?=Html::submitButton('Passwort ändern', ['class'=>'primary ui button']);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
