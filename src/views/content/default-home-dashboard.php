<?php
return [
    'home-tab' => [
        'class' => \fafcms\fafcms\items\Tab::class,
        'settings' => [
            'label' => ['fafcms-core', 'Home'],
        ],
        'contents' => [
            'row-1' => [
                'class' => \fafcms\fafcms\items\Row::class,
                'contents' => [
                    'column-1' => [
                        'class' => \fafcms\fafcms\items\Column::class,
                        'settings' => [
                            'm' => 8,
                        ],
                        'contents' => [
                            'master-data-card' => [
                                'class' => \fafcms\fafcms\items\Card::class,
                                'settings' => [
                                    'title' => ['fafcms-core', 'Home'],
                                    'icon' => 'speedometer',
                                ],
                                'contents' => [
                                    'master-data-card' => [
                                        'class' => \fafcms\fafcms\items\HomeList::class,
                                    ],
                                ]
                            ],
                        ]
                    ],
                    'column-2' => [
                        'class' => \fafcms\fafcms\items\Column::class,
                        'settings' => [
                            'm' => 8,
                        ],
                        'contents' => [
                            'access-restrictions-card' => [
                                'class' => \fafcms\fafcms\items\Card::class,
                                'settings' => [
                                    'title' => ['fafcms-core', 'Notifications'],
                                    'icon' => 'bell-outline',
                                ],
                                'contents' => [
                                ]
                            ],
                        ]
                    ],
                ],
            ],
            'row-2' => [
                'class' => \fafcms\fafcms\items\Row::class,
                'contents' => [
                    'column-1' => [
                        'class' => \fafcms\fafcms\items\Column::class,
                        'settings' => [
                            'm' => 8
                        ],
                        'contents' => [
                            'content-card' => [
                                'class' => \fafcms\fafcms\items\Card::class,
                                'settings' => [
                                    'title' => ['fafcms-core', 'Projects'],
                                    'icon' => 'scatter-plot-outline',
                                ],
                                'contents' => [
                                ]
                            ],
                        ]
                    ],
                    'column-2' => [
                        'class' => \fafcms\fafcms\items\Column::class,
                        'settings' => [
                            'm' => 8,
                        ],
                        'contents' => [
                            'access-restrictions-card' => [
                                'class' => \fafcms\fafcms\items\Card::class,
                                'settings' => [
                                    'title' => ['fafcms-core', 'News and updates'],
                                    'icon' => 'newspaper',
                                ],
                                'contents' => [
                                ]
                            ],
                        ]
                    ],
                ],
            ],
            'row-3' => [
                'class' => \fafcms\fafcms\items\Row::class,
                'contents' => [
                    'column-1' => [
                        'class' => \fafcms\fafcms\items\Column::class,
                        'settings' => [
                            'm' => 8
                        ],
                        'contents' => [
                            'content-card' => [
                                'class' => \fafcms\fafcms\items\Card::class,
                                'settings' => [
                                    'title' => ['fafcms-core', 'Who is online'],
                                    'icon' => 'account-network-outline',
                                ],
                                'contents' => [
                                ]
                            ],
                        ]
                    ],
                    'column-2' => [
                        'class' => \fafcms\fafcms\items\Column::class,
                        'settings' => [
                            'm' => 8
                        ],
                        'contents' => [
                            'content-card' => [
                                'class' => \fafcms\fafcms\items\Card::class,
                                'settings' => [
                                    'title' => ['fafcms-core', 'Favorites'],
                                    'icon' => 'star-outline',
                                ],
                                'contents' => [
                                ]
                            ],
                        ]
                    ],
                ],
            ],
        ]
    ]
];
