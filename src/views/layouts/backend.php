<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $content string */

use fafcms\fafcms\assets\fafcms\backend\FafcmsActionBarAsset;
use fafcms\fafcms\components\ViewComponent;
use yiiui\yii2flagiconcss\widget\FlagIcon;
use yii\helpers\Html;
use fafcms\fafcms\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->registerBackendAssetComponents();

$rightSidebar = $this->renderNavigation(ViewComponent::NAVIGATION_SIDEBAR_RIGHT, ['class' => ['menu-view ui compact vertical right fixed labeled icon mini'], 'renderEmpty' => false]);
$actionBar = $this->renderNavigation(ViewComponent::NAVIGATION_ACTION_BAR, ['class' => ['menu' => false, 'fluid-buttons'], 'renderEmpty' => false], ['class' => ['item' => false, 'ui button']], 'dropdown', false);

$bodyOptions = Yii::$app->params['body'] ?? [];
$bodyOptions['data-flashed-notifications'] = Yii::$app->session->getAllFlashes();
Html::addCssClass($bodyOptions, 'ui blurring');

if (Yii::$app->fafcms->getIsInstalled() && Yii::$app->fafcms->isSidebarVisible()) {
    Html::addCssClass($bodyOptions, 'sidebar-main-visible');
}

if ($rightSidebar !== '') {
    Html::addCssClass($bodyOptions, 'sidebar-right-visible');
}

if ($actionBar !== '') {
    FafcmsActionBarAsset::register($this);
}

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="referrer" content="same-origin">
        <?php $this->registerCsrfMetaTags() ?>
        <link rel="canonical" href="<?= Yii::$app->request->absoluteUrl ?>">
        <link rel="alternate" href="<?= Yii::$app->request->absoluteUrl ?>" hreflang="de">
        <?= Html::tag('base', '', ['href' => Url::to(['/'.Yii::$app->fafcms->getBackendUrl()])]) ?>
        <title><?= Html::encode(implode(' - ', array_reverse(array_merge([Yii::$app->name], is_array($this->title)?$this->title:[$this->title])))) ?></title>
        <?php $this->head() ?>
    </head>
    <?= Html::beginTag('body', $bodyOptions) ?>
        <?php $this->beginBody() ?>
        <div id="page-load-dimmer" class="ui active dimmer">
            <div class="ui text loader large slow">
                <?= Yii::t('fafcms-core', 'Loading') ?>
                <noscript>
                    <div>
                        <br>
                        <div class="alert alert-danger text-center">
                            <div class="ui error message">
                                <div class="header">
                                    <?= Yii::t('fafcms-core', 'no_js_warning_header') ?>
                                </div>
                                <p>
                                    <?= Yii::t('fafcms-core', 'no_js_warning') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </noscript>
            </div>
        </div>
        <div class="ui pushable content-main">
            <div class="ui overlay visible left sidebar sidebar-main fafcms-scrollbars">
                <div class="ui basic segments">
                    <div class="ui segment gradient-background">
                        <?php
                            $projectSelection = Html::a(
                                Yii::$app->fafcms->getCurrentProjectLogo(),
                                ['/main/change-project'],
                                [
                                    'class' => 'ajax-modal ui logo icon image popup-hover',
                                    'data' => [
                                        'title' => Yii::t('fafcms-core', 'Click to change current project.'),
                                        'data-content' => 'test',//data-html="<div class='header'>User Rating</div><div class='content'><div class='ui yellow rating'><i class='active star icon'></i><i class='active star icon'></i><i class='active star icon'></i><i class='active star icon'></i><i class='active star icon'></i></div></div>"
                                        'ajax-modal-id' => 'change-project-modal',
                                    ]
                                ]
                            ) . Html::a(
                                Yii::$app->name,
                                ['/main/change-project'],
                            );

                            echo '
                                <div class="ui items inverted">
                                  <div class="item">
                                    <div class="ui mini image">
                                      ' . Yii::$app->fafcms->getCurrentProjectLogo() . '
                                    </div>
                                    <div class="content">
                                      <a class="header">' . Yii::$app->name . '</a>
                                      <div class="meta">
                                        <span>' . Yii::$app->fafcms->getCurrentProject()->description . '</span>
                                      </div>
                                      <div class="description">
                                        <p>' . Yii::$app->fafcms->getCurrentProjectLanguage()->domain->domain . '</p>
                                      </div>
                                      <div class="extra">' . Yii::$app->fafcms->getCurrentProjectLanguage()->name . '</div>
                                    </div>
                                  </div>
                                </div>
                            ';
                        ?>
                    </div>
                    <?php if (!Yii::$app->user->isGuest): ?>
                        <div class="segment language-selection">
                            <?php
                                $languageSelection = '';

                                foreach (Yii::$app->fafcms->getCurrentProject()->projectProjectlanguages as $projectlanguage) {
                                    $activeLanguage = $projectlanguage->id === Yii::$app->fafcms->getCurrentProjectLanguageId();

                                    $languageSelection .= Html::a(
                                        FlagIcon::widget([
                                            'countryCode' => $projectlanguage->language->flag->name
                                        ]),
                                        ['/main/change-project-language', 'id' => $projectlanguage->id],
                                        [
                                            'class' => ['item', ($activeLanguage ? 'active' : ''), 'popup-hover'],
                                            'data-content' => $projectlanguage->name . ($activeLanguage ? ' (' . Yii::t('fafcms-core', 'active') . ')' : ''),
                                            'data-position' => 'bottom left',
                                        ],
                                    );
                                }

                                $languageSelection .= Html::a(
                                    '<i class="plus icon"></i>',
                                    ['/project'],
                                    [
                                        'class' => 'item language-selection-add popup-hover',
                                        'data-content' => Yii::t('fafcms-core', 'Add language'),
                                        'data-position' => 'bottom left',
                                    ]
                                );

                                echo '<div class="ui mini horizontal divided selection list nowrap">' . $languageSelection . '</div>';
                            ?>
                        </div>
                    <?php endif; ?>
                    <div class="segment">
                        <?= $this->renderNavigation(ViewComponent::NAVIGATION_SIDEBAR_LEFT, ['class' => 'menu-sidebar ui vertical menu']) ?>
                    </div>
                </div>
            </div>
            <div class="pusher">
                <?= $rightSidebar ?>
                <header class="ui top tiny fixed menu inverted menu-header">
                    <?= $this->renderNavigation(ViewComponent::NAVIGATION_HEADER_LEFT, ['class' => 'menu-header-left'], [], 'dropdown') ?>
                    <?= $this->renderNavigation(ViewComponent::NAVIGATION_HEADER_RIGHT, ['class' => 'menu-header-right right'], [], 'dropdown') ?>
                </header>
                <div class="ui vertical inverted segment breadcrumb-header">
                    <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs'] ?? []]) ?>
                </div>
                <div class="content-main-container">
                    <main>
                        <!--[if lt IE 11]>
                            <div class="ui error message">
                                <div class="header">
                                    <?= Yii::t('fafcms-core', 'old_browser_warning_header') ?>
                                </div>
                                <p>
                                    <?= Yii::t('fafcms-core', 'old_browser_warning') ?>
                                </p>
                            </div>
                        <![endif]-->
                        <?= $content ?>
                    </main>
                    <footer class="ui vertical footer segment">
                        <div class="ui container">
                            <div class="ui stackable grid">
                                <div class="nine wide column">
                                    <?= $this->renderNavigation(ViewComponent::NAVIGATION_FOOTER_LEFT, ['class' => ['menu' => false, 'menu-footer-left ui horizontal divided link list']], [], 'dropdown') ?>
                                </div>
                                <div class="seven wide column right aligned">
                                    <?= $this->renderNavigation(ViewComponent::NAVIGATION_FOOTER_RIGHT, ['class' => ['menu' => false, 'menu-footerright ui horizontal divided link list']], [], 'dropdown') ?>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <?php if ($actionBar !== ''): ?>
                        <div class="action-bar ui raised segment active">
                            <button class="ui icon button toggle-action-bar">
                                <i class="icon mdi mdi-chevron-double-right"></i>
                            </button>
                            <?= $actionBar ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
