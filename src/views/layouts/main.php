<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $content string */

use yii\helpers\Html;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="referrer" content="same-origin">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode(implode(' - ', array_reverse(array_merge([Yii::$app->name], is_array($this->title)?$this->title:[$this->title])))) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
            <h1>Oh no!</h1>
            <p>Something went really wrong...</p>
            <p>Please check the <a href="https://www.finally-a-fast.com/forum/">FAFCMS Forum</a> for help.</p>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
