<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $content string */

use fafcms\fafcms\assets\fafcms\FafcmsAppAsset;
use fafcms\sitemanager\models\Layout;
use fafcms\parser\component\Parser;
use fafcms\sitemanager\assets\TypekitAsset;
use fafcms\fafcms\controllers\FrontendController;
use fafcms\settingmanager\Bootstrap as SettingmanagerBootstrap;
use fafcms\fafcms\components\ViewComponent;
use yii\helpers\Json;

FafcmsAppAsset::register($this);

Yii::$app->fafcms->registerAssets($this);

if (Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('typekit_id') !== null) {
    $typekitConfig = Json::htmlEncode([
        'kitId' => Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('typekit_id'),
        'scriptTimeout' => Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('typekit_timeout', 3000),
        'async' => Yii::$app->getModule(SettingmanagerBootstrap::$id)->getSetting('typekit_async', true)
    ]);

    $this->registerJs('(function(d) {var config='.$typekitConfig.',h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src="https://use.typekit.net/"+config.kitId+".js";tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)})(document);', ViewComponent::POS_HEAD);
}

$this->beginPage();

$messages = Yii::$app->session->getAllFlashes();

$data = [
    'content' => $content,
    'messages' => $messages
];

if (!isset(Yii::$app->fafcmsParser->data['currentLayoutId'])) {
    Yii::$app->fafcmsParser->data['currentLayoutId'] = Yii::$app->fafcms->getCurrentProjectLanguage()->default_layout_id ?? Yii::$app->fafcms->getCurrentProject()->default_layout_id;
}

if (Yii::$app->fafcmsParser->data['currentLayoutId'] === null) {
    echo Yii::t('fafcms-core', 'Missing layout.');
} else {
    $this->title = FrontendController::getSiteTitle();
    $this->registerCsrfMetaTags();

    $this->registerLinkTag([
        'rel' => 'canonical',
        'href' => Yii::$app->request->absoluteUrl
    ]);

    $this->registerLinkTag([
        'rel' => 'alternate',
        'hreflang' => 'de',
        'href' => Yii::$app->request->absoluteUrl
    ]);
/*
        //$this->addColumn(Contentmeta::tableName(), 'og_article_tag', $this->string(255)->null()->defaultValue(null)->after('og_description'));
        //$this->addColumn(Contentmeta::tableName(), 'og_book_tag', $this->string(255)->null()->defaultValue(null)->after('og_description'));

        /* TODO eigenes modul / tabelle
        $this->addColumn(Contentmeta::tableName(), 'og_book:author', $this->string(255)->null()->defaultValue(null)->after('og_description'));
        $this->addColumn(Contentmeta::tableName(), 'og_article:author ', $this->string(255)->null()->defaultValue(null)->after('og_description'));
        $this->addColumn(Contentmeta::tableName(), 'profile:first_name ', $this->string(255)->null()->defaultValue(null)->after('og_description'));
        $this->addColumn(Contentmeta::tableName(), 'profile:last_name ', $this->string(255)->null()->defaultValue(null)->after('og_description'));
        $this->addColumn(Contentmeta::tableName(), 'profile:username ', $this->string(255)->null()->defaultValue(null)->after('og_description'));
        $this->addColumn(Contentmeta::tableName(), 'profile:gender ', $this->string(255)->null()->defaultValue(null)->after('og_description'));*/

        //$this->addColumn(Contentmeta::tableName(), 'og_audio', $this->string(255)->null()->defaultValue(null)->after('keywords'));

        /*Yii::$app->view->openGraphDatas['fb:app_id'] = '1777332555845964';
            Yii::$app->view->openGraphDatas['og:site_name'] = Yii::$app->name;

            Yii::$app->view->openGraphDatas['og:title'] = $model->title.(!empty($model->subheadline)?', '.$model->subheadline:'');
            Yii::$app->view->openGraphDatas['og:description'] = $model->description;
            Yii::$app->view->openGraphDatas['og:url'] = $model->getUrl(true);
            Yii::$app->view->openGraphDatas['og:type'] = 'article';
            Yii::$app->view->openGraphDatas['og:locale'] = 'de_DE';

                Yii::$app->view->openGraphDatas['og:image'] = Yii::$app->file->getImageUrl($model->imageFile, 10);
                    Yii::$app->view->openGraphDatas['og:image:type']   = $fileType->mime_type;
                    Yii::$app->view->openGraphDatas['og:image:width']  = $dimensions['width'];
                    Yii::$app->view->openGraphDatas['og:image:height'] = $dimensions['height'];
                }
            }

            Yii::$app->view->openGraphDatas['article:published_time'] = $model->publication;
            Yii::$app->view->openGraphDatas['article:section'] = $model->mainChannel->name;

            $topics = ArrayHelper::getColumn($model->getArticleTopics()->joinWith('topic')->where('topic.status = \'active\'')->all(), 'topic.name');

            Yii::$app->view->openGraphDatas['article:tag'] = implode(',', $topics);*/
    Yii::$app->fafcms->prepareRender();

    echo Yii::$app->fafcmsParser->parse(Parser::TYPE_PAGE, Layout::getContentById(Yii::$app->fafcmsParser->data['currentLayoutId']), Parser::ROOT, $data);
}

$this->endPage();
