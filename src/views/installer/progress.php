<?php
use \fafcms\fafcms\controllers\InstallerController;

/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $form yii\helpers\ActiveForm */
/* @var $model \fafcms\fafcms\models\InstallerForm */
?>
<?= Yii::t('fafcms-core-setup', 'step_{current}_of_{max}', ['current' => $currentStep['number'] + 1, 'max' => InstallerController::getStepCount()]) ?> (<?= Yii::$app->formatter->asPercent(($currentStep['number'] + 1) / InstallerController::getStepCount())?>)
<div class="progress">
    <div class="determinate" style="width: <?=$currentStep['number'] / InstallerController::getStepCount() * 100?>%"></div>
</div>
