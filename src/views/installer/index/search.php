<?php
use yii\helpers\Html;
?>
<?= $form->field($model, 'search')->textInput() ?>

<?= Html::checkbox('showDependencies', Yii::$app->session->get('installerShowDependencies', false), ['label' => Yii::t('fafcms-core-setup', 'Show dependencies')]) ?>
