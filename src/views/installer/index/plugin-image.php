<?php
use yii\helpers\Html;

echo Html::a('<div class="circle">'.
        (isset($model['extra']->fafcms->logo)?Html::tag('img', null, [
            'data-src' => $model['extra']->fafcms->logo,
            'class' => 'lazyload'
        ]):'<i class="mdi mdi-'.$model['typeIcon'].'"></i>').
    '</div>'.
    '<div class="card-title">'.($model['extra']->fafcms->title??$model['name']).'</div>',
    ['module-details', 'id' => $model['name']],
    ['class' => 'plugin-image waves-effect waves-block waves-light center-align gradient-background']
);
