<?php
use yii\helpers\Html;
?>
<p class="fafcms-scrollbars">
    <?= $model['description'] ?>
</p>
<div class="plugin-chips fafcms-scrollbars">
    <div class="chip"><?= $model['type'] ?></div>
    <?php if (!empty($model['version'])):?>
        <?= Html::a($model['version'], $model['homepage'], ['target' => '_blank', 'class' => 'chip']); ?>
    <?php endif;?>
    <?php if (!empty($model['license'])):?>
        <?= Html::a($model['license'], 'https://choosealicense.com/appendix/#'.mb_strtolower($model['license']), ['target' => '_blank', 'class' => 'chip']); ?>
    <?php endif;?>
    <?php if (!empty($model['homepage'])):?>
        <?= Html::a('Homepage', $model['homepage'], ['target' => '_blank', 'class' => 'chip']); ?>
    <?php endif;?>
</div>
