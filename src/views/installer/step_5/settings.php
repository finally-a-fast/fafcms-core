<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $form fafcms\fafcms\widgets\ActiveForm */
/* @var $model fafcms\fafcms\models\InstallerForm */
?>

<?= $form->field($model, 'domain')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'projectDescription')->textarea() ?>
