<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $form fafcms\fafcms\widgets\ActiveForm */
/* @var $model fafcms\fafcms\models\InstallerForm */
?>

<?= $form->field($model, 'insertDefaultSettings')->checkbox() ?>

<?= $form->field($model, 'insertDefaultLayout')->checkbox() ?>

<?= $form->field($model, 'insertDefaultSites')->checkbox() ?>

<?= $form->field($model, 'insertExampleArticles')->checkbox() ?>
