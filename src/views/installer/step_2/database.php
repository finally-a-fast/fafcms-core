<?= $form->field($model, 'databaseAdvanced')->checkbox() ?>

<?= $form->field($model, 'databaseDsn')->textInput() ?>

<?= $form->field($model, 'databaseType')->dropDownList($model->attributeOptions()['databaseType']) ?>

<?= $form->field($model, 'databaseHost')->textInput() ?>

<?= $form->field($model, 'databaseName')->textInput() ?>

<?= $form->field($model, 'databaseUser')->textInput() ?>

<?= $form->field($model, 'databasePassword')->textInput() ?>

<?= $form->field($model, 'databaseTablePrefix', [
    'errorOptions' => ['encode' => false]
])->textInput() ?>

<?= $form->field($model, 'run_migrations')->checkbox() ?>
