<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $form yii\helpers\ActiveForm */
/* @var $model \fafcms\fafcms\models\InstallerForm */

echo $this->render('//common/rows', [
    'rows' => [
        [
            [
                'options' => ['class' => 'col s12'],
                'content' => $this->render('//common/card', [
                    'cardContentOptions' => [
                        'class' => 'center-align',
                    ],
                    'content' => $this->render('progress', [
                        'model' => $model,
                        'form' => $form,
                        'currentStep' => $currentStep
                    ]),
                ])
            ],
        ],
        [
            [
                'options' => ['class' => 'col s12'],
                'content' =>  $this->render($currentStep['view'], [
                    'model' => $model,
                    'form' => $form,
                    'currentStep' => $currentStep,
                    'buttons' => $buttons
                ]),
            ],
        ]
    ]
]);
