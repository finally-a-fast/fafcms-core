<?php
/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $form yii\helpers\ActiveForm */
/* @var $model \fafcms\fafcms\models\InstallerForm */

echo $this->render('//common/rows', [
    'rows' => [
        [
            [
                'options' => ['class' => 'col offset-xl3 xl6 offset-l1 l10 s12'],
                'content' => $this->render('//common/card', [
                    'icon'               => $currentStep['icon'],
                    'title'              => $currentStep['title'],
                    'buttons'            => $buttons,
                    'cardContentOptions' => [
                        'class' => 'center-align',
                    ],
                    'content' => $this->render('step_5/settings', [
                        'model' => $model,
                        'form'  => $form,
                    ]),
                ])
            ],
        ],
    ],
]);
