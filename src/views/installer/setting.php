<?php
use fafcms\helpers\classes\PluginSetting;

/**
 * @var fafcms\fafcms\components\ViewComponent $this
 * @var fafcms\fafcms\models\SettingForm $model
 * @var yii\widgets\ActiveForm $form
 * @var string $title
 * @var array $plugin
 */
?>
<dl>
    <dt><sup>*</sup></dt>
    <dd><?= Yii::t('fafcms-core', 'Mandatory fields') ?></dd>
</dl>
<div class="row">
    <div class="col s12">
        <div class="card">
            <div class="card-content">
                <span class="card-title"><?= $title ?></span>
                <?php foreach ($plugin['settings'] as $setting): ?>
                    <?php
                        /**
                         * @var PluginSetting $setting
                         */
                        echo $setting->getContainer($form, $model);
                    ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
