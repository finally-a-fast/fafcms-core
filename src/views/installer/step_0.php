<?php
use fafcms\fafcms\controllers\InstallerController;

/* @var $this fafcms\fafcms\components\ViewComponent */
/* @var $form yii\helpers\ActiveForm */
/* @var $model \fafcms\fafcms\models\InstallerForm */

echo $this->render('//common/rows', [
    'rows' => [
        [
            [
                'options' => ['class' => 'col offset-xl3 xl6 offset-l1 l10 s12'],
                'content' => $this->render('//common/card', [
                    'icon' => $currentStep['icon'],
                    'title' => Yii::t('fafcms-core-setup', 'setup'),
                    'cardContentOptions' => [
                        'class' => 'center-align',
                    ],
                    'content' => $this->render('step_0/language', [
                        'model' => $model,
                        'form' => $form,
                    ]),
                    'buttons' => $buttons
                ])
            ],
        ],
    ],
]);
