<?php

use fafcms\fafcms\widgets\ActiveForm;

/**
 * @var $dataProvider \yii\data\ArrayDataProvider
 */
$form = ActiveForm::begin();
?>
<div class="row">
    <div class="col s12">
        <?= $this->render('//common/card', [
            'title' => Yii::t('fafcms-core', 'Update and install modules'),
            'content' => $this->render('index/search', [
                'form' => $form,
                'model' => $model
            ]),
            'buttons' => [[
                'icon' => 'update',
                'label' => Yii::t('fafcms-core',  'Update {{count}} modules'),
                'url' => ['update'],
                'options' => [
                    'class' => 'hide full-update-btn',
                ],
            ], [
                'icon' => 'magnify',
                'label' => Yii::t('fafcms-core', 'Search'),
                'type' => 'submit',
                'options' => [
                    'class' => ['primary'],
                ],
            ],]
        ]) ?>
    </div>
</div>
<div class="row">
    <?php foreach ($dataProvider->models as $model): ?>
        <div class="col s12 m6 l4 xl3">
            <?= $this->render('//common/card', [
                'cardOptions' => [
                    'id' => $model['name'],
                    'class' => 'plugin-card'
                ],
                'imageContent' => $this->render('index/plugin-image', [
                    'model' => $model
                ]),
                'content' => $this->render('index/plugin-info', [
                    'model' => $model
                ]),
                'buttons' => [[
                    'icon' => 'update',
                    'label' => Yii::t('fafcms-core',  'Update'),
                    'url' => ['module-update', 'id' => $model['name']],
                    'options' => [
                        'class' => 'update-btn hide',
                        'data-confirm' => Yii::t('fafcms-core', 'Are you sure you want to update this module?'),
                        'data-method' => 'post',
                    ],
                ], [
                    'icon' => 'power-plug',
                    'visible' => $model['type'] === 'module' && !$model['loaded'],
                    'label' => Yii::t('fafcms-core',  'Enable'),
                    'url' => ['module-enable', 'id' => $model['name']],
                    'options' => [
                        'data-confirm' => Yii::t('fafcms-core', 'Are you sure you want to enable this module?'),
                        'data-method' => 'post',
                    ],
                ], [
                    'icon' => 'power-plug-off',
                    'visible' => $model['type'] === 'module' && $model['loaded'],
                    'label' => Yii::t('fafcms-core',  'Disable'),
                    'url' => ['module-disable', 'id' => $model['name']],
                    'options' => [
                        'data-confirm' => Yii::t('fafcms-core', 'Are you sure you want to disable this module?'),
                        'data-method' => 'post',
                    ],
                ], [
                    'icon' => 'settings-outline',
                    'visible' => count($model['settings']) > 0,
                    'label' => Yii::t('fafcms-core',  'Settings'),
                    'url' => ['module-settings', 'id' => $model['name']],
                ], [
                    'icon' => 'information-variant',
                    'label' => Yii::t('fafcms-core',  'Details'),
                    'url' => ['module-details', 'id' => $model['name']],
                    'options' => [
                        'class' => ['primary'],
                    ],
                ]]
            ]) ?>
        </div>
    <?php endforeach; ?>
</div>
<?php
ActiveForm::end();
