<?php

use fafcms\helpers\AssetConverter;

$bundles = null;

if (YII_CONSOLE) {
    $generatedConfig = @include(FAFCMS_CONFIG_PATH . '/generated.php');
    $bundles = $generatedConfig['assets']['frontend']['bundleReplacements']??null;
}

return [
    'basePath' => '@webroot/assets',
    'baseUrl' => '@web/assets',
    'hashCallback' => YII_ENV === 'dev' ? function ($path) {
        $mostRecentFileMTime = 0;

        $fileinfo = new SplFileInfo($path);

        if ($fileinfo->isFile()) {
            $mostRecentFileMTime = $fileinfo->getMTime();
        } else {
            $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::CHILD_FIRST);

            foreach ($iterator as $fileinfo) {
                if ($fileinfo->isFile() && $fileinfo->getMTime() > $mostRecentFileMTime) {
                    $mostRecentFileMTime = $fileinfo->getMTime();
                }
            }
        }

        return sprintf('%x', crc32((is_file($path) ? dirname($path) : $path) . $mostRecentFileMTime));
    }:null,
    'bundles' => $bundles,
    'converter' => [
        'class' => AssetConverter::class,
        'commands' => [
            'less' => ['css', (defined('PHP_PATH') ? PHP_PATH . ' ' : PHP_BINDIR . '/php ') . FAFCMS_VENDOR_PATH . '/wikimedia/less.php/bin/lessc --include-path={targetpath}' . PATH_SEPARATOR . FAFCMS_BASE_PATH.' {from} > {to}'],
            'scss' => ['css', (defined('PHP_PATH') ? PHP_PATH . ' ' : PHP_BINDIR . '/php ') . FAFCMS_VENDOR_PATH . '/scssphp/scssphp/bin/pscss --load-path {targetpath}' . PATH_SEPARATOR . FAFCMS_BASE_PATH.' < {from} > {to}'],
            'sass' => ['css', (defined('PHP_PATH') ? PHP_PATH . ' ' : PHP_BINDIR . '/php ') . FAFCMS_VENDOR_PATH . '/scssphp/scssphp/bin/pscss --load-path {targetpath}' . PATH_SEPARATOR . FAFCMS_BASE_PATH.' < {from} > {to}'],
            'styl' => ['css', 'stylus < {from} > {to}'],
            'coffee' => ['js', 'coffee -p {from} > {to}'],
            'ts' => ['js', 'tsc --out {to} {from}'],
        ]
    ],
];
