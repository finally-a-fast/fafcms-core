<?php
use yii\base\Event;
use yii\web\User;
use yii\queue\cli\Queue;
use yii\db\ActiveRecord;
use fafcms\fafcms\Module;
use yiiui\yii2flagiconcss\widget\FlagIcon;

if (Yii::$app->fafcms->getIsInstalled()) {
    Yii::$app->queue->on(Queue::EVENT_WORKER_LOOP, static function ($event) {
        if (!(Yii::$app->params['fafcmsQueueIsWebCronjob'] ?? false)) {
            Module::getLoadedModule()->setPluginSettingValue('last_real_cronjob', new DateTime());
        }
    });
}

function activeRecordAction($event) {
    if (($event->sender->fafcmsLogChange ?? false) === true && Yii::$app->fafcms->getIsInstalled() && method_exists(Yii::$app, 'getUser') && ($user = Yii::$app->getUser()->getIdentity()) !== null && $user->hasAttribute('last_change_id') && $user->hasAttribute('last_change_type')) {
        $dateTime = new DateTime('now', new DateTimeZone(Yii::$app->formatter->defaultTimeZone));

        $user->last_change_type = get_class($event->sender);

        if ($user->last_change_type === get_class($user)) {
            return;
        }

        if (isset($event->sender->id)) {
            $user->last_change_id = $event->sender->id;
        }

        $user->last_change_at = $dateTime->format('Y-m-d H:i:s');
        $user->save();
    }
};

Event::on(ActiveRecord::className(), ActiveRecord::EVENT_AFTER_UPDATE, static function ($event) {
    activeRecordAction($event);
});

Event::on(ActiveRecord::className(), ActiveRecord::EVENT_AFTER_INSERT, static function ($event) {
    activeRecordAction($event);
});

Event::on(User::className(), User::EVENT_AFTER_LOGIN, static function ($event) {
    if (Yii::$app->fafcms->getIsInstalled() && $event->identity->hasAttribute('last_login_at')) {
        Yii::$app->getSession()->set('last_login_at', $event->identity->last_login_at);

        $dateTime = new DateTime('now', new DateTimeZone(Yii::$app->formatter->defaultTimeZone));
        $event->identity->last_login_at = $dateTime->format('Y-m-d H:i:s');
        $event->identity->save();
    }
});

Event::on(User::className(), User::EVENT_AFTER_LOGOUT, static function ($event) {
    if (Yii::$app->fafcms->getIsInstalled() && $event->identity->hasAttribute('last_logout_at')) {
        $dateTime = new DateTime('now', new DateTimeZone(Yii::$app->formatter->defaultTimeZone));
        $event->identity->last_logout_at = $dateTime->format('Y-m-d H:i:s');
        $event->identity->save();
    }
});

Yii::$container->set(FlagIcon::class, [
    'tagName' => 'i',
    'options' => ['class' => 'icon']
]);
