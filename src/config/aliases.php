<?php
return [
    '@bower'    => '@vendor/bower-asset',
    '@npm'      => '@vendor/npm-asset',
    '@webroot'  => '@app/web',
    '@web'      => '/',
    '@project'  => FAFCMS_BASE_PATH,
    '@fafcmsCore' => FAFCMS_CORE_PATH
];
