<?php
return [
    'yii\\widgets\\MaskedInputAsset' => [
        'sourcePath' => '@vendor/robinherbots/jquery.inputmask/dist',
        'js' => [
            'jquery.inputmask.min.js',
        ]
    ],
    'yiiui\\yii2materialdesignicons\\MaterialDesignIconsAsset' => [
        'cssOptions' => [
            'fafcmsAsync' => true,
        ]
    ],
    'yiiui\\yii2overlayscrollbars\\assets\\OverlayScrollbarsAsset' => [
        'cssOptions' => [
            'fafcmsAsync' => true,
        ]
    ],
];
