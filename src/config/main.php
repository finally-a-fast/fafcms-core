<?php

defined('FAFCMS_NO_CONFIG') or define('FAFCMS_NO_CONFIG', false);
defined('YII_CONSOLE') or define('YII_CONSOLE', false);
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'prod');

$aliases = require(FAFCMS_CORE_PATH . '/config/aliases.php');
$assets = require(FAFCMS_CORE_PATH . '/config/assets.php');
$generatedConfig = @include(FAFCMS_CONFIG_PATH . '/generated.php');
$generatedConfig = $generatedConfig ?: [];
$installed = $generatedConfig['installed'] ?? false;

\fafcms\fafcms\controllers\InstallerController::checkConfig($installed, $generatedConfig);

$config = [
    'id' => 'fafcms',
    'language' => $generatedConfig['defaultLanguage'] ?? 'en',
    'name' => 'Finally a fast CMS',
    'vendorPath' => FAFCMS_VENDOR_PATH,
    'bootstrap' => [
        'log',
        'fafcms'
    ],
    'aliases' => $aliases,
    'basePath' => FAFCMS_BASE_PATH,
    'viewPath' => '@fafcmsCore/views',
    'controllerNamespace' => 'fafcms\fafcms\controllers',
    'components' => [
        'formatter' => [
            'class' => \yii\i18n\Formatter::class,
            'sizeFormatBase' => 1000 // todo create usersetting
        ],
        'fafcmsParser' => \fafcms\parser\component\Parser::class,
        'injector' => \fafcms\fafcms\components\InjectorComponent::class,
        'installer' => \fafcms\fafcms\components\InstallerComponent::class,
        'view' => [
            'class' => \fafcms\fafcms\components\ViewComponent::class,
        ],
        'cache' => $generatedConfig['cache'][YII_ENV] ?? null,
        'pluginCache' => $generatedConfig['pluginCache'][YII_ENV] ?? null,
        'translationCache' => $generatedConfig['translationCache'][YII_ENV] ?? null,
        'settingCache' => $generatedConfig['settingCache'][YII_ENV] ?? null,
        'fileCache' => $generatedConfig['fileCache'][YII_ENV] ?? null,
        'dataCache' => ([
            'class' => \fafcms\fafcms\components\DataCacheComponent::class
        ]),
        'urlManager' => [
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'normalizer' => [
                'class' => 'yii\web\UrlNormalizer',
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'enableSwiftMailerLogging' => true,
            'viewPath' => '@fafcmsCore/mail',
        ],
        'assetManager' => $assets,//not console
        'session' => (!YII_CONSOLE && $installed?[
            'class' => \yii\web\DbSession::class,
            'sessionTable' => 'session',
            'timeout' => 86400
        ] : null),
        'authManager' => [
            'class' => 'fafcms\helpers\RbacManager'
        ],
        'log' => $generatedConfig['log'][YII_ENV] ?? null,
        'db' => $generatedConfig['db'][YII_ENV] ?? null,
        'i18n' => [
            'class' => \fafcms\fafcms\components\I18NComponent::class,
        ],
    ],
];

if ($installed) {
    $config['bootstrap'][] = 'queue';

    $config['components']['queue'] = [
        'class' => \yii\queue\db\Queue::class,
        'mutex' => \yii\mutex\MysqlMutex::class,
        'as log' => \yii\queue\LogBehavior::class
    ];
}

if (YII_CONSOLE) {
    $config['bootstrap'][] = 'updater';
    $config['modules']['updater'] = [
        'class' => \fafcms\updater\Module::class,
        'updatePath' => '@app/updates',
        'updateNamespace' => 'app\updates',
        'tableName' => '{{%fafcms-update}}',
    ];
} else {
    $config['components']['request'] = [
        'class' => \fafcms\helpers\Request::class,
        'cookieValidationKey' => $generatedConfig['cookieValidationKey'],
        'noCsrfValidationRoutes' => [
            'autodiscover/autodiscover.xml',
            'AutoDiscover/autodiscover.xml',
        ]
    ];

    $config['components']['errorHandler'] = [
        'errorAction' => ($installed ? 'main/error' : 'installer/error')
    ];

//    if ($installed) {
    $config['components']['user'] = [
        'class' => \fafcms\fafcms\components\UserComponent::class,
    ];
//    }

    if (YII_DEBUG) {
        $config['bootstrap'][] = 'debug';
        $config['modules']['debug'] = [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['*'],
            'traceLine' => '<a href="phpstorm://open?file={file}&line={line}">{file}:{line}</a>',
        ];
    }
}

return $config;
