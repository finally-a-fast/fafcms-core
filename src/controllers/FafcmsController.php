<?php
/**
 * @author    Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license   https://www.finally-a-fast.com/packages/fafcms-helpers/license MIT
 * @link      https://www.finally-a-fast.com/packages/fafcms-helpers
 * @see       https://www.finally-a-fast.com/packages/fafcms-helpers/docs Documentation of fafcms-helpers
 * @since     File available since Release 1.0.0
 */

namespace fafcms\fafcms\controllers;

use fafcms\fafcms\components\InjectorComponent;

use fafcms\fafcms\helpers\StringHelper;
use fafcms\fafcms\items\Html as HtmlItem;
use fafcms\fafcms\models\Projectlanguage;
use Yii;

use yii\base\{InvalidCallException, InvalidConfigException, Model};

use yii\console\Controller;
use yii\web\{
    ForbiddenHttpException,
    Response
};

use yii\db\Expression;
use yii\helpers\Html;

/**
 * Class FafcmsController
 *
 * @package fafcms\fafcms\controllers
 */
class FafcmsController extends Controller
{
    public static $modelClass;

    /**
     * @param      $modelClass
     * @param      $sourceProjectLanguageId
     * @param      $targetProjectLanguageId
     * @param null $ids
     *
     * @throws \Throwable
     */
    public function actionTranslate($modelClass, $sourceProjectLanguageId, $targetProjectLanguageId, $ids = null)
    {
        $authModelClass  = $modelClass::$authModelClass ?? $modelClass;
        $translations = [];

        if ($ids !== null) {//TODO change to make it possible to handle multiple ids
            $model = $modelClass::find()->where(['id' => $ids])->one();
            $translations = $model->getTranslations()->select('projectlanguage_id')->column();
        }

        $projectLanguage = Projectlanguage::find()->where(['id' => $targetProjectLanguageId])->one();

        /**
         * @var $models ActiveRecord[]
         */
        $modelsQuery = $modelClass::find()
            ->where([
                'AND',
                ['projectlanguage_id' => $sourceProjectLanguageId],
                [
                    '=',
                    $modelClass::find()
                        ->alias('translations')
                        ->select('COUNT(*)')
                        ->where([
                            'translations.translation_base_id' => new Expression($modelClass::tableName() . '.id'),
                            'translations.projectlanguage_id' => $targetProjectLanguageId
                        ]),
                    0
                ]
            ]);

        if ($ids !== null) {
            $modelsQuery->andWhere(['id' => explode(',', $ids)]);
        }

        $models = $modelsQuery->orderBy(['id' => SORT_DESC])->all();

        if (!$this->confirm('Found ' . count($models) . ' records to translate. Translate these?')) {
            $this->stdout('Canceled');
            Yii::$app->end();
        }

        foreach ($models as $model) {
            try {
                $translationResult = $model->translate($targetProjectLanguageId);

                if ($translationResult['success']) {
                    $type = 'success';
                    $modelLabel = $model->getExtendedLabel(false);

                    if (!empty($modelLabel)) {
                        $message = Yii::t('fafcms-core', '{modelClass} "{modelLabel}" has been tranlated to "{language}".', [
                            'modelClass' => $model->getEditData()['singular'],
                            'modelLabel' => $modelLabel,
                            'language' => Projectlanguage::extendedLabel($projectLanguage, false),
                        ]);
                    } else {
                        $message = Yii::t('fafcms-core', '{modelClass} has been tranlated to "{language}".', [
                            'modelClass' => $model->getEditData()['singular'],
                            'language' => Projectlanguage::extendedLabel($projectLanguage, false),
                        ]);
                    }

                    $this->stdout($type . ': ' . $message . PHP_EOL . PHP_EOL);
                } else {
                    $type = 'error';
                    $modelLabel = $model->getExtendedLabel(false);

                    if (!empty($modelLabel)) {
                        $message = Yii::t('fafcms-core', 'Error while saving {modelClass} "{modelLabel}"!', [
                            'modelClass' => $model->getEditData()['singular'],
                            'modelLabel' => $modelLabel
                        ]);
                    } else {
                        $message = Yii::t('fafcms-core', 'Error while saving {modelClass}!', [
                            'modelClass' => $model->getEditData()['singular'],
                        ]);
                    }

                    $message = $message . '<br><br>' . implode(PHP_EOL . PHP_EOL, $translationResult['errors']);

                    $this->stderr($type . ': ' . $message . PHP_EOL . PHP_EOL);
                }
            } catch (\Exception $e) {
                $this->stderr('Error at model id: ' . $model->id);
                throw $e;
            }
        }
    }
}
