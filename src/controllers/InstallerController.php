<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @author Stefan Brandenburger <stefanbrandenburger6@gmail.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 * @since File available since Release 1.0.0
 */

namespace fafcms\fafcms\controllers;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\models\Domain;
use fafcms\fafcms\models\InstallerForm;
use fafcms\fafcms\models\InstallerPluginForm;
use fafcms\fafcms\models\Language;
use fafcms\fafcms\models\Project;
use fafcms\fafcms\models\Projectlanguage;
use fafcms\fafcms\models\SettingForm;
use fafcms\fafcms\models\User;
use fafcms\settingmanager\models\Setting;
use Yii;
use yii\base\ErrorException;
use yii\console\controllers\MigrateController;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\log\Logger;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Class InstallerController
 * @package fafcms\fafcms\controllers
 */
class InstallerController extends Controller
{
    public function behaviors(): array
    {
        if (!Yii::$app->fafcms->getIsInstalled()) {
            return [];
        }

        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string|Response
     * @throws ErrorException
     */
    public function actionIndex()
    {
        if (!Yii::$app->fafcms->getIsInstalled()) {
            return $this->redirect(['setup']);
        }

        if (Yii::$app->getRequest()->get('showDependencies') !== null) {
            $params = Yii::$app->getRequest()->getQueryParams();
            unset($params['showDependencies']);
            Yii::$app->session->set('installerShowDependencies', (bool)Yii::$app->getRequest()->get('showDependencies'));
            return $this->redirect(Url::to(array_merge([''], $params)));
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => Yii::$app->installer->getInstalledPackages(),
            'pagination' => [
                'defaultPageSize' => 99999
            ],
            'sort' => [
                'attributes' => [
                    'name'
                ],
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ]
            ],
        ]);

        $this->view->params['breadcrumbs'][] = [
            'label' => Yii::t('fafcms-core', 'Modules'),
            'url' => ['index']
        ];

        $this->view->title = Yii::t('fafcms-core', 'Modules');

        $buttons = [[
            'icon' => 'arrow-left-bold-outline',
            'label' => Yii::t('fafcms-core',  'Back'),
            'url' => Yii::$app->getUser()->getReturnUrl()
        ]];

        Yii::$app->view->addNavigationItems(FafcmsComponent::NAVIGATION_ACTION_BAR, $buttons);

        $model = new InstallerPluginForm();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * @param string $id
     * @return string
     * @throws ErrorException
     * @throws NotFoundHttpException
     */
    public function actionModuleDetails(string $id): string
    {
        $installedPlugins = Yii::$app->installer->getInstalledPackages();

        if (isset($installedPlugins[$id], $installedPlugins[$id]['extra']->bootstrap)) {
            Yii::$app->view->params['pluginId'] = $id;
            return $this->renderContent(Yii::$app->view->renderView('fafcms\\Plugin'));
        }

        throw new NotFoundHttpException('Module '.$id.' doesn\'t exist');
    }

    /**
     * @param string $id
     * @return string
     * @throws ErrorException
     * @throws NotFoundHttpException
     */
    public function actionModuleSettings(string $id): string
    {
        $installedPlugins = Yii::$app->installer->getInstalledPackages();

        if (isset($installedPlugins[$id]['extra']['bootstrap'])) {
            $plugin = $installedPlugins[$id];

            $model = new SettingForm($plugin['settings'], $plugin['settingRules']);
            $title = Yii::t('fafcms-core', '{moduleName} module settings', ['moduleName' => $plugin['name']]);
            $this->view->title[] = $title;

            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate() && $model->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('fafcms-core', '{modelClass} has been saved.', [
                        'modelClass' => Setting::instance()->getEditData()['singular'],
                    ]));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'Error while saving {modelClass}!', [
                        'modelClass' => Setting::instance()->getEditData()['singular'],
                    ]).'<br><br>'.implode('<br><br>', $model->getErrorSummary(true)));
                }
            }

            $buttons = function($model, $form) {
                return [[
                    'type' => 'secondary',
                    'icon' => 'arrow-left-bold-outline',
                    'label' => Yii::t('fafcms-core',  'Back'),
                    'url' => Yii::$app->getUser()->getReturnUrl()
                ], [
                    'form' => $form->id,
                    'options' => [
                        'class' => ['primary'],
                    ],
                    'type' => 'submit',
                    'icon' => 'content-save-outline',
                    'label' =>  Yii::t('fafcms-core', 'Save {modelClass}', [
                        'modelClass' => Setting::instance()->getEditData()['singular'],
                    ]),
                ]];
            };

            return $this->render('@fafcms/fafcms/views/common/edit', [
                'model' => $model,
                'formOptions' => ['enableClientValidation' => false],
                'content' => function ($model, $form) use ($title, $plugin) {
                    return $this->renderPartial('setting', [
                        'model' => $model,
                        'form' => $form,
                        'title' => $title,
                        'plugin' => $plugin
                    ]);
                },
                'buttons' => $buttons,
                'isWidget' => false
            ]);
        }

        throw new NotFoundHttpException('Module '.$id.' doesn\'t exist');
    }

    /**
     * @param string|null $id
     * @return array
     * @throws ErrorException
     * @throws \yii\base\Exception
     */
    public function actionModuleOutdated(string $id = null): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($id !== null && !isset($installedPackages[$id])) {
            throw new ErrorException('Module \''.htmlentities($id).'\' is not installed');
        }

        return '';
        return $this->runAsync('outdated', '', true, 600);
    }

    /**
     * @param string $id
     * @return array
     * @throws ErrorException
     * @throws \yii\base\Exception
     */
    public function actionModuleInstall(string $id): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $installedPackages = Yii::$app->installer->getInstalledPackages();

        if (isset($installedPackages[$id])) {
            throw new ErrorException('Module \''.htmlentities($id).'\' is already installed');
        }

        return $this->runAsync('require', $id);
    }

    /**
     * @param string $id
     * @return array
     * @throws ErrorException
     * @throws \yii\base\Exception
     */
    public function actionModuleUpdate(string $id): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $installedPackages = Yii::$app->installer->getInstalledPackages();

        if (!isset($installedPackages[$id])) {
            throw new ErrorException('Module \''.htmlentities($id).'\' is not installed');
        }

        return $this->runAsync('update '.$id);
    }

    /**
     * @param string $action
     * @param string $id
     * @param bool $json
     * @return array
     * @throws ErrorException
     * @throws \yii\base\Exception
     */
    public function runAsync(string $action, string $id = '', bool $json = false, $cache = null): array
    {
        if (!FileHelper::createDirectory(FAFCMS_BASE_PATH.'/runtime/composer')) {
            throw new ErrorException('Cannot create the folder \''.FAFCMS_BASE_PATH.'/runtime/composer'.'\'.');
        }

        $actionId = Inflector::slug($action.' '.$id);
        $actionFile = FAFCMS_BASE_PATH.'/runtime/composer/success-'.$actionId.'.txt';

        if (file_exists($actionFile)) {
            $success = file_get_contents($actionFile);
            $done = substr($success, -12) === 'FAFCMS-DONE'."\n";

            if ($done) {
                unlink($actionFile);
                $result = substr($success, 0, -12);

                if ($cache !== null) {
                    Yii::$app->cache->set('composer-action-'.$actionId, $result, $cache);
                }

                return ['status' => 'done', 'result' => $result];
            }

            return ['status' => 'running'];
        }

        if ($cache !== null) {
            $cacheData = Yii::$app->cache->get('composer-action-'.$actionId);

            if ($cacheData !== false) {
                return ['status' => 'cached', 'result' => $cacheData];
            }
        }

        $command = 'export COMPOSER_HOME='.FAFCMS_BASE_PATH.'/.composer'.
            ' && php composer.phar selfupdate --no-ansi --no-progress --no-interaction --quiet'.
            ' && php composer.phar '.$action.' '.($id !== ''?escapeshellarg($id):'').' --no-ansi --no-interaction '.($json?'--format=json':'').
            ' && echo "FAFCMS-DONE"'.
            ' &';

        $descriptor = [
            1 => ['file', FAFCMS_BASE_PATH.'/runtime/composer/success-'.$actionId.'.txt', 'a'],
            2 => ['file', FAFCMS_BASE_PATH.'/runtime/composer/error-'.$actionId.'.txt', 'a'],
        ];

        $pipes = [];
        $proc = proc_open($command, $descriptor, $pipes, FAFCMS_BASE_PATH);

        foreach ($pipes as $pipe) {
            fclose($pipe);
        }

        $status = proc_close($proc);

        if ($status !== 0) {
            throw new ErrorException('Cannot run \''.htmlentities($action.' '.$id).'\'.');
        }

        return [
            'status' => 'started'
        ];
    }

    /**
     * @return array
     */
    private function getLoadedPlugins(): array
    {
        return array_filter(Yii::$app->fafcms->loadedPlugins, function ($plugin) {
            return !($plugin['core']??false);
        });
    }

    /**
     * @return array
     */
    private function getCorePlugins(): array
    {
        return array_filter(Yii::$app->fafcms->loadedPlugins, function ($plugin) {
            return ($plugin['core']??false);
        });
    }

    /**
     * @param $id
     * @return Response
     * @throws ErrorException
     * @throws NotFoundHttpException
     */
    public function actionModuleEnable($id)
    {
        $installedPlugins = Yii::$app->installer->getInstalledPackages();

        if (isset($installedPlugins[$id], $installedPlugins[$id]['extra']->bootstrap)) {
            $bootstrap = $installedPlugins[$id]['extra']->bootstrap;
            $module = substr($bootstrap, 0, strrpos($bootstrap, '\\')).'\Module';

            $loadedPlugins = $this->getLoadedPlugins();
            $corePlugins = $this->getCorePlugins();

            if (isset($corePlugins[$id])) {
                throw new ForbiddenHttpException('Core module "'.$id.'" is always enabled.');
            }

            $loadedPlugins[$id] = [
                'module' => $module,
                'bootstrap' => $bootstrap
            ];

            if (InstallerController::writeConfig($loadedPlugins, FAFCMS_CONFIG_PATH . '/plugins.php')) {
                Yii::$app->session->setFlash('success', Yii::t('fafcms-core', '{module} has been enabled.', [
                    'module' => $id,
                ]));

                return $this->redirect(['index']);
            }
        }

        throw new NotFoundHttpException('Module "'.$id.'" doesn\'t exist');
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionModuleDisable($id)
    {
        //TODO use fafcms unload function
        $loadedPlugins = $this->getLoadedPlugins();

        if (isset($loadedPlugins[$id])) {
            unset($loadedPlugins[$id]);

            if (InstallerController::writeConfig($loadedPlugins, FAFCMS_CONFIG_PATH . '/plugins.php')) {
                Yii::$app->session->setFlash('success', Yii::t('fafcms-core', '{module} has been disabled.', [
                    'module' => $id,
                ]));

                return $this->redirect(['index']);
            }
        }

        throw new NotFoundHttpException('Module '.$id.' doesn\'t exist');
    }

    public static function updateDatabase()
    {
        if (!defined('STDOUT')) {
            define('STDOUT', fopen('/tmp/stdout', 'w'));
        }

        ob_start();
        PHP_VERSION_ID >= 80000 ? ob_implicit_flush(false) : ob_implicit_flush(0);

        $migrations = ArrayHelper::getColumn(Yii::$app->params['packages'], 'migrations');

        $consoleController = new MigrateController(Yii::$app->controller->id, Yii::$app, [
            'migrationNamespaces' => $migrations,
        ]);

        $action = Yii::$app->request->get('action', 'list');

        if ($action === 'list') {
            $consoleController->runAction('new', ['all']);
        } else if ($action === 'update') {
            $consoleController->runAction('up', ['interactive'  => false]);
        } else if ($action === 'downgrade') {
            $consoleController->runAction('down', ['interactive' => false]);
        }

        $handle = fopen('/tmp/stdout', 'r');
        $message = '';

        while (($buffer = fgets($handle, 4096)) !== false) {
            $message .= $buffer . "\n";
        }

        fclose($handle);

        return ob_get_clean().$message;
    }

    //region setup
    public static function steps(): array
    {
        return [
            0 => [
                'icon' => 'translate',
                'title'  => InstallerForm::instance()->attributeLabels()['language'],
                'action' => 'step0',
                'view'   => 'step_0',
            ],
            1 => [
                'icon' => 'numeric-2-circle-outline',
                'title'  => Yii::t('fafcms-core-setup', 'system_check'),
                'action' => 'step1',
                'view'   => 'step_1',
            ],
            2 => [
                'icon' => 'numeric-3-circle-outline',
                'title'  => Yii::t('fafcms-core-setup', 'database'),
                'action' => 'step2',
                'view'   => 'step_2',
            ],
            3 => [
                'icon' => 'numeric-4-circle-outline',
                'title'  => Yii::t('fafcms-core-setup', 'Migrations'),
                'action' => 'step3',
                'view'   => 'step_3',
            ],
            4 => [
                'icon' => 'numeric-5-circle-outline',
                'title'  => Yii::t('fafcms-core-setup', 'admin'),
                'action' => 'step4',
                'view'   => 'step_4',
            ],
            5 => [
                'icon' => 'numeric-6-circle-outline',
                'title'  => Yii::t('fafcms-core-setup', 'Settings'),
                'action' => 'step5',
                'view'   => 'step_5',
            ],
            6 => [
                'icon' => 'numeric-7-circle-outline',
                'title'  => Yii::t('fafcms-core-setup', 'Demo Data'),
                'action' => 'step6',
                'view'   => 'step_6',
            ],
        ];
    }

    public static function getStepCount()
    {
        return count(static::steps());
    }

    public function actions(): array
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => '//main/backend-error'
            ],
        ];
    }

    public function actionSetup()
    {
        $this->view->title = [Yii::t('fafcms-core-setup', 'setup')];
        $model = new InstallerForm();
        $currentStepNumber = (int)Yii::$app->getRequest()->get('step', 0);

        if ($currentStepNumber < 0) {
            return $this->redirect(['', 'step' => 0]);
        }

        $lastStep = Yii::$app->getRequest()->getCookies()->getValue('lastStep', 0);

        if ($currentStepNumber > $lastStep + (Yii::$app->request->isPost?1:0)) {
            return $this->redirect(['', 'step' => $lastStep]);
        }

        $steps = self::steps();
        $currentStep = $steps[$currentStepNumber];
        $currentStep['number'] = $currentStepNumber;
        $this->view->title[] = $currentStep['title']??$currentStepNumber;

        for ($i = 0; $i < $currentStepNumber + 1; $i++) {
            $items[] = [
                'icon' => $steps[$i]['icon'],
                'visible' => true,
                'title' => $steps[$i]['title']??$i,
                'href' => ['', 'step' => $i]
            ];
        }

        Yii::$app->fafcms->addSidebarItems('install', [
            'page' => [
                'type' => 'page'
            ],
            'install' => [
                'title' => Yii::t('fafcms-core-setup', 'setup'),
                'items' => $items ?? [],
            ]
        ]);

        $model->setScenario('step_' . $currentStepNumber);
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($model->validate()) {
                $actionName = $steps[$currentStepNumber]['action'];
                $result = $this->$actionName($model);

                if ($result instanceof Response) {
                    return $result;
                }

                if ($result) {
                    Yii::$app->getResponse()->getCookies()->add(new Cookie([
                        'name'  => 'lastStep',
                        'value' => $currentStepNumber + 1,
                    ]));

                    return $this->redirect(['', 'step' => $currentStepNumber + 1]);
                }
            }

            Yii::$app->session->setFlash('error', Yii::t('fafcms-core-setup', 'An error happened!').'<br><br>'.implode('<br><br>', $model->getErrorSummary(true)));
        }

        $buttons = static function($model, $form) use ($currentStep) {
            return [[
                'url' => ['', 'step' => $currentStep['number'] - 1],
                'visible' => $currentStep['number'] > 0,
                'icon' => 'chevron-left',
                'label' => Yii::t('fafcms-core', 'back'),
            ], [
                'type' => 'submit',
                'icon' => 'chevron-right right',
                'label' => Yii::t('fafcms-core', 'next'),
                'options' => ['class' => 'primary']
            ]];
        };

        return $this->render('@fafcms/fafcms/views/common/edit', [
            'formOptions' => [
                'id' => strtolower($model->formName()).'-form',
                'action' => ['', 'step' => $currentStep['number']],
            ],
            'model' => $model,
            'content' => function ($model, $form) use ($buttons, $currentStep) {
                if (isset($buttons) && $buttons instanceof \Closure) {
                    $buttons = $buttons($model, $form);
                }

                return $this->renderPartial('setup', [
                    'model' => $model,
                    'form' => $form,
                    'currentStep' => $currentStep,
                    'buttons' => $buttons
                ]);
            },
            'buttons' => $buttons,
            'buttonCard' => false,
        ]);
    }

    private function step0($model): bool
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name'  => 'language',
            'value' => $model->language
        ]));

        Yii::$app->language = $model->language;

        return true;
    }

    private function step1(InstallerForm $model): bool
    {
        //$generatedConfig = @include(FAFCMS_BASE_PATH . '/config/generated.php');
        //        \fafcms\fafcms\controllers\InstallerController::writeConfig($generatedConfig, FAFCMS_BASE_PATH . '/config/generated.php');
        // var_dump('1');die();
        return true;
    }

    /**
     * @param InstallerForm $model
     *
     * @return bool|Response
     */
    private function step2(InstallerForm $model)
    {
        $generatedConfig = include(FAFCMS_BASE_PATH . '/config/generated.php');

        $db = [
            'class'       => 'yii\db\Connection',
            'dsn'         => ($model->databaseAdvanced ? $model->databaseDsn : $model->databaseType . ':host=' . $model->databaseHost . ';dbname=' . $model->databaseName),
            'tablePrefix' => $model->databaseTablePrefix,
            'username'    => $model->databaseUser,
            'password'    => $model->databasePassword,
            'charset'     => 'utf8',
        ];

        $generatedConfig['db'] = [
            'prod' => array_merge($db, [
                'enableLogging'       => false,
                'enableProfiling'     => false,
                'enableSchemaCache'   => true,
                'schemaCacheDuration' => 604800,
                'schemaCache'         => 'cache',
                'enableQueryCache'    => true,
                'queryCacheDuration'  => 86400,
                'queryCache'          => 'cache',
            ]),
            'dev'  => $db,
            'test' => $db
        ];

        $success = self::writeConfig($generatedConfig, FAFCMS_BASE_PATH . '/config/generated.php');

        if (!$success) {
            return false;
        }

        if (!$model->run_migrations) {
            Yii::$app->getResponse()->getCookies()->add(new Cookie([
                'name'  => 'lastStep',
                'value' => 4,
            ]));

            return $this->redirect(['', 'step' => 4]);
        }

        return true;
    }

    private function step3(InstallerForm $model): bool
    {
        if ($model->run_migrations) {
            $output = '';
            $exitCode = 0;
            exec('../fafcms migrate/up --interactive=0', $output, $exitCode);

            if ($exitCode !== 0) {
                Yii::error($output, __METHOD__);
                $model->addError('migrations', 'Error while migrating. See logs for further information');
                return false;
            }
        }

        return true;
    }

    private function step4(InstallerForm $model): bool
    {
        $generatedConfig = @include(FAFCMS_BASE_PATH . '/config/generated.php');
        $generatedConfig['backendUrl'] = $model->backendUrl;

        $userClass = InjectorComponent::getClass(User::class);
        $admin     = new $userClass([
            'status' => $userClass::STATUS_ACTIVE,
            'email'  => $model->adminEmail,
        ]);

        $admin->setPassword($model->adminPassword);
        $admin->generateAuthKey();

        if (!$admin->save()) {
            $model->addError('adminUser', '<br><br>'.implode('<br><br>', $model->getErrorSummary(true)));
            return false;
        }

        Yii::$app->getAuthManager()->assign(
            Yii::$app->getAuthManager()->getRole('admin'),
            $admin->getPrimaryKey()
        );

        if (self::writeConfig($generatedConfig, FAFCMS_BASE_PATH . '/config/generated.php')) {
            return true;
        }

        return false;
    }

    /**
     * @param InstallerForm $model
     *
     * @return bool
     */
    private function step5(InstallerForm $model): bool
    {
        $projectClass         = InjectorComponent::getClass(Project::class);
        $projectLanguageClass = InjectorComponent::getClass(Projectlanguage::class);
        $domainClass          = InjectorComponent::getClass(Domain::class);

        $project = new Project([
            'status'      => $projectClass::STATUS_ACTIVE,
            'name'        => $model->name,
            'description' => $model->projectDescription,
        ]);

        if (!$project->save()) {
            $model->addError('name', '<br><br>'.implode('<br><br>', $project->getErrorSummary(true)));
            return false;
        }

        $domain = new $domainClass([
            'status'     => $domainClass::STATUS_ACTIVE,
            'domain'     => $model->domain,
            'project_id' => $project->getPrimaryKey(),
        ]);

        if (!$domain->save()) {
            $model->addError('domain', '<br><br>'.implode('<br><br>', $domain->getErrorSummary(true)));
            return false;
        }

        $language = InjectorComponent::getClass(Language::class)::find()->where(['ietfcode' => Yii::$app->language])->one();

        $projectLanguage = new $projectLanguageClass([
            'status'      => $projectLanguageClass::STATUS_ACTIVE,
            'name'        => $language->name,
            'path'        => '', //todo
            'project_id'  => $project->getPrimaryKey(),
            'domain_id'   => $domain->getPrimaryKey(),
            'language_id' => $language->getPrimaryKey(),
        ]);

        if (!$projectLanguage->save()) {
            $model->addError('language', '<br><br>'.implode('<br><br>', $projectLanguage->getErrorSummary(true)));
            return false;
        }

        return true;
    }

    private function step6(InstallerForm $model)
    {
        if ($model) {

        }

        $generatedConfig = include(FAFCMS_BASE_PATH . '/config/generated.php');
        $generatedConfig['installed'] = true;

        if (self::writeConfig($generatedConfig, FAFCMS_BASE_PATH . '/config/generated.php')) {
            return $this->redirect([$generatedConfig['backendUrl'] . '/login']);
        }

        return false;
    }

    /**
     * @param array $config
     * @param string $file
     * @return bool
     *
     * Export array formatter by steven at nevvix dot com http://php.net/manual/de/function.var-export.php#122853
     */
    public static function writeConfig(array $config, string $file): bool
    {
        if (FAFCMS_NO_CONFIG) {
            return false;
        }

        $export = var_export($config, true);
        $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);
        $array = preg_split("/\r\n|\n|\r/", $export);
        $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [null, ']$1', ' => ['], $array);
        $export = implode(PHP_EOL, array_filter(["["] + $array));

        $configFile = fopen($file, 'w');
        return fwrite($configFile, '<?php'.PHP_EOL.'return '.$export.';'.PHP_EOL) !== false && fclose($configFile);
    }

    /**
     * @param bool $installed
     * @param array $generatedConfig
     * @throws \Exception
     */
    public static function checkConfig(bool $installed, array &$generatedConfig): void
    {
        $writeConfig = false;

        if (!$installed) {
            if (!file_exists(FAFCMS_CONFIG_PATH . '/generated.php')) {
                $generatedConfig = ['installed' => false];
                $writeConfig = true;
            }

            if (empty($generatedConfig['cookieValidationKey'])) {
                if (!extension_loaded('openssl')) {
                    throw new \Exception('The OpenSSL PHP extension is required by FAFCMS.');
                }

                $length = 32;
                $bytes = openssl_random_pseudo_bytes($length);
                $generatedConfig['cookieValidationKey'] = strtr(substr(base64_encode($bytes), 0, $length), '+/=', '_-.');
                $writeConfig = true;
            }
        }

        if (!isset($generatedConfig['cache']) || !is_array($generatedConfig['cache'])) {
            $cacheId = md5(random_int(0, 1000000000));

            $generatedConfig['cache'] = [
                'dev' => [
                    'class' => 'yii\\caching\\FileCache',
                    'keyPrefix' => $cacheId
                ],
                'prod' => [
                    'class' => 'yii\\caching\\FileCache',
                    'keyPrefix' => $cacheId
                ]
            ];

            $writeConfig = true;
        }

        if (!isset($generatedConfig['pluginCache']) || !is_array($generatedConfig['pluginCache'])) {
            $cacheId = md5(random_int(0, 1000000000));

            $generatedConfig['pluginCache'] = [
                'dev' => [
                    'class' => 'yii\\caching\\FileCache',
                    'keyPrefix' => $cacheId
                ],
                'prod' => [
                    'class' => 'yii\\caching\\FileCache',
                    'keyPrefix' => $cacheId
                ]
            ];

            $writeConfig = true;
        }

        if (!isset($generatedConfig['fileCache']) || !is_array($generatedConfig['fileCache'])) {
            $cacheId = md5(random_int(0, 1000000000));

            $generatedConfig['fileCache'] = [
                'dev' => [
                    'class' => 'yii\\caching\\FileCache',
                    'keyPrefix' => $cacheId
                ],
                'prod' => [
                    'class' => 'yii\\caching\\FileCache',
                    'keyPrefix' => $cacheId
                ]
            ];

            $writeConfig = true;
        }

        if (!isset($generatedConfig['translationCache']) || !is_array($generatedConfig['translationCache'])) {
            $cacheId = md5(random_int(0, 1000000000));

            $generatedConfig['translationCache'] = [
                'dev' => [
                    'class' => 'yii\\caching\\FileCache',
                    'keyPrefix' => $cacheId
                ],
                'prod' => [
                    'class' => 'yii\\caching\\FileCache',
                    'keyPrefix' => $cacheId
                ]
            ];

            $writeConfig = true;
        }

        if (!isset($generatedConfig['settingCache']) || !is_array($generatedConfig['settingCache'])) {
            $cacheId = md5(random_int(0, 1000000000));

            $generatedConfig['settingCache'] = [
                'dev' => [
                    'class' => 'yii\\caching\\FileCache',
                    'keyPrefix' => $cacheId
                ],
                'prod' => [
                    'class' => 'yii\\caching\\FileCache',
                    'keyPrefix' => $cacheId
                ]
            ];

            $writeConfig = true;
        }

        if (!isset($generatedConfig['log']) || !is_array($generatedConfig['log'])) {
            $generatedConfig['log'] = [
                'prod' => [
                    'traceLevel' => 3,
                    'targets' => [[
                        'class' => 'yii\\log\\FileTarget',
                        'levels' => ['error', 'warning'],
                    ]],
                ],
                'dev' => [
                    'traceLevel' => 6,
                    'targets' => [[
                        'class' => 'yii\\log\\FileTarget',
                        'levels' => ['error', 'warning'],
                    ]],
                ],
                'test' => [
                    'traceLevel' => 6,
                    'targets' => [[
                        'class' => 'yii\\log\\FileTarget',
                        'levels' => ['error', 'warning'],
                    ]]
                ]
            ];

            $writeConfig = true;
        }

        if (!isset($generatedConfig['backendUrl']) || empty($generatedConfig['backendUrl'])) {
            $generatedConfig['backendUrl'] = 'backend';
            $writeConfig = true;
        }

        if (!isset($generatedConfig['apiUrl']) || empty($generatedConfig['apiUrl'])) {
            $generatedConfig['apiUrl'] = 'api';
            $writeConfig = true;
        }

        if (!isset($generatedConfig['defaultMigrationPath']) || empty($generatedConfig['defaultMigrationPath'])) {
            $generatedConfig['defaultMigrationPath'] = '@app/migrations';
            $writeConfig = true;
        }

        if ($writeConfig) {
            self::writeConfig($generatedConfig, FAFCMS_CONFIG_PATH . '/generated.php');
        }
    }
    //endregion setup
}
