<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 * @since File available since Release 1.0.0
 */

namespace fafcms\fafcms\controllers;

use fafcms\fafcms\models\Domain;
use fafcms\helpers\DefaultController;

/**
 * Class DomainController
 *
 * @package fafcms\fafcms\controllers
 */
class DomainController extends DefaultController
{
    public static $modelClass = Domain::class;
}
