<?php

namespace fafcms\fafcms\controllers;

use fafcms\filemanager\controllers\FilemanagerController;
use fafcms\filemanager\models\File;
use fafcms\filemanager\Module;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\sitemanager\models\Layout;
use fafcms\sitemanager\models\Site;
use fafcms\parser\component\Parser;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use DOMElement;
use DOMDocument;
use DateTime;

/**
 * Frontend controller
 */
class FrontendController extends Controller
{
    public function behaviors()
    {
        return [[
                'class' => 'yii\filters\PageCache',
                'enabled' => false,
                'duration' => 60,
                'variations' => [
                    Yii::$app->request->get('site')
                ],
                /*'dependency' => [
                    'class' => 'yii\caching\DbDependency',
                    'sql' => 'SELECT COUNT(*) FROM post',
                ],*/
            ],/* [
                'class' => 'yii\filters\HttpCache',
                'only' => ['index'],
                'lastModified' => function ($action, $params) {
                    $q = new \yii\db\Query();
                    return $q->from('post')->max('updated_at');
                },
            ],*/
        ];
    }

    public function actions()
    {
        $captchaOptions = [
            'class' => 'yii\captcha\CaptchaAction',
            'imageLibrary' => 'imagick',
            'transparent' => true,
            'foreColor' => 0x000000,
            'minLength' => 8,
            'maxLength' => 8,
            'padding' => 0,
            'width' => 150,
        ];

        if (Module::getLoadedModule()->getPluginSettingValue('image_manipulation_library') === 3) {
            $captchaOptions['imageLibrary'] = 'gd';
        }

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => '//main/frontend-error'
            ],
            'captcha' => $captchaOptions,
        ];
    }

    public static function getSiteContent(int $id, $data = null)
    {
        Yii::$app->fafcms->setCurrentPageId($id);

        $content = Site::getContentById($id);

        if ($content === null) {
            return null;
        }

        Yii::$app->fafcmsParser->data['currentSite'] = static function () use ($id) {
            return Site::find()->where(['id' => $id])->one();
        };

        Yii::$app->fafcms->prepareRender();

        if ($data === null) {
            $data = Yii::$app->fafcmsParser->data;
        }

        return Yii::$app->fafcmsParser->parse(Parser::TYPE_PAGE, $content, Parser::ROOT, $data);
    }

    public function actionView($site = '')
    {
        $contentmeta = self::getContentmeta($this, $site);

        if ($contentmeta instanceof Response) {
            return $contentmeta;
        }

        $contentOptions = Yii::$app->fafcms->getContentClass($contentmeta['model_class']);

        if ($contentOptions === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $contentModel = self::getContentModel($contentmeta, $contentOptions);

        self::setCurrentContentMeta($contentmeta, $contentOptions['name'], $contentModel);
        self::setSiteMeta($contentmeta);

        $id = self::getSiteId($contentModel, $contentOptions);
        $siteContent = self::getSiteContent($id);
        $renderMode = Yii::$app->view->getRenderMode();

        self::setOpenGraph($contentmeta);
        return Yii::$app->view->renderSiteContent($this, $renderMode, $siteContent);
    }

    /**
     * @param Contentmeta $contentmeta
     * @param array $contentOptions
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    public static function getContentModel(Contentmeta $contentmeta, array $contentOptions): ActiveRecord
    {
        $contentModel = $contentmeta['model_class']::find()->where([$contentOptions['id'] => $contentmeta['model_id']])->one();

        if ($contentModel === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $contentModel;
    }

    /**
     * @param Controller $controller
     * @param string     $site
     * @param bool       $ignoreType
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public static function getContentmeta(Controller $controller, string $site, bool $ignoreType = false)
    {
        $contentmetaId = Contentmeta::getIdByUrl($site, false, $ignoreType);
        $contentmeta = null;

        if ($contentmetaId !== null) {
            $contentmeta = Contentmeta::find()->andWhere(['id' => $contentmetaId]);

            if ($ignoreType) {
                $contentmeta = $contentmeta->one();
            } else {
                $contentmeta = $contentmeta->content();
            }
        }

        if ($contentmeta === null) {
            if ($site === '' && $site !== ltrim(Yii::$app->homeUrl, '/')) {
                return $controller->goHome();
            }

            // TODO
            // var_dump($contentmetaId);die();
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (!$ignoreType && ($contentmeta['model_class'] === null || $contentmeta['model_id'] === null || !Yii::$app->fafcms->isValidContentClass($contentmeta['model_class']))) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $contentmeta;
    }

    /**
     * @param Contentmeta $contentmeta
     * @param string $contentName
     * @param ActiveRecord $content
     */
    public static function setCurrentContentMeta(Contentmeta $contentmeta, string $contentName, ActiveRecord $content): void
    {
        if (!isset(Yii::$app->fafcmsParser->data['currentContentmeta'])) {
            Yii::$app->fafcmsParser->data['currentContentmeta'] = &$contentmeta;
        }

        if (!isset(Yii::$app->fafcmsParser->data['current' . $contentName])) {
            Yii::$app->fafcmsParser->data['current' . $contentName] = &$content;
        }

        if (!isset(Yii::$app->fafcmsParser->data['currentLayoutId'])) {
            Yii::$app->fafcmsParser->data['currentLayoutId'] = $contentmeta->layout_id;
        }

        if (!isset(Yii::$app->fafcmsParser->data['currentLayout'])) {
            Yii::$app->fafcmsParser->data['currentLayout'] = static function() {
                return Layout::find()->where(['id' => Yii::$app->fafcmsParser->data['currentLayoutId']])->one();
            };
        }

        if (!isset(Yii::$app->fafcmsParser->data['currentContent'])) {
            Yii::$app->fafcmsParser->data['currentContent'] = &$content;
        }

        if (!isset(Yii::$app->fafcmsParser->data['currentDataType'])) {
            Yii::$app->fafcmsParser->data['currentDataType'] = $contentName;
        }

        if (!isset(Yii::$app->fafcmsParser->data['currentDataId'])) {
            Yii::$app->fafcmsParser->data['currentDataId'] = $content->id ?? null;
        }
    }

    /**
     * @param ActiveRecord $content
     * @param array $contentOptions
     * @return int
     */
    public static function getSiteId(ActiveRecord $content, array $contentOptions): int
    {
        return (int)ArrayHelper::getValue($content, $contentOptions['site_id']);
    }

    /**
     * @param Contentmeta $contentmeta
     */
    public static function setSiteMeta(Contentmeta $contentmeta): void
    {
        Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => $contentmeta->description
        ]);

        if ($contentmeta->robots_disallow) {
            // TODO more options https://developers.google.com/search/reference/robots_meta_tag?hl=de
            /**
             * @var $contentmeta Contentmeta
             */
            $robotNames = preg_split('/(?<!\\\\)(?:\\\\\\\\)*(,)/', mb_strtolower(trim($contentmeta->robots_disallow_names)));

            foreach ($robotNames as $robotName) {
                $robotName = str_replace('\\\\', '\\', str_replace('\,', ',', $robotName));

                if ($robotName === '') {
                    $robotName = 'robots';
                }

                Yii::$app->view->registerMetaTag([
                    'name' => $robotName,
                    'content' => 'noindex'
                ]);
            }
        }

        Yii::$app->view->params['breadcrumbs'][] = $contentmeta;

        $title = [];

        foreach (Yii::$app->view->params['breadcrumbs'] as $index => $breadcrumbContentmeta) {
            if ($index === 0) {
                continue;
            }

            $title[] = $breadcrumbContentmeta['title'];
        }

        Yii::$app->view->title = $title;
    }

    public static function getSiteTitle(): string
    {
        if ((Yii::$app->fafcms->getCurrentProjectLanguage()->show_site_name_in_title ?? Yii::$app->fafcms->getCurrentProject()->show_site_name_in_title ?? 1) === 1) {
            $titleItems = [Yii::$app->name];
        } else {
            $titleItems = [];
        }

        if (Yii::$app->fafcms->getCurrentPageId() !== Yii::$app->fafcms->getStartPageId() || (Yii::$app->fafcms->getCurrentProjectLanguage()->show_start_page_in_title ?? Yii::$app->fafcms->getCurrentProject()->show_start_page_in_title ?? 0) === 1) {
            $titleItems = array_merge($titleItems, Yii::$app->view->title ?? []);
        }

        if ((Yii::$app->fafcms->getCurrentProjectLanguage()->reverse_title ?? Yii::$app->fafcms->getCurrentProject()->reverse_title ?? 0) === 1) {
            $titleItems = array_reverse($titleItems);
        }

        return implode(Yii::$app->fafcms->getCurrentProjectLanguage()->title_separator ?? Yii::$app->fafcms->getCurrentProject()->title_separator ?? ' a ', $titleItems);
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRobots()
    {
        $currentProjectLanguage = Yii::$app->fafcms->getCurrentProjectLanguage();
        $https = $currentProjectLanguage->domain->is_https_validated === 1;

        $contentmetas = Contentmeta::find()->andWhere([
            'robots_disallow' => 1,
        ])->contents();

        $disallowByRobots = [];

        foreach ($contentmetas as $contentmeta) {
            /**
             * @var $contentmeta Contentmeta
             */
            $robotNames = preg_split('/(?<!\\\\)(?:\\\\\\\\)*(,)/', mb_strtolower(trim($contentmeta->robots_disallow_names)));

            foreach ($robotNames as $robotName) {
                $robotName = str_replace('\\\\', '\\', str_replace('\,', ',', $robotName));
                $disallowByRobots[$robotName][] = $contentmeta->getRelativeUrl();
            }
        }

        $disallow = '';
        $disallowAny = '';

        if (!isset($disallowByRobots[''])) {
            $disallowByRobots[''][] = '';
        }

        foreach ($disallowByRobots as $robot => $disallowedSites) {
            $disallowRule = 'User-agent: '.($robot === ''?'*':$robot)."\n";

            foreach ($disallowedSites as $disallowedSite) {
                $disallowRule .= 'Disallow: '.$disallowedSite."\n";
            }

            if ($robot !== '') {
                foreach ($disallowByRobots[''] as $disallowedSite) {
                    if ($disallowedSite !== '') {
                        $disallowRule .= 'Disallow: '.$disallowedSite."\n";
                    }
                }

                $disallow .= $disallowRule."\n";
            } else {
                $disallowAny = $disallowRule;
            }
        }

        $robots = $disallow.
            ($disallowAny !== ''?$disallowAny."\n":'').
            'Sitemap: http'.($https?'s':'').'://'.$currentProjectLanguage->domain->domain.'/sitemap.xml';

        Yii::$app->response->getHeaders()->add('Content-Type', 'text/plain; charset=UTF-8');

        Yii::$app->response->content = $robots;
        Yii::$app->response->format = Response::FORMAT_RAW;
    }

    private $_sitemapPageSize = 1000;

    public function actionSitemap(): void
    {
        $currentProjectLanguage = Yii::$app->fafcms->getCurrentProjectLanguage();
        $https = $currentProjectLanguage->domain->is_https_validated === 1;

        $dom = new DOMDocument('1.0', Yii::$app->response->charset);
        $root = new DOMElement('sitemapindex');
        $dom->appendChild($root);
        $root->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $root->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $root->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd');

        $pagination = new Pagination(['totalCount' => $this->getSitemapItemQuery()->contentCount(), 'pageSize' => $this->_sitemapPageSize]);

        while ($pagination->page < $pagination->pageCount) {
            $pagination->page++;

            $sitemap = new DOMElement('sitemap');
            $root->appendChild($sitemap);

            $sitemap->appendChild(new DOMElement('loc', Url::to(['/sitemap-'.$pagination->page.'.xml'], 'http'.($https?'s':''))));
            $sitemap->appendChild(new DOMElement('lastmod', date('Y-m-d')));
        }

        Yii::$app->response->content = $dom->saveXML();
        Yii::$app->response->format = Response::FORMAT_XML;
    }

    /**
     * @param string $page
     */
    public function actionSitemapPage(string $page): void
    {
        $currentProjectLanguage = Yii::$app->fafcms->getCurrentProjectLanguage();
        $https = $currentProjectLanguage->domain->is_https_validated === 1;

        $pagination = new Pagination(['totalCount' => $this->getSitemapItemQuery()->contentCount(), 'pageSize' => $this->_sitemapPageSize]);
        $pagination->page = $page - 1;

        if ($page < 1 || $page > $pagination->pageCount) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $contentmetas = $this->getSitemapItemQuery()
            ->offset($pagination->offset)
            ->limit($this->_sitemapPageSize)
            ->select([
                'id',
                'sitemap_changefreq',
                'sitemap_priority',
                'model_class',
                'model_id'
            ])
            ->asArray()
            ->contents();

        $dom = new DOMDocument('1.0', Yii::$app->response->charset);
        $sitemap = new DOMElement('urlset');
        $dom->appendChild($sitemap);
        $sitemap->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $sitemap->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $sitemap->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd');

        foreach ($contentmetas as $contentmeta) {
            $this->addSitemapItem($sitemap, $https, $contentmeta);
        }

        Yii::$app->response->content = $dom->saveXML();
        Yii::$app->response->format = Response::FORMAT_XML;
    }

    private function addSitemapItem($sitemap, bool $https, array $contentmeta)
    {
        $contentOptions = Yii::$app->fafcms->getContentClass($contentmeta['model_class']);

        if ($contentOptions === null) {
            return;
        }

        $select = [];

        if ($contentmeta['model_class']::instance()->hasProperty('updated_at')) {
            $select[] = 'updated_at';
        }

        if ($contentmeta['model_class']::instance()->hasProperty('created_at')) {
            $select[] = 'created_at';
        }

        if ($contentmeta['model_class']::instance()->hasProperty('publication')) {
            $select[] = 'publication';
        }

        if ($contentmeta['model_class']::instance()->hasProperty('lastmod')) {
            $select[] = 'lastmod';
        }

        $content = $contentmeta['model_class']::find()
            ->select($select)
            ->where([$contentOptions['id'] => $contentmeta['model_id']])
            ->asArray()
            ->one();

        if ($content === null) {
            return;
        }

        $sitemapItem = new DOMElement('url');
        $sitemap->appendChild($sitemapItem);

        $sitemapItem->appendChild(new DOMElement('loc', Url::to(Contentmeta::getUrlById($contentmeta['id']), 'http'.($https?'s':''))));

        if (!empty($content['updated_at']) || !empty($content['created_at']) || !empty($content['publication']) || !empty($content['lastmod'])) {
            $lastmod = \DateTime::createFromFormat('Y-m-d H:i:s', (!empty($content['updated_at'])?$content['updated_at']:(!empty($content['created_at'])?$content['created_at']:(!empty($content['publication'])?$content['publication']:$content['lastmod']))));

            if ($lastmod !== false) {
                $sitemapItem->appendChild(new DOMElement('lastmod', $lastmod->format('Y-m-d')));
            }
        }

        $sitemapItem->appendChild(new DOMElement('changefreq', $contentmeta['sitemap_changefreq']));
        $sitemapItem->appendChild(new DOMElement('priority', $contentmeta['sitemap_priority'] / 10));
    }

    private function getSitemapItemQuery()
    {
        return Contentmeta::find()->andWhere([
            'sitemap_list' => 1
        ]);
    }

    /**
     * @param string $name
     * @param array  $possibleValues
     */
    public static function addOpenGraphData(string $name, array $possibleValues): void
    {
        $valueTypes = [
            'og:article:published_time' => 'datetime',
            'og:article:modified_time' => 'datetime',
            'og:article:expiration_time' => 'datetime',
            'og:locale' => 'locale',
            'og:locale:alternate' => 'locale',
            'og:image' => 'file'
        ];

        foreach ($possibleValues as $possibleValue) {
            if ($possibleValue !== null && $possibleValue !== '') {
                if (isset($valueTypes[$name])) {
                    switch ($valueTypes[$name]) {
                        case 'datetime':
                            $possibleValue = Yii::$app->formatter->asDatetime($possibleValue, 'php:' . DateTime::ATOM);
                            break;
                        case 'locale':
                            $possibleValue = str_replace('-', '_', $possibleValue);
                            break;
                        case 'file':
                            $file = File::find()->where(['id' => $possibleValue])->one();

                            if ($file !== null) {
                                if ($name === 'og:image') {
                                    $fileVariation = FilemanagerController::getFileVariation(['Open Graph', 1200, 628, 'bestfit:true;extend:true;background-image:self;background-image-blur:true;background-image-blur-sigma:10;format:jpg'], $file, true);
                                    $meta = File::loadMeta($file, $fileVariation);

                                    $fileUrl = File::getUrl($file, 'http', ['Open Graph', 1200, 628, 'bestfit:true;extend:true;background-image:self;background-image-blur:true;background-image-blur-sigma:10;format:jpg']);
                                    $secureFileUrl = File::getUrl($file, 'https', ['Open Graph', 1200, 628, 'bestfit:true;extend:true;background-image:self;background-image-blur:true;background-image-blur-sigma:10;format:jpg']);

                                    self::addOpenGraphData('og:image:url', [
                                        $fileUrl
                                    ]);

                                    self::addOpenGraphData('og:image:secure_url', [
                                        $secureFileUrl
                                    ]);

                                    self::addOpenGraphData('og:image:type', [
                                        'image/jpeg'
                                    ]);

                                    self::addOpenGraphData('og:image:width', [
                                        $meta['width']
                                    ]);

                                    self::addOpenGraphData('og:image:height', [
                                        $meta['height']
                                    ]);

                                    self::addOpenGraphData('og:image:alt', [
                                        $file->alt
                                    ]);

                                    return;
                                }
                            }
                        break;
                    }
                }

                Yii::$app->view->registerMetaTag(['property' => $name, 'content' => $possibleValue]);
                break;
            }
        }
    }

    /**
     * @param Contentmeta $contentmeta
     *
     * @throws \Throwable
     */
    public static function setOpenGraph(Contentmeta $contentmeta): void
    {
        self::addOpenGraphData('fb:app_id', [
            Yii::$app->fafcms->getCurrentProjectLanguage()->fb_app_id,
            Yii::$app->fafcms->getCurrentProject()->fb_app_id
        ]);

        self::addOpenGraphData('og:site_name', [
            Yii::$app->fafcms->getCurrentProjectLanguage()->og_site_name,
            Yii::$app->fafcms->getCurrentProject()->og_site_name,
            Yii::$app->fafcms->getCurrentProjectLanguage()->site_name,
            Yii::$app->fafcms->getCurrentProject()->site_name,
            Yii::$app->fafcms->getCurrentProject()->name,
        ]);

        self::addOpenGraphData('og:title', [
            $contentmeta->og_title
        ]);

        self::addOpenGraphData('og:type', [
            $contentmeta->og_type
        ]);

        self::addOpenGraphData('og:description', [
            $contentmeta->og_description
        ]);

        self::addOpenGraphData('og:determiner', [
            $contentmeta->og_determiner
        ]);

        self::addOpenGraphData('og:url', [
            $contentmeta->getAbsoluteUrl()
        ]);

        self::addOpenGraphData('og:image', [
            $contentmeta->og_image
        ]);

        self::addOpenGraphData('og:locale', [
            Yii::$app->fafcms->getAvailableLanguagesById()[Yii::$app->fafcms->getCurrentProjectLanguage()->language_id]['ietfcode']
        ]);

        foreach (Yii::$app->fafcms->getCurrentProject()->projectProjectlanguages as $projectlanguage) {
            if ($projectlanguage->id !== Yii::$app->fafcms->getCurrentProjectLanguage()->id) {
                self::addOpenGraphData('og:locale:alternate', [
                    $projectlanguage->language->ietfcode
                ]);
            }
        }

        if ($contentmeta->og_type === 'article') {
            self::addOpenGraphData('og:article:published_time', [
                $contentmeta->og_article_published_time
            ]);

            self::addOpenGraphData('og:article:modified_time', [
                $contentmeta->og_article_modified_time
            ]);

            self::addOpenGraphData('og:article:expiration_time', [
                $contentmeta->og_article_expiration_time
            ]);

            /*
                article:author - profile array - Writers of the article.
                article:section - string - A high-level section name. E.g. Technology
                article:tag - string array - Tag words associated with this article.
            */
        } elseif ($contentmeta->og_type === 'book') {
            /*
                book - Namespace URI: https://ogp.me/ns/book#
                book:author - profile array - Who wrote this book.
                book:isbn - string - The ISBN
                book:release_date - datetime - The date the book was released.
                book:tag - string array - Tag words associated with this book.
             */
        } elseif ($contentmeta->og_type === 'profile') {
            /*
                profile:first_name - string - A name normally given to an individual by a parent or self-chosen.
                profile:last_name - string - A name inherited from a family or marriage and by which the individual is commonly known.
                profile:username - string - A short unique string to identify them.
                profile:gender - enum(male, female) - Their gender.
            */
        }

        /* https://ogp.me/#data_types */
    }
}
