<?php

namespace fafcms\fafcms\controllers;

use fafcms\fafcms\minifier\Minifier;
use fafcms\fafcms\minifier\MinifierSrcFile;
use Yii;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Class ThemeController
 *
 * @package fafcms\fafcms\controllers
 */
class ThemeController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['asset'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionAsset()
    {
        $components = [
            'fafcms' => [
                'path' => '@fafcmsCore/assets/css',
                'assets' => [
                    'main.scss'
                ]
            ],
            'fomantic' => [
                'path' => '@bower/fomantic-ui/src/definitions',
                'assets' => [
                    'globals/reset.less'
                ]
            ]
        ];

        $requestedComponent = Yii::$app->request->get('component');
        $requestedAsset = Yii::$app->request->get('file');

        $component = $components[$requestedComponent] ?? null;
        $componentAssets = $components[$requestedComponent]['assets'] ?? null;

        if ($component === null || $componentAssets === null || !in_array($requestedAsset, $componentAssets, true)) {
            throw new NotFoundHttpException();
        }

        $componentPath = Yii::getAlias($component['path']);

        $assetFile = $componentPath . DIRECTORY_SEPARATOR . $requestedAsset;

        if (!file_exists($assetFile)) {
            throw new NotFoundHttpException();
        }

        $theme = 'default';

        $targetPath = '/assets/themes/backend/' . $theme . '/' . $requestedComponent . '/';
        $fileExtensionPosition = strrpos($requestedAsset, '.');

        if ($fileExtensionPosition === false) {
            throw new NotFoundHttpException();
        }

        $targetName = substr($requestedAsset, 0, $fileExtensionPosition);
        $targetFilePath = Yii::getAlias('@webroot' . $targetPath . $targetName . '.min.css');

        FileHelper::createDirectory(Yii::getAlias('@webroot' . $targetPath));

        if (!file_exists($targetFilePath)) {
            $src = [
                new MinifierSrcFile([
                   'file' => $assetFile
                ])
            ];

            (new Minifier([
                'src' => $src,
                'dist' => $targetFilePath,
                'sourceMap' => false
            ]))->run();
        }

        return Yii::$app->response->sendFile($targetFilePath, $targetName . '.css', ['inline' => true]);
    }
}
