<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 * @since File available since Release 1.0.0
 */

namespace fafcms\fafcms\controllers;

use fafcms\fafcms\models\Customfield;
use fafcms\helpers\ActiveRecord;
use fafcms\helpers\DefaultController;

/**
 * Class CustomfieldController
 *
 * @package fafcms\fafcms\controllers
 */
class CustomfieldController extends DefaultController
{
    public static $modelClass = Customfield::class;

    /**
     * {@inheritdoc}
     */
    protected function deleteModel(ActiveRecord $model): bool
    {
        return $model->delete();
    }
}
