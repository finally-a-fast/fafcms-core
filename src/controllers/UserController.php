<?php

namespace fafcms\fafcms\controllers;

use fafcms\fafcms\models\ChangePasswordForm;
use fafcms\fafcms\models\LoginForm;
use fafcms\fafcms\models\PasswordRequestForm;
use fafcms\fafcms\models\User;
use fafcms\helpers\DefaultController;
use Yii;
use yii\base\InvalidArgumentException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use Yii\web\Response;

/**
 * Class UserController
 *
 * @package fafcms\fafcms\controllers
 */
class UserController extends DefaultController
{
    /** @var string */
    public static $modelClass = User::class;

    /**
     * @todo merge with parent
     * @return array[]
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'aktivierung', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['login', 'aktivierung', 'request-password-reset', 'reset-password'],
                        'allow' => false,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return Response
     */
    public function actionChangeAccount(): Response
    {
        return $this->redirect(['/user/update', 'id' => Yii::$app->user->id]);
    }

    /**
     * Logs out the current user.
     *
     * @return Response
     */
    public function actionLogout(): Response
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * @return array|string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionChangePassword()
    {
        return $this->actionUpdate(Yii::$app->user->id, 'changePasswordView');
    }

    /**
     * Logs in a user.
     *
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogin()
    {
        $modelClass = LoginForm::class;

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return $this->renderActionContent(Yii::$app->view->renderView('model\\edit\\' . $modelClass, null, [
            'modelClass' => $modelClass,
            'createTitle' => ['fafcms-core', 'Login'],
            'addIndexToBreadcrumb' => false
        ]));
    }

    /**
     * Requests password reset.
     *
     * @return array|string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRequestPasswordReset()
    {
        $modelClass = PasswordRequestForm::class;

        return $this->renderActionContent(Yii::$app->view->renderView('model\\edit\\' . $modelClass, null, [
            'modelClass' => $modelClass,
            'createTitle' => ['fafcms-core', 'Request new password'],
            'addIndexToBreadcrumb' => false
        ]));
    }

    /**
     * Resets password.
     *
     * @param string $k
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($k)
    {
        try {
            $model = new ChangePasswordForm($k);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Ihr Passwort wurde erfolgreich geändert. Sie können sich nun mit Ihren neuen Zugangsdaten einloggen.');

            return Yii::$app->getResponse()->redirect(ArrayHelper::merge(Yii::$app->user->loginUrl, ['email' => $model->email]));
        }

        return $this->render('@fafcms/fafcms/views/common/rows', [
            'rows' => [[[
                'options' => ['class' => 'col offset-xl3 xl6 offset-l2 l8 m12 s12'],
                'content' => $this->renderPartial('//main/changePassword', [
                    'model' => $model
                ])
            ]]],
        ]);
    }
}
