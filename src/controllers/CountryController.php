<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\controllers;

use fafcms\fafcms\models\Country;
use fafcms\helpers\DefaultController;

/**
 * Class CountryController
 * @package fafcms\fafcms\controllers
 */
class CountryController extends DefaultController
{
    public static $modelClass = Country::class;
}
