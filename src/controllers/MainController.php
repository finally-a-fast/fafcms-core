<?php

namespace fafcms\fafcms\controllers;

use fafcms\fafcms\items\Html as HtmlItem;
use fafcms\fafcms\models\Project;
use fafcms\fafcms\models\ProjectSelectForm;
use Yii;
use fafcms\fafcms\Bootstrap as FafcmsBootstrap;
use fafcms\fafcms\models\ChangePasswordForm;
use fafcms\fafcms\models\LoginForm;
use fafcms\fafcms\models\PasswordRequestForm;
use yii\base\ErrorException;
use yii\filters\HttpCache;
use yii\filters\PageCache;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\BadRequestHttpException;
use yii\helpers\Inflector;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\caching\CacheInterface;
use yii\base\InvalidArgumentException;
use DarkGhostHunter\Preloader\Preloader;


/**
 * Class MainController
 * @package fafcms\fafcms\controllers
 */
class MainController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => '//main/backend-error'
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['cron', 'js-translation'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            [
                'class' => PageCache::class,
                'only' => ['js-translation'],
                'cache' => 'translationCache',
                'duration' => 0,
                'variations' => [
                    Yii::$app->request->get('language'),
                ],
            ],
            [
                'class' => HttpCache::class,
                'cacheControlHeader' => 'public, max-age=31536000',
                'only' => ['js-translation'],
                'lastModified' => function () {
                    return Yii::$app->translationCache->getOrSet('js-translation', function () {
                        return time();
                    }, 0);
                },
            ],
        ];
    }

    public function actionSaveUserSetting()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $type = Yii::$app->request->get('type');
        $variation = Yii::$app->request->get('variation');
        $value = Yii::$app->request->get('value');

        if ($variation === null) {
            return false;
        }

        if ($type === 'collapsible') {
            $type = 'collapsibleCollapsed';
        } elseif ($type === 'card') {
            $type = 'cardCollapsed';
        }  elseif ($type === 'sidebar') {
            $type = 'sidebarVisible';
        } else {
            return false;
        }

        return Yii::$app->getModule(FafcmsBootstrap::$id)->setUserSettingValue($type, $value, null, null, $variation);
    }

    public function actionCache()
    {
        return $this->renderPartial('@fafcms/fafcms/views/main/cache');
    }

    public function actionGeneratePreloader()
    {
        if (class_exists('DarkGhostHunter\Preloader\Preloader')) {
            if (Preloader::make()->memoryLimit(0)->useRequire( FAFCMS_VENDOR_PATH . '/autoload.php')->writeTo(FAFCMS_BASE_PATH . '/preloader.php')) {
                Yii::$app->session->setFlash('success', Yii::t('fafcms-core', 'Opcache preloader file has been generated!'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'Opcache preloader file could not be generated!'));
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'DarkGhostHunter\Preloader\Preloader not found please install it by running "composer require darkghosthunter/preloader"!'));
        }

        return $this->goBack(Yii::$app->getRequest()->getReferrer());
    }

    public function actionFlushCache($cache = null)
    {
        $components = Yii::$app->getComponents();

        if ($cache === 'opCache') {
            if (extension_loaded('Zend OPcache')) {
                if (opcache_reset()) {
                    Yii::$app->session->setFlash('success',
                        Yii::t('fafcms-core', '{cache} flushed!', ['cache' => Inflector::camel2words($cache)]));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('fafcms-core', '{cache} could not be flushed!', ['cache' => Inflector::camel2words($cache)]));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('fafcms-core', '{cache} is not installed!', ['cache' => Inflector::camel2words($cache)]));
            }
        } elseif ($cache === 'assetCache') {
            $assetDir = Yii::getAlias('@webroot/assets');
            $errors = [];

            foreach (FileHelper::findDirectories($assetDir, ['recursive' => false, 'except' => ['.*']]) as $directory) {
                try {
                    FileHelper::removeDirectory($directory);
                } catch (ErrorException $e) {
                    $errors[] = $e->getMessage();
                }
            }

            foreach (FileHelper::findFiles($assetDir, ['recursive' => false, 'except' => ['.*']]) as $file) {
                if (!FileHelper::unlink($file)) {
                    $errors[] = Yii::t('fafcms', 'Cannnot delete {file}.', ['file' => $file]);
                }
            }

            if (count($errors) === 0) {
                Yii::$app->session->setFlash('success', Yii::t('fafcms-core', '{cache} flushed!', ['cache' => Inflector::camel2words($cache)]));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('fafcms-core', '{cache} could not be flushed!', ['cache' => Inflector::camel2words($cache)]) . print_r($errors, true));
            }
        } elseif ($cache !== null) {
            if (isset($components[$cache], Yii::$app->$cache) && Yii::$app->$cache instanceof CacheInterface) {
                if (Yii::$app->$cache->flush()) {
                    Yii::$app->session->setFlash('success', Yii::t('fafcms-core', '{cache} flushed!', ['cache' => Inflector::camel2words($cache)]));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('fafcms-core', '{cache} could not be flushed!', ['cache' => Inflector::camel2words($cache)]));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'No cache named "{cache}" found!', ['cache' => $cache]));
            }
        } else {
            $messages = [
                'success' => [],
                'error' => []
            ];

            if (extension_loaded('Zend OPcache')) {
                if (opcache_reset()) {
                    $messages['success'][] = Yii::t('fafcms-core', '{cache} flushed!', ['cache' => Inflector::camel2words($cache)]);
                } else {
                    $messages['error'][] = Yii::t('fafcms-core', '{cache} could not be flushed!', ['cache' => Inflector::camel2words($cache)]);
                }
            }

            foreach (array_keys($components) as $cacheName) {
                if (isset($components[$cacheName], Yii::$app->$cacheName) && Yii::$app->$cacheName instanceof CacheInterface) {
                    if (Yii::$app->$cacheName->flush()) {
                        $messages['success'][] = Yii::t('fafcms-core', '{cache} flushed!', ['cache' => Inflector::camel2words($cacheName)]);
                    } else {
                        $messages['error'][] = Yii::t('fafcms-core', '{cache} could not be flushed!', ['cache' => Inflector::camel2words($cacheName)]);
                    }
                }
            }

            if (count($messages['error']) === 0) {
                Yii::$app->session->setFlash('success', Yii::t('fafcms-core', 'All caches flushed!'));
            } elseif (count($messages['success']) === 0) {
                Yii::$app->session->setFlash('error', Yii::t('fafcms-core', 'All caches could not be flushed!'));
            } else {
                Yii::$app->session->setFlash('warning', implode('<br>', $messages['success']).'<br><br>'.implode('<br>', $messages['error']));
            }
        }

        return $this->goBack(Yii::$app->getRequest()->getReferrer());
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionCron()
    {
        if (Yii::$app->getModule(FafcmsBootstrap::$id)->getPluginSettingValue('use_web_cronjob') || Yii::$app->getModule(FafcmsBootstrap::$id)->getPluginSettingValue('always_allow_web_cronjob')) {
            Yii::$app->params['fafcmsQueueIsWebCronjob'] = true;
            Yii::$app->queue->run(false);

            return 'done';
        }

        throw new ForbiddenHttpException('A real cronjob has already been set up. Therefore the call of the web cronjob is not necessary.');
    }

    /**
     * @return array|string|Response|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        if ((int)Project::find()->count() === 1) {
            return $this->redirect(['main/project-index']);
        }

        return Yii::$app->view->renderActionContent(Yii::$app->view->renderView('fafcms\\HomeDashboard'));
    }

    /**
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionProjectIndex(): ?string
    {
        return Yii::$app->view->renderActionContent(Yii::$app->view->renderView('fafcms\\ProjectDashboard'));
    }

    /**
     * @param $id
     *
     * @return Response
     * @throws \Throwable
     */
    public function actionChangeLanguage($id): Response
    {
        Yii::$app->fafcms->setCurrentLanguageId($id);
        Yii::$app->session->setFlash('success', Yii::t('fafcms-core', 'Changed current language to "{language}"', ['language' => Yii::$app->fafcms->getCurrentLanguage()['name']]));

        return $this->goBack(Yii::$app->getRequest()->getReferrer());
    }

    /**
     * @param null $id
     *
     * @return array|string|Response|null
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function actionChangeProject($id = null)
    {
        if ($id === null) {
            $html = '';

            foreach (Project::find()->asArray()->all() as $project) {
                $html .= Html::a($project['name'], ['/main/change-project', 'id' => $project['id']], ['class' => 'collection-item']);
            }

            return Yii::$app->view->renderActionContent(Yii::$app->view->renderContentItems([
                'project-list' => [
                    'class' => HtmlItem::class,
                    'contents' => '<div class="collection">' . $html . '</div>'
                ],
            ]));
        }


        Yii::$app->fafcms->setCurrentProjectId($id);
        Yii::$app->session->setFlash('success', Yii::t('fafcms-core', 'Changed current project to "{project}"', ['project' => Yii::$app->fafcms->getCurrentProject()->name]));

        return $this->goBack(Yii::$app->getRequest()->getReferrer());
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function actionChangeProjectLanguage($id): Response
    {
        Yii::$app->fafcms->setCurrentProjectLanguageId($id);
        Yii::$app->session->setFlash('success', Yii::t('fafcms-core', 'Changed current project language to "{language}"', ['language' => Yii::$app->fafcms->getCurrentProjectLanguage()->name]));

        return $this->goBack(Yii::$app->getRequest()->getReferrer());
    }

    /**
     * @param $language
     *
     * @return Response
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionJsTranslation($language): Response
    {
        $translations = [];
        $translationMessages = Yii::$app->i18n->searchTranslationMessages();

        foreach ($translationMessages as $translationMessage) {
            $translationMessageLanguage = null;

            if (isset($translationMessage['translations'][$language])) {
                $translationMessageLanguage = $translationMessage['translations'][$language];
            } elseif (($seperatorPos = strpos($language, '-')) !== false) {
                $translationMessageLanguage = $translationMessage['translations'][substr($language, 0, $seperatorPos)] ?? null;
            }

            $translations[$translationMessage['category']] = $translationMessageLanguage['messages'] ?? array_combine($translationMessage['available'], $translationMessage['available']);
        }

        $response =  Yii::$app->response->sendContentAsFile(
            'window.faf = window.faf || {}; window.faf.translations = Object.assign(window.faf.translations || {}, ' . Json::encode([$language => $translations]) . ')',
            $language . '.js',
            [
                'mimeType' => 'application/javascript',
                'inline' => true
            ]
        );

        $response->headers->remove('Expires');
        $response->headers->remove('Content-Disposition');

        return $response;
    }
}
