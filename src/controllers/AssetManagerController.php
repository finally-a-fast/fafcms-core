<?php
/**
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-core/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-core
 * @see https://www.finally-a-fast.com/packages/fafcms-core/docs Documentation of fafcms-core
 */

namespace fafcms\fafcms\controllers;

use fafcms\fafcms\minifier\Minifier;
use fafcms\fafcms\minifier\MinifierSrcFile;
use yii\console\Controller;

/**
 * Class AssetManagerController
 *
 * @package fafcms\fafcms\controllers
 */
class AssetManagerController extends Controller
{
    public function actionMinify(string $srcFiles, ?string $distFile = null, bool $minify = true, bool $autoPrefix = true, bool $sourceMap = false): void
    {
        $src = [];

        foreach (explode(PATH_SEPARATOR, $srcFiles) as $srcFile) {
            $src[] = new MinifierSrcFile([
                'file' => $srcFile
            ]);
        }

        $this->stdout((new Minifier([
            'src' => $src,
            'dist' => $distFile,
            'minify' => $minify,
            'autoPrefix' => $autoPrefix,
            'sourceMap' => $sourceMap
        ]))->run());
    }
}
