<?php

namespace fafcms\fafcms\helpers;

use yii\helpers\VarDumper;

/**
 * Class StringHelper
 *
 * @package fafcms\fafcms\helpers
 */
class StringHelper extends \yii\helpers\StringHelper
{
    public const CODE_EXPORT_EXPRESSION = '$$EXPRESSION%%';

    /**
     * @param string $string
     * @param int    $spaces
     *
     * @return string
     */
    public static function indent(string $string, int $spaces): string
    {
        $parts = array_filter(explode(PHP_EOL, $string));

        $parts = array_map(static function ($part) use ($spaces) {
            return str_repeat(' ', $spaces) . $part;
        }, $parts);

        return implode(PHP_EOL, $parts);
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public static function removeQuotesFromExpression(string $string): string
    {
        $expression = preg_quote(self::CODE_EXPORT_EXPRESSION, '/');

        return preg_replace_callback('/(?<quote>[\'"])' . $expression . '(?<code>(?=.*?\b.*\b)(?:(?!' . $expression . '(?P=quote)).)*)' . $expression . '(?P=quote)/msi', static function($matches) {
            return stripslashes($matches['code']);
        }, $string);
    }

    /**
     * @param mixed $var
     * @param int   $level
     *
     * @return string
     */
    public static function codeExport($var, int $level = 0): string
    {
        return trim(self::removeQuotesFromExpression(self::indent(VarDumper::export($var), $level * 4)));
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public static function codeExportExpression(string $string): string
    {
        return self::CODE_EXPORT_EXPRESSION . $string . self::CODE_EXPORT_EXPRESSION;
    }

    /**
     * @param string $string
     * @param array  $items
     *
     * @return bool
     */
    public static function contains(string $string, array $items): bool
    {
        foreach ($items as $item) {
            if (stripos($string, $item) !== false) {
                return true;
            }
        }

        return false;
    }
}
