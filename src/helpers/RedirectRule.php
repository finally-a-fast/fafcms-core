<?php
namespace common\helpers;

use Yii;
use yii\web\UrlRule;

class RedirectRule extends UrlRule
{
    public $permanent  = [];
    public $temporary = [];
    public $route = 'redirecthelper';
    public $pattern = 'redirecthelper';

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        if (isset($this->permanent[$pathInfo])) {
            Yii::$app->response->redirect($this->permanent[$pathInfo], 301);
            Yii::$app->end();
        }
        else if (isset($this->temporary[$pathInfo])) {
            Yii::$app->response->redirect($this->temporary[$pathInfo], 302);
            Yii::$app->end();
        }

        return parent::parseRequest($manager, $request);
    }
}