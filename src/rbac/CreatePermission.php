<?php

namespace fafcms\fafcms\rbac;

use yii\rbac\Permission;

/**
 * Class CreatePermission
 * @package fafcms\fafcms\rbac
 */
class CreatePermission extends Permission
{
    public $name = 'create';

    public $ruleName = ModelRule::class;

    public $description = ['fafcms-core', 'Allow to create.'];
}
