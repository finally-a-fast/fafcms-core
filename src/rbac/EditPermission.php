<?php

namespace fafcms\fafcms\rbac;

use yii\rbac\Permission;

/**
 * Class EditPermission
 * @package fafcms\fafcms\rbac
 */
class EditPermission extends Permission
{
    public $name = 'edit';

    public $ruleName = ModelRule::class;

    public $description = ['fafcms-core', 'Allow to edit.'];
}
