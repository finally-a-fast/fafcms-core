<?php

namespace fafcms\fafcms\rbac;

use yii\rbac\Permission;

/**
 * Class ViewPermission
 * @package fafcms\fafcms\rbac
 */
class ViewPermission extends Permission
{
    public $name = 'view';

    public $ruleName = ModelRule::class;

    public $description = ['fafcms-core', 'Allow to view.'];
}
