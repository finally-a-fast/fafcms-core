<?php

namespace fafcms\fafcms\rbac;

use fafcms\fafcms\models\AuthModel;
use yii\rbac\Rule;
use Yii;

/**
 * Class ModelRule
 * @package fafcms\fafcms\rbac
 */
class ModelRule extends Rule
{
    public $name = 'model';

    public function execute($user, $item, $params)
    {
        $roles = Yii::$app->authManager->getRolesByUser($user);

        return AuthModel::find()->where([
            'AND',
            [
                'OR',
                [
                    'type' => AuthModel::TYPE_USER,
                    'user_id' => $user
                ],
                [
                    'type' => AuthModel::TYPE_ROLE,
                    'role_item_name' => array_keys($roles)
                ]
            ],
            [
                'OR',
                [
                    'model_type' => AuthModel::MODEL_TYPE_TABLE,
                    'model_class' => $params['modelClass'],
                ],
                [
                    'model_type' => AuthModel::MODEL_TYPE_RECORD,
                    'model_id' => $params['modelId']??null,
                ]
            ],
            [
                'permission_item_name' => $item->name
            ]
        ])->exists();
    }
}
