<?php

namespace fafcms\fafcms\rbac;

use yii\rbac\Permission;

/**
 * Class DeletePermission
 * @package fafcms\fafcms\rbac
 */
class DeletePermission extends Permission
{
    public $name = 'delete';

    public $ruleName = ModelRule::class;

    public $description = ['fafcms-core', 'Allow to delete.'];
}
