<?php

namespace fafcms\fafcms\rbac;

use yii\rbac\Permission;

/**
 * Class BackendPermission
 * @package fafcms\fafcms\rbac
 */
class BackendPermission extends Permission
{
    public $name = 'accessBackend';

    public $description = ['fafcms-core', 'Allow backend access'];
}
