<?php
defined('FAFCMS_CONFIG_PATH') or define('FAFCMS_CONFIG_PATH', FAFCMS_BASE_PATH . '/config');

require(FAFCMS_VENDOR_PATH . '/autoload.php');

$overwriteConfig = @include(FAFCMS_CONFIG_PATH . '/overwrite.php');
$mainConfig = require(FAFCMS_CORE_PATH . '/config/main.php');

if ($overwriteConfig !== false) {
    $config = yii\helpers\ArrayHelper::merge($mainConfig, $overwriteConfig);
} else {
    $config = $mainConfig;
}

require(FAFCMS_VENDOR_PATH . '/yiisoft/yii2/Yii.php');
require(FAFCMS_CORE_PATH . '/config/bootstrap-before-app.php');
@include(FAFCMS_CONFIG_PATH . '/bootstrap-before-app.php');

$app = new \fafcms\fafcms\classes\WebApplication($config);

require(FAFCMS_CORE_PATH . '/config/bootstrap.php');
@include(FAFCMS_CONFIG_PATH . '/bootstrap.php');

$app->run();
