<?php

namespace fafcms\fafcms\queries;

use fafcms\fafcms\components\InjectorComponent;
use fafcms\helpers\ActiveRecord;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\Expression;
use yii\helpers\FormatConverter;
use DateTime;
use DateTimeZone;
use yii\db\ActiveQuery;

/**
 * Class DefaultQuery
 *
 * @package fafcms\fafcms\queries
 */
class DefaultQuery extends ActiveQuery
{
    /**
     * @var string
     */
    protected string $tableName;

    /**
     * @var ActiveRecord
     */
    protected ActiveRecord $modelInstance;

    /**
     * @var array|null
     */
    protected ?array $cleanAttributeActions = null;

    /**
     * @var array|string[]
     */
    protected array $attributeActions = [
        'hashId' => 'hashIdAttribute'
    ];

    /**
     * @var bool
     */
    protected bool $applyByStatus = true;

    /**
     * @var bool
     */
    protected bool $applyByProject = true;


    /**
     * @var bool
     */
    protected bool $applyByProjectLanguage = true;

    /**
     * @var array|null
     */
    protected ?array $appliedStatus = null;

    /**
     * @var array|null
     */
    protected ?array $appliedProject = null;

    /**
     * @var array|null
     */
    protected ?array $appliedProjectLanguage = null;

    /**
     * @var bool
     */
    public bool $disableFilter = false;

    protected array $operatorsAllowNull = [
        'not between' => false,
        'between' => false,
        'not in' => false,
        'in' => false,
        'or not like' => false,
        'or like' => false,
        'not like' => false,
        'like' => false,
        '>' => true,
        '<' => true,
        '>=' => true,
        '<=' => true,
        '=' => true,
        '!=' => true,
        '<>' => true
    ];

    /**
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        parent::init();

        /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
        $this->modelInstance = InjectorComponent::instance($this->modelClass);
        /** @noinspection PhpUndefinedMethodInspection */
        $this->tableName = $this->modelClass::tableName();
    }

    /**
     * @param string               $column
     * @param string               $modelClass
     * @param string|string[]|null $status
     *
     * @return array|null
     */
    public function getStatusCondition(string $column, string $modelClass, $status = null): ?array
    {
        if ($status === 'all') {
            return null;
        }

        if ($status === null) {
            if (YII_CONSOLE || (Yii::$app->fafcms && Yii::$app->fafcms->getIsBackend()) || (!Yii::$app->user->getIsGuest() && Yii::$app->session->get('preview_show_inactive', false))) {
                return ['<>', $column, $modelClass::STATUS_DELETED];
            }

            return [$column => $modelClass::STATUS_ACTIVE];
        }

        return [$column => $status];
    }

    /**
     * @param string|string[]|null $status
     * @param bool $checkDisplayStart
     * @param bool $checkDisplayEnd
     *
     * @return $this
     * @throws \Exception
     */
    public function byStatus($status = null, bool $checkDisplayStart = true, bool $checkDisplayEnd = true): self
    {
        $this->applyByStatus = false;
        $this->appliedStatus = func_get_args();

        if ($this->disableFilter || ($status === null && YII_CONSOLE)) {
            return $this;
        }

        if ($this->modelInstance->hasAttribute('status')) {
            $status = $this->getStatusCondition($this->tableName .  '.status', $this->modelClass, $status);

            if ($status !== null) {
                $this->andWhere($status);
            }
        }

        if ((Yii::$app->fafcms ?? null) !== null && Yii::$app->fafcms->getIsFrontend()) {
            $timeZone = 'UTC';
            $datetimeFormat = 'medium';

            if ((Yii::$app->formatter ?? null) !== null) {
                $timeZone = Yii::$app->formatter->defaultTimeZone;
                $datetimeFormat = Yii::$app->formatter->datetimeFormat;
            }

            $previewTime = new DateTime('now', new DateTimeZone($timeZone));

            if (!YII_CONSOLE && !Yii::$app->user->getIsGuest() && !empty(Yii::$app->session->get('preview_show_date'))) {
                $previewTime = DateTime::createFromFormat(
                    FormatConverter::convertDateIcuToPhp($datetimeFormat, 'datetime'),
                    Yii::$app->session->get('preview_show_date'),
                    new DateTimeZone($timeZone)
                );
            }

            $previewTime = 'STR_TO_DATE(\''.$previewTime->format('Y-m-d H:i:s').'\', \'%Y-%m-%d %H:%i:%s\')';

            if ($checkDisplayStart && $this->modelInstance->hasAttribute('display_start')) {
                $this->andWhere([
                    'or',
                    $this->tableName.'.display_start IS NULL',
                    $this->tableName.'.display_start <= ' . $previewTime
                ]);
            }

            if ($checkDisplayEnd && $this->modelInstance->hasAttribute('display_end')) {
                $this->andWhere([
                    'or',
                    $this->tableName.'.display_end IS NULL',
                    $this->tableName.'.display_end > ' . $previewTime
                ]);
            }
        }

        return $this;
    }

    /**
     * @param string|int|string[]|int[]|null $project
     *
     * @return $this
     * @throws \Throwable
     */
    public function byProject($project = null): self
    {
        $this->applyByProject = false;
        $this->appliedProject = func_get_args();

        if (
            $this->modelInstance->hasAttribute('project_id') &&
            !$this->disableFilter &&
            $project !== 'all' &&
            (!YII_CONSOLE || $project !== null)
        ) {
            if ($project === null) {
                $project = Yii::$app->fafcms->getCurrentProjectId();
            }

            if (is_array($project) && isset($this->operatorsAllowNull[$project[0]])) {
                $this->andWhere($project);
            } else {
                $this->andWhere(['OR', [$this->tableName .  '.project_id' => $project], [$this->tableName .  '.project_id' => null]]);
            }
        }


        return $this;
    }

    /**
     * @param string|int|string[]|int[]|null $projectLanguage
     *
     * @return $this
     * @throws \Throwable
     */
    public function byProjectLanguage($projectLanguage = null): self
    {
        $this->applyByProjectLanguage = false;
        $this->appliedProjectLanguage = func_get_args();

        if (
            $this->modelInstance->hasAttribute('projectlanguage_id') &&
            !$this->disableFilter &&
            $projectLanguage !== 'all' &&
            (!YII_CONSOLE || $projectLanguage !== null)
        ) {
            if ($projectLanguage === null) {
                $projectLanguage = Yii::$app->fafcms->getCurrentProjectLanguageId();
            }

            if (is_array($projectLanguage) && isset($this->operatorsAllowNull[$projectLanguage[0]])) {
                $this->andWhere($projectLanguage);
            } else {
                $this->andWhere(['OR', [$this->tableName .  '.projectlanguage_id' => $projectLanguage], [$this->tableName .  '.projectlanguage_id' => null]]);
            }
        }

        return $this;
    }

    /**
     * @param null $db
     *
     * @return array
     * @throws InvalidConfigException
     */
    public function all($db = null): array
    {
        if ($this->applyByStatus) {
            $this->byStatus();
        }

        if ($this->applyByProject) {
            $this->byProject();
        }

        if ($this->applyByProjectLanguage) {
            $this->byProjectLanguage();
        }

        $this->changeAttributes();

        return parent::all($db);
    }

    /**
     * @param null $db
     *
     * @return array|\yii\db\ActiveRecord|null
     * @throws InvalidConfigException
     */
    public function one($db = null)
    {
        if ($this->applyByStatus) {
            $this->byStatus();
        }

        if ($this->applyByProject) {
            $this->byProject();
        }

        if ($this->applyByProjectLanguage) {
            $this->byProjectLanguage();
        }

        $this->changeAttributes();

        return parent::one($db);
    }

    /**
     * @param null $db
     *
     * @return bool
     * @throws InvalidConfigException
     */
    public function exists($db = null): bool
    {
        if ($this->applyByStatus) {
            $this->byStatus();
        }

        if ($this->applyByProject) {
            $this->byProject();
        }

        if ($this->applyByProjectLanguage) {
            $this->byProjectLanguage();
        }

        $this->changeAttributes();

        return parent::exists($db);
    }

    /**
     * @param string $q
     * @param null   $db
     *
     * @return bool|int|string|null
     * @throws InvalidConfigException
     */
    public function count($q = '*', $db = null)
    {
        if ($this->applyByStatus) {
            $this->byStatus();
        }

        if ($this->applyByProject) {
            $this->byProject();
        }

        if ($this->applyByProjectLanguage) {
            $this->byProjectLanguage();
        }

        $this->changeAttributes();

        return parent::count($q, $db);
    }

    /**
     * @param null $db
     *
     * @return array
     * @throws InvalidConfigException
     * @throws \Throwable
     */
    public function column($db = null)
    {
        if ($this->applyByStatus) {
            $this->byStatus();
        }

        if ($this->applyByProject) {
            $this->byProject();
        }

        if ($this->applyByProjectLanguage) {
            $this->byProjectLanguage();
        }

        $this->changeAttributes();

        return parent::column($db);
    }

    /**
     * @throws InvalidConfigException
     */
    protected function changeAttributes(): void
    {
        if ($this->cleanAttributeActions === null) {
            $this->cleanAttributeActions = [];

            foreach ($this->attributeActions as $index => $item) {
                $this->cleanAttributeActions[$this->cleanAttribute($index)] = $item;
            }
        }

        if (!empty($this->where)) {
            $this->where = $this->changeWhereAttributes(['AND', $this->where]);
        }
    }

    /**
     * @param string $attribute
     * @return string
     * @throws InvalidConfigException
     */
    protected function cleanAttribute(string $attribute): string
    {
        $alias = array_keys($this->getTablesUsedInFrom())[0];

        if (strpos($attribute, '(') === false) {
            $attribute = preg_replace('/^' . preg_quote($alias, '/') . '\.(.*)$/', '$1', $attribute);
            if (strpos($attribute, '[[') === 0) {
                $prefixedColumn = "{$alias}.{$attribute}";
            } else {
                $prefixedColumn = "{$alias}.[[{$attribute}]]";
            }
        } else {
            // there is an expression, can't prefix it reliably
            $prefixedColumn = $attribute;
        }

        return $prefixedColumn;
    }

    /**
     * @param string $name
     * @param string|array $value
     *
     * @return array
     */
    protected function hashIdAttribute(string $name, $value): array
    {
        if (is_array($value)) {
            $ids = [];

            foreach ($value as $id) {
                $ids[] = Yii::$app->fafcms->hashToId($id, $this->modelClass);
            }
        } else {
            $ids = Yii::$app->fafcms->hashToId($value, $this->modelClass);
        }

        return [
            'name' => str_ireplace('hashId', 'id', $name),
            'value' => $ids
        ];
    }

    /**
     * @param array $where
     *
     * @return array
     * @throws InvalidConfigException
     */
    protected function changeWhereAttributes(array $where): array
    {
        $newWhere = [];

        foreach ($where as $attribute => $filter) {
            if (is_int($attribute)) {
                if (is_array($filter)) {
                    if (is_string($filter[0] ?? null) && is_string($filter[1] ?? null)) {
                        $currentOperator = strtolower($filter[0]);

                        if (isset($this->operatorsAllowNull[$currentOperator])) {
                            $filterAttribute = $this->cleanAttribute($filter[1]);

                            if (isset($this->cleanAttributeActions[$filterAttribute])) {
                                $newAttribute = $this->{$this->cleanAttributeActions[$filterAttribute]}($filterAttribute, $filter[2]);

                                $filter[1] = $newAttribute['name'];

                                if  ($newAttribute['value'] === null && !$this->operatorsAllowNull[$currentOperator]) {
                                    $filter[0] = '=';
                                    $filter[1] = new Expression('0');
                                    $filter[2] = '1';
                                } else {
                                    $filter[2] = $newAttribute['value'];
                                }
                            }

                            $newWhere[$attribute] = $filter;
                            continue;
                        }
                    }

                    $newWhere[$attribute] = $this->changeWhereAttributes($filter);
                    continue;
                }

                $newWhere[$attribute] = $filter;
                continue;
            }

            $filterAttribute = $this->cleanAttribute($attribute);

            if (isset($this->cleanAttributeActions[$filterAttribute])) {
                $newAttribute = $this->{$this->cleanAttributeActions[$filterAttribute]}($filterAttribute, $filter);

                $newWhere[$newAttribute['name']] = $newAttribute['value'];
                continue;
            }

            $newWhere[$attribute] = $filter;
        }

        return $newWhere;
    }
}
