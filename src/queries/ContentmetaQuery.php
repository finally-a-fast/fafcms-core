<?php

namespace fafcms\fafcms\queries;

use Yii;
use yii\helpers\FormatConverter;

class ContentmetaQuery extends DefaultQuery
{
    public function content()
    {
        $contents = $this->contents();
        return $contents[0] ?? null;
    }

    public function contents()
    {
        $this->andWhere([
            'AND',
            [
                'type' => 'content',
            ],
            ['<>', 'model_class', '']
        ]);

        if ($this->select !== null) {
            $this->addSelect([
                $this->tableName . '.model_class',
                $this->tableName . '.model_id',
            ]);
        }

        $contentmetas = $this;

        if ($this->appliedStatus !== null) {
            $contentmetas = $contentmetas->byStatus(...$this->appliedStatus);
        }

        if ($this->appliedProject !== null) {
            $contentmetas = $contentmetas->byProject(...$this->appliedProject);
        }

        if ($this->appliedProjectLanguage !== null) {
            $contentmetas = $contentmetas->byProjectLanguage(...$this->appliedProjectLanguage);
        }

        $contentmetas = $contentmetas->all();

        $contents = [];

        /**
         * TODO it should be checked already on db side
         */
        foreach ($contentmetas as $contentmeta) {
            $contentOptions = Yii::$app->fafcms->getContentClass($contentmeta['model_class']);

            if ($contentOptions !== null) {
                $contentmetaExists = $contentmeta['model_class']::find()
                    ->where([$contentOptions['id'] => $contentmeta['model_id']]);

                if ($this->appliedStatus !== null) {
                    $contentmetaExists = $contentmetaExists->byStatus(...$this->appliedStatus);
                }

                if ($this->appliedProject !== null) {
                    $contentmetaExists = $contentmetaExists->byProject(...$this->appliedProject);
                }

                if ($this->appliedProjectLanguage !== null) {
                    $contentmetaExists = $contentmetaExists->byProjectLanguage(...$this->appliedProjectLanguage);
                }

                if ($contentmetaExists->exists()) {
                    $contents[] = $contentmeta;
                }
            }
        }

        return $contents;
    }

    public function contentCount()
    {
        $this->select(['model_class', 'model_id']);
        $contents = $this->contents();

        return count($contents);
    }
}
