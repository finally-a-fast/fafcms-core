<?php

namespace fafcms\fafcms\queries;

use yii\base\Component;
use yii\base\InvalidArgumentException;
use yii\db\ActiveQueryTrait;
use yii\db\QueryInterface;
use yii\db\QueryTrait;
use yii\helpers\ArrayHelper;

/**
 * Class ArrayQuery
 *
 * @package fafcms\fafcms\queries
 */
class ArrayQuery extends Component implements QueryInterface
{
    use QueryTrait;
    use ActiveQueryTrait;

    /**
     * @event Event an event that is triggered when the query is initialized via [[init()]].
     */
    const EVENT_INIT = 'init';

    /**
     * @var string
     */
    public string $primaryKeyName = 'id';

    /**
     * @var array
     */
    public array $from = [];

    /**
     * ArrayQuery constructor.
     *
     * @param string     $modelClass
     * @param array|null $config
     */
    public function __construct(string $modelClass, ?array $config = [])
    {
        $this->modelClass = $modelClass;
        parent::__construct($config);
    }

    /**
     * Initializes the object.
     * This method is called at the end of the constructor. The default implementation will trigger
     * an [[EVENT_INIT]] event. If you override this method, make sure you call the parent implementation at the end
     * to ensure triggering of the event.
     */
    public function init()
    {
        parent::init();

        $this->from = $this->modelClass::getRecords();
        $this->trigger(self::EVENT_INIT);
    }

    /**
     * @param null $db
     *
     * @return array
     * @throws \Exception
     */
    public function all($db = null): array
    {
        return $this->populate($this->fetchData());
    }

    /**
     * @param null $db
     *
     * @return array|bool|mixed|null
     * @throws \Exception
     */
    public function one($db = null)
    {
        $row = $this->fetchData()[0] ?? false;

        if ($row !== false) {
            $models = $this->populate([$row]);
            return reset($models) ?: null;
        }

        return null;
    }

    /**
     * @param string $q
     * @param null   $db
     *
     * @return int
     */
    public function count($q = '*', $db = null): int
    {
        return count($this->fetchData());
    }

    /**
     * @param null $db
     *
     * @return bool
     */
    public function exists($db = null): bool
    {
        return !empty($this->fetchData());
    }

    /**
     * Sets data to be selected from.
     *
     * @param array $data
     *
     * @return $this
     */
    public function from(array $data)
    {
        $this->from = $data;
        return $this;
    }

    /**
     * @param array $rows
     *
     * @return array
     * @throws \Exception
     */
    public function populate(array $rows): array
    {
        if (empty($rows)) {
            return [];
        }

        $rows = $this->createModels($rows);

        if (!$this->asArray) {
            foreach ($rows as $model) {
                $model->afterFind();
            }
        }

        if ($this->indexBy === null) {
            return $rows;
        }

        $result = [];

        foreach ($rows as $row) {
            $result[ArrayHelper::getValue($row, $this->indexBy)] = $row;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function fetchData(): array
    {
        $data = $this->filterCondition($this->from, $this->where);

        if (!empty($this->orderBy)) {
            ArrayHelper::multisort($data, array_keys($this->orderBy), array_values($this->orderBy));
        }

        if ($this->offset === -1) {
            $this->offset = 0;
        }

        if ($this->limit === -1) {
            $this->limit = null;
        }

        if (empty($this->limit) && empty($this->offset)) {
            return $data;
        }

        return array_slice($data, $this->offset, $this->limit);
    }

    /**
     * @param array      $data
     * @param array|null $condition
     *
     * @return array
     */
    public function filterCondition(array $data, ?array $condition = []): array
    {
        if ($condition === null || $condition === []) {
            return $data;
        }

        if (isset($condition[0])) {
            $conditionType = array_shift($condition);

            $operator = strtoupper($conditionType);
            $method = 'filterSimpleCondition';

            switch ($operator) {
                case 'NOT':
                    $method = 'filterNotCondition';
                    break;
                case 'AND':
                    $method = 'filterAndCondition';
                    break;
                case 'OR':
                    $method = 'filterOrCondition';
                    break;
                case 'BETWEEN':
                case 'NOT BETWEEN':
                    $method = 'filterBetweenCondition';
                    break;
                case 'IN':
                case 'NOT IN':
                    $method = 'filterInCondition';
                    break;
                case 'LIKE':
                case 'NOT LIKE':
                case 'OR LIKE':
                case 'OR NOT LIKE':
                    $method = 'filterLikeCondition';
                    break;
                case 'CALLBACK':
                    $method = 'filterCallbackCondition';
            }

            return array_values($this->$method($data, $operator, $condition));
        }

        return array_values($this->filterHashCondition($data, $condition));
    }

    /**
     * @param array $data
     * @param array $condition
     *
     * @return array
     */
    public function filterHashCondition(array $data, array $condition): array
    {
        foreach ($condition as $column => $value) {
            if (is_array($value)) {
                // IN condition
                $data = $this->filterInCondition($data, 'IN', [$column, $value]);
            } else {
                $data = array_filter($data, function ($row) use ($column, $value) {
                    if ($value instanceof \Closure) {
                        return call_user_func($value, $row[$column]);
                    }

                    return $row[$column] == $value;
                });
            }
        }

        return $data;
    }

    /**
     * Applies 2 or more conditions using 'AND' logic.
     *
     * @param array $data data to be filtered
     * @param string $operator operator
     * @param array $operands conditions to be united
     *
     * @return array filtered data
     */
    protected function filterAndCondition(array $data, $operator, $operands): array
    {
        foreach ($operands as $operand) {
            if (is_array($operand)) {
                $data = $this->filterCondition($data, $operand);
            }
        }

        return $data;
    }

    /**
     * Applies 2 or more conditions using 'OR' logic.
     *
     * @param array $data data to be filtered
     * @param string $operator operator
     * @param array $operands conditions to be united
     *
     * @return array filtered data
     */
    protected function filterOrCondition(array $data, $operator, $operands): array
    {
        $parts = [];
        foreach ($operands as $operand) {
            if (is_array($operand)) {
                $parts[] = $this->filterCondition($data, $operand);
            }
        }

        if (empty($parts)) {
            return $data;
        }

        $data = [];
        foreach ($parts as $part) {
            foreach ($part as $row) {
                $pk = $row[$this->primaryKeyName];
                $data[$pk] = $row;
            }
        }

        return $data;
    }

    /**
     * Inverts a filter condition.
     *
     * @param array $data data to be filtered
     * @param string $operator operator
     * @param array $operands operands to be inverted
     *
     * @return array filtered data
     *
     * @throws InvalidArgumentException if wrong number of operands have been given
     */
    protected function filterNotCondition(array $data, $operator, $operands): array
    {
        if (count($operands) != 1) {
            throw new InvalidArgumentException("Operator '$operator' requires exactly one operand.");
        }

        $operand = reset($operands);
        $filteredData = $this->filterCondition($data, $operand);
        if (empty($filteredData)) {
            return $data;
        }

        $pkName = $this->primaryKeyName;
        foreach ($data as $key => $row) {
            foreach ($filteredData as $filteredRowKey => $filteredRow) {
                if ($row[$pkName] === $filteredRow[$pkName]) {
                    unset($data[$key]);
                    unset($filteredData[$filteredRowKey]);
                    break;
                }
            }
        }

        return $data;
    }

    /**
     * Applies `BETWEEN` condition.
     *
     * @param array $data data to be filtered
     * @param string $operator operator
     * @param array $operands the first operand is the column name. The second and third operands
     * describe the interval that column value should be in
     *
     * @return array filtered data
     *
     * @throws InvalidArgumentException if wrong number of operands have been given
     */
    protected function filterBetweenCondition(array $data, $operator, $operands): array
    {
        if (!isset($operands[0], $operands[1], $operands[2])) {
            throw new InvalidArgumentException("Operator '$operator' requires three operands.");
        }

        [$column, $value1, $value2] = $operands;

        if (strncmp('NOT', $operator, 3) === 0) {
            return array_filter($data, function ($row) use ($column, $value1, $value2) {
                return $row[$column] < $value1 || $row[$column] > $value2;
            });
        }

        return array_filter($data, function ($row) use ($column, $value1, $value2) {
            return $row[$column] >= $value1 && $row[$column] <= $value2;
        });
    }

    /**
     * Applies 'IN' condition.
     *
     * @param array $data data to be filtered
     * @param string $operator operator
     * @param array $operands the first operand is the column name.
     * The second operand is an array of values that column value should be among
     *
     * @return array filtered data
     *
     * @throws InvalidArgumentException if wrong number of operands have been given
     */
    protected function filterInCondition(array $data, $operator, $operands): array
    {
        if (!isset($operands[0], $operands[1])) {
            throw new InvalidArgumentException("Operator '$operator' requires two operands.");
        }

        [$column, $values] = $operands;

        if ($values === [] || $column === []) {
            return $operator === 'IN' ? [] : $data;
        }

        $values = (array) $values;

        if (count((array) $column) > 1) {
            throw new InvalidArgumentException("Operator '$operator' allows only a single column.");
        }

        if (is_array($column)) {
            $column = reset($column);
        }

        foreach ($values as $i => $value) {
            if (is_array($value)) {
                $values[$i] = isset($value[$column]) ? $value[$column] : null;
            }
        }

        if (strncmp('NOT', $operator, 3) === 0) {
            return array_filter($data, function ($row) use ($column, $values) {
                return !in_array($row[$column], $values);
            });
        }

        return array_filter($data, function ($row) use ($column, $values) {
            return in_array($row[$column], $values);
        });
    }

    /**
     * Applies 'LIKE' condition.
     *
     * @param array $data data to be filtered
     * @param string $operator operator
     * @param array $operands the first operand is the column name. The second operand is a single value
     * or an array of values that column value should be compared with
     *
     * @return array filtered data
     *
     * @throws InvalidArgumentException if wrong number of operands have been given
     */
    protected function filterLikeCondition(array $data, $operator, $operands): array
    {
        if (!isset($operands[0], $operands[1])) {
            throw new InvalidArgumentException("Operator '$operator' requires two operands.");
        }

        [$column, $values] = $operands;

        if (!is_array($values)) {
            $values = [$values];
        }

        $not = (stripos($operator, 'NOT ') !== false);
        $or = (stripos($operator, 'OR ') !== false);

        if ($not) {
            if (empty($values)) {
                return $data;
            }

            if ($or) {
                return array_filter($data, function ($row) use ($column, $values) {
                    foreach ($values as $value) {
                        if (stripos($row[$column], $value) === false) {
                            return true;
                        }
                    }

                    return false;
                });
            }

            return array_filter($data, function ($row) use ($column, $values) {
                foreach ($values as $value) {
                    if (stripos($row[$column], $value) !== false) {
                        return false;
                    }
                }

                return true;
            });
        }

        if (empty($values)) {
            return [];
        }

        if ($or) {
            return array_filter($data, function ($row) use ($column, $values) {
                foreach ($values as $value) {
                    if (stripos($row[$column], $value) !== false) {
                        return true;
                    }
                }

                return false;
            });
        }

        return array_filter($data, function ($row) use ($column, $values) {
            foreach ($values as $value) {
                if (stripos($row[$column], $value) === false) {
                    return false;
                }
            }

            return true;
        });
    }

    /**
     * Applies 'CALLBACK' condition.
     *
     * @param array $data data to be filtered
     * @param string $operator operator
     * @param array $operands the only one operand is the PHP callback, which should be compatible with
     * `array_filter()` PHP function, e.g.:
     *
     * ```php
     * function ($row) {
     *     //return bool whether row matches condition or not
     * }
     * ```
     *
     * @return array filtered data
     *
     * @throws InvalidArgumentException if wrong number of operands have been given
     *
     * @since 1.0.3
     */
    public function filterCallbackCondition(array $data, $operator, $operands): array
    {
        if (count($operands) != 1) {
            throw new InvalidArgumentException("Operator '$operator' requires exactly one operand.");
        }

        $callback = reset($operands);

        return array_filter($data, $callback);
    }

    /**
     * Applies comparison condition, e.g. `column operator value`.
     *
     * @param array $data data to be filtered
     * @param string $operator operator
     * @param array $operands
     *
     * @return array filtered data
     *
     * @throws InvalidArgumentException if wrong number of operands have been given or operator is not supported
     *
     * @since 1.0.4
     */
    public function filterSimpleCondition(array $data, $operator, $operands): array
    {
        if (count($operands) !== 2) {
            throw new InvalidArgumentException("Operator '$operator' requires two operands.");
        }
        [$column, $value] = $operands;

        return array_filter($data, function ($row) use ($operator, $column, $value) {
            switch ($operator) {
                case '=':
                case '==':
                    return $row[$column] == $value;
                case '===':
                    return $row[$column] === $value;
                case '!=':
                case '<>':
                    return $row[$column] != $value;
                case '!==':
                    return $row[$column] !== $value;
                case '>':
                    return $row[$column] > $value;
                case '<':
                    return $row[$column] < $value;
                case '>=':
                    return $row[$column] >= $value;
                case '<=':
                    return $row[$column] <= $value;
                default:
                    throw new InvalidArgumentException("Operator '$operator' is not supported.");
            }
        });
    }
}
