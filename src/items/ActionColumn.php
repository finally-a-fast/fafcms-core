<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use fafcms\fafcms\widgets\columns\ActionColumn as ActionColumnWidget;
use fafcms\helpers\classes\ContentItemSetting;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\fafcms\components\FafcmsComponent;
use yii\helpers\ArrayHelper;

/**
 * Class ActionColumn
 * @package fafcms\fafcms\items
 */
class ActionColumn extends ContentItem
{
    public int $type = self::TYPE_COLUMN;

    /**
     * @var $actionColumnOptions array
     */
    public $actionColumnOptions = [];

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Index column');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates a column for index.');
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Label'),
                'name' => 'label',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Hide label'),
                'name' => 'hideLabel',
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'inputType' => Checkbox::class,
                'defaultValue' => false
            ]),
        ];
    }

    public function run(): array
    {
        return ArrayHelper::merge([
            'class' => ActionColumnWidget::class,
            'sort' => PHP_INT_MAX,
            'visibleButtons' => [
                'update' => static function ($model) {
                    return Yii::$app->user->can('edit', ['modelClass' => Yii::$app->view->params['authModelClass'], 'modelId' => $model['id']]);
                },
                'delete' => static function ($model) {
                    return Yii::$app->user->can('delete', ['modelClass' => Yii::$app->view->params['authModelClass'], 'modelId' => $model['id']]);
                },
                'translate' => static function ($model) {
                    return Yii::$app->user->can('create', ['modelClass' => Yii::$app->view->params['authModelClass'], 'modelId' => $model['id']]) && method_exists($model, 'getProjectlanguage_id');
                },
            ],
        ], $this->actionColumnOptions);
    }
}
