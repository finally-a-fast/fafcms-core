<?php

namespace fafcms\fafcms\items;

use Closure;
use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\inputs\TextInput;
use fafcms\helpers\ActiveRecord;
use Yii;
use fafcms\helpers\classes\ContentItemSetting;
use fafcms\fafcms\components\FafcmsComponent;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;

/**
 * Class IndexView
 *
 * @package fafcms\fafcms\items
 */
class IndexView extends BaseView
{
    /**
     * @inheritdoc
     */
    public int $type = self::TYPE_GRID;

    /**
     * @inheritdoc
     */
    public array $name = ['fafcms-core', 'Index view'];

    /**
     * @inheritdoc
     */
    public function label(): string
    {
        return Yii::t('fafcms-core', 'Index');
    }

    /**
     * @inheritdoc
     */
    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates an list to search.');
    }

    /**
     * @inheritdoc
     */
    public function itemSettings(): ?array
    {
        return array_merge(parent::itemSettings(), [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Model class'),
                'name' => 'modelClass',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Search model class'),
                'name' => 'searchModelClass',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Search filter'),
                'name' => 'searchFilter',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class,
                'defaultValue' => []
            ]),
        ]);
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     * @throws InvalidConfigException
     */
    public function run(): string
    {
        $this->getViewContent();

        $modelClass       = $this->getSetting('modelClass');
        $isWidget         = $this->getSetting('isWidget');
        $searchModelClass = $this->getSetting('searchModelClass');
        $viewButtons      = $this->buttons;
        $authModelClass   = $modelClass::$authModelClass ?? $modelClass;
        $checkAccess      = $this->checkAccess && ($authModelClass::$checkAccess ?? true);

        $allowed = Yii::$app->user->can('view', ['modelClass' => $authModelClass]);

        if ($checkAccess && !$allowed) {
            throw new ForbiddenHttpException(Yii::t('fafcms-core', 'Your not allowed to {action} {model}.', ['action' => Yii::t('fafcms-core', 'view'), 'model' => $modelClass::instance()->getEditDataPlural()]));
        }

        if ($searchModelClass === null) {
            $searchModelClass = $modelClass::$searchModelClass ?? $modelClass;
        }

        $model = InjectorComponent::createObject($searchModelClass);
        $modelScenarios = $model->scenarios();

        if (defined($modelClass . '::SCENARIO_SEARCH') && isset($modelScenarios[$modelClass::SCENARIO_SEARCH])) {
            $model->setScenario($modelClass::SCENARIO_SEARCH);
        }

        if (method_exists($model, 'search')) {
            $dataProvider = $model->search(array_merge($this->getSetting('searchFilter'), Yii::$app->request->queryParams));
        } else {
            $dataProvider        = InjectorComponent::createObject(ActiveDataProvider::class);
            $dataProvider->query = $modelClass::find();
        }

        $title = $model->getEditData()['plural'];

        if (!$isWidget) {
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => $model->getEditData()['plural'],
                'url' => ['index']
            ];

            Yii::$app->view->title = [$title];
        }

        $contentItems = $this->contents;

        $parentModel = Yii::$app->view->params['model'] ?? null;
        $parentModelClass = Yii::$app->view->params['modelClass'] ?? null;
        $parentAuthModelClass = Yii::$app->view->params['authModelClass'] ?? null;
        $parentSearchModelClass = Yii::$app->view->params['searchModelClass'] ?? null;

        Yii::$app->view->params['model'] = $model;
        Yii::$app->view->params['modelClass'] = $modelClass;
        Yii::$app->view->params['authModelClass'] = $authModelClass;
        Yii::$app->view->params['searchModelClass'] = $searchModelClass;

        $columns = Yii::$app->view->renderContentItems($contentItems, true);

        $indexView = Yii::$app->view->render('@fafcms/fafcms/views/common/index', [
            'title' => $model->getEditData()['plural'],
            'icon' => $model->getEditData()['icon'],
            'filterModel' => $model,
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'isWidget' => $isWidget,
            'buttons' => static function($dataProvider, $filterModel, $indexView) use ($viewButtons, $modelClass, $authModelClass, $isWidget) {
                $buttons = [
                    'back' => [
                        'icon' => 'arrow-left',
                        'position' => 'first',
                        'label' => Yii::t('fafcms-core', 'Back'),
                        'url' => Yii::$app->getUser()->getReturnUrl(),
                        'visible' => !Yii::$app->getRequest()->getIsAjax() && !$isWidget
                    ],
                    'close' => [
                        'icon' => 'close',
                        'position' => 'first',
                        'label' => Yii::t('fafcms-core', 'Cancel'),
                        'type' => 'button',
                        'options' => ['class' => 'cancel'],
                        'visible' => Yii::$app->getRequest()->getIsAjax()
                    ],
                    'more' => [
                        'icon' => 'dots-horizontal',
                        'position' => 'last',
                        'label' => Yii::t('fafcms-core', 'More actions'),
                        'options' => [
                            'class' => ['top right pointing'],
                            'data-direction' => 'upward'
                        ],
                        'items' => [],
                        'visible' => function($item) {
                            if (count($item['items']) > 0) {
                                foreach ($item['items'] as $childItem) {
                                    if (
                                        !isset($childItem['visible']) ||
                                        (($childItem['visible'] instanceof Closure) && $childItem['visible']($childItem)) ||
                                        $childItem['visible']
                                    ) {
                                        return true;
                                    }
                                }
                            }

                            return false;
                        },
                    ]
                ];

                $buttons['create'] = [
                    'after' => 'back',
                    'options' => [
                        'class' => ['primary'],
                    ],
                    'visible' => Yii::$app->getUser()->can('create', ['modelClass' => $authModelClass]),
                    'icon' => 'plus',
                    'label' => Yii::t('fafcms-core',  'Create {modelClass}', [
                        'modelClass' => $filterModel->getEditData()['singular'],
                    ]),
                    'url' => ['/' . $filterModel->getEditDataUrl() . '/create'],
                ];

                if ($filterModel instanceof ActiveRecord) {
                    $buttons['more']['items']['translate'] = [
                        'visible' => Yii::$app->getUser()->can('create', ['modelClass' => $authModelClass]) && method_exists($modelClass, 'getProjectlanguage_id'),
                        'icon' => 'translate',
                        'label' => Yii::t('fafcms-core',  'Translate'),
                        'url' => ['/' . $filterModel->getEditDataUrl() . '/translate'],
                    ];

                    $buttons['more']['items']['export'] = [
                        'after' => 'translate',
                        'icon' => 'download',
                        'label' =>  Yii::t('fafcms-core', 'Export'),
                        'url' => ['/' . $filterModel->getEditDataUrl() . '/export', 'ids' => $filterModel->id],
                        'options' => [
                            'target' => '_blank',
                        ],
                        'visible' => Yii::$app->user->can('edit', ['modelClass' => $authModelClass])
                    ];
                }

                if ($viewButtons !== null) {
                    foreach ($viewButtons as $viewButton) {
                        if ($viewButton instanceof Closure) {
                            $buttons = call_user_func($viewButton, $buttons, $filterModel, $dataProvider, $indexView);
                        } else {
                            throw new InvalidConfigException('Buttons of model "'.$modelClass.'" must be an instance of '. Closure::class.' got '.gettype($viewButton).' instead.');
                        }
                    }
                }

                return $buttons;
            },
        ]);

        Yii::$app->view->params['model'] = $parentModel;
        Yii::$app->view->params['modelClass'] = $parentModelClass;
        Yii::$app->view->params['authModelClass'] = $parentAuthModelClass;
        Yii::$app->view->params['searchModelClass'] = $parentSearchModelClass;

        return $indexView;
    }
}
