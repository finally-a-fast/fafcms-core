<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\inputs\Checkbox;
use fafcms\helpers\interfaces\EditViewInterface;
use fafcms\helpers\interfaces\FieldConfigInterface;
use fafcms\helpers\interfaces\IndexViewInterface;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\helpers\classes\ContentItemSetting;
use fafcms\fafcms\components\FafcmsComponent;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class BaseView
 *
 * @package fafcms\fafcms\items
 */
class BaseView extends ContentItem
{
    /**
     * @inheritdoc
     */
    public int $type = self::TYPE_CONTAINER;

    /**
     * @inheritdoc
     */
    public bool $preRenderContent = false;

    /**
     * @var string|null
     */
    public ?string $fullyQualifiedTypeName = null;

    /**
     * @var null|string|int
     */
    public $contentViewId = null;

    /**
     * @var array|string[]
     */
    public array $name = ['fafcms-core', 'View'];

    /**
     * @var bool
     */
    public bool $checkAccess = true;

    /**
     * @var array
     */
    public array $buttons = [];

    /**
     * @inheritdoc
     */
    public function label(): string
    {
        return Yii::t('fafcms-core', 'View');
    }

    /**
     * @inheritdoc
     */
    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates an view with items to search.');
    }

    /**
     * @inheritdoc
     */
    public function editorOptions(): array
    {
        return [
            'tag' => 'div',
            'options' => [
                'class' => []
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Is widget'),
                'name' => 'isWidget',
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'inputType' => Checkbox::class,
                'defaultValue' => false,
            ]),
        ];
    }

    /**
     * @throws InvalidConfigException
     */
    protected function getViewContent(): void
    {
        $fullyQualifiedTypeName = $this->fullyQualifiedTypeName;
        $contentViewId = $this->contentViewId;
        $viewButtons = [];
        $viewContentItems = [];

        $model = null;
        $modelClass = null;
        $modelViewName = '';

        if ($contentViewId === null) {
            $contentViewId = 'default';
        }

        if ($fullyQualifiedTypeName !== null) {
            $contentView = null;

            if (!is_numeric($contentViewId)) {
                $contentViews = [];

                if (isset(Yii::$app->view->contentViews[$fullyQualifiedTypeName])) {
                    $contentViews = Yii::$app->view->contentViews[$fullyQualifiedTypeName];
                } elseif (strpos($fullyQualifiedTypeName, 'model\\edit\\') === 0 || strpos($fullyQualifiedTypeName, 'model\\index\\') === 0) {
                    $modelClass = $this->getSetting('modelClass');

                    if ($modelClass === null) {
                        throw new InvalidCallException('Model class is missing');
                    }

                    $modelClass = InjectorComponent::getClass($modelClass);

                    if (!isset(class_implements($modelClass)[FieldConfigInterface::class])) {
                        throw new InvalidConfigException('Class "' . $modelClass . '" must implement "' . FieldConfigInterface::class . '".');
                    }

                    if (strpos($fullyQualifiedTypeName, 'model\\edit\\') === 0) {
                        if (!isset(class_implements($modelClass)[EditViewInterface::class])) {
                            throw new InvalidConfigException('Class "' . $modelClass . '" must implement "' . EditViewInterface::class . '".');
                        }

                        $modelViewName = 'editView';
                    } elseif (strpos($fullyQualifiedTypeName, 'model\\index\\') === 0) {
                        if (!isset(class_implements($modelClass)[IndexViewInterface::class])) {
                            throw new InvalidConfigException('Class "' . $modelClass . '" must implement "' . IndexViewInterface::class . '".');
                        }

                        $modelViewName = 'indexView';
                    }

                    $contentViews = $modelClass::$modelViewName();

                    if (isset($contentItems['buttons'])) {
                        $viewButtons[] = $contentItems['buttons'];
                    }

                    $model = $modelClass::instance();
                    $methodNames = preg_grep('/^get' . ucfirst($modelViewName) . 'Buttons/', get_class_methods($model));

                    if (count($methodNames) > 0) {
                        foreach($methodNames as $method) {
                            $viewButtons[] = $model->$method();
                        }
                    }
                }

                if (isset($contentViews)) {
                    $defaultContentView = null;

                    $contentView = $contentViews[$contentViewId] ?? null;
                }
            }

            if ($contentView !== null) {
                if (isset($contentView['contents'])) {
                    $viewContentItems = $contentView['contents'];
                } elseif (
                    isset($contentView['file'])
                ) {
                    if (
                        ($file = Yii::getAlias($contentView['file']) . '.php') !== false &&
                        file_exists($file) &&
                        ($contents = @include($file)) !== false
                    ) {
                        $viewContentItems = $contents;
                    }
                } else {
                    $viewContentItems = $contentView;
                }

                $viewContentItems = $this->getExtendedViewItems($viewContentItems, $fullyQualifiedTypeName, $contentViewId, $modelClass, $model, $modelViewName);

                $this->contents = $viewContentItems;
                $this->buttons = array_merge($viewButtons, $contentView['buttons'] ?? []);

                if (isset($contentView['label'])) {
                    $this->name = $contentView['label'];
                }

                if (isset($contentView['scenario']) && $this->getSetting('scenario') === null) {
                    $this->settings['scenario'] = $contentView['scenario'];
                }
            }
        }
    }

    /**
     * @param array       $viewContentItems
     * @param string      $fullyQualifiedTypeName
     * @param string      $contentViewId
     * @param string|null $modelClass
     * @param Model|null  $model
     * @param string|null $modelViewName
     *
     * @return array|array[]
     * @throws InvalidConfigException
     */
    private function getExtendedViewItems(array $viewContentItems, string $fullyQualifiedTypeName, string $contentViewId, ?string $modelClass, ?Model $model, ?string $modelViewName): array
    {
        $defaultViewItems = [$viewContentItems];

        if ($contentViewId === 'default' && $model !== null && $modelViewName !== null) {
            $methodNames = preg_grep('/^getDefault' . ucfirst($modelViewName) . 'Items/', get_class_methods($model));

            if (count($methodNames) > 0) {
                foreach($methodNames as $method) {
                    $defaultViewItems[] = $model->$method();
                }
            }
        }

        $extendedViewItems = Yii::$app->view->getViewItems($fullyQualifiedTypeName, $contentViewId);

        foreach (ArrayHelper::remove($extendedViewItems, 'all', []) as $viewItems) {
            $defaultViewItems[] = $viewItems;
        }

        if ($model !== null) {
            foreach ($extendedViewItems as $interfaceName => $interfaceViewItems) {
                if (isset(class_implements($model)[$interfaceName])) {
                    foreach ($interfaceViewItems as $viewItems) {
                        $defaultViewItems[] = $viewItems;
                    }
                }
            }
        }

        if (count($defaultViewItems) === 1) {
            $viewContentItems = $defaultViewItems[0];
        } else {
            $viewContentItems = ArrayHelper::merge(...$defaultViewItems);
        }

        if ($modelClass !== null && $modelViewName === 'editView') {
            $firstItem = (array_values($viewContentItems)[0]['class'] ?? '');

            if ($contentViewId === 'default') {
                if ($firstItem !== Tab::class) {
                    throw new InvalidConfigException('Default edit view of "' . $modelClass . '" must start with "' . Tabs::class . '".');
                }

                return [[
                    'class' => Tabs::class,
                    'contents' => $viewContentItems
                ]];
            }

            if ($firstItem !== BaseView::class && $firstItem !== Tabs::class) {
                return [[
                    'class' => Tabs::class,
                    'contents' => [[
                        'class' => Tab::class,
                        'contents' => $viewContentItems
                    ]]
                ]];
            }
        }

        return $viewContentItems;
    }

    /**
     * @inheritdoc
     * @return string
     * @throws InvalidConfigException
     */
    public function run(): string
    {
        $this->getViewContent();

        $isWidget = $this->getSetting('isWidget');
        $title = Yii::$app->fafcms->getTextOrTranslation($this->name);

        if (!$isWidget) {
            Yii::$app->view->title = [
                $title,
            ];
        }

        $viewButtons = $this->buttons;

        return Yii::$app->view->render('@fafcms/fafcms/views/common/base', [
            'isWidget' => $this->getSetting('isWidget'),
            'content' => Yii::$app->view->renderContentItems($this->contents),
            'buttons' => static function() use ($viewButtons, $title) {
                $buttons = [];

                if ($viewButtons !== null) {
                    foreach ($viewButtons as $viewButton) {
                        if ($viewButton instanceof \Closure) {
                            $buttons = $viewButton($buttons);
                        } else {
                            throw new InvalidConfigException('Buttons of view "' . $title . '" must be an instance of ' . \Closure::class . ' got ' . gettype($viewButton) . ' instead.');
                        }
                    }
                }

                return $buttons;
            },
        ]);
    }
}
