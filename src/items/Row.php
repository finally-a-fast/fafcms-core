<?php

namespace fafcms\fafcms\items;

use Yii;
use yii\helpers\Html;
use fafcms\helpers\abstractions\ContentItem;

/**
 * Class Row
 * @package fafcms\helpers\abstractions\ContentItem
 */
class Row extends ContentItem
{
    public int $type = self::TYPE_CONTAINER;

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Row');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates a row.');
    }

    public function editorOptions(): array
    {
        return [
            'tag' => 'div',
            'options' => ['class' => 'row']
        ];
    }

    public function run(): string
    {
        return Yii::$app->view->render('@fafcms/fafcms/views/common/grid', [
            'rows' => [[[
                'raw' => true,
                'content' => $this->renderedContent,
            ]]],
        ]);
    }
}
