<?php

namespace fafcms\fafcms\items;

use Closure;
use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\NumberInput;
use fafcms\fafcms\inputs\TextInput;
use fafcms\helpers\ActiveRecord;
use Yii;
use fafcms\helpers\classes\ContentItemSetting;
use fafcms\fafcms\components\FafcmsComponent;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;

/**
 * Class EditView
 *
 * @package fafcms\fafcms\items
 */
class EditView extends BaseView
{
    /**
     * @inheritdoc
     */
    public array $name = ['fafcms-core', 'Edit view'];

    /**
     * @inheritdoc
     */
    public function label(): string
    {
        return Yii::t('fafcms-core', 'Edit View');
    }

    /**
     * @inheritdoc
     */
    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates an form to edit.');
    }

    /**
     * @inheritdoc
     */
    public function itemSettings(): ?array
    {
        return array_merge(parent::itemSettings(), [
            new ContentItemSetting($this, [
                'label'     => Yii::t('fafcms-core', 'Model class'),
                'name'      => 'modelClass',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class,
            ]),
            new ContentItemSetting($this, [
                'label'     => Yii::t('fafcms-core', 'Model id'),
                'name'      => 'modelId',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
            ]),
            new ContentItemSetting($this, [
                'label'     => Yii::t('fafcms-core', 'Scenario'),
                'name'      => 'scenario',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class,
            ]),
            new ContentItemSetting($this, [
                'label'     => Yii::t('fafcms-core', 'Create title'),
                'name'      => 'createTitle',
                'valueType' => FafcmsComponent::VALUE_TYPE_TRANSLATION,
                'inputType' => TextInput::class,
            ]),
            new ContentItemSetting($this, [
                'label'     => Yii::t('fafcms-core', 'Update title'),
                'name'      => 'updateTitle',
                'valueType' => FafcmsComponent::VALUE_TYPE_TRANSLATION,
                'inputType' => TextInput::class,
            ]),
            new ContentItemSetting($this, [
                'label'     => Yii::t('fafcms-core', 'Create action name'),
                'name'      => 'createActionName',
                'valueType' => FafcmsComponent::VALUE_TYPE_TRANSLATION,
                'inputType' => TextInput::class,
            ]),
            new ContentItemSetting($this, [
                'label'     => Yii::t('fafcms-core', 'Update action name'),
                'name'      => 'updateActionName',
                'valueType' => FafcmsComponent::VALUE_TYPE_TRANSLATION,
                'inputType' => TextInput::class,
            ]),
            new ContentItemSetting($this, [
                'label'        => Yii::t('fafcms-core', 'Add index to breadcrumb'),
                'name'         => 'addIndexToBreadcrumb',
                'valueType'    => FafcmsComponent::VALUE_TYPE_BOOL,
                'inputType'    => Checkbox::class,
                'defaultValue' => true,
            ]),
        ]);
    }

    /**
     * @param string      $permissionName
     * @param string      $authModelClass
     * @param string|null $plural
     *
     * @throws ForbiddenHttpException
     */
    protected function checkUserPermission(string $permissionName, string $authModelClass, ?string $plural): void
    {
        $allowed = Yii::$app->user->can($permissionName, ['modelClass' => $authModelClass]);

        if (!$allowed) {
            $actionText = Yii::$app->fafcms->getTextOrTranslation($this->getSetting('updateActionName') ?? ['fafcms-core', 'update']);

            $message = [
                'fafcms-core',
                'Your not allowed to {action}.',
                [
                    'action' => $actionText
                ]
            ];

            if ($plural !== null) {
                $message = [
                    'fafcms-core',
                    'Your not allowed to {action} {model}.',
                    [
                        'action' => $actionText,
                        'model' => $plural
                    ]
                ];
            }

            throw new ForbiddenHttpException(Yii::$app->fafcms->getTextOrTranslation($message));
        }
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     * @throws InvalidConfigException
     * @throws NotFoundHttpException
     */
    public function run(): string
    {
        $this->getViewContent();

        $modelClass     = $this->getSetting('modelClass');
        $isWidget       = $this->getSetting('isWidget');
        $modelId        = $this->getSetting('modelId');
        $scenario       = $this->getSetting('scenario');
        $viewButtons    = $this->buttons;
        $authModelClass = $modelClass::$authModelClass ?? $modelClass;
        $checkAccess    = $this->checkAccess && ($authModelClass::$checkAccess ?? true);

        $singular = $modelClass;
        $plural = null;

        if (method_exists($modelClass, 'getEditDataSingular')) {
            $singular = $modelClass::instance()->getEditDataSingular();
        }

        if (method_exists($modelClass, 'getEditDataPlural')) {
            $plural = $modelClass::instance()->getEditDataPlural();
        }

        if ($modelId === null) {
            if ($checkAccess) {
                $this->checkUserPermission('create', $authModelClass, $plural);
            }

            $model          = InjectorComponent::createObject($modelClass);
            $modelScenarios = $model->scenarios();

            if (isset($scenario, $modelScenarios[$scenario]) || (defined($modelClass . '::SCENARIO_CREATE') && isset($modelScenarios[$modelClass::SCENARIO_CREATE]))) {
                $model->setScenario($scenario ?? $modelClass::SCENARIO_CREATE);
            }

            $model->loadDefaultValues();

            $title = Yii::$app->fafcms->getTextOrTranslation($this->getSetting('createTitle') ?? [
                'fafcms-core',
                'Create {modelClass}',
                ['modelClass' => $singular]
            ]);
        } else {
            if ($checkAccess) {
                $this->checkUserPermission('edit', $authModelClass, $plural);
            }

            if (($model = $modelClass::findOne($modelId)) === null) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }

            $modelScenarios  = $model->scenarios();

            if (isset($scenario, $modelScenarios[$scenario]) || (defined($modelClass . '::SCENARIO_CREATE') && isset($modelScenarios[$modelClass::SCENARIO_UPDATE]))) {
                $model->setScenario($scenario ?? $modelClass::SCENARIO_UPDATE);
            }

            $title = Yii::$app->fafcms->getTextOrTranslation(
                $this->getSetting('updateTitle') ??
                (
                    method_exists($model, 'getExtendedLabel') ? [
                        'fafcms-core',
                        'Update {modelClass} "{modelTitle}"',
                        ['modelClass' => $singular, 'modelTitle' => $model->getExtendedLabel(false)]
                    ] : [
                        'fafcms-core',
                        'Update {modelClass}',
                        ['modelClass' => $singular]
                    ]
                )
            );
        }

        if (!$isWidget) {
            if ($this->getSetting('addIndexToBreadcrumb')) {
                Yii::$app->view->params['breadcrumbs'][] = [
                    'label' => $plural,
                    'url' => ['index']
                ];

                Yii::$app->view->title[] = $plural;
            }

            Yii::$app->view->title[] = $title;

            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => $title
            ];

            if ($this->name !== ['fafcms-core', 'Edit view']) {
                $viewTitle = Yii::$app->fafcms->getTextOrTranslation($this->name);

                Yii::$app->view->params['breadcrumbs'][] = [
                    'label' => $viewTitle
                ];

                Yii::$app->view->title[] = $viewTitle;
            }
        }

        $contentItems = $this->contents;
        $formOptions = [];

        if (method_exists($model, 'getFormOptions')) {
            $formOptions = $model->getFormOptions();
        }

        $allowAjaxForm = true;

        if (method_exists($model, 'getAllowAjaxForm')) {
            $allowAjaxForm = $model->getAllowAjaxForm();
        }

        if ($allowAjaxForm) {
            Html::addCssClass($formOptions['options'], 'ajax-form');
        }

        $addLoaderToSubmitButtons = true;

        if (method_exists($model, 'getAddLoaderToSubmitButtons')) {
            $addLoaderToSubmitButtons = $model->getAddLoaderToSubmitButtons();
        }

        if (!$addLoaderToSubmitButtons) {
            Html::addCssClass($formOptions['options'], 'no-loader');
        }

        $viewParams = [
            'content' => static function($model, $form) use ($contentItems, $modelClass) {
                Yii::$app->view->params['model'] = $model;
                Yii::$app->view->params['modelClass'] = $modelClass;
                Yii::$app->view->params['form'] = $form;

                return Yii::$app->view->renderContentItems($contentItems);
            },
            'isWidget' => Yii::$app->view->getRenderMode() === 'renderAjax' || $isWidget,
            'formOptions' => $formOptions,
            'buttons' => static function($model, $form, $editView) use ($viewButtons, $modelClass, $authModelClass) {
                $buttons = [
                    'close' => [
                        'icon' => 'close',
                        'position' => 'first',
                        'label' => Yii::t('fafcms-core', 'Cancel'),
                        'options' => ['class' => 'cancel'],
                        'visible' => Yii::$app->getRequest()->getIsAjax()
                    ],
                    'back' => [
                        'icon' => 'arrow-left',
                        'position' => 'first',
                        'label' => Yii::t('fafcms-core', 'Back'),
                        'url' => ['index'],
                        'visible' => !Yii::$app->getRequest()->getIsAjax()
                    ],
                    'save' => [
                        'options' => [
                            'class' => ['primary'],
                            'form' => $form->id,
                        ],
                        'type' => 'submit',
                        'visible' => Yii::$app->user->can('create', ['modelClass' => $authModelClass]) || Yii::$app->user->can('edit', ['modelClass' => $authModelClass]),
                        'icon' => 'content-save-outline',
                        'label' =>  Yii::t('fafcms-core', 'Save'),
                    ],
                    'more' => [
                        'icon' => 'dots-horizontal',
                        'position' => 'last',
                        'label' => Yii::t('fafcms-core', 'More actions'),
                        'options' => [
                            'class' => ['top right pointing'],
                            'data-direction' => 'upward'
                        ],
                        'items' => [],
                        'visible' => function($item) {
                            if (count($item['items']) > 0) {
                                foreach ($item['items'] as $childItem) {
                                    if (
                                        !isset($childItem['visible']) ||
                                        (($childItem['visible'] instanceof Closure) && $childItem['visible']($childItem)) ||
                                        $childItem['visible']
                                    ) {
                                        return true;
                                    }
                                }
                            }

                            return false;
                        },
                    ],
                ];

                if ($model instanceof ActiveRecord) {
                    $buttons['more']['items']['export'] = [
                        'after' => 'translate',
                        'icon' => 'download',
                        'label' =>  Yii::t('fafcms-core', 'Export'),
                        'url' => ['/' . $model->getEditDataUrl() . '/export', 'ids' => $model->id],
                        'options' => [
                            'target' => '_blank',
                        ],
                        'visible' => $model->isNewRecord === false && Yii::$app->user->can('edit', ['modelClass' => $authModelClass])
                    ];
                }

                if ($viewButtons !== null) {
                    foreach ($viewButtons as $viewButton) {
                        if ($viewButton instanceof Closure) {
                            $buttons = $viewButton($buttons, $model, $form, $editView);
                        } else {
                            throw new InvalidConfigException('Buttons of model "' . $modelClass . '" must be an instance of ' . Closure::class . ' got ' . gettype($viewButton) . ' instead.');
                        }
                    }
                }

                return $buttons;
            },
        ];

        $result = Yii::$app->fafcms->handleAjaxForm(
            $model,
            [
                'type' => 'error',
                'message' => static function($model, $result, $isNewRecord) use ($singular) {
                    if ($isNewRecord && method_exists($model, 'getErrorMessageAfterCreate')) {
                        return $model->getErrorMessageAfterCreate();
                    }

                    if (!$isNewRecord && method_exists($model, 'getErrorMessageAfterUpdate')) {
                        return $model->getErrorMessageAfterUpdate();
                    }

                    if (method_exists($model, 'getErrorMessageAfterSave')) {
                        return $model->getErrorMessageAfterSave();
                    }

                    $modelLabel = null;

                    if (method_exists($model, 'getExtendedLabel')) {
                        $modelLabel = $model->getExtendedLabel(false);
                    }

                    $message = Yii::t('fafcms-core', 'Error while saving {modelClass}!', [
                        'modelClass' => $singular,
                    ]);

                    if (!empty($modelLabel)) {
                        $message = Yii::t('fafcms-core', 'Error while saving {modelClass} "{modelLabel}"!', [
                            'modelClass' => $singular,
                            'modelLabel' => $modelLabel
                        ]);
                    }

                    return $message . '<br><br>' . implode('<br><br>', $model->getErrorSummary(true));
                }
            ],
            [
                'type' => 'success',
                'message' => static function($model, $result, $isNewRecord) use ($singular) {
                    if ($isNewRecord && method_exists($model, 'getSuccessMessageAfterCreate')) {
                        return $model->getSuccessMessageAfterCreate();
                    }

                    if (!$isNewRecord && method_exists($model, 'getSuccessMessageAfterUpdate')) {
                        return $model->getSuccessMessageAfterUpdate();
                    }

                    if (method_exists($model, 'getSuccessMessageAfterSave')) {
                        return $model->getSuccessMessageAfterSave();
                    }

                    $modelLabel = null;

                    if (method_exists($model, 'getExtendedLabel')) {
                        $modelLabel = $model->getExtendedLabel(false);
                    }

                    if (!empty($modelLabel)) {
                        return Yii::t('fafcms-core', '{modelClass} "{modelLabel}" has been saved.', [
                            'modelClass' => $singular,
                            'modelLabel' => $modelLabel
                        ]);
                    }

                    return Yii::t('fafcms-core', '{modelClass} has been saved.', [
                        'modelClass' => $singular,
                    ]);
                },
                'redirect' => static function($model, $result, $isNewRecord) {
                    if ($isNewRecord) {
                        $redirect = true;

                        if (method_exists($model, 'getRedirectAfterCreate')) {
                            $redirect = $model->getRedirectAfterCreate();
                        }
                    } else {
                        $redirect = false;

                        if (method_exists($model, 'getRedirectAfterUpdate')) {
                            $redirect = $model->getRedirectAfterUpdate();
                        }
                    }

                    if (method_exists($model, 'getRedirectAfterSave')) {
                        $redirect = $model->getRedirectAfterSave();
                    }

                    return $redirect;
                },
                'url' => static function($model, $result, $isNewRecord) {
                    if ($isNewRecord && method_exists($model, 'getRedirectUrlAfterCreate')) {
                        return $model->getRedirectUrlAfterCreate();
                    }

                    if (!$isNewRecord && method_exists($model, 'getRedirectUrlAfterUpdate')) {
                        return $model->getRedirectUrlAfterUpdate();
                    }

                    if (method_exists($model, 'getRedirectUrlAfterSave')) {
                        return $model->getRedirectUrlAfterSave();
                    }

                    if (isset($model->id) && method_exists($model, 'getEditDataUrl')) {
                        return Url::to(['/' . $model->getEditDataUrl() . '/update', 'id' => $model->id]);
                    }

                    return null;
                }
            ],
            '@fafcms/fafcms/views/common/edit',
            $viewParams,
            'save',
            null,
            false,
            false
        );

        if (!is_string($result)) {
            Yii::$app->view->renderedSiteMode = 'raw';
            Yii::$app->view->renderedSiteContent = $result;
            return '';
        }

        return $result;
    }
}
