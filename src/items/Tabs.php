<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\fafcms\components\FafcmsComponent;
use fafcms\helpers\classes\ContentItemSetting;

/**
 * Class Tabs
 * @package fafcms\fafcms\items
 */
class Tabs extends ContentItem
{
    public int $type = self::TYPE_CONTAINER;

    public bool $preRenderContent = false;

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Tabs');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates a tab container.');
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Id'),
                'name' => 'id',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class,
                'defaultValue' => static function() {
                    return Yii::$app->security->generateRandomString();
                }
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Show single tab'),
                'name' => 'singleTab',
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'inputType' => Checkbox::class,
                'defaultValue' => false
            ]),
        ];
    }

    public function run(): string
    {
        $tabs = [];

        foreach ($this->contents as $item) {
            $contentItem = null;
            $tabContent = Yii::$app->view->renderContentItem($item, $contentItem);

            $tabs[] = [
                'id' => $contentItem->getSetting('id'),
                'label' => Yii::$app->fafcms->getTextOrTranslation($contentItem->getSetting('label')),
                'active' => $contentItem->getSetting('active'),
                'content' => $tabContent
            ];
        }

        return Yii::$app->view->render('@fafcms/fafcms/views/common/tabs', [
            'id' => $this->getSetting('id'),
            'singleTab' => $this->getSetting('singleTab'),
            'tabs' => $tabs
        ]);
    }
}
