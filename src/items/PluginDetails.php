<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use fafcms\helpers\classes\ContentItemSetting;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\fafcms\components\FafcmsComponent;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class PluginDetails
 *
 * @package fafcms\fafcms\items
 */
class PluginDetails extends ContentItem
{
    public int $type = self::TYPE_CONTAINER;

    public bool $preRenderContent = false;

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Plugin details');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Shows details of an plugin');
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Plugin'),
                'name' => 'pluginId',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class
            ]),
        ];
    }

    public function run(): string
    {
        $pluginId = $this->getSetting('pluginId') ?? Yii::$app->view->params['pluginId'];
        $installedPlugins = Yii::$app->fafcms->getInstalledPackages();

/*  public 'name' => string 'Core' (length=4)
  public 'logo' => string 'https://www.finally-a-fast.com/packages/fafcms-core/assets/logo.jpg' (length=67)
  public 'header' => string 'https://www.finally-a-fast.com/packages/fafcms-core/assets/header.jpg' (length=69)
  public 'screenshots' =>
    array (size=2)
      0 =>
        object(stdClass)[1523]
          public 'img' => string 'https://www.finally-a-fast.com/packages/fafcms-core/assets/screenshot-frontend.jpg' (length=82)
          public 'title' => string 'Frontend' (length=8)
      1 =>
        object(stdClass)[1522]
          public 'img' => string 'https://www.finally-a-fast.com/packages/fafcms-core/assets/screenshot-backend.jpg' (length=81)
          public 'title' => string 'Backend' (length=7)*/

        if (isset($installedPlugins[$pluginId], $installedPlugins[$pluginId]['extra']->bootstrap)) {
            //$converter = new GithubFlavoredMarkdownConverter(['html_input' => 'escape', 'allow_unsafe_links' => false]);

            Yii::$app->view->params['pluginData'] = $installedPlugins[$pluginId]['extra']->fafcms;

            //var_dump(Yii::$app->view->params['pluginData']);
        }

        return Yii::$app->view->renderContentItems($this->contents);
    }
}
