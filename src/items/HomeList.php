<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use fafcms\helpers\classes\ContentItemSetting;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\fafcms\components\FafcmsComponent;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class HomeList
 * @package fafcms\fafcms\items
 */
class HomeList extends ContentItem
{
    public int $type = self::TYPE_ITEM;

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Form field');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates a form field.');
    }

    public function run(): string
    {
            /*
            <?php if ($homeBox) :?>
                <tr>
                    <td>Auswertungen:</td>
                    <td>
                        <?php
                        $dates = [];

                        for ($year = date('Y'); $year >= 2016; $year--) {
                            for ($month = ($year === date('Y')?date('m'):12); $month >= ($year === 2016?6:1); $month--) {
                                $dates[$year.'-'.(strlen($month) === 1?'0':'').$month] = (strlen($month) === 1?'0':'').$month.'.'.$year;
                            }
                        }

                        echo DROPDOWN::widget([
                            'name' => 'anaylsis_date',
                            'data' => $dates,
                            'value' => $dates[date('Y-m')],
                            'options' => [
                                'placeholder' => 'Zeitpunkt'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </td>
                </tr>
            <?php endif;?>*/

        $html = '';

        if (Yii::$app->getSession()->get('last_login_at') !== null && Yii::$app->getUser()->getIdentity()->last_logout_at < Yii::$app->getSession()->get('last_login_at')) {
            $html .= '<div class="collection-item avatar"><i class="mdi mdi-shield-alert-outline circle red"></i><b class="title red-text">'.Yii::t('fafcms-core', 'Warning!').'</b><p>'.Yii::t('fafcms-core', 'You didn\'t log out last time. For security reasons, please log out after finishing your work by clicking on {user} in the top right corner and then on Log out.', [
                'user' => Yii::$app->getUser()->getChip()
            ]).'</p>'. '</div>';
        }

        if (Yii::$app->getSession()->get('last_login_at') !== null) {
            $html .= '<div class="collection-item avatar"><i class="mdi mdi-shield-key-outline circle blue"></i><b class="title blue-text">Letzter Login</b><p>'.Yii::$app->formatter->asDatetime(Yii::$app->getSession()->get('last_login_at')).'</p>'. '</div>';
        }

        if (Yii::$app->getUser()->getIdentity()->last_change_type !== null) {
            $className = Yii::$app->getUser()->getIdentity()->last_change_type;
            $lastChange = $className::find()->where(['id' => Yii::$app->getUser()->getIdentity()->last_change_id])->one();

            $lastChangeIcon = 'pencil-outline';
            $lastChangeLabel = $className;
            $lastChangeUrl = null;

            if (method_exists($lastChange, 'getEditDataUrl')) {
                $lastChangeUrl = [$lastChange->getEditDataUrl(), 'id' => Yii::$app->getUser()->getIdentity()->last_change_id];
            }

            if (method_exists($lastChange, 'getEditDataSingular')) {
                $lastChangeLabel = $lastChange->getEditDataSingular();
            }

            if (method_exists($lastChange, 'getExtendedLabel')) {
                $lastChangeLabel .= ': ' . $lastChange->getExtendedLabel();
            }

            if (method_exists($lastChange, 'getEditDataIcon')) {
                $lastChangeIcon = $lastChange->getEditDataIcon();
            }

            $lastChangeItem = '<i class="mdi mdi-'.$lastChangeIcon.' circle green"></i><b class="title green-text">Zuletzt bearbeitet</b><p>'.$lastChangeLabel.'</p>';

            if ($lastChangeUrl === null) {
                $html .= Html::tag('div', $lastChangeItem, ['class' => 'collection-item avatar']);
            } else {
                $html .= Html::a($lastChangeItem, $lastChangeUrl, ['class' => 'collection-item avatar']);
            }
        }

        return '<div class="collection">' . $html . '</div>';
    }
}
