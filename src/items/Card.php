<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\inputs\NumberInput;
use fafcms\fafcms\inputs\TextInput;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\helpers\classes\ContentItemSetting;
use fafcms\fafcms\components\FafcmsComponent;

/**
 * Class Card
 * @package fafcms\fafcms\items
 */
class Card extends ContentItem
{
    public int $type = self::TYPE_CONTAINER;

    /**
     * @var array
     */
    public array $buttons = [];

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Card');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates a Card.');
    }

    public function editorOptions(): array
    {
        return [
            'tag' => 'div',
            'options' => [
                'class' => [
                    'col',
                    $this->getSetting('s') !== null ? 's' . $this->getSetting('s') : '',
                    $this->getSetting('m') !== null ? 'm' . $this->getSetting('m') : '',
                    $this->getSetting('l') !== null ? 'l' . $this->getSetting('l') : '',
                    $this->getSetting('xl') !== null ? 'xl' . $this->getSetting('xl') : '',
                ]
            ]
        ];
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label'        => Yii::t('fafcms-core', 'Title'),
                'name'         => 'title',
                'valueType'    => FafcmsComponent::VALUE_TYPE_TEXT,
                'inputType'    => TextInput::class,
                'defaultValue' => Yii::t('fafcms-core', 'Card'),
            ]),
            new ContentItemSetting($this, [
                'label'     => Yii::t('fafcms-core', 'Icon'),
                'name'      => 'icon',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
            ]),
            new ContentItemSetting($this, [
                'label'        => Yii::t('fafcms-core', 'ID'),
                'name'         => 'id',
                'valueType'    => FafcmsComponent::VALUE_TYPE_TEXT,
                'inputType'    => TextInput::class,
            ]),
        ];
    }

    public function run(): string
    {
        return Yii::$app->view->render('@fafcms/fafcms/views/common/card', [
            'title'              => Yii::$app->fafcms->getTextOrTranslation($this->getSetting('title')),
            'icon'               => $this->getSetting('icon'),
            'content'            => $this->renderedContent,
            'buttons'            => $this->buttons,
            'cardContentOptions' => [
                'id' => $this->getSetting('id'),
            ],
        ]);
    }
}
