<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\inputs\TextInput;
use Yii;
use yii\helpers\Inflector;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\fafcms\components\FafcmsComponent;
use fafcms\helpers\classes\ContentItemSetting;

/**
 * Class Tab
 * @package fafcms\fafcms\items
 */
class Tab extends ContentItem
{
    public int $type = self::TYPE_CONTAINER;

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Tab');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates a tab.');
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Id'),
                'name' => 'id',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class,
                'defaultValue' => static function($contentItemSetting) {
                    return Inflector::slug(Yii::$app->fafcms->getTextOrTranslation($contentItemSetting->contentItem->getSetting('label')));
                }
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Label'),
                'name' => 'label',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class,
                'defaultValue' => Yii::t('fafcms-core', 'Tab')
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Active'),
                'name' => 'active',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class,
                'defaultValue' => false
            ]),
        ];
    }

    public function run(): string
    {
        return $this->renderedContent;
    }
}
