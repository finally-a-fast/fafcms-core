<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use fafcms\helpers\classes\ContentItemSetting;
use League\CommonMark\GithubFlavoredMarkdownConverter;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\fafcms\components\FafcmsComponent;
use yii\base\InvalidArgumentException;
use yii\helpers\ArrayHelper;

/**
 * Class PluginDocs
 *
 * @package fafcms\fafcms\items
 */
class PluginDocs extends ContentItem
{
    public int $type = self::TYPE_ITEM;

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Plugin docs');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Shows docs of an plugin');
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Plugin'),
                'name' => 'pluginId',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class
            ]),
        ];
    }

    public function run(): string
    {
        $pluginId = $this->getSetting('pluginId') ?? Yii::$app->view->params['pluginId'];
        $installedPlugins = Yii::$app->fafcms->getInstalledPackages();

        if (isset($installedPlugins[$pluginId], $installedPlugins[$pluginId]['extra']->bootstrap)) {
            $tabs = [];

            Yii::$app->view->params['pluginData'] = $installedPlugins[$pluginId]['extra']->fafcms;

            foreach (Yii::$app->view->params['pluginData']->docs as $name => $doc) {
                $content = Yii::$app->pluginCache->getOrSet('plugin-docs-' . $pluginId . '-' . md5($name), function () use ($doc) {
                    if (filter_var($doc, FILTER_VALIDATE_URL) === FALSE) {
                        return false;
                    }

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $doc);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
                    $output = curl_exec($ch);
                    curl_close($ch);

                    if ($output === false) {
                        return false;
                    }

                    $converter = new GithubFlavoredMarkdownConverter([
                        'html_input' => 'escape',
                        'allow_unsafe_links' => false,
                        'max_nesting_level' => 5
                    ]);

                    return $converter->convertToHtml($output);
                }, 86400);

                $tabs[] = [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => $name,
                    ],
                    'contents' => [
                        'content' => [
                            'class' => Html::class,
                            'contents' => $content
                        ]
                    ]
                ];
            }

            return Yii::$app->view->renderContentItems([
                [
                    'class' => Tabs::class,
                    'settings' => [
                        'singleTab' => true
                    ],
                    'contents' => $tabs
                ]
            ]);
        }

        return '';
    }
}
