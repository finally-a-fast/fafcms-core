<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\inputs\NumberInput;
use Yii;
use yii\helpers\Html;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\helpers\classes\ContentItemSetting;
use fafcms\fafcms\components\FafcmsComponent;

/**
 * Class Column
 * @package fafcms\fafcms\items
 */
class Column extends ContentItem
{
    public int $type = self::TYPE_CONTAINER;

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Column');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates a Column.');
    }

    public function editorOptions(): array
    {
        return [
            'tag' => 'div',
            'options' => [
                'class' => [
                    'col',
                    $this->getSetting('s') !== null ? 's' . $this->getSetting('s') : '',
                    $this->getSetting('m') !== null ? 'm' . $this->getSetting('m') : '',
                    $this->getSetting('l') !== null ? 'l' . $this->getSetting('l') : '',
                    $this->getSetting('xl') !== null ? 'xl' . $this->getSetting('xl') : '',
                    $this->getSetting('xl') !== null ? 'xxl' . $this->getSetting('xxl') : '',
                ],
            ]
        ];
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Mobile Devices'),
                'name' => 's',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
                'defaultValue' => 16
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Tablet Devices'),
                'name' => 'm',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Desktop Devices'),
                'name' => 'l',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Large Desktop Devices'),
                'name' => 'xl',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Widescreen Desktop Devices'),
                'name' => 'xxl',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class,
            ])
        ];
    }

    public function run(): string
    {
        $mobile = $this->getSetting('s');

        $tablet = $this->getSetting('m');

        if (empty($tablet)) {
            $tablet = $mobile;
        }

        $computer = $this->getSetting('l');

        if (empty($computer)) {
            $computer = $tablet;
        }

        $large = $this->getSetting('xl');

        if (empty($large)) {
            $large = $computer;
        }

        $widescreen = $this->getSetting('xxl');

        if (empty($widescreen)) {
            $widescreen = $large;
        }

        return Html::tag('div', $this->renderedContent, [
            'class' => [
                'column',
                $mobile !== null ? (FafcmsComponent::CLASS_NAME_NUMBER_MAP[$mobile] . ' wide mobile') : '',
                $tablet !== null ? (FafcmsComponent::CLASS_NAME_NUMBER_MAP[$tablet] . ' wide tablet') : '',
                $computer !== null ? (FafcmsComponent::CLASS_NAME_NUMBER_MAP[$computer] . ' wide computer') : '',
                $large !== null ? (FafcmsComponent::CLASS_NAME_NUMBER_MAP[$large] . ' wide large screen') : '',
                $widescreen !== null ? (FafcmsComponent::CLASS_NAME_NUMBER_MAP[$widescreen] . ' wide widescreen') : '',
            ]
        ]);
    }
}
