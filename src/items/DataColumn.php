<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\NumberInput;
use fafcms\fafcms\inputs\TextInput;
use fafcms\fafcms\widgets\columns\DataColumn as DataColumnWidget;
use fafcms\fafcms\widgets\GridView;
use fafcms\helpers\classes\ContentItemSetting;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\fafcms\components\FafcmsComponent;
use yii\base\InvalidArgumentException;

/**
 * Class DataColumn
 * @package fafcms\fafcms\items
 */
class DataColumn extends ContentItem
{
    public int $type = self::TYPE_COLUMN;

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Data column');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates a data column for index view.');
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Field'),
                'name' => 'field',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Sort'),
                'name' => 'sort',
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'inputType' => NumberInput::class
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Label'),
                'name' => 'label',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Hide label'),
                'name' => 'hideLabel',
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'inputType' => Checkbox::class,
                'defaultValue' => false
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Link'),
                'name' => 'link',
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'inputType' => Checkbox::class,
                'defaultValue' => false
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Link action'),
                'name' => 'linkAction',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class,
                'defaultValue' => 'update'
            ]),
        ];
    }

    public function run(): array
    {
        $model = Yii::$app->view->params['model'];
        $fieldName = $this->getSetting('field');
        $fieldConfigs = Yii::$app->fafcms->getModelFieldConfig($model);
        $fieldConfig = $fieldConfigs[$fieldName]??null;

        if ($fieldConfig === null) {
            throw new InvalidArgumentException('Unknown form field "'.$fieldName.'" in model "'.get_class($model).'".');
        }

        $label = null;

        if ($this->getSetting('hideLabel')) {
            $label = false;
        } elseif ($this->getSetting('label') !== null) {
            $label = Yii::$app->fafcms->getTextOrTranslation($this->getSetting('label'));
        }

        return [
            'class' => DataColumnWidget::class,
            'attribute' => $fieldName,
            'sort' => $this->getSetting('sort'),
            'label' => $label,
            'link' => $this->getSetting('link'),
            'linkAction' => $this->getSetting('linkAction'),
            'format' => $fieldConfig['type']::$columnFormat
        ];
    }
}
