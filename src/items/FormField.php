<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use fafcms\helpers\classes\ContentItemSetting;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\fafcms\components\FafcmsComponent;

/**
 * Class FormField
 *
 * @package fafcms\fafcms\items
 */
class FormField extends ContentItem
{
    public int $type = self::TYPE_ITEM;
    public const NEW_RELATION = 'NEW';

    public array $fieldDefinition = [];

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Form field');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Creates a form field.');
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Field'),
                'name' => 'field',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Field definition'),
                'name' => 'fieldDefinition',
                'valueType' => FafcmsComponent::VALUE_TYPE_ARRAY,
                'defaultValue' => []
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Label'),
                'name' => 'label',
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'inputType' => TextInput::class
            ]),
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Hide label'),
                'name' => 'hideLabel',
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'inputType' => Checkbox::class,
                'defaultValue' => false
            ]),
        ];
    }

    public function run(): string
    {
        $model = Yii::$app->view->params['model'];
        $fieldName = $this->getSetting('field');
        $fieldConfigs = Yii::$app->fafcms->getModelFieldConfig($model);
        $fieldConfig = $fieldConfigs[$fieldName] ?? null;

        if ($this->getSetting('hideLabel')) {
            $fieldConfig['labelOptions']['label'] = false;
        } elseif ($this->getSetting('label') !== null) {
            $fieldConfig['labelOptions']['label'] = Yii::$app->fafcms->getTextOrTranslation($this->getSetting('label'));
        }

        $fieldDefinition = array_merge(
            Yii::$app->fafcms->getFieldDefinition($model, $fieldName, $fieldConfigs, $fieldConfig),
            $this->getSetting('fieldDefinition')
        );

        if($fieldName === 'contentmeta.topicids') {
           // var_dump($fieldConfig, $fieldDefinition);die();
        }
        try {
        return Yii::$app->fafcms->getInput(
            Yii::$app->view->params['form'],
            $fieldDefinition['model'],
            $fieldDefinition['fieldName'],
            $fieldDefinition['fieldType'],
            $fieldDefinition['labelOptions'],
            $fieldDefinition['fieldConfig'],
            $fieldDefinition['inputName'],
            $fieldDefinition['inputId']
        );} catch (\TypeError $e) {
            var_dump(Yii::$app->fafcms->getFieldDefinition($model, $fieldName, $fieldConfigs, $fieldConfig));die();
        }
    }
}
