<?php

namespace fafcms\fafcms\items;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\helpers\classes\ContentItemSetting;
use fafcms\parser\component\Parser;
use Yii;
use fafcms\helpers\abstractions\ContentItem;
use fafcms\fafcms\components\FafcmsComponent;

/**
 * Class Html
 * @package fafcms\fafcms\items
 */
class Html extends ContentItem
{
    public int $type = self::TYPE_ITEM;
    public bool $preRenderContent = false;

    public function label(): string
    {
        return Yii::t('fafcms-core', 'Html');
    }

    public function description(): string
    {
        return Yii::t('fafcms-core', 'Show custom Html.');
    }

    public function itemSettings(): ?array
    {
        return [
            new ContentItemSetting($this, [
                'label' => Yii::t('fafcms-core', 'Parse Content'),
                'name' => 'parse',
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'inputType' => Checkbox::class,
                'defaultValue' => true
            ]),
        ];
    }

    public function run(): string
    {
        $html = implode('', $this->contents);

        if ($this->getSetting('parse')) {
            Yii::$app->fafcms->prepareRender();
            $html = Yii::$app->fafcmsParser->parse(Parser::TYPE_PAGE, $html, Parser::ROOT, Yii::$app->view->params ?? null);
        }

        return '<div>' . $html . '</div>';
    }
}
