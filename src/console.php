<?php
define('YII_CONSOLE', true);
defined('FAFCMS_CONFIG_PATH') or define('FAFCMS_CONFIG_PATH', FAFCMS_BASE_PATH . '/config');

require(FAFCMS_VENDOR_PATH . '/autoload.php');

$overwriteConfig = @include(FAFCMS_CONFIG_PATH . '/overwrite.php');
$mainConfig = require(FAFCMS_CORE_PATH . '/config/main.php');

$config = yii\helpers\ArrayHelper::merge($mainConfig, $overwriteConfig ?: []);

require(FAFCMS_VENDOR_PATH . '/autoload.php');
require(FAFCMS_VENDOR_PATH . '/yiisoft/yii2/Yii.php');

$app = new \fafcms\fafcms\classes\ConsoleApplication($config);

require(FAFCMS_CORE_PATH . '/config/bootstrap.php');
@include(FAFCMS_CONFIG_PATH . '/bootstrap.php');

if (FAFCMS_NO_CONFIG) {
    Yii::setAlias('@fafcms/fafcms', FAFCMS_CORE_PATH);
}

$exitCode = $app->run();
exit($exitCode);
