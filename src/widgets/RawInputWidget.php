<?php

namespace fafcms\fafcms\widgets;

use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * Class RawInputWidget
 *
 * @package fafcms\fafcms\widgets
 */
class RawInputWidget extends InputWidget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        $value = $this->value;

        if ($this->hasModel()) {
            if (isset($options['value'])) {
                $value = $options['value'];
                unset($options['value']);
            } else {
                $value = Html::getAttributeValue($this->model, $this->attribute);
            }
        }

        return Html::tag('div', $value, $this->options);
    }
}
