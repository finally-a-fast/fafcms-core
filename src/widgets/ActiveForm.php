<?php

namespace fafcms\fafcms\widgets;

use fafcms\fafcms\assets\fomantic\behaviors\FomanticFormAsset as FomanticFormAssetBehavior;
use fafcms\fafcms\assets\fomantic\collections\FomanticFormAsset as FomanticFormAssetCollection;
use Yii;
use yii\helpers\Html;

/**
 * Class ActiveForm
 *
 * @package fafcms\fafcms\widgets
 */
class ActiveForm extends \yii\widgets\ActiveForm
{
    /**
     * @var string
     */
    public $fieldClass = ActiveField::class;

    /**
     * @var string
     */
    public $errorCssClass = 'field error';

    /**
     * @var string
     */
    public $successCssClass = 'field success';

    /**
     * @var string
     */
    public $validatingCssClass = 'field loading';

    /**
     * @var bool
     */
    public bool $disableSubmitOnEnter = true;

    /**
     * @return string
     */
    public function run(): string
    {
        FomanticFormAssetBehavior::register(Yii::$app->view);
        FomanticFormAssetCollection::register(Yii::$app->view);

        Html::addCssClass($this->options, 'ui form');

        if (!empty($this->_fields)) {
            throw new InvalidCallException('Each beginField() should have a matching endField() call.');
        }

        $content = ob_get_clean();
        $html = Html::beginForm($this->action, $this->method, $this->options);

        if ($this->disableSubmitOnEnter) {
            $html .= Html::submitInput('', ['style' => 'display:none', 'disabled' => true, 'aria-hidden' => true]);
        }

        $html .= $content;

        if ($this->enableClientScript) {
            $this->registerClientScript();
        }

        $html .= Html::endForm();

        return $html;
    }
}
