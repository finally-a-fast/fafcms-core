<?php

namespace fafcms\fafcms\widgets;

use yii\helpers\Json;
use yii\web\JsExpression;
use yii\widgets\MaskedInputAsset;

/**
 * Class MaskedInput
 *
 * @package fafcms\fafcms\widgets
 */
class MaskedInput extends \yii\widgets\MaskedInput
{
    /**
     * {@inheritdoc}
     */
    protected function renderInputHtml($type)
    {
        $this->field->{$type . 'Input'}($this->options);
        return $this->field->parts['{input}'];
    }
}
