<?php

namespace fafcms\fafcms\widgets;

use fafcms\fafcms\assets\TinyMceAsset;
use Yii;
use yii\web\JsExpression;
use yii\widgets\InputWidget;
use yii\helpers\Html;
use yii\helpers\Json;

class TinyMce extends InputWidget
{
    public $clientOptions = [];

    public function run()
    {
        $view = $this->getView();

        Html::addCssClass($this->options, 'active');
        $this->options['placeholder'] = '';

        if (($this->clientOptions['plugins'] ?? null) === null) {
            $this->clientOptions['plugins'] = [
                'autoresize searchreplace code autolink directionality visualblocks visualchars fullscreen link table charmap hr nonbreaking anchor advlist lists wordcount help paste',// fafcms',
            ];
        }

        $this->clientOptions['menubar'] = 'edit insert format table view help';

        if (($this->clientOptions['toolbar1'] ?? null) === null) {
            $this->clientOptions['toolbar1'] = 'removeformat | forecolor backcolor | bold italic underline strikethrough superscript subscript | alignleft aligncenter alignright alignjustify | numlist bullist | link';// | fafcms';
        }

        if (($this->clientOptions['autoresize_max_height'] ?? null) === null) {
            $this->clientOptions['autoresize_max_height'] = 500;
        }

        $extendedValidElements = '';
        $customElements = '';

        /*
        foreach (array_keys(Yii::$app->fafcmsParser->replacements) as $replacement) {
            $extendedValidElements .= ($extendedValidElements !== ''?',':'').Yii::$app->fafcmsParser->name.'-'.$replacement.'*[data-tag-type='.Yii::$app->fafcmsParser->name.'-'.$replacement.'|*]';
            $customElements .= ($customElements !== ''?',':'').Yii::$app->fafcmsParser->name.'-'.$replacement;
        }*/

        $this->clientOptions['branding'] = false;
        $this->clientOptions['image_advtab'] = true;
        $this->clientOptions['visualblocks_default_state'] = true;

        $url = $view->getAssetManager()->publish(dirname(__DIR__) . '/assets/tinymce/css/tinymce.min.css');
        $this->clientOptions['content_css'] = $url[1];

        $this->clientOptions['extended_valid_elements'] = [
            $extendedValidElements.',i[*],b[*]'
        ];
        //$this->clientOptions['custom_elements'] = $customElements;
/*

var dfreeBodyConfig = {
  selector: '.dfree-body',
  menubar: true,
  inline: true,
  toolbar: true,
  plugins: [
    'autolink',
    'codesample',
    'link',
    'lists',
    'media',
    'powerpaste',
    'table',
    'image',
    'quickbars',
    'codesample',
    'help'
  ],
  toolbar: false,
  quickbars_insert_toolbar: 'quicktable image media codesample',
  quickbars_selection_toolbar: 'bold italic underline | formatselect | blockquote quicklink',
  contextmenu: 'undo redo | inserttable | cell row column deletetable | help',
  powerpaste_word_import: 'clean',
  powerpaste_html_import: 'clean',
  content_css: '//www.tiny.cloud/css/codepen.min.css'
};*/

//        tinymce.PluginManager.add('fafcms', function(editor, url) {
//            /*
//            // Add a button that opens a window
//            editor.addButton('fafcms', {
//                text: 'Custom EM',
//                icon: false,
//                onclick: function() {
//                    // Open window
//                    editor.windowManager.open({
//                        title: 'Please input text',
//                        body: [
//                            {type: 'textbox', name: 'description', label: 'Text'}
//                        ],
//                        onsubmit: function(e) {
//                            // Insert content when the window form is submitted
//                            editor.insertContent('<emstart>EM Start</emstart><p>' + e.data.description + '</p><emend>EM End</emend>');
//                        }
//                    });
//                }
//            });
//
//            // Adds a menu item to the tools menu
//            editor.addMenuItem('customEmElementMenuItem', {
//                text: 'Custom EM Element',
//                context: 'tools',
//                onclick: function() {
//                    editor.insertContent('<emstart>EM Start</emstart><p>Example text!</p><emend>EM End</emend>');
//                }
//            });*/
//        });

        TinyMceAsset::register($view);

        if ($this->hasModel()) {
            echo Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            echo Html::textarea($this->name, $this->value, $this->options);
        }

        $this->registerClientScript();
    }

    /**
     * Registers tinyMCE js plugin
     */
    protected function registerClientScript()
    {
        $view = $this->getView();

        TinyMceAsset::register($view);

        $id = $this->options['id'];
        $this->clientOptions['selector'] = '#' . $id;

        $languagePath = Yii::getAlias('@fafcms/sitemanager/assets/tinymce/js/langs/');
        $languageFile = null;

        if (Yii::$app->language !== 'en') {
            $languageFile = $languagePath . Yii::$app->language . '.js';

            if (!file_exists($languageFile)) {
                $languageFile = $languagePath . explode('_', Yii::$app->language)[0] . '.js';

                if (!file_exists($languageFile)) {
                    $languageFile = null;
                }
            }
        }

        if ($languageFile !== null) {
            $url = $view->getAssetManager()->publish($languageFile);
            $this->clientOptions['language_url'] = $url[1];
            $this->clientOptions['language'] = Yii::$app->language;
        }

        $this->clientOptions['setup'] = new JsExpression('function (editor) {
            editor.on(\'change blur\', function (e) {
                editor.save()
            })
        }');

        $view->registerJs('tinymce.init(' . Json::encode($this->clientOptions) . ');');
    }
}
