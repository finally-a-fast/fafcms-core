<?php

declare(strict_types=1);

namespace fafcms\fafcms\widgets;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class Breadcrumbs extends Widget
{
    /**
     * @var array<int, array<string, array|string>>
     */
    public array $links = [];

    /**
     * @var array|null
     */
    public ?array $homeLink = null;

    public function run(): string
    {
        $links = $this->renderItem($this->homeLink ?? [
            'label' => ['fafcms-core', 'Home'],
            'url' => Yii::$app->homeUrl,
            'divider' => ''
        ]);

        $linkCount = count($this->links);

        foreach ($this->links as $index => $link) {
            if (!is_array($link)) {
                $link = ['label' => 'link'];
            }

            if ($index === $linkCount - 1) {
                unset($link['url']);
            }

            $links .= $this->renderItem($link);
        }

        return Html::tag('div', $links, ['class' => 'ui breadcrumb inverted small']);
    }

    /**
     * @param array<string, array> $link
     *
     * @return string
     */
    protected function renderItem(array $link): string
    {
        $encodeLabel = ArrayHelper::remove($link, 'encode', true);

        $label = Yii::$app->fafcms->getTextOrTranslation($link['label']);

        if ($encodeLabel) {
            $label = Html::encode($label);
        }

        $options = $link;

        if (!isset($options['options']) || !is_array($options['options'])) {
            $options['options'] = [];
        }

        Html::addCssClass($options['options'], 'section');

        if (!isset($options['divider'])) {
            $options['divider'] = '<i class="right mdi mdi-chevron-right icon divider"></i>';
        }

        if (isset($link['url'])) {
            return $options['divider'] . Html::a($label, $link['url'], $options['options']);
        }

        Html::addCssClass($options['options'], 'active');
        return $options['divider'] . Html::tag('div', $label, $options['options']);
    }
}
