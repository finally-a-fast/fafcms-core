<?php

namespace fafcms\fafcms\widgets;

use fafcms\fafcms\assets\fafcms\backend\FafcmsCalendarAsset;
use fafcms\fafcms\assets\fafcms\backend\FafcmsCheckboxAsset;
use fafcms\fafcms\assets\fafcms\backend\FafcmsDropdownAsset;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

class ActiveField extends \yii\widgets\ActiveField
{
    public $options = ['class' => ['field' => 'field']];
    public $inputOptions = [];
    public $errorOptions = ['class' => 'ui message error'];
    public $labelOptions = [];
    public $hintOptions = ['class' => 'ui message info'];

    /**
     * @param array $options
     * @param bool  $enclosedByLabel
     *
     * @return ActiveField
     */
    public function checkbox($options = [], $checkboxOptions = [], $enclosedByLabel = false)
    {
        FafcmsCheckboxAsset::register(Yii::$app->view);

        $this->template = "{checkboxContainerStart}\n{input}\n{label}\n{checkboxContainerEnd}\n{hint}\n{error}";

        Html::addCssClass($checkboxOptions, 'ui checkbox');
        $this->parts['{checkboxContainerStart}'] = Html::beginTag('div', $checkboxOptions);
        $this->parts['{checkboxContainerEnd}'] = Html::endTag('div');

        parent::checkbox($options, $enclosedByLabel);

        return $this;
    }

    /**
     * @param array $options
     *
     * @return $this|ActiveField
     */
    public function hiddenInput($options = [])
    {
        $this->template = '{input}';
        $this->options['tag'] = null;
        return parent::hiddenInput($options);
    }

    /**
     * @param array $items
     * @param array $options
     *
     * @return $this|ActiveField
     */
    public function dropDownList($items, $options = [])
    {
        FafcmsDropdownAsset::register(Yii::$app->view);

        Html::addCssClass($options, 'ui dropdown');

        return parent::dropDownList($items, $options);
    }

    /**
     * @param       $items
     * @param array $options
     *
     * @return $this
     */
    public function extendedDropDownList($items, $options = [])
    {
        FafcmsDropdownAsset::register(Yii::$app->view);

        Html::addCssClass($options, 'ui dropdown selection');

        $options = array_merge($this->inputOptions, $options);

        if (($options['options']['disabled'] ?? false) === true || ($options['options']['disabled'] ?? false) === 'disabled') {
            Html::addCssClass($options, 'disabled');
        }

        if (($options['options']['readonly'] ?? false) === true || ($options['options']['readonly'] ?? false) === 'readonly') {
            Html::addCssClass($options, 'readonly');
        }

        if ($this->form->validationStateOn === ActiveForm::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);

        $name = $options['options']['name'] ?? Html::getInputName($this->model, $this->attribute);
        $id = $options['options']['id'] ?? Html::getInputId($this->model, $this->attribute);
        $selection = Html::getAttributeValue($this->model, $this->attribute);

        $html = '';

        foreach ($items as $itemKey => $itemLabel) {
            $html .= Html::tag('div', $itemLabel, ['class' => 'item', 'data-value' => $itemKey]);
        }

        $selectOptions = [];

        if (isset($options['options'])) {
            $selectOptions = ArrayHelper::remove($options, 'options');
        }

        if (!isset($selectOptions['id'])) {
            $selectOptions['id'] = $id;
        }

        if ($selectOptions['multiple'] ?? false) {
            Html::addCssClass($options, 'multiple');
        }

        //TODO
        $itemOptions = $selectOptions['options'] ?? [];
        $itemGroups = $selectOptions['groups'] ?? [];

        $this->parts['{input}'] = Html::tag(
            'div',
            Html::dropDownList($name, $selection, $items, $selectOptions) .
            '<div class="default text"></div><i class="dropdown icon"></i>' .
            Html::tag(
                'div',
                $html,
                ['class' => 'menu']
            ),
            $options
        );

        return $this;
    }

    /**
     * @param array $options
     *
     * @return ActiveField
     */
    public function dateTimeInput($options = [])
    {
        FafcmsCalendarAsset::register(Yii::$app->view);

        $options = array_merge($this->inputOptions, $options);

        if (($options['disabled'] ?? false) === true || ($options['disabled'] ?? false) === 'disabled') {
            Html::addCssClass($options, 'disabled');
        }

        if (($options['readonly'] ?? false) === true || ($options['readonly'] ?? false) === 'readonly') {
            Html::addCssClass($options, 'readonly');
        }

        if ($this->form->validationStateOn === ActiveForm::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);

        $name = Html::getInputName($this->model, $this->attribute);
        $id = Html::getInputId($this->model, $this->attribute);
        $value = Html::getAttributeValue($this->model, $this->attribute);

        $inputOptions = ArrayHelper::remove($options, 'inputOptions', []);

        if (!isset($inputOptions['id'])) {
            $inputOptions['id'] = $id;
        }

        $icon = ArrayHelper::remove($options, 'icon', 'mdi mdi-calendar-clock');

        Html::addCssClass($options, 'ui calendar');
        unset($options['id']);

        $this->parts['{input}'] = Html::tag(
            'div',
            Html::tag(
                'div',
                Html::tag('i', '', ['class' => ['icon', $icon]]) .
                Html::input('text', $name, $value, $inputOptions),
                ['class' => 'ui input left icon']
            ),
            $options
        );

        return $this;
    }

    /**
     * @param array $options
     *
     * @return ActiveField
     */
    public function colorInput($options = [])
    {
        Html::addCssClass($options, ['input' => 'color']);
        return parent::input('color', $options);
    }

    /**
     * @param array $options
     *
     * @return ActiveField
     */
    public function emailInput($options = [])
    {
        Html::addCssClass($options, ['input' => 'email']);
        return parent::input('email', $options);
    }

    /**
     * @param array $options
     *
     * @return ActiveField
     */
    public function numberInput($options = [])
    {
        Html::addCssClass($options, ['input' => 'number']);
        return parent::input('number', $options);
    }

    /**
     * @param array $options
     *
     * @return ActiveField
     */
    public function rangeInput($options = [])
    {
        Html::addCssClass($options, ['input' => 'range']);
        return parent::input('range', $options);
    }

    /**
     * @param array $options
     *
     * @return ActiveField
     */
    public function searchInput($options = [])
    {
        Html::addCssClass($options, ['input' => 'search']);
        return parent::input('search', $options);
    }

    /**
     * @param array $options
     *
     * @return ActiveField
     */
    public function telInput($options = [])
    {
        Html::addCssClass($options, ['input' => 'tel']);
        return parent::input('tel', $options);
    }

    /**
     * @param array $options
     *
     * @return ActiveField
     */
    public function urlInput($options = [])
    {
        Html::addCssClass($options, ['input' => 'url']);
        return parent::input('url', $options);
    }
}
