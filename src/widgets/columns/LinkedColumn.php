<?php

namespace yiiui\yii2advancedgridview\grid\columns;

use yii\helpers\Html;
use Closure;

class LinkedColumn extends DataColumn
{
    public $sort;
    public $format = 'raw';
    public $url = null;
    public $linkOptions = [];

    public function getDataCellValue($model, $key, $index)
    {
        $content = parent::getDataCellValue($model, $key, $index);
        $url = $this->url;

        if ($url === null) {
            $url = ['/'.$model->getEditData()['url'].'/update', 'id' => $model->id];
        }
        else if ($url instanceof Closure) {
            $url = call_user_func($url, $model, $key, $index, $this);
        }

        if ($this->linkOptions instanceof Closure) {
            $options = call_user_func($this->linkOptions, $model, $key, $index, $this);
        } else {
            $options = $this->linkOptions;
        }

        return Html::a($content, $url, $options);
    }
}
