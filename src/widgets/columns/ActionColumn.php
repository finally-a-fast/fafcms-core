<?php

declare(strict_types=1);

namespace fafcms\fafcms\widgets\columns;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;
use yii\helpers\Url;

class ActionColumn extends \yii\grid\ActionColumn
{
    public $sort;
    public $template = '{create} {view} {update} {copy} {translate} {delete}';
    public $outlineIcons = true;
    public $iconClass = 'text-darken-2';
    public $buttonBackground = 'lighten-2';

    public function init()
    {
        parent::init();

        Html::addCssClass($this->filterOptions, 'action-column');
        Html::addCssClass($this->contentOptions, 'action-column');
    }

    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        $this->initDefaultButton('create', 'plus', [
            'iconOptions' => ['class' => 'primary-text '.$this->iconClass],
        ]);

        $this->initDefaultButton('view', 'magnify', [
            'iconOptions' => ['class' => 'blue-text '.$this->iconClass],
        ]);

        $this->initDefaultButton('update', 'pencil' . ($this->outlineIcons?'-outline':''), [
            'iconOptions' => ['class' => 'green-text '.$this->iconClass],
        ]);

        $this->initDefaultButton('copy', 'content-duplicate', [
            'iconOptions' => ['class' => 'orange-text ' . $this->iconClass],
            'data-confirm' => Yii::t('fafcms-core', 'Are you sure you want to duplicate this item?'),
            'data-method' => 'post',
        ]);

        $this->initDefaultButton('translate', 'translate', [
            'data-confirm' => Yii::t('fafcms-core', 'Are you sure you want to translate this item?'),
        ]);

        $this->initDefaultButton('delete', 'delete'.($this->outlineIcons?'-outline':''), [
            'iconOptions' => ['class' => 'red-text '.$this->iconClass],
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method' => 'post',
        ]);
    }

    public function createUrl($action, $model, $key, $index)
    {
        $id = 'id';

        if ($action === 'translate') {
            $id = 'ids';
        }

        if (is_callable($this->urlCreator)) {
            return call_user_func($this->urlCreator, $action, $model, $key, $index, $this);
        }

        $params = is_array($key) ? $key : [$id => (string) $key];

        if (method_exists($model, 'getEditDataUrl')) {
            $params[0] = Url::to(['/' . $model->getEditDataUrl() . '/' . $action]);
        } else {
            $params[0] = $this->controller ? $this->controller . '/' . $action : $action;
        }

        return Url::toRoute($params);
    }

    protected function initDefaultButton($name, $iconName, $additionalOptions = [])
    {
        if (!isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions) {
                switch ($name) {
                    case 'create':
                        $title = Yii::t('yii', 'Create');
                        break;
                    case 'view':
                        $title = Yii::t('yii', 'View');
                        break;
                    case 'update':
                        $title = Yii::t('yii', 'Update');
                        break;
                    case 'update':
                        $title = Yii::t('yii', 'Translate');
                        break;
                    case 'copy':
                        $title = Yii::t('fafcms-core', 'Duplicate');
                        break;
                    case 'delete':
                        $title = Yii::t('yii', 'Delete');
                        break;
                    default:
                        $title = ucfirst($name);
                }

                $options = $this->buttonOptions[$name] ?? [];

                if (isset($options['label'])) {
                    $title = $options['label'];
                    unset($options['label']);
                }

                Html::addCssClass($options, ArrayHelper::remove($additionalOptions, 'class'));
                Html::addCssClass($options, ['default' => ($name === 'update' ? 'ui button' : 'item') . ' ' . $this->buttonBackground]);

                $iconOptions = ArrayHelper::remove($additionalOptions, 'iconOptions');

                $options = array_merge([
                    'data-pjax' => '0'
                ], $additionalOptions, $options);

                Html::addCssClass($iconOptions, 'icon mdi mdi-' . $iconName);

                $icon = Html::tag('i', '', $iconOptions);
                return Html::a($icon . ($name === 'update' ? '' : $title), $url, $options);
            };
        }
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        $buttonTemplate = str_replace('{update}', '', $this->template);

        $button = function ($matches) use ($model, $key, $index) {
            $name = $matches;

            if (!is_string($matches)) {
                $name = $matches[1];
            }

            if (isset($this->visibleButtons[$name])) {
                $isVisible = $this->visibleButtons[$name] instanceof \Closure
                    ? call_user_func($this->visibleButtons[$name], $model, $key, $index)
                    : $this->visibleButtons[$name];
            } else {
                $isVisible = false;
            }

            if ($isVisible && isset($this->buttons[$name])) {
                $url = $this->createUrl($name, $model, $key, $index);
                return call_user_func($this->buttons[$name], $url, $model, $key);
            }

            return '';
        };

        $buttons = preg_replace_callback('/\\{([\w\-\/]+)\\}/', $button, $buttonTemplate);

        $editButton = $button('update');

        return '<div class="fluid-buttons action-column-container">
            ' . $editButton . '
            <div class="ui icon top right pointing dropdown button">
              <i class="icon mdi mdi-dots-horizontal"></i>
              <div class="menu">
                ' . $buttons . '
              </div>
            </div>
        </div>';
    }
}
