<?php

namespace yiiui\yii2advancedgridview\grid\columns;

use Closure;

class CheckboxColumn extends \yii\grid\CheckboxColumn
{
    public $sort;
    public $headerCheckBoxOptions;
    public $htmlClass = \yii\helpers\Html::class;
    public $addHiddenInput = true;
    public $hiddenInputValue = 0;

    public function renderDataCell($model, $key, $index)
    {
        if ($this->contentOptions instanceof Closure) {
            $options = call_user_func($this->contentOptions, $model, $key, $index, $this);
        } else {
            $options = $this->contentOptions;
        }

        return $this->htmlClass::tag('td', $this->htmlClass::tag('div', $this->htmlClass::tag('div', $this->renderDataCellContent($model, $key, $index))), $options);
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->checkboxOptions instanceof Closure) {
            $options = call_user_func($this->checkboxOptions, $model, $key, $index, $this);
        } else {
            $options = $this->checkboxOptions;
        }

        if (!isset($options['value'])) {
            $options['value'] = is_array($key) ? Json::encode($key) : $key;
        }

        if ($this->cssClass !== null) {
            $this->htmlClass::addCssClass($options, $this->cssClass);
        }

        return $this->htmlClass::checkbox($this->name, !empty($options['checked']), $options);
    }

    /**
     * Renders the header cell.
     */
    public function renderHeaderCell()
    {
        $hiddenInputHtml = '';

        if ($this->addHiddenInput) {
            $hiddenInputHtml = $this->htmlClass::hiddenInput(substr($this->name, 0, -2), $this->hiddenInputValue, []);
        }

        return $this->htmlClass::tag('th', $hiddenInputHtml.$this->htmlClass::tag('div', $this->renderHeaderCellContent()), $this->headerOptions);
    }

    protected function renderHeaderCellContent()
    {
        if ($this->header !== null || !$this->multiple) {
            return parent::renderHeaderCellContent();
        }

        $this->htmlClass::addCssClass($this->headerCheckBoxOptions, 'check-all');

        return $this->htmlClass::checkbox($this->getHeaderCheckBoxName(), false, $this->headerCheckBoxOptions);
    }

    public function renderFilterCell()
    {
        return $this->htmlClass::tag('td', $this->htmlClass::tag('div', $this->renderFilterCellContent()), $this->filterOptions);
    }
}
