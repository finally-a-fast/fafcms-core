<?php

declare(strict_types=1);

namespace fafcms\fafcms\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class LinkPager
 *
 * @package fafcms\fafcms\widgets
 */
class LinkPager extends \yii\widgets\LinkPager
{
    /**
     * @inheritdoc
     */
    public $disabledListItemSubTagOptions = ['tag' => 'div'];

    /**
     * @inheritdoc
     */
    public $options = ['class' => 'ui pagination menu', 'tag' => 'div'];

    /**
     * @inheritdoc
     */
    public $pageCssClass = ['item'];

    /**
     * @inheritdoc
     */
    public $activePageCssClass = ['active'];

    /**
     * @inheritdoc
     */
    public $disabledPageCssClass = ['disabled'];

    /**
     * @inheritdoc
     */
    public $prevPageCssClass = ['icon'];

    /**
     * @inheritdoc
     */
    public $prevPageLabel = '<i class="icon mdi mdi-chevron-left"></i>';

    /**
     * @inheritdoc
     */
    public $nextPageCssClass = ['icon'];

    /**
     * @inheritdoc
     */
    public $nextPageLabel = '<i class="icon mdi mdi-chevron-right"></i>';

    /**
     * @inheritdoc
     */
    protected function renderPageButton($label, $page, $class, $disabled, $active): string
    {
        $options = $this->linkContainerOptions;
        Html::addCssClass($options, $this->pageCssClass);
        Html::addCssClass($options, $class ?? '');

        if ($active) {
            Html::addCssClass($options, $this->activePageCssClass);
        }

        if ($disabled) {
            Html::addCssClass($options, $this->disabledPageCssClass);
            $disabledItemOptions = $this->disabledListItemSubTagOptions;
            $tag = ArrayHelper::remove($disabledItemOptions, 'tag', 'span');

            $disabledItemOptions = ArrayHelper::merge($options, $disabledItemOptions);
            return Html::tag($tag, $label, $disabledItemOptions);
        }

        $linkOptions = $this->linkOptions;
        $linkOptions['data-page'] = $page;

        $linkOptions = ArrayHelper::merge($options, $linkOptions);

        return Html::a($label, $this->pagination->createUrl($page), $linkOptions);
    }
}
