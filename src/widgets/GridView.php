<?php

declare(strict_types=1);

namespace fafcms\fafcms\widgets;

use fafcms\fafcms\assets\fafcms\backend\FafcmsGridViewAsset;
use fafcms\fafcms\assets\fomantic\behaviors\FomanticFormAsset;
use fafcms\fafcms\assets\fomantic\behaviors\FomanticFormAsset as FomanticFormAssetBehavior;
use fafcms\fafcms\assets\fomantic\collections\FomanticFormAsset as FomanticFormAssetCollection;
use fafcms\fafcms\widgets\columns\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class GridView extends \yii\grid\GridView
{
    public $dataColumnClass = DataColumn::class;
    public $tableOptions = ['class' => 'ui table'];
    public $wrapperOptions = [];
    public $scrollWrapperOptions = [];
    public $summaryOptions = ['class' => 'ui right floated summary'];
    public $filterRowOptions = ['class' => 'filters ui form'];

    public function run()
    {
        FafcmsGridViewAsset::register($this->getView());
        FomanticFormAssetBehavior::register($this->getView());
        FomanticFormAssetCollection::register($this->getView());

        Html::addCssClass($this->scrollWrapperOptions, 'table-scroll-wrapper');

        $this->layout = Html::tag('div', Html::tag('div', '{items}', $this->scrollWrapperOptions), ['class' => 'table-action-wrapper fafcms-scrollbars ui table']);
        $this->layout .= '<br>{summary}{pager}';

        if (is_array($this->pager) && !ArrayHelper::keyExists('class', $this->pager)) {
            $this->pager = [
                'class' => LinkPager::class,
            ];
        }

        parent::run();
    }
}
