<?php

namespace fafcms\fafcms;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\DateTimePicker;
use fafcms\fafcms\inputs\DropDownList;
use fafcms\fafcms\inputs\NumberInput;
use fafcms\fafcms\inputs\ExtendedDropDownList;
use fafcms\fafcms\inputs\SwitchCheckbox;
use fafcms\fafcms\inputs\TextInput;
use fafcms\helpers\classes\PluginSetting;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\sitemanager\models\Site;
use Yii;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\helpers\classes\UserSetting;

/**
 * Class Module
 * @package fafcms\fafcms
 */
class Module extends PluginModule
{
    public function getPluginSettingRules(): array
    {
        return [
            [['tag_site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['tag_site_id' => 'id']],
            [['password_reset_email_from_address'], 'filter', 'filter' => 'trim'],
            [['password_reset_email_from_address'], 'email'],
            [['password_reset_email_from_label'], 'filter', 'filter' => 'trim'],
            [['password_reset_email_from_label', 'hash_id_security_key'], 'string'],
            [['password_reset_token_expiration_time'], 'integer'],
            [['use_web_cronjob', 'always_allow_web_cronjob'], 'boolean']
        ];
    }

    protected function pluginSettingDefinitions(): array
    {
        return [
            new PluginSetting($this, [
                'name' => 'last_real_cronjob',
                'label' => Yii::t('fafcms-core', 'Last real cronjob'),
                'description' => Yii::t('fafcms-core', 'If an real cronjob hasn\'t been excecuted since 2 hours the web cronjob will be automatically activated.'),
                'projectBased' => false,
                'languageBased' => false,
                'inputType' => DateTimePicker::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_DATETIME,
                'inputOptions' => [
                    'options' => ['disabled' => true]
                ]
            ]),
            new PluginSetting($this, [
                'name' => 'use_web_cronjob',
                'label' => Yii::t('fafcms-core', 'Use web cronjob'),
                'description' => Yii::t('fafcms-core', 'You should activate cronjobs with supervisor or system cronjob. When these options are not available you can use the slower web cronjob. See <a href="https://github.com/yiisoft/yii2-queue/blob/master/docs/guide/worker.md" target="_blank">https://github.com/yiisoft/yii2-queue/blob/master/docs/guide/worker.md</a> for more Infos.'),
                'projectBased' => false,
                'languageBased' => false,
                'inputType' => SwitchCheckbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'defaultValue' => true,
                'fieldConfig' => [
                    'offLabel' => Yii::t('fafcms-core', 'No'),
                    'onLabel' => Yii::t('fafcms-core', 'Yes')
                ]
            ]),
            new PluginSetting($this, [
                'name' => 'always_allow_web_cronjob',
                'label' => Yii::t('fafcms-core', 'Always allow web cronjob'),
                'description' => Yii::t('fafcms-core', 'Allow access to the webcrobjob, even if a real cronjob has been set up.'),
                'projectBased' => false,
                'languageBased' => false,
                'inputType' => SwitchCheckbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'defaultValue' => false,
                'fieldConfig' => [
                    'offLabel' => Yii::t('fafcms-core', 'No'),
                    'onLabel' => Yii::t('fafcms-core', 'Yes')
                ]
            ]),
            new PluginSetting($this, [
                'name' => 'tag_site_id',
                'label' => Yii::t('fafcms-core', 'Default tag site'),
                'inputType' => ExtendedDropDownList::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'items' => static function() {
                    return Site::getOptions(true);
                }
            ]),
            new PluginSetting($this, [
                'name' => 'password_reset_email_from_address',
                'label' => Yii::t('fafcms-core', 'Password reset email from address'),
                'projectBased' => false,
                'languageBased' => false,
                'inputType' => TextInput::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
                'defaultValue' => static function() {
                    $host = Yii::$app->request->getHostName();
                    return 'noreply@' . substr($host, strrpos($host, '.', strrpos($host, '.') - strlen($host) - 1) + 1);
                }
            ]),
            new PluginSetting($this, [
                'name' => 'password_reset_email_from_label',
                'label' => Yii::t('fafcms-core', 'Password reset email from label'),
                'projectBased' => false,
                'languageBased' => false,
                'inputType' => TextInput::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_VARCHAR,
            ]),
            new PluginSetting($this, [
                'name' => 'password_reset_token_expiration_time',
                'label' => Yii::t('fafcms-core', 'Password reset token expiration time'),
                'description' => Yii::t('fafcms-core', 'Time in seconds (one hour = 3600 seconds, one day = 86400 seconds)'),
                'projectBased' => false,
                'languageBased' => false,
                'inputType' => NumberInput::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'defaultValue' => 86400
            ]),
            new PluginSetting($this, [
                'name' => 'hash_id_security_key',
                'label' => Yii::t('fafcms-core', 'The security key to generate id hashes.'),
                'inputType' => TextInput::class,
                'projectBased' => false,
                'languageBased' => false,
                'defaultValue' => static function() {
                    return Yii::$app->getSecurity()->generateRandomString(255);
                }
            ]),
        ];
    }

    protected function userSettingDefinitions(): array
    {
        return [
            new UserSetting($this, [
                'name' => 'currentProject',
                'label' => Yii::t('fafcms-core', 'currentProject'),
                'inputType' => DropDownList::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'items' => []//Project::getOptions
            ]),
            new UserSetting($this, [
                'name' => 'currentProjectLanguage',
                'label' => Yii::t('fafcms-core', 'currentProjectLanguage'),
                'inputType' => DropDownList::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_INT,
                'items' => []//Project::getOptions
            ]),
            new UserSetting($this, [
                'name' => 'collapsibleCollapsed',
                'label' => Yii::t('fafcms-core', 'collapsibleCollapsed'),
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'defaultValue' => true
            ]),
            new UserSetting($this, [
                'name' => 'cardCollapsed',
                'label' => Yii::t('fafcms-core', 'cardCollapsed'),
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'defaultValue' => true
            ]),
            new UserSetting($this, [
                'name' => 'sidebarVisible',
                'label' => Yii::t('fafcms-core', 'sidebarVisible'),
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
                'defaultValue' => true
            ]),
        ];
    }
}
