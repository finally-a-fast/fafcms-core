[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Readme | Core
================================================

[![Latest Stable Version](https://img.shields.io/packagist/v/finally-a-fast/fafcms-core?label=stable&style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-core)
[![Latest Version](https://img.shields.io/packagist/v/finally-a-fast/fafcms-core?include_prereleases&label=unstable&style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-core)
[![PHP Version](https://img.shields.io/packagist/php-v/finally-a-fast/fafcms-core/dev-master?style=flat-square)](https://www.php.net/downloads.php)
[![License](https://img.shields.io/packagist/l/finally-a-fast/fafcms-core?style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-core)
[![Total Downloads](https://img.shields.io/packagist/dt/finally-a-fast/fafcms-core?style=flat-square)](https://packagist.org/packages/finally-a-fast/fafcms-core)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat-square)](http://www.yiiframework.com/)


This is the core of Finally a fast CMS.

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require finally-a-fast/fafcms-core
```
or add
```
"finally-a-fast/fafcms-core": "dev-master"
```
to the require section of your `composer.json` file.

Documentation
------------

[Documentation](https://www.finally-a-fast.com/) will be added soon at https://www.finally-a-fast.com/.

License
-------

**fafcms-core** is released under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
